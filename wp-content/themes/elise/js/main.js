/*!
 * Elise scripts
 */
(function($) {
	"use strict";

	// Fit Vid
    $('.fit-vid').fitVids();
    $('.fit-vid').each(function(){
	    var AudIframeH = $(this).children('iframe').height();
	    $(this).css('height',AudIframeH);

	    $(window).resize(function(){	
		    var AudIframeH = $('.fit-vid iframe').height();
		    $(this).css('height',AudIframeH);
		});	
    })

    $(window).resize(function(){	
	    var VidIframeH = $('.fit-vid iframe').height();
	    $('.fit-vid').css('height',VidIframeH);
	});	

    // fit audio height fix	
    $('.fit-aud').each(function(){
	    var AudIframeH = $(this).children('iframe').height();
	    $(this).css('height',AudIframeH);

	    $(window).resize(function(){	
		    var AudIframeH = $('.fit-aud iframe').height();
		    $(this).css('height',AudIframeH);
		});	
    })

    

	// Article share
	var pageHref = window.location.href.replace(window.location.hash, '');

	function facebookShare() {
		window.open( 'https://www.facebook.com/sharer/sharer.php?u='+ pageHref, "facebookWindow", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
		return false;
	}

	function twitterShare() {
		window.open( 'http://twitter.com/intent/tweet?text=This is great! - '+pageHref, "twitterWindow", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
		return false;
	}

	function googleplusShare() {
		window.open( 'https://plus.google.com/share?url='+pageHref, "googleplusWindow", "height=400,width=515,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
		return false;
	}

	function pinterestShare() {
		var imgShare = $(".blog-media img").attr('src');
		window.open( 'http://pinterest.com/pin/create/button/?url='+pageHref+'&media='+imgShare, "pinterestWindow", "height=640,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
		return false;
	}

	$('.facebook-share').click(facebookShare);
	$('.twitter-share').click(twitterShare);
	$('.google-plus-share').click(googleplusShare);
	$('.pinterest-share').click(pinterestShare);

	$('.btn-share a').click(function(){ return false; })


	// portfolio masonry
	function portfolioMasonry() {
		var $masonryContainer = $('.portfolio-masonry'),
			loader = '<div class=\"spinner portfolio-spinner\"></div>';

		$masonryContainer.prepend(loader);

		$('.portfolio-masonry').each(function(){
			var portContWidth = $(this).width(),
				portContWidthOut = $(this).children('.row').outerWidth(),
				gapSize = portContWidthOut - portContWidth,
				paddGap = $(this).find('[class*="col-md"]').innerWidth() - $(this).find('[class*="col-md"]').width(),

				// ratioHeight = Math.round((((portContWidth / 3.333333333) - gapSize) / 1.3333333333)),
				ratioHeight = Math.round((((portContWidth / 3) - paddGap) / 1.33)),
				bigHeight = ratioHeight * 2,

				smallH = $(this).find('.portfolio-masonry-small .portfolio-item').height(),
				wideH = $(this).find('.portfolio-masonry-wide .portfolio-item').height(),
				bigH = $(this).find('.portfolio-masonry-big .portfolio-item').height(),
				tallH = $(this).find('.portfolio-masonry-tall .portfolio-item').height(),

				elH = '',
				styleBH = 0;

			if (smallH != null) {
				elH = smallH;
			}
			else if (wideH != null) {
				elH = wideH;
			} 
			else if (bigH != null) {
				elH = (bigH / 2) - (paddGap / 2);
			} 
			else {
				elH = null;
			}

			if ($(this).hasClass('portfolio-style-bottom')) {
				styleBH = 79;
			}
				
			$(this).find('.portfolio-masonry-wide .portfolio-item img').css("height", elH - styleBH);
			$(this).find('.portfolio-masonry-big .portfolio-item img').css("height", (elH * 2) + paddGap - styleBH);
			$(this).find('.portfolio-masonry-tall .portfolio-item img').css("height", (elH * 2) + paddGap - styleBH);

		})

		var $portfolioCont = $('.portfolio-masonry .row').imagesLoaded(function(){
			$('.portfolio-spinner').remove();

			// responsivePortfolioHeightFix();
			$portfolioCont.removeClass('pm-hidden').addClass('pm-visible').isotope({
			  // options...
			  itemSelector: '[class*="col-"]',
			  masonry: {
			    columnWidth: '[class*="col-"]'
			  }
			});
		})
	}

	portfolioMasonry();

	function portfolioFiltering() {
		// portoflio filtering
		// cache container
		$('.portfolio-items--container .row').each(function(){
			var $container = $(this);

			$container.imagesLoaded(function(){
				// initialize isotope
				$container.isotope({
				  resizesContainer: true,
				  animationEngine: 'best-available'
				});
				
				// filter items when filter link is clicked
				$('.filters a').click(function(){
				  var selector = $(this).attr('data-filter');
				  $container.isotope({ filter: selector });
				  
				  //active classes
				  $('.filters li a').removeClass('active');
				  $(this).addClass('active');
				  
				  return false;
				});

			})
		})
	}

	portfolioFiltering();

	function showFiltering() {
		$('.btn-filtering a').toggle(function(){
			$(this).children('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-up');
			$('.project-filtering-wrap').slideDown(250);
			return false;
		}, function(){
			$(this).children('i').removeClass('fa fa-caret-up').addClass('fa fa-caret-down');
			$('.project-filtering-wrap').slideUp(250);
			return false;
		})
	}

	showFiltering();

	function masonryGalleryFix() {
		var galleryImgHeight = $('.format-gallery .blog-media .rsContent img').height(),
			galleryTmbHeight = $('.format-gallery .blog-media .rsThumb img').height(),
			galleryHeight = galleryImgHeight + galleryTmbHeight + 12;

		$('.blog-masonry .format-gallery .blog-media').css('height', galleryHeight);
	}


	// blog masonry
	function blogMasonry() {

		var $masonryContainer = $('.blog-masonry'),
			loader = '<div class=\"spinner blog-spinner\"></div>';
			$masonryContainer.before(loader);
		
		var $blogCont = $('.blog-masonry').imagesLoaded(function(){

			masonryGalleryFix();

			$('.blog-spinner').remove();
			$blogCont.removeClass('bm-hidden').addClass('bm-visible').isotope({
			  // options...
			  itemSelector: '[class^="col-"]',
			  masonry: {
			    columnWidth: '[class^="col-"]'
			  }
			});

		}) 	

		$(window).on('resize', function(){
			setTimeout(function(){
				masonryGalleryFix();
				$blogCont.isotope('layout');
			}, 1000);
		})
	}

	blogMasonry();


	// timeline
	function shortcodeTimeline() {

		var $timelineContainer = $('.elise_timeline_wrapper.timeline-center'),
			loader = '<div class=\"spinner timeline-spinner\"></div>';
			$timelineContainer.before(loader);
		
		var $timelineCont = $('.elise_timeline_wrapper.timeline-center').imagesLoaded(function(){

			$('.timeline-spinner').remove();
			$timelineCont.removeClass('timeline-hidden').addClass('timeline-visible').isotope({
			  // options...
			  itemSelector: '.elise_timeline_block_wrap',
			  stamp: '.timeline-stamp',
			  masonry: {
			    columnWidth: '.elise_timeline_block_wrap'
			  }
			});

			// var viewportWidth = $('.elise_timeline_wrapper').width(),
			// 	windowWidth = $(window).width(),
			// 	viewW = viewportWidth / 2;
			$('.elise_timeline_block_wrap').each(function(){
				var posLeft = $(this).position().left;

			if (posLeft == 0 ) {
				$(this).removeClass('tl_block_right');
				$(this).addClass('tl_block_left');
			} else {
				$(this).removeClass('tl_block_left');
				$(this).addClass('tl_block_right');
			}

			})

			// $('.elise_timeline_block_wrap').offscreen({
			//     // smartResize: true,
			//     rightClass: 'tl_block_right',
			//     leftClass: 'tl_block_left'
			// });
		}) 	
	}

	shortcodeTimeline();

	$(window).resize(function(){
		shortcodeTimeline();
	})

	$(window).on("orientationchange",function(){
	  shortcodeTimeline();
	});


	// blog masonry
	function imageGrid() {

		var $masonryContainer = $('.image_grid_ul'),
			loader = '<div class=\"spinner ig-spinner\"></div>';
			$masonryContainer.before(loader);
		
		var $blogCont = $('.image_grid_ul').imagesLoaded(function(){

			$('.ig-spinner').remove();
			$blogCont.removeClass('ig-hidden').addClass('ig-visible').isotope({
			  // options...
			  itemSelector: '.isotope-item',
			  masonry: {
			    columnWidth: '.isotope-item'
			  }
			});

		}) 	

		$(window).on('resize', function(){
			setTimeout(function(){
				$blogCont.isotope('layout');
			}, 1000);
		})
	}

	imageGrid();


	function secondary_navigation() {

		var triggerBttn = $(".secondary-nav-btn"),
			// triggerBttn = document.getElementById( 'secondary-nav-btn' ),
			overlay = document.querySelector( '.secondary-navigation' ),
			closeBttn = $('.sec-nav-close-btn a'),
			closeOvrl = $('.sec-nav-overlay'),
			transEndEventNames = {
				'WebkitTransition': 'webkitTransitionEnd',
				'MozTransition': 'transitionend',
				'OTransition': 'oTransitionEnd',
				'msTransition': 'MSTransitionEnd',
				'transition': 'transitionend'
			},
			transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
			support = { transitions : Modernizr.csstransitions };

		function toggleOverlay() {
			if( classie.has( overlay, 'open' ) ) {
				classie.remove( overlay, 'open' );
				classie.add( overlay, 'closed' );
				var onEndTransitionFn = function( ev ) {
					if( support.transitions ) {
						if( ev.propertyName !== 'visibility' ) return;
						this.removeEventListener( transEndEventName, onEndTransitionFn );
					}
					classie.remove( overlay, 'closed' );
				};
				if( support.transitions ) {
					overlay.addEventListener( transEndEventName, onEndTransitionFn );
				}
				else {
					onEndTransitionFn();
				}
			}
			else if( !classie.has( overlay, 'closed' ) ) {
				classie.add( overlay, 'open' );
			}
		}

		triggerBttn.click(function(){ 
			// $('html').css({'overflow':'hidden', 'margin-right': getScrollBarWidth() });
			toggleOverlay(); 
			//$('.secondary-navigation').trigger('blur');
			return false; 
		})
		closeBttn.click(function(){	
			toggleOverlay(); 
			// $('html').css({'overflow':'visible', 'margin-right':'0'});
			return false; 
		})
		closeOvrl.click(function(){	
			toggleOverlay(); 
			// $('html').css({'overflow':'visible', 'margin-right':'0'});
			return false; 
		})
	};

	secondary_navigation();

	$(".btn-comments a").click(function() {
		$("html, body").animate({ scrollTop: $('#comments').offset().top - 50 }, 500);
		return false;
	})

	// $('body.single-portfolio .main-nav ul li a:contains("Portfolio")').parents('li').addClass('current-menu-item');

	// // Blog masonry -  time fix
	// var masonryTimeFix = function blogMasonryTimeFix( postFormat ) {
	// 	$('.blog-masonry '+ postFormat +' .blog-media').hover(function() {
	//     	$(this).parent(postFormat).find('.time').hide(200);
	// 	  }, function() {
	// 	    $(this).parent(postFormat).find('.time').show(200);
	// 	  }
	// 	);
	// }

	// masonryTimeFix('.format-video');
	// masonryTimeFix('.format-audio');
	// masonryTimeFix('.format-gallery');

	// Blog Medium formats audio/video height fix;
	var blogMedFix = function blogMediumHeightFix( postFormat ) {
		$('.blog-medium ' + postFormat).each(function(){
		var heightContent = $(this).children('.bc-wrap').height(),
			heightMedia = $(this).children('.blog-media').height()

			if (heightMedia > heightContent) {
				$(this).css('min-height', heightMedia + 59)
			} else {
				$(this).css('min-height', heightContent + 50)
			}
		})
	}

	blogMedFix('.format-video');
	blogMedFix('.format-audio');


	// Royal Slider 
	// Single Project Gallery
	function imageSlider() {
		var contNav = 'thumbnails';

		$('.image-slider').each(function(){
			var attr = $(this).attr('data-nav');

			if (typeof attr !== typeof undefined && attr !== false) {
			    if (attr == 'nav_thumbs') {
			    	contNav = 'thumbnails';
			    } 
			    else if (attr == 'nav_bullets') {
			    	contNav = 'bullets';
			    } 
			    else if (attr == 'nav_none') {
			    	contNav = 'none';
			    } 
			} else {
			    contNav = 'thumbnails';
			}

			// $(this).imagesLoaded(function(){

			$(this).royalSlider({
			    autoHeight: true,
			    arrowsNav: true,
			    arrowsNavAutoHide: true,
			    controlsInside: true,
			    fadeinLoadedSlide: true,
			    controlNavigationSpacing: 0,
			    controlNavigation: contNav,
			    imageScaleMode: 'none',
			    imageAlignCenter: true,
			    loop: false,
			    loopRewind: true,
			    numImagesToPreload: 4,
			    keyboardNavEnabled: true,
			    usePreloader: false,
			    slidesSpacing: 4,
			    thumbs: {
			      appendSpan: true,
			      firstMargin: true,
			      arrows: true,
			      autoCenter: false
			    }
		 	});

			// });
		})

	}

	imageSlider();

	$(window).load(function() {
     // update size of slider (if you already initialized it before)
     	$('.image-slider').royalSlider('updateSliderSize', true);
	});

	$('.project-image-slider-wide').royalSlider({
	    autoHeight: true,
	    arrowsNav: true,
	    arrowsNavAutoHide: true,
	    controlsInside: false,
	    fadeinLoadedSlide: false,
	    controlNavigationSpacing: 0,
	    controlNavigation: 'thumbnails',
	    imageScaleMode: 'none',
	    imageAlignCenter: true,
	    loop: false,
	    loopRewind: true,
	    numImagesToPreload: 4,
	    keyboardNavEnabled: true,
	    usePreloader: false,
	    slidesSpacing: 60,
	    addActiveClass: true,
	    thumbs: {
	      appendSpan: true,
	      firstMargin: true,
	      arrows: true,
	      autoCenter: true
	    }
 	});



	function recentsCarousel() {
		$('.content-carousel').each(function(){

			var navi = $(this).attr('data-carousel-nav'),
				autoplay = $(this).attr('data-autoplay'),
				autoplayEn = false;

				if (autoplay != 0) {
					autoplayEn = true;
				}

			$(this).royalSlider({
			    autoHeight: true,
			    arrowsNav: true,
			    arrowsNavAutoHide: true,
			    controlsInside: false,
			    fadeinLoadedSlide: true,
			    controlNavigationSpacing: 0,
			    controlNavigation: navi,
			    navigateByClick: false,
			    loop: false,
			    loopRewind: true,
			    keyboardNavEnabled: true,
			    slidesSpacing: 30,
		    	autoPlay: {
		    		// autoplay options go gere
		    		enabled: autoplayEn,
		    		pauseOnHover: true,
		    		stopAtAction: true,
		    		delay: +autoplay,
		    	}
		 	});

		})

	 	$('.recentprojects-carousel').each(function(){
			var itemsGap = $(this).attr('data-items-gap');

			$(this).royalSlider({
			    autoHeight: true,
			    arrowsNav: true,
			    arrowsNavAutoHide: true,
			    controlsInside: false,
			    fadeinLoadedSlide: true,
			    controlNavigationSpacing: 0,
			    controlNavigation: 'bullets',
			    navigateByClick: false,
			    loop: false,
			    loopRewind: true,
			    keyboardNavEnabled: true,
			    slidesSpacing: +itemsGap,
		 	});

	 	})
	}

	recentsCarousel();


	$(window).load(function() {
     // update size of slider (if you already initialized it before)
     	$('.content-carousel').royalSlider('updateSliderSize', true);
	});

	$('.quick-view').magnificPopup({
		//type: 'image',
		closeOnContentClick: true,
		mainClass: 'my-mfp-zoom-in',		
		midClick: true,
		closeBtnInside: false,
		image: {
			verticalFit: true
		},
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},  
		removalDelay: 500, //delay removal by X to allow out-animation
	  	callbacks: {
		    beforeOpen: function() {
		      // just a hack that adds mfp-anim class to markup 
		       this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
		       this.st.iframe.markup = this.st.iframe.markup.replace('mfp-iframe-scaler', 'mfp-iframe-scaler mfp-with-anim');
		       this.st.mainClass = this.st.el.attr('data-effect');
		    }
	  	},
	});

	$('a.magnpopup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'my-mfp-zoom-in',		
		midClick: true,
		closeBtnInside: false,
		image: {
			verticalFit: true
		},
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},  
		removalDelay: 500, //delay removal by X to allow out-animation
	  	callbacks: {
		    beforeOpen: function() {
		      // just a hack that adds mfp-anim class to markup 
		       this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
		       this.st.iframe.markup = this.st.iframe.markup.replace('mfp-iframe-scaler', 'mfp-iframe-scaler mfp-with-anim');
		       this.st.mainClass = this.st.el.attr('data-effect');
		    }
	  	},
	});

	// Sticky Header
	function stickyHeader() {
		var $navbarSticky = $('#sticky-header'),
			slideSpeed = 200,

			topBar = $("#top-bar").height(),
			navBar = $("#navbar").height(),
			pageTitle = $(".page-title-container").height(),

			stickyNavShow = topBar + navBar + 50,

			lastScrollTop = 0, delta = 5;

		// layout fix
		function stickyHeaderLayoutFix() {
			if ($('body').hasClass('layout-boxed') || $('body').hasClass('layout-bordered')) {
				var layoutWrapWidth = $('.layout-wrapper').width(),
					windowWidth = $(window).width(),

					layoutGap = (windowWidth - layoutWrapWidth) / 2;

				// console.log(layoutGap);
				$navbarSticky.css({
					width: layoutWrapWidth,
					left: layoutGap
				});
			}
		}


		stickyHeaderLayoutFix();
		$(window).resize(function(){
			stickyHeaderLayoutFix();
		})
	

		// scroll
		$(window).scroll(function(){
		   var st = $(this).scrollTop();

			if ($navbarSticky.hasClass('sh-show-alw')) {
				if(st > stickyNavShow ) {
					$navbarSticky.removeClass('sh-hidden').addClass('sh-visible');
				} else {
					$navbarSticky.removeClass('sh-visible').addClass('sh-hidden');
				}
			} 
			else if ($navbarSticky.hasClass('sh-show-scrollup')) {
				if(st > stickyNavShow ) {
					if(Math.abs(lastScrollTop - st) <= delta)
					  return;
					if (st > lastScrollTop){
					   if ($(this).scrollTop() > stickyNavShow ) {
					   		$navbarSticky.removeClass('sh-visible').addClass('sh-hidden');
					   }
					} else {
					  // upscroll code
					  $navbarSticky.removeClass('sh-hidden').addClass('sh-visible');
					}
				} else {
					$navbarSticky.removeClass('sh-visible').addClass('sh-hidden');
				}
				lastScrollTop = st;
			}
		});
	}

	stickyHeader();

	function searchBar() {
		var openBtn = $('.nav-search a'),
			closeBtn = $('.search-bar .close-btn a'),
			
			searchBarSel = '.search-bar';


		openBtn.click(function(){
			$(this).parents('.col-md-12').find(searchBarSel).addClass('search-bar-visible').removeClass('search-bar-hidden');
			$(this).parents('.col-md-12').find(searchBarSel +' input').focus();

			$(this).parents('.col-md-12').find('[class*="logo"]').addClass('search-bar-hidden').removeClass('search-bar-visible');
			$(this).parents('.col-md-12').find('.nav-container').addClass('search-bar-hidden').removeClass('search-bar-visible');
			return false;
		})

		closeBtn.click(function(){
			$(this).parents(searchBarSel).removeClass('search-bar-visible').addClass('search-bar-hidden');

			$(this).parents('.col-md-12').find('[class*="logo"]').addClass('search-bar-visible').removeClass('search-bar-hidden');
			$(this).parents('.col-md-12').find('.nav-container').addClass('search-bar-visible').removeClass('search-bar-hidden');

			return false;
		})

		$(document).keyup(function(e) {
		  if (e.keyCode == 27) { 
		  	$(searchBarSel).removeClass('search-bar-visible').addClass('search-bar-hidden');
		  	$('[class*="logo"]').addClass('search-bar-visible').removeClass('search-bar-hidden');
			$('.nav-container').addClass('search-bar-visible').removeClass('search-bar-hidden');
		  }
		});
	}

	searchBar();

	function backToTop() {
		var $backToTopContainer = $('.back-to-top'),
			backToTopBtn = $('.back-to-top-btn, .elise_sep_btt .elise_sep_text'),

			windowHeight = $(window).height();

		$(window).scroll(function(){
			var st = $(this).scrollTop();

			if (st > windowHeight) {
				$backToTopContainer.removeClass('btt-hidden').addClass('btt-visible');
			} else {
				$backToTopContainer.removeClass('btt-visible').addClass('btt-hidden');
			}
		})

		backToTopBtn.click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
		});
	}

	backToTop();

	function vcRowWindowHeight() {
		var windowHeight = $(window).height();

		$('.vc_window_height').css({
			'height': windowHeight
		});
	}

	vcRowWindowHeight();

	function vcRowHeightVA() {
		var $vc_va = $('.vc_row_vertical_align');

		$vc_va.each(function(){
			var vcHeight = $(this).outerHeight(),
				vcContHeight = $(this).find('.vc_container_inner').outerHeight(),
				basicHeight = $(this).attr('data-basic-height');

				if (vcContHeight > vcHeight) {
					$(this).css('height', vcContHeight + 40);
				} else {
					if ($(this).hasClass('vc_row_custom_height')) {
						$(this).css('height', basicHeight);
					}
				}
		})
	}

	vcRowHeightVA();

	function parallaxBG() {
		if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
		    skrollr.init({
		        forceHeight: false
		    });
		}
	}

	function elise_counter() {
		$('.elise-shortcode-counter').waypoint(function() {
	    	$('.elise-counter').countTo();
		}, { triggerOnce: true, offset: '80%' })
	};

	elise_counter();

	function menuWidthChecker() {
		var navbarWidth = $('#navbar .container').width(),
			logoWidth = $('#navbar .container .logo').outerWidth(true),
			navWidth = $('#navbar .container .main-nav').outerWidth(true),
			navLWidth = navbarWidth - logoWidth;

		if (Modernizr.mq('(min-width: 1201px)')) {
			if (navWidth > navLWidth) {
				$('#navbar .container .main-nav').addClass('hidden-lg');
				$('#navbar .container .nav-mobile').addClass('visible-lg');
				$('#sticky-header').addClass('hidden-lg');
			} 
		}

		if (Modernizr.mq('(max-width: 1200px)') && Modernizr.mq('(min-width: 992px)')) {
			if (navWidth > navLWidth) {
				$('#navbar .container .main-nav').addClass('hidden-md');
				$('#navbar .container .nav-mobile').addClass('visible-md');
				$('#sticky-header').addClass('hidden-md');
			}
		} 
	}

	menuWidthChecker();

	// Full width content common height
	function FWCommonHeight() {
		$('.vc_row_fullwidthcontent > .full-width-container > .row').each(function(){
			var vcCol = $(this).children('.wpb_column');

			var maxHeight = -1,
				i = 0;
			vcCol.each(function() {
			    var height = $(this).outerHeight(); 
			    i++;
			    maxHeight = height > maxHeight ? height : maxHeight;
			});

			// && Modernizr.mq('(min-width: 992px)')

			if (i > 1) {
				vcCol.css('min-height', maxHeight);
			}
		})
	}

	FWCommonHeight();

	// VC Shortcodes
	function vcPieChart() {
		$('.elise-pie-chart').each(function(){
			$(this).waypoint(function() {
				var $this = $(this),
					pieValAttr = $this.attr('data-value'),
					pieVal = pieValAttr * 0.01,
					pieDur = $this.attr('data-duration'),
					pieBg = $this.attr('data-colorbg'),
					pieSize = $this.attr('data-size'),
					pieThick = $this.attr('data-thickness'),
					pieFill = $this.attr('data-fill'),
					pieCol1 = $this.attr('data-color1'),
					pieCol2 = $this.attr('data-color2'),
					showUnit = $this.attr('data-unit'),
					pieFillColor = '',
					appUnit = '';

					if (pieFill == 'gradient') {
						pieFillColor = { gradient: [""+pieCol2+"", ""+pieCol1+""] };
					}
					else if (pieFill == 'color') {
						pieFillColor = { color: ""+pieCol1+"" };
					}

					if (showUnit == 'true') {
						appUnit = '<i>%</i>';
					}

				$(this).circleProgress({
				    value: pieVal,
				    size: +pieSize,
				    thickness: +pieThick,
				    animation: { 
				    	duration: +pieDur, 
				    	ease: "circleProgressEase" 
				    }, 
				    startAngle: 11,
				    emptyFill: pieBg,
				    fill: pieFillColor,
				}).on('circle-animation-progress', function(event, progress) {
				    $(this).find('.elise-counter').html(parseInt(pieValAttr * progress) + appUnit);
				});
			}, { triggerOnce: true, offset: '80%' })
		})
	}

	vcPieChart();

	function progressBar() {
		jQuery('.vc_progress_bar').waypoint(function () {
	        jQuery(this).find('.vc_single_bar').each(function (index) {
	          var $this = jQuery(this),
	            bar = $this.find('.vc_bar'),
	            val = bar.data('percentage-value');

	          setTimeout(function () {
	            bar.css({"width":val + '%'});
	          }, index * 200);
	        });
	    }, { triggerOnce: true, offset:'80%' });
	}

	progressBar();

	function vc_waypoints() {
		if (typeof jQuery.fn.waypoint !== 'undefined') {
			jQuery('.wpb_animate_when_almost_visible:not(.wpb_start_animation)').waypoint(function () {
			jQuery(this).addClass('wpb_start_animation');
			}, { triggerOnce: true, offset:'80%' });
		}
	}

	vc_waypoints();

	// Visual Composer FrontEnd
	$(window).load(function() {
		shortcodeTimeline();
		recentsCarousel();
		blogMasonry();
		portfolioFiltering();
		portfolioMasonry();
		elise_counter();
		imageSlider();
		imageGrid();
		vcPieChart();
		menuWidthChecker();
		progressBar();
		vc_waypoints();
		FWCommonHeight();
		parallaxBG();
	})
	$(window).resize(function() {
		shortcodeTimeline();
		recentsCarousel();
		blogMasonry();
		portfolioFiltering();
		portfolioMasonry();
		elise_counter();
		imageSlider();
		imageGrid();
	    vcRowWindowHeight();
	   	vcRowHeightVA();
	   	vcPieChart();
		menuWidthChecker();
		FWCommonHeight();
		parallaxBG();
	});




})(jQuery);
