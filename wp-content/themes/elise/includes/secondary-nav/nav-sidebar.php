<?php 
wp_reset_query();
global $elise_options;
global $post;
?>

<div class="secondary-navigation sidebar-navigation">
	<div class="sec-nav-overlay"></div>
	<div class="sidebar-nav-wrap">

    <div class="sec-nav-close-btn"><a href="#"></a></div>

    <!-- logotype container -->
    <div class="sidebar-nav-logo">
      <a href="<?php echo home_url(); ?>">
        <?php 
        if ( isset($elise_options['opt-logo-mobile']['url']) && !empty($elise_options['opt-logo-mobile']['url'])) { ?>
          <img src="<?php echo esc_url($elise_options['opt-logo-mobile']['url']) ?>" alt="">
        <?php }
        elseif ( isset($elise_options['opt-logo']['url']) && !empty($elise_options['opt-logo']['url'])) { ?>
          <img src="<?php echo esc_url($elise_options['opt-logo']['url']) ?>" alt="">
        <?php } 
        else { bloginfo('name'); } ?>
      </a>
    </div><!-- logotype container end -->

		<nav class="sidebar-nav">
      <?php wp_nav_menu(
          array(
            'theme_location' => 'full-mobile-nav',
            'link_before'     => '<span>',
            'link_after'      => '</span>',
          )
        );
      ?>
		</nav>

    <div class="sidebar-nav-widgets">

      <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar-nav') ) : ?>   
      <?php endif; ?> 

    </div>

	</div>

</div>
