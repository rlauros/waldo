<?php 
  global $elise_options;
  global $post;

  if ($elise_options['opt-blog-style'] != 4) {
    $elise_options['opt-show-thumbnail'] = true;
  }

  // blog media
  if ($elise_options['opt-show-thumbnail'] == true && !post_password_required() ) {
    if (!function_exists('blog_media_audio')) {
      function blog_media_audio() {

        global $elise_options;

        echo '<div class="blog-media"><div class="fit-aud">';
        
        if ($elise_options['opt-format-audio-type'] == 1) {
          $allowed_tags = array(
            'iframe' => array(
                'align' => array(),
                'width' => array(),
                'height' => array(),
                'frameborder' => array(),
                'name' => array(),
                'src' => array(),
                'id' => array(),
                'class' => array(),
                'style' => array(),
                'scrolling' => array(),
                'marginwidth' => array(),
                'marginheight' => array(),
            ),
          );

          echo wp_kses($elise_options['opt-format-audio-embed'], $allowed_tags);
        } 
        else { ?>

          <?php 
            $audio_mp3 = $elise_options['opt-format-audio-mp3']['url'];
            $audio_oga = $elise_options['opt-format-audio-oga']['url'];
            $song_author = $elise_options['opt-format-audio-author'];
            $song_title = $elise_options['opt-format-audio-title'];
                  
            if ($audio_mp3) {
          ?>
           
          <!-- JPLAYER -->
          <div id="jplayer-audio-<?php echo get_the_ID(); ?>"></div>

          <div class="audio-player">
            <div class="jp-interface">
              <div id="jp_container_1">

                <div class="jp-cover">                
                  <?php if (has_post_thumbnail()) { 
                    the_post_thumbnail('thumbnail'); 
                  } else {
                    echo '<img height="150" width="150" class="blank-image" src="'. IMAGES .'/blank.png" alt="">';
                  } ?>

                  <div class="play-pause">
                  <a href="javascript:;" class="jp-play" tabindex="1"><i class="typcn typcn-media-play"></i></a>
                  <a href="javascript:;" class="jp-pause" tabindex="1"><i class="typcn typcn-media-pause"></i></a>
                  </div>
                </div>

                <div class="jp-info">

                  <div class="jp-author">
                    <div class="song-author"><?php echo $song_author ? esc_html($song_author) : __('Author', 'Elise') ; ?></div>
                    <span class="separator">-</span>
                    <div class="song-title"><?php echo $song_title ? esc_html($song_title) : __('Song Title', 'Elise') ; ?></div>
                  </div>

                  <div class="jp-volume">
                    <a href="javascript:;" class="jp-mute" tabindex="2" title="mute"><i class="fa fa-volume-up"></i></a>
                    <a href="javascript:;" class="jp-unmute" tabindex="2" title="unmute"><i class="fa fa-volume-off"></i></a>
                    <!--volume bar-->
                    <div class="jp-volume-bar">
                        <div class="jp-volume-bar-value"></div>
                    </div>
                  </div>

                  <div class="jp-time">
                    <div class="jp-current-time"></div>
                    <span class="separator">/</span>
                    <div class="jp-duration"></div>
                  </div>

                  <div class="jp-progress">
                    <div class="jp-seek-bar">
                      <div class="jp-play-bar"></div>
                    </div>
                  </div>

                </div>
              </div>   
            </div>  

          </div>   

          <script type="text/javascript">
            (function($) {
              "use strict";
            
              $("#jplayer-audio-<?php echo get_the_ID(); ?>").jPlayer({
                ready: function(event) {
                  $(this).jPlayer("setMedia", {
                    mp3: "<?php echo esc_url($audio_mp3) ?>",
                    oga: "<?php echo esc_url($audio_oga) ?>"
                  });
                },
                swfPath: "http://jplayer.org/latest/js",
                supplied: "mp3"
              });   

            })(jQuery);          
          </script>
          <!-- JPLAYER END -->


      <?php
         } // endif mp3 file not empty 
        } // endif custom audio

        echo '</div></div>';
      }
    }
  }

  // // has-post-thumbnail when password protected fix
  // $hpth = '';
  // if (has_post_thumbnail() && post_password_required()) {
  //   $hpth = 'has-post-thumbnail';
  // }
?>

<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { 

  if ($elise_options['opt-show-sidebar'] == true) {
    $masonry_col_width = 'col-sm-6';
  } 
  elseif ($elise_options['opt-show-sidebar'] == false) {
    $masonry_col_width = 'col-md-4 col-sm-6';
  } ?>

  <div class="<?php echo esc_attr($masonry_col_width); ?>">
<?php } ?>

<!-- Blog Post -->
<article id="post-<?php the_ID(); ?>" <?php post_class('has-post-thumbnail'); ?>>

  <?php
  if ($elise_options['opt-show-thumbnail'] == true && !post_password_required()  && !is_single()) {
    if ($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) {
      if ($elise_options['opt-thumbnail-on-top'] == true) {
        blog_media_audio();
      }
    } 
    elseif (($elise_options['opt-blog-style'] != 1 || $elise_options['opt-blog-style'] != 4) && $elise_options['opt-show-thumbnail'] == true) {
      blog_media_audio();
    }
  }

  if (is_single() && $elise_options['opt-show-thumbnail'] == true && !post_password_required() ) {
    blog_media_audio();
  }
  ?>

  <div class="bc-wrap">

  <?php if (!is_single()) { ?>
    <div class="blog-header">
         <div class="time" title="<?php the_time(get_option('date_format')); echo ' - '; the_time(get_option('time_format')); ?>"><a href="<?php the_permalink(); ?>"><span class="month"><?php the_time('M'); ?></span><?php the_time('d'); ?></a></div>

      <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

      <div class="entry-meta">
        <p><?php _e('Posted by ', 'Elise') ?>
          <span class="author"><?php the_author_posts_link(); ?></span>
           <?php _e('in ', 'Elise') ?> 
           <span class="categories"><?php the_category(',&nbsp;'); ?></span>
           
          <?php
            // Only show the comments link if comments are allowed and it's not password protected
          
            if (comments_open() && !post_password_required()) { 
              echo ' &middot; ';
              comments_popup_link(__('No Comments', 'Elise'), __('1 Comment', 'Elise'), __('% Comments', 'Elise'), 'comments');
            }
          ?>
          
         </p>
      </div>

    </div>
  <?php } else { ?> <!-- endif header -->

    <div class="entry-meta">
      <?php _e('Posted on ', 'Elise');
       echo the_time(get_option('date_format')); _e(' at ', 'Elise'); the_time(get_option('time_format')) ?>
    </div>

  <?php } ?>


  <?php
  if (($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) && !post_password_required() && $elise_options['opt-show-thumbnail'] == true && !is_single()) {
    if ($elise_options['opt-thumbnail-on-top'] == false) {
      blog_media_audio();
    }
  }
  ?>

  <div class="blog-content">
    <?php  ?>

    <?php if (!is_single() && !post_password_required() && $elise_options['opt-excerpts'] == 1 ) {
      the_excerpt();
    } else {
      the_content(__('Continue Reading', 'Elise'));
    } ?>

  </div>
  </div>
</article><!-- Blog Post End -->


<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { ?>
  </div>
<?php } ?>