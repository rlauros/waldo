<?php 
  global $elise_options;
  global $post;

  if ($elise_options['opt-blog-style'] != 4) {
    $elise_options['opt-show-thumbnail'] = true;
  }

  // blog media
  if (has_post_thumbnail() && !post_password_required() && $elise_options['opt-show-thumbnail'] == true) {
    if (!function_exists('blog_media')) {
      function blog_media() {

        global $elise_options;

        $content_width = intval($elise_options['opt-content-width']);

        // blog page images
        if ($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) {
          $image_w = ($elise_options['opt-show-sidebar'] == 1) ? round($content_width * .75 - 45, 0) : round($content_width - 30, 0);
          $image_h = '350';
        }
        elseif ($elise_options['opt-blog-style'] == 2) {
          $image_w = ($elise_options['opt-show-sidebar'] == 1) ? '312' : '412';
          $image_h = ($elise_options['opt-show-sidebar'] == 1) ? '230' : '280';
        }
        elseif ($elise_options['opt-blog-style'] == 3 ) {
          $image_w = ($elise_options['opt-show-sidebar'] == 1) ? round((($content_width * .75) - 15) / 2 - 30, 0) : round(($content_width / 3) - 30, 0);
          $image_h = ($elise_options['opt-show-sidebar'] == 1) ? '250' : '240';
        }
        // elseif ($elise_options['opt-blog-style'] == 4 ) {
        //   $image_w = round($content_width - 30, 0);
        //   $image_h = '500';
        // }

        // single image sizes
        if (is_single()) {

          if ($elise_options['opt-blog-page-style'] == 1 || $elise_options['opt-blog-page-style'] == 2) {
            $image_w = round($content_width * .75 - 45, 0);
          }
          if ($elise_options['opt-blog-page-style'] == 3 || $elise_options['opt-blog-page-style'] == 4) {
            $image_w = round($content_width - 30, 0);
          }

          $image_h = '';
        } 

        // get thumb
        $thumb = get_post_thumbnail_id();
        $img_url = wp_get_attachment_url( $thumb, 'full' ); //get full URL to image (use "large" or "medium" if the images too big)
        $image = aq_resize( $img_url, $image_w, $image_h, true, false, true ); //resize & crop the image

        // blog media
        echo '<div class="blog-media">';
        if (!is_single()) { 
        echo '<a href="'. get_permalink() .'">'; }
          if($image) {
            echo '<img src="'. esc_url($image[0]) .'" width="'. esc_attr($image[1]) .'" height="'. esc_attr($image[2]) .'" alt="" />';
          }
        if (!is_single()) { echo '</a>'; }
        echo '</div>';
      }
    }
  }

  // has-post-thumbnail when password protected fix
  $hpth = '';
  if (has_post_thumbnail() && post_password_required()) {
    $hpth = 'has-post-thumbnail';
  }
?>

<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { 

  if ($elise_options['opt-show-sidebar'] == true) {
    $masonry_col_width = 'col-sm-6';
  } 
  elseif ($elise_options['opt-show-sidebar'] == false) {
    $masonry_col_width = 'col-md-4 col-sm-6';
  } ?>

  <div class="<?php echo esc_attr($masonry_col_width); ?>">
<?php } ?>

<!-- Blog Post -->
<article id="post-<?php the_ID(); ?>" <?php post_class($hpth); ?>>

  <?php
  if (has_post_thumbnail() && !post_password_required() && $elise_options['opt-show-thumbnail'] == true && !is_single()) {
    if ($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) {
      if ($elise_options['opt-thumbnail-on-top'] == true) {
        blog_media();
      }
    } 
    elseif (($elise_options['opt-blog-style'] != 1 || $elise_options['opt-blog-style'] != 4) && $elise_options['opt-show-thumbnail'] == true) {
      blog_media();
    }
  }

  if (has_post_thumbnail() && !post_password_required() && is_single() && $elise_options['opt-show-thumbnail'] == true) {
    blog_media();
  }
  ?>

  <div class="bc-wrap">

  <?php if (!is_single()) { ?>
    <div class="blog-header">
      <div class="time" title="<?php the_time(get_option('date_format')); echo ' - '; the_time(get_option('time_format')); ?>"><a href="<?php the_permalink(); ?>"><span class="month"><?php the_time('M'); ?></span><?php the_time('d'); ?></a></div>

      <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

      <div class="entry-meta">
        <p><?php _e('Posted by ', 'Elise') ?>
          <span class="author"><?php the_author_posts_link(); ?></span>
           <?php _e('in ', 'Elise') ?>
           <span class="categories"><?php the_category(',&nbsp;'); ?></span>
           
          <?php
            // Only show the comments link if comments are allowed and it's not password protected
          
            if (comments_open() && !post_password_required()) { 
              echo ' &middot; ';
              comments_popup_link(__('No Comments', 'Elise'), __('1 Comment', 'Elise'), __('% Comments', 'Elise'), 'comments');
            }
          ?>
          
         </p>
      </div>

    </div>
  <?php } else { ?> <!-- endif header -->

  <div class="entry-meta">
    <?php _e('Posted on ', 'Elise');
     echo the_time(get_option('date_format')); _e(' at ', 'Elise'); the_time(get_option('time_format')) ?>
  </div>

  <?php } ?>
  <?php
  if (has_post_thumbnail() && !post_password_required() && ($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) && $elise_options['opt-show-thumbnail'] == true && !is_single()) {
    if ($elise_options['opt-thumbnail-on-top'] == false) {
      blog_media();
    }
  }
  ?>

  <div class="blog-content">
    <?php  ?>

    <?php if (!is_single() && !post_password_required() && $elise_options['opt-excerpts'] == 1 ) {
      the_excerpt();
    } else {
      the_content(__('Continue Reading', 'Elise'));
    } ?>

  </div>
  </div>
</article><!-- Blog Post End -->


<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { ?>
  </div>
<?php } ?>