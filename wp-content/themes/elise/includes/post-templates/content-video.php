<?php 
  global $elise_options;
  global $post;

  if ($elise_options['opt-blog-style'] != 4) {
    $elise_options['opt-show-thumbnail'] = true;
  }

  // blog media
  if (!post_password_required() && $elise_options['opt-show-thumbnail'] == true) {
    if (!function_exists('blog_media_video')) {
      function blog_media_video() {

        global $elise_options;

        $video_link = $elise_options['opt-format-video-url'];
        $video_embeed = $elise_options['opt-format-video-embed'];

        echo '<div class="blog-media"><div class="fit-vid">';
        
        if ($elise_options['opt-format-video-type'] == 1 && !empty($video_link)) {

          echo elise_convert_videos(esc_url($video_link));

        } 
        if ($elise_options['opt-format-video-type'] == 2 && !empty($video_embeed)) {          
          $allowed_tags = array(
            'iframe' => array(
                'align' => array(),
                'width' => array(),
                'height' => array(),
                'frameborder' => array(),
                'name' => array(),
                'src' => array(),
                'id' => array(),
                'class' => array(),
                'style' => array(),
                'scrolling' => array(),
                'marginwidth' => array(),
                'marginheight' => array(),
            ),
          );

          echo wp_kses($video_embeed, $allowed_tags);
        } 

        echo '</div></div>';
      }
    }
  }

  // // has-post-thumbnail when password protected fix
  // $hpth = '';
  // if (has_post_thumbnail() && post_password_required()) {
  //   $hpth = 'has-post-thumbnail';
  // }
?>

<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { 

  if ($elise_options['opt-show-sidebar'] == true) {
    $masonry_col_width = 'col-sm-6';
  } 
  elseif ($elise_options['opt-show-sidebar'] == false) {
    $masonry_col_width = 'col-md-4 col-sm-6';
  } ?>

  <div class="<?php echo esc_attr($masonry_col_width); ?>">
<?php } ?>

<!-- Blog Post -->
<article id="post-<?php the_ID(); ?>" <?php post_class('has-post-thumbnail'); ?>>

  <?php
  if ($elise_options['opt-show-thumbnail'] == true && !post_password_required() &&  !is_single()) {
    if ($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) {
      if ($elise_options['opt-thumbnail-on-top'] == true) {
        blog_media_video();
      }
    } 
    elseif (($elise_options['opt-blog-style'] != 1 || $elise_options['opt-blog-style'] != 4) && $elise_options['opt-show-thumbnail'] == true) {
      blog_media_video();
    }
  }

  if (is_single() && !post_password_required() && $elise_options['opt-show-thumbnail'] == true) {
    blog_media_video();
  }
  ?>

  <div class="bc-wrap">

  <?php if (!is_single()) { ?>
    <div class="blog-header">
         <div class="time" title="<?php the_time(get_option('date_format')); echo ' - '; the_time(get_option('time_format')); ?>"><a href="<?php the_permalink(); ?>"><span class="month"><?php the_time('M'); ?></span><?php the_time('d'); ?></a></div>

      <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

      <div class="entry-meta">
        <p><?php _e('Posted by ', 'Elise') ?>
          <span class="author"><?php the_author_posts_link(); ?></span>
           <?php _e('in ', 'Elise') ?>
           <span class="categories"><?php the_category(',&nbsp;'); ?></span>
           
          <?php
            // Only show the comments link if comments are allowed and it's not password protected
          
            if (comments_open() && !post_password_required()) { 
              echo ' &middot; ';
              comments_popup_link(__('No Comments', 'Elise'), __('1 Comment', 'Elise'), __('% Comments', 'Elise'), 'comments');
            }
          ?>
          
         </p>
      </div>

    </div>
  <?php } else { ?> <!-- endif header -->

    <div class="entry-meta">
      <?php _e('Posted on ', 'Elise');
       echo the_time(get_option('date_format')); _e(' at ', 'Elise'); the_time(get_option('time_format')) ?>
    </div>

  <?php } ?>


  <?php
  if (($elise_options['opt-blog-style'] == 1 || $elise_options['opt-blog-style'] == 4) && !post_password_required() && $elise_options['opt-show-thumbnail'] == true && !is_single()) {
    if ($elise_options['opt-thumbnail-on-top'] == false) {
      blog_media_video();
    }
  }
  ?>

  <div class="blog-content">
    <?php  ?>

    <?php if (!is_single() && !post_password_required() && $elise_options['opt-excerpts'] == 1 ) {
      the_excerpt();
    } else {
      the_content(__('Continue Reading', 'Elise'));
    } ?>

  </div>
  </div>
</article><!-- Blog Post End -->


<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { ?>
  </div>
<?php } ?>