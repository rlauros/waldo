<?php 
  global $elise_options;
  global $post;

?>

<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { 

  if ($elise_options['opt-show-sidebar'] == true) {
    $masonry_col_width = 'col-sm-6';
  } 
  elseif ($elise_options['opt-show-sidebar'] == false) {
    $masonry_col_width = 'col-md-4 col-sm-6';
  } ?>

  <div class="<?php echo esc_attr($masonry_col_width); ?>">
<?php } ?>

<!-- Blog Post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="bc-wrap">


    <div class="blog-header">
        <?php if (!is_single()) { ?>
         <div class="time" title="<?php the_time(get_option('date_format')); echo ' - '; the_time(get_option('time_format')); ?>"><a href="<?php the_permalink(); ?>"><span class="month"><?php the_time('M'); ?></span><?php the_time('d'); ?></a></div>
        <?php } ?> <!-- endif header -->

      <?php if (!is_single()) { ?>  
      <div class="entry-meta">
        <p><?php _e('Posted by ', 'Elise') ?>
          <span class="author"><?php the_author_posts_link(); ?></span>
           <?php _e('in ', 'Elise') ?> 
           <span class="categories"><?php the_category(',&nbsp;'); ?></span>
           
          <?php
            // Only show the comments link if comments are allowed and it's not password protected
          
            if (comments_open() && !post_password_required()) { 
              echo ' &middot; ';
              comments_popup_link(__('No Comments', 'Elise'), __('1 Comment', 'Elise'), __('% Comments', 'Elise'), 'comments');
            }
          ?>
          
         </p>
      </div>
    <?php } else { ?> <!-- endif header -->

      <div class="entry-meta">
        <?php _e('Posted on ', 'Elise');
         echo the_time(get_option('date_format')); _e(' at ', 'Elise'); the_time(get_option('time_format')) ?>
      </div>

    <?php } ?>

    <?php if (!post_password_required()) { ?>

      <h3><a href="<?php echo esc_url($elise_options['opt-format-link-url']) ?>"><?php the_title(); ?></a></h3>

      <a href="<?php echo esc_url($elise_options['opt-format-link-url']) ?>" class="link"><?php echo esc_url($elise_options['opt-format-link-url']) ?></a>

    <?php } else { echo get_the_password_form(); } ?>
    </div>
    
  </div>
</article><!-- Blog Post End -->


<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { ?>
  </div>
<?php } ?>