<?php 
  global $elise_options;
  global $post;

?>

<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { 

  if ($elise_options['opt-show-sidebar'] == true) {
    $masonry_col_width = 'col-sm-6';
  } 
  elseif ($elise_options['opt-show-sidebar'] == false) {
    $masonry_col_width = 'col-md-4 col-sm-6';
  } ?>

  <div class="<?php echo esc_attr($masonry_col_width); ?>">
<?php } ?>

<!-- Blog Post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="bc-wrap">

  <?php if (!is_single()) { ?>
    <div class="blog-header">
         <div class="time" title="<?php the_time(get_option('date_format')); echo ' - '; the_time(get_option('time_format')); ?>"><a href="<?php the_permalink(); ?>"><span class="month"><?php the_time('M'); ?></span><?php the_time('d'); ?></a></div>

      <div class="entry-meta">
        <p><?php _e('Posted by ', 'Elise') ?>
          <span class="author"><?php the_author_posts_link(); ?></span>
           <?php _e('in ', 'Elise') ?>
           <span class="categories"><?php the_category(',&nbsp;'); ?></span>
           
          <?php
            // Only show the comments link if comments are allowed and it's not password protected
          
            if (comments_open() && !post_password_required()) { 
              echo ' &middot; ';
              comments_popup_link(__('No Comments', 'Elise'), __('1 Comment', 'Elise'), __('% Comments', 'Elise'), 'comments');
            }
          ?>
          
         </p>
      </div>

    </div>
  <?php } else { ?> <!-- endif header -->

    <div class="entry-meta">
      <?php _e('Posted on ', 'Elise');
       echo the_time(get_option('date_format')); _e(' at ', 'Elise'); the_time(get_option('time_format')) ?>
    </div>

  <?php } ?>

    <div class="blog-content">
      <?php if (!post_password_required()) { ?>
      <blockquote>
        <?php the_content(__('Continue Reading', 'Elise')); ?>

        <?php if (!empty($elise_options['opt-format-quote-author'])) { ?>
          <footer>
            <?php echo esc_html($elise_options['opt-format-quote-author']); ?>
          </footer>
        <?php } ?>

      </blockquote>
      <?php } else { echo get_the_password_form(); } ?>
    </div>
    
  </div>
</div><!-- Blog Post End -->


<?php if (!is_single() && $elise_options['opt-blog-style'] == 3) { ?>
  </div>
<?php } ?>