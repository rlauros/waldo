<?php global $elise_options; ?>

  <div class="content project-details section project-layout-large">
  <div class="container">
  <div class="row">
  <div class="col-md-12">
  <?php if (!post_password_required() ) { ?> 
  <?php 

  if ($elise_options['opt-project-image-gallery'] == 1) {
    $project_gallery_style = 'image-slider royalSlider rsElise rsNavInner';
  }
  elseif ($elise_options['opt-project-image-gallery'] == 2) {
    $project_gallery_style = 'project-image-inline';
  }


  if ($elise_options['opt-project-desc-position'] == 2 ) {

    $project_gallery = $elise_options['opt-meta-project-gallery']; 
    $project_attachments = explode(',', $project_gallery);

    if ($project_gallery) {
      echo '<div class="project-gallery margin-bottom '. $project_gallery_style. '">';

        $content_width = $elise_options['opt-content-width'];
        $image_w = round($content_width - 30, 0);
        $image_h = '';

        foreach ($project_attachments as $project_attachment) {
          echo '<div class="rsContent">'; 
          $project_video = wp_prepare_attachment_for_js( $project_attachment );
          $check_video = ($project_video['caption'] == '[video]' ? 'data-rsVideo="'. esc_attr(esc_url($project_video['alt'])) .'"' : '');

          //$project_image = wp_get_attachment_image_src($project_attachment, 'portfolio-full');
            
          $img_url = wp_get_attachment_url($project_attachment, 'full');
          $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );
          
          if ($elise_options['opt-project-image-gallery'] == 2 && $project_video['caption'] == '[video]') {
            echo '<div class="fit-vid">';
            $video_link = $project_video['alt'];
            echo elise_convert_videos(esc_url($video_link));
            echo '</div>';
          } 
          else {
            echo '<img class="rsImg" src="'. esc_url($image[0]) .'" width="'. esc_attr($image[1]) .'" height="'. esc_attr($image[2]) .'"  alt="" '. $check_video .' />';
          }
          
          if ($elise_options['opt-project-image-gallery'] != 2) {
            echo wp_get_attachment_image( $project_attachment, 'thumbnail', '', array('class' => 'attachment-thumbnail rsTmb'));
          }

          echo '</div>';
        }

      echo '</div>';
    }
  }

  ?>
    
  <?php 
    if ($elise_options['opt-project-desc-style'] == 1) {
      $column_desc_class = 'col-md-12'; 
      $column_meta_class = 'col-md-12'; 
      $project_meta_style = 'project-meta-fullwidth';
    } 
    elseif ($elise_options['opt-project-desc-style'] == 2) {
      $column_desc_class = 'col-md-9 col-md-pull-3'; 
      $column_meta_class = 'col-md-3 col-md-push-9'; 
      $project_meta_style = 'project-meta-sidebar';
    }

  ?>

  <div class="row <?php echo esc_attr($project_meta_style) ?>">

    <div class="<?php echo esc_attr($column_meta_class) ?> project-info-wrap">
      <div class="project-info">

        <ul>

          <li class="info-item project-name">
            <small><?php _e('Name', 'Elise') ?></small>
            <span><?php the_title(); ?></span>
          </li>


           <?php $terms = get_the_terms( $post->ID , 'portfolio_category' ); 

           if (!empty($terms)) {
           ?>
          <li class="info-item project-categories">
            <small><?php _e('Category', 'Elise') ?></small>
            <span>
              <?php
                foreach ( $terms as $term ) {
                  echo '<span class="portfolio-cat">'. ucfirst($term->name) . '</span>';
                }
              ?>
            </span>
          </li>

          <?php } ?>

          <?php
          $project_year = $elise_options['opt-meta-project-year'];
          if (!empty($project_year)) {
          ?>
          <li class="info-item">
            <small><?php _e('Year', 'Elise') ?></small>
            <span><?php echo esc_html($project_year); ?></span>
          </li>
          <?php } ?> <!-- endif project-year -->

          <?php
          $project_client = $elise_options['opt-meta-project-client'];
          if (!empty($project_client)) {
          ?>
          <li class="info-item">
            <small><?php _e('Client', 'Elise') ?></small>
            <span><?php echo esc_html($project_client); ?></span>
          </li>
          <?php } ?> <!-- endif project-client -->

          <?php
          $project_url = $elise_options['opt-meta-project-url'];
          if (!empty($project_url)) {
          ?>
          <li class="info-item project-url">
            <a href="<?php echo esc_url($project_url) ?>" class="btn btn-default btn-block" target="_blank"><?php _e('Launch Project', 'Elise') ?></a>
          </li>
          <?php } ?> <!-- endif project-url -->

        </ul>

      </div>
    </div>

    <div class="<?php echo esc_attr($column_desc_class) ?>">
      <div class="project-description">
        <?php the_content(); ?>
      </div>
    </div>

  </div>

  <?php 

  if ($elise_options['opt-project-desc-position'] == 1 ) {

    $project_gallery = $elise_options['opt-meta-project-gallery']; 
    $project_attachments = explode(',', $project_gallery);

    if ($project_gallery) {
      echo '<div class="project-gallery margin-top '. $project_gallery_style. '">';

        $content_width = $elise_options['opt-content-width'];
        $image_w = round($content_width - 30, 0);
        $image_h = '';

        foreach ($project_attachments as $project_attachment) {
          echo '<div class="rsContent">'; 
          $project_video = wp_prepare_attachment_for_js( $project_attachment );
          $check_video = ($project_video['caption'] == '[video]' ? 'data-rsVideo="'. esc_attr(esc_url($project_video['alt'])) .'"' : '');

          //$project_image = wp_get_attachment_image_src($project_attachment, 'portfolio-full');
            
          $img_url = wp_get_attachment_url($project_attachment, 'full');
          $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );
          
          if ($elise_options['opt-project-image-gallery'] == 2 && $project_video['caption'] == '[video]') {
            echo '<div class="fit-vid">';
            $video_link = $project_video['alt'];
            echo elise_convert_videos(esc_url($video_link));
            echo '</div>';
          } 
          else {
            echo '<img class="rsImg" src="'. esc_url($image[0]) .'" width="'. esc_attr($image[1]) .'" height="'. esc_attr($image[2]) .'"  alt="" '. $check_video .' />';
          }
          
          if ($elise_options['opt-project-image-gallery'] != 2) {
            echo wp_get_attachment_image( $project_attachment, 'thumbnail', '', array('class' => 'attachment-thumbnail rsTmb'));
          }

          echo '</div>';
        }

      echo '</div>';
    }
  }

  ?>
  <?php } else { echo get_the_password_form(); }?> 
  </div> <!-- section end -->
  </div>
  </div>
  </div>


