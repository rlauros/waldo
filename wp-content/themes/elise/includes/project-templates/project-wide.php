<?php global $elise_options; ?>

  <?php if (!post_password_required() ) { ?> 
  <div class="project-gallery-wide-wrapper">
  <div class="container">
  <div class="row">
  <div class="col-md-12">

  <?php 

    $project_gallery = $elise_options['opt-meta-project-gallery']; 
    $project_attachments = explode(',', $project_gallery);

    if ($project_attachments) {
      echo '<div class="project-gallery project-image-slider-wide royalSlider rsElise rsVisible rsNavInner">';

        $content_width = $elise_options['opt-content-width'];
        $image_w = round($content_width - 30, 0);
        $image_h = ($elise_options['opt-gallery-wide-height'] != 600) ? $elise_options['opt-gallery-wide-height'] : 600;

        foreach ($project_attachments as $project_attachment) {
          echo '<div class="rsContent">'; 
          $project_video = wp_prepare_attachment_for_js( $project_attachment );
          $check_video = ($project_video['caption'] == '[video]' ? 'data-rsVideo="'. esc_attr(esc_url($project_video['alt'])) .'"' : '');

          $img_url = wp_get_attachment_url($project_attachment, 'full');
          $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );
          
          echo '<img class="rsImg" src="'. esc_url($image[0]) .'" width="'. esc_attr($image[1]) .'" height="'. esc_attr($image[2]) .'"  alt="" '. $check_video .' />';
          
          // if ($elise_options['opt-project-image-gallery'] != 2) {
            echo wp_get_attachment_image( $project_attachment, 'thumbnail', '', array('class' => 'attachment-thumbnail rsTmb'));
          // }

          echo '</div>';
        }
      echo '</div>';
    }

  ?>

  </div>
  </div>
  </div> <!-- section end -->
  </div>
  <?php } ?>
    
  <?php 
    if ($elise_options['opt-project-desc-style'] == 1) {
      $column_desc_class = 'col-md-12'; 
      $column_meta_class = 'col-md-12'; 
      $project_meta_style = 'project-meta-fullwidth';
    } 
    elseif ($elise_options['opt-project-desc-style'] == 2) {
      $column_desc_class = 'col-md-9 col-md-pull-3'; 
      $column_meta_class = 'col-md-3 col-md-push-9'; 
      $project_meta_style = 'project-meta-sidebar';
    }

  ?>

  <div class="content project-details project-layout-wide section">
  <div class="container">
  <div class="row">
  <div class="col-md-12">
  <?php if (!post_password_required() ) { ?> 

  <div class="row project-meta <?php echo esc_attr($project_meta_style) ?>">

    <div class="<?php echo esc_attr($column_meta_class) ?> project-info-wrap">
      <div class="project-info">

        <ul>

          <li class="info-item project-name">
            <small><?php _e('Name', 'Elise') ?></small>
            <span><?php the_title(); ?></span>
          </li>


           <?php $terms = get_the_terms( $post->ID , 'portfolio_category' ); 

           if (!empty($terms)) {
           ?>
          <li class="info-item project-categories">
            <small><?php _e('Category', 'Elise') ?></small>
            <span>
              <?php
                foreach ( $terms as $term ) {
                  echo '<span class="portfolio-cat">'. ucfirst($term->name) . '</span>';
                }
              ?>
            </span>
          </li>

          <?php } ?>

          <?php
          $project_year = $elise_options['opt-meta-project-year'];
          if (!empty($project_year)) {
          ?>
          <li class="info-item">
            <small><?php _e('Year', 'Elise') ?></small>
            <span><?php echo esc_html($project_year); ?></span>
          </li>
          <?php } ?> <!-- endif project-year -->

          <?php
          $project_client = $elise_options['opt-meta-project-client'];
          if (!empty($project_client)) {
          ?>
          <li class="info-item">
            <small><?php _e('Client', 'Elise') ?></small>
            <span><?php echo esc_html($project_client); ?></span>
          </li>
          <?php } ?> <!-- endif project-client -->

          <?php
          $project_url = $elise_options['opt-meta-project-url'];
          if (!empty($project_url)) {
          ?>
          <li class="info-item project-url">
            <a href="<?php echo esc_url($project_url) ?>" class="btn btn-default btn-block" target="_blank"><?php _e('Launch Project', 'Elise') ?></a>
          </li>
          <?php } ?> <!-- endif project-url -->

        </ul>

      </div>
    </div>

    <div class="<?php echo esc_attr($column_desc_class) ?>">
      <div class="project-description">
        <?php the_content(); ?>
      </div>
    </div>

  </div>


  <?php } else { echo get_the_password_form(); }?> 
  </div> <!-- section end -->
  </div>
  </div>
  </div>