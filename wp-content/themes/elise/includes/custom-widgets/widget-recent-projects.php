<?php
/**
 * Plugin Name: Recent Projects Widget
 * Description: A widget that displays Tab with recent/popular posts and recent comments.
 * Version: 1.0
 * Author: LeopardThemes
 */


add_action( 'widgets_init', 'elise_recent_projects' );


function elise_recent_projects() {
	register_widget( 'Elise_Recent_Projects' );
}


class Elise_Recent_Projects extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_projects', 'description' => __('A widget that displays recent projects. ', 'Elise') );
		parent::__construct('recent-projects', __('Elise - Recent Projects', 'Elise'), $widget_ops);
		$this->alt_option_name = 'recent_projects';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_projects', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Projects', 'Elise' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number_recent_projects = ( ! empty( $instance['number_recent_projects'] ) ) ? absint( $instance['number_recent_projects'] ) : 4;
		if ( ! $number_recent_projects )
			$number_recent_projects = 4;


		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */


?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>


		  	<?php 
				$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			          'posts_per_page' => $number_recent_projects,
			          'post_type' => 'portfolio',
			          // 'tax_query' => array(
			          //   array(
			          //     'taxonomy' => 'project_categories',
			          //     'field' => 'term_id',
			          //     'terms' => $selected_categories
			          //   )
			          // )
				) ) );

				if ($r->have_posts()) :
			?>

			<ul class="recent_project">
				<?php while ( $r->have_posts() ) : $r->the_post(); ?>
					<li>
						<div class="widget-portfolio-item">
							<a href="<?php the_permalink(); ?>">                
								<?php if (has_post_thumbnail()) { 
				                  the_post_thumbnail('thumbnail'); 
				                } else {
				                	echo '<img height="150" width="150" class="blank-image" src="'. IMAGES .'/blank/blank-1x1.png" alt="">';
				                } ?>
				               	<div class="overlay">
				               		<div class="see-more"><h5><?php get_the_title() ? the_title() : the_ID(); ?></h5></div>
				               	</div>
							</a>
						</div>
					</li>
				<?php endwhile; endif; ?>
			</ul>

			<?php wp_reset_postdata(); ?>




		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		//wp_reset_postdata();

		// endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_projects', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number_recent_projects'] = (int) $new_instance['number_recent_projects'];
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_projects']) )
			delete_option('widget_recent_projects');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_projects', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number_recent_projects    = isset( $instance['number_recent_projects'] ) ? absint( $instance['number_recent_projects'] ) : 4;

?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'Elise' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number_recent_projects' ); ?>"><?php _e( 'Number of projects to show:', 'Elise' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number_recent_projects' ); ?>" name="<?php echo $this->get_field_name( 'number_recent_projects' ); ?>" type="text" value="<?php echo $number_recent_projects; ?>" size="3" /></p>

<?php
	}
}


?>