<?php
/**
 * Plugin Name: Recent Posts Tab Widget
 * Description: A widget that displays Tab with recent/popular posts and recent comments.
 * Version: 1.0
 * Author: LeopardThemes
 */


add_action( 'widgets_init', 'elise_recent_posts_tab' );


function elise_recent_posts_tab() {
	register_widget( 'Elise_Recent_Posts_Tab' );
}


class Elise_Recent_Posts_Tab extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_posts_tab', 'description' => __('A widget that displays Tabs with recent/popular posts and recent comments. ', 'Elise') );
		parent::__construct('recent-posts-tab', __('Elise - Recent Posts Tabs', 'Elise'), $widget_ops);
		$this->alt_option_name = 'recent_posts_tab';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_posts_tab', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		// $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts Tab' );

		/** This filter is documented in wp-includes/default-widgets.php */
		// $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number_recent = ( ! empty( $instance['number_recent'] ) ) ? absint( $instance['number_recent'] ) : 3;
		if ( ! $number_recent )
			$number_recent = 3;

		$number_comments = ( ! empty( $instance['number_comments'] ) ) ? absint( $instance['number_comments'] ) : 3;
		if ( ! $number_comments )
			$number_comments = 3;

		$show_popular_posts = isset( $instance['show_popular_posts'] ) ? $instance['show_popular_posts'] : true;
		$show_recent_comments = isset( $instance['show_recent_comments'] ) ? $instance['show_recent_comments'] : true;

		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */


?>
		<?php echo $before_widget; ?>
		<?php // if ( $title ) echo $before_title . $title . $after_title; ?>

		<?php $random_tab_id = rand(1, 9999) ?>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs">
		  	<li class="recent-posts-tab active"><a href="#recent-posts-tab-<?php echo intval($random_tab_id) ?>" data-toggle="tab"><?php _e('Recent', 'Elise') ?></a></li>
		  	<?php if ($show_popular_posts) { ?><li class="popular-posts-tab"><a href="#popular-posts-tab-<?php echo intval($random_tab_id) ?>" data-toggle="tab"><?php _e('Popular', 'Elise') ?></a></li><?php } ?>
			<?php if ($show_recent_comments) { ?><li class="recent-comments-tab"><a href="#recent-comments-tab-<?php echo intval($random_tab_id) ?>" data-toggle="tab"><i class="fa fa-comments"></i></a></li><?php } ?>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
		  <div class="tab-pane fade in active" id="recent-posts-tab-<?php echo intval($random_tab_id) ?>">

		  	<?php 
				$r = new WP_Query( apply_filters( 'widget_posts_args', array(
					'posts_per_page'      => intval($number_recent),
					'no_found_rows'       => true,
					'post_status'         => 'publish',
					'ignore_sticky_posts' => true
				) ) );

				if ($r->have_posts()) :
			?>

			<ul class="recent_posts">
				<?php while ( $r->have_posts() ) : $r->the_post(); ?>
					<li>
						<span class="post-date"><?php echo get_the_date(); ?></span>
						<a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
					</li>
				<?php endwhile; endif; ?>
			</ul>

			<?php wp_reset_postdata(); ?>

		  </div>

		  <?php if ($show_popular_posts) { ?>

		  <div class="tab-pane fade" id="popular-posts-tab-<?php echo intval($random_tab_id) ?>">

		  	<?php 
				$r = new WP_Query( apply_filters( 'widget_posts_args', array(
					'posts_per_page'      => intval($number_recent),
					'no_found_rows'       => true,
					'meta_key'			  => 'post_views_count',
					'orderby'			  => 'meta_value_num',
					'order'				  => 'DESC',
					'post_status'         => 'publish',
					'ignore_sticky_posts' => true
				) ) );

				if ($r->have_posts()) :
			?>

			<ul class="popular_posts">
				<?php while ( $r->have_posts() ) : $r->the_post(); ?>
					<li>
						<span class="post-date"><?php echo get_the_date(); ?></span>
						<span class="post-views"><?php echo elise_get_post_count(get_the_ID()); ?></span>
						<a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
					</li>
				<?php endwhile; endif; ?>
			</ul>

			<?php wp_reset_postdata(); ?>


		  </div>

		  <?php } ?>
		  <?php if ($show_recent_comments) { ?>

		  <div class="tab-pane fade" id="recent-comments-tab-<?php echo intval($random_tab_id) ?>">
		  	<?php 
		  		global $comment;

		  		$output = '';	

				$comments = get_comments( apply_filters( 'widget_comments_args', array(
					'number'      => intval($number_comments),
					'status'      => 'approve',
					'post_status' => 'publish'
				) ) );

				$output .= '<ul class="recent_comments">';
				if ( $comments ) {
					// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
					$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
					_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

					foreach ( (array) $comments as $comment) {
						$output .=  '<li><span class="comment-author-tab">' . /* translators: comments widget: 1: comment author, 2: post link */ sprintf(__('%1$s says %2$s', 'Elise'), get_comment_author_link(), '</span><a class="recent_comments_excerpt" href="' . esc_url( get_comment_link($comment->comment_ID) ) . '">' . wp_html_excerpt( $comment->comment_content, 60, ' [...]' ) . '</a>') . '</li>';
					}
				}
				$output .= '</ul>';

				echo $output;
				?>

		  </div>

		  <?php } ?>

		</div>


		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		//wp_reset_postdata();

		// endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_posts_tab', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		//$instance['title'] = strip_tags($new_instance['title']);
		$instance['number_recent'] = (int) $new_instance['number_recent'];
		$instance['number_comments'] = (int) $new_instance['number_comments'];
		$instance['show_popular_posts'] = isset( $new_instance['show_popular_posts'] ) ? (bool) $new_instance['show_popular_posts'] : false;
		$instance['show_recent_comments'] = isset( $new_instance['show_recent_comments'] ) ? (bool) $new_instance['show_recent_comments'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_posts_tab']) )
			delete_option('widget_recent_posts_tab');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_posts_tab', 'widget');
	}

	function form( $instance ) {
		//$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number_recent    = isset( $instance['number_recent'] ) ? absint( $instance['number_recent'] ) : 3;
		$number_comments    = isset( $instance['number_comments'] ) ? absint( $instance['number_comments'] ) : 3;
		$show_popular_posts = isset( $instance['show_popular_posts'] ) ? (bool) $instance['show_popular_posts'] : true;
		$show_recent_comments = isset( $instance['show_recent_comments'] ) ? (bool) $instance['show_recent_comments'] : true;
?>
		<p><label for="<?php echo $this->get_field_id( 'number_recent' ); ?>"><?php _e( 'Number of recent/popular posts to show:', 'Elise' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number_recent' ); ?>" name="<?php echo $this->get_field_name( 'number_recent' ); ?>" type="text" value="<?php echo $number_recent; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_popular_posts ); ?> id="<?php echo $this->get_field_id( 'show_popular_posts' ); ?>" name="<?php echo $this->get_field_name( 'show_popular_posts' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_popular_posts' ); ?>"><?php _e( 'Display Popular Posts Tab', 'Elise' ); ?></label></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_recent_comments ); ?> id="<?php echo $this->get_field_id( 'show_recent_comments' ); ?>" name="<?php echo $this->get_field_name( 'show_recent_comments' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_recent_comments' ); ?>"><?php _e( 'Display Recent Comments Tab', 'Elise' ); ?></label></p>
		
		<p><label for="<?php echo $this->get_field_id( 'number_comments' ); ?>"><?php _e( 'Number of recent comments to show:', 'Elise' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number_comments' ); ?>" name="<?php echo $this->get_field_name( 'number_comments' ); ?>" type="text" value="<?php echo $number_comments; ?>" size="3" /></p>

<?php
	}
}


?>