<?php 
global $elise_options;

if ($elise_options['opt-navbar-style'] == 3) {
  if ($elise_options['opt-menu-position'] == 1) {
    $menu_position_class = 'menu-left';
  }
  elseif ($elise_options['opt-menu-position'] == 2) {
    $menu_position_class = 'menu-center';
  }
  elseif ($elise_options['opt-menu-position'] == 3) {
    $menu_position_class = 'menu-right';
  }
}
?>

<header>
  <div id="navbar">

        <div class="nav-top-bar-wrap">

          <div class="container">
            <div class="row">
              <div class="col-md-12">

                <!-- logotype container -->
                <div class="logo">
                  <a href="<?php echo home_url(); ?>">
                    <?php 
                    if ( isset($elise_options['opt-logo']['url']) && !empty($elise_options['opt-logo']['url']) ) { ?>
                      <img src="<?php echo esc_url($elise_options['opt-logo']['url']) ?>" alt="">
                    <?php } else { bloginfo('name'); } ?>
                  </a>
                </div><!-- logotype container end -->

                <div class="logo-desc hidden-xs"><?php esc_html(bloginfo('description')); ?></div>

                <div class="nav-social-icons hidden-xs hidden-sm"><?php header_social_icons(); ?></div>

              </div>
            </div>
          </div>

        </div>

        <div class="nav-bottom-bar-wrap">

          <div class="container">
            <div class="row">
              <div class="col-md-12">

              <?php if ($elise_options['opt-nav-search'] == 1) {
                  echo '<div class="search-bar search-bar-hidden">';
                  echo '<span class="close-btn"><a href="#"><i class="fa fa-times"></i></a></span>';
                  get_search_form(); 
                  echo '</div>';
              } ?>

                <div class="nav-container <?php echo esc_attr($menu_position_class) ?>">

                  <nav class="main-nav <?php echo ($elise_options['opt-responsive'] == true) ? 'hidden-xs hidden-sm' : '' ?>">
                    <?php 
                      $elise_meta_custom_menu = $elise_options['opt-meta-custom-menu'];
                      if ($elise_meta_custom_menu) {
                        $elise_menu_id = $elise_meta_custom_menu;
                      } else {
                        $elise_menu_id = '';
                      }

                      wp_nav_menu(
                        array(
                          'theme_location' => 'main-nav',
                          'menu'           => $elise_menu_id,
                          'link_before'     => '<span>',
                          'link_after'      => '</span>',
                        )
                      );
                    ?>
                  </nav>

                  <nav class="nav-mobile nav-secondary-nav <?php echo ($elise_options['opt-responsive'] == true) ? 'visible-xs visible-sm' : 'hidden' ?>">
                    <a href="#" class="secondary-nav-btn"><i class="fa fa-bars"></i></a>
                  </nav>
                </div>

              </div>
            </div>
          </div>

        </div>

  </div>
</header>


<?php 
?>