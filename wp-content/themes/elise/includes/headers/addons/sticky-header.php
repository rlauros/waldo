<?php
global $elise_options;
	
if ($elise_options['opt-menu-position'] == 1) {
  $menu_position_class = 'nav-menu-left';
}
elseif ($elise_options['opt-menu-position'] == 2) {
  $menu_position_class = 'nav-menu-center';
}
elseif ($elise_options['opt-menu-position'] == 3) {
  $menu_position_class = 'nav-menu-right';
}

if ($elise_options['opt-sticky-header-style'] == 1) {
	$sticky_header_style = 'sh-show-alw';
}
elseif ($elise_options['opt-sticky-header-style'] == 2) {
	$sticky_header_style = 'sh-show-scrollup';
}


?> 
<!-- Sticky Header -->
<div id="sticky-header" class="navbar-sticky hidden-xs hidden-sm <?php echo esc_attr($sticky_header_style) ?> <?php echo esc_attr($menu_position_class) ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			
              <?php if ($elise_options['opt-nav-search'] == 1) {
                  echo '<div class="search-bar search-bar-hidden">';
                  echo '<span class="close-btn"><a href="#"><i class="fa fa-times"></i></a></span>';
                  get_search_form(); 
                  echo '</div>';
              } ?>

		        <!-- logotype container -->
		        <div class="logo-sticky">
		          <a href="<?php echo home_url(); ?>">
		            <?php 
                // $logo_sticky = $elise_options['opt-logo-stickyh']['url'];
		            // $logo = $elise_options['opt-logo']['url'];

                if (  isset($elise_options['opt-logo-stickyh']['url']) && !empty($elise_options['opt-logo-stickyh']['url'])) { ?>
                  <img src="<?php echo esc_url($elise_options['opt-logo-stickyh']['url']) ?>" alt="">
                <?php }
		            elseif ( isset($elise_options['opt-logo']['url']) && !empty($elise_options['opt-logo']['url']) ) { ?>
		              <img src="<?php echo esc_url($elise_options['opt-logo']['url']) ?>" alt="">
		            <?php } 
                else { bloginfo('name'); } ?>

		          </a>
		        </div><!-- logotype container end -->

		        <div class="nav-container">

		        <nav class="main-nav">
                    <?php 
                      $elise_meta_custom_menu = $elise_options['opt-meta-custom-menu'];
                      if ($elise_meta_custom_menu) {
                        $elise_menu_id = $elise_meta_custom_menu;
                      } else {
                        $elise_menu_id = '';
                      }

                      wp_nav_menu(
                        array(
                          'theme_location' => 'main-nav',
                          'menu'           => $elise_menu_id,
                          'link_before'     => '<span>',
                          'link_after'      => '</span>',
                        )
                      );
                    ?>
		        </nav>
		        </div>

			</div>
		</div>
	</div>
</div><!-- Sticky header End -->
