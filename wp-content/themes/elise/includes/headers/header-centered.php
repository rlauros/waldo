<?php 
global $elise_options;
global $post;

if ($elise_options['opt-menu-position'] == 1) {
  $menu_position_class = 'nav-menu-left';
}
elseif ($elise_options['opt-menu-position'] == 2) {
  $menu_position_class = 'nav-menu-center';
}
elseif ($elise_options['opt-menu-position'] == 3) {
  $menu_position_class = 'nav-menu-right';
}

$hover_style = '';
if ($elise_options['opt-hover-style'] == 1) {
  $hover_style = 'hover-full';
}
elseif ($elise_options['opt-navbar-style'] != 4) {
  $hover_style = 'hover-boxed';
}

// if ($elise_options['opt-navbar-transparent'] == 1 && $elise_options['opt-navbar-style'] != 4) {
//   $hover_style = 'hover-boxed';
// }

?>

<header>

  <?php 
    if (function_exists('elise_topbar')) {
      elise_topbar();
    }
  ?>

  <div id="navbar" class="<?php echo esc_attr($menu_position_class) ?>">
    <div class="container">
      <div class="row">

        <div class="col-md-12 <?php if ($elise_options['opt-navbar-style'] == 4) { echo 'header-style-bar'; } ?>">


        <!-- logotype container -->
        <div class="logo">
          <a href="<?php echo home_url(); ?>">
            <?php 
            if ( isset($elise_options['opt-logo']['url']) && !empty($elise_options['opt-logo']['url']) ) { ?>
              <img src="<?php echo esc_url($elise_options['opt-logo']['url']) ?>" alt="">
            <?php } else { bloginfo('name'); } ?>
          </a>
        </div><!-- logotype container end -->

        <?php if($elise_options['opt-hover-style'] == 1 && $elise_options['opt-navbar-transparent'] != 1) { ?>
        </div>
      </div>
    </div>

    <div class="nav-centered-bar-wrap">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
        <?php } ?>

            <?php if ($elise_options['opt-nav-search'] == 1) {
                echo '<div class="search-bar search-bar-hidden">';
                echo '<span class="close-btn"><a href="#"><i class="fa fa-times"></i></a></span>';
                get_search_form(); 
                echo '</div>';
            } ?>

            <?php if($elise_options['opt-hover-style'] == 2 || $elise_options['opt-navbar-transparent'] == 1) {
              echo '<div class="hover-boxed-wrap">';
            } ?>

            <div class="nav-container">

            <nav class="main-nav <?php echo ($elise_options['opt-responsive'] == true) ? 'hidden-xs hidden-sm' : '' ?> <?php echo esc_attr($hover_style) ?>">
                <?php 
                  $elise_meta_custom_menu = $elise_options['opt-meta-custom-menu'];
                  if ($elise_meta_custom_menu) {
                    $elise_menu_id = $elise_meta_custom_menu;
                  } else {
                    $elise_menu_id = '';
                  }

                  wp_nav_menu(
                    array(
                      'theme_location' => 'main-nav',
                      'menu'           => $elise_menu_id,
                      'link_before'     => '<span>',
                      'link_after'      => '</span>',
                    )
                  );
                ?>
              </nav>

              <nav class="nav-mobile nav-secondary-nav <?php echo ($elise_options['opt-responsive'] == true) ? 'visible-xs visible-sm' : 'hidden' ?>">
                <a href="#" class="secondary-nav-btn"><i class="fa fa-bars"></i></a>
              </nav>
              
            </div>

          </div>
          <?php if($elise_options['opt-hover-style'] == 2 || $elise_options['opt-navbar-transparent'] == 1) { 
            echo '<!-- hover-boxed-wrap end -->';
           } ?>
        </div>
      </div>

    </div>
  </div>
</header>
