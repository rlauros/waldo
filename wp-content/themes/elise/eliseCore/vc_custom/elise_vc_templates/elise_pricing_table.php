<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'columns' => '',
    'css_animation' => '',
), $atts));

if ($columns == 1) {
	$columns_class = 'pt-column-1 ';
}
elseif ($columns == 2) {
	$columns_class = 'pt-columns-2 ';
}
elseif ($columns == 3) {
	$columns_class = 'pt-columns-3 ';
}
elseif ($columns == 4) {
	$columns_class = 'pt-columns-4 ';
}
elseif ($columns == 5) {
	$columns_class = 'pt-columns-5 ';
}

?>
<div class="elise-pricing-table <?php echo esc_attr($columns_class) . $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>">
	<?php echo wpb_js_remove_wpautop($content); //Parse inner shortcodes ?>
</div>