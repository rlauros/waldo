<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'value' => '50',
    'duration' => '2000',
    'size' => '250',
    'thickness' => '15',
    'color_bg' => 'rgba(0,0,0,.1)',
    'color_fill' => 'color',
    'color_1' => '#8dc73f',
    'color_2' => '#3FC7B9',
    'value_color' => '',
    'align' => 'center',
    'show_unit' => '',
    'value_text' => '',
    'css_animation' => '',
), $atts));

$value_size = 'pc-large';
if ($size <= 110 && $size > 80 ) {
	$value_size = 'pc-medium';
} 
elseif ($size <= 80 ) {
    $value_size = 'pc-small';
} 

$value_color_style = '';
if ($value_color!='') {
	$value_color_style = 'style="color:'. esc_attr($value_color) .'"';
}

if ($align == 'left') {
	$align_class = 'elise-pie-left';
}
elseif ($align == 'right') {
	$align_class = 'elise-pie-right';
}
elseif ($align == 'center') {
	$align_class = 'elise-pie-center';
}

$show_unit_attr = '';
if ($show_unit == 'show') {
    $show_unit_attr = 'data-unit="true" ';
}


?>
<div class="elise-pie-chart-wrapper <?php echo esc_attr($align_class) . $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>">
	<div class="elise-pie-chart" data-value="<?php echo esc_attr($value) ?>" data-duration="<?php echo esc_attr($duration) ?>" data-fill="<?php echo esc_attr($color_fill) ?>" data-color1="<?php echo esc_attr($color_1) ?>" <?php echo ($color_fill == 'gradient' ? 'data-color2="'. esc_attr($color_2) .'"' : '') ?> data-colorbg="<?php echo esc_attr($color_bg) ?>" data-size="<?php echo esc_attr($size) ?>" data-thickness="<?php echo esc_attr($thickness) ?>" <?php echo $show_unit_attr ?>>
		<div class="elise-pie-value-wrapper">
			<span class="elise-counter <?php echo esc_attr($value_size) ?>" <?php echo $value_color_style ?>>0</span>
			<?php if ($size > 110 && !empty($value_text)) { ?><small <?php echo $value_color_style ?>><?php echo esc_html($value_text) ?></small><?php } ?>
		</div>
	</div>
</div>