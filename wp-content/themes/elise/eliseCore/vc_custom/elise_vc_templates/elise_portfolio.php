<?php
global $elise_options, $post;
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'portfolio_cats' => '',
    'pf_layout' => 3,
    // 'pf_style' => 1,
    'pf_filtering' => 1,
    'filtering_position' => 'filtering-left',
    'pf_items' => -1,
    'portfolio_gap' => 30,
    'portfolio_item_style' => 'portfolio-style-overlay',
    'portfolio_pagination' => 'no-paged',
    'items_per_page' => 3,
    'f_active_bg' => '',
    'f_active_color' => '',
    'f_color' => '',
    'show_quick_view' => 'show',
), $atts));

?>

<?php 
  $content_width = $elise_options['opt-content-width'];
  if ($pf_layout == 1) { // 1 column
    $portfolio_layout = 'col-md-12';
    $item_thumbnail_layout = 'portfolio-1col';
  }
  elseif ($pf_layout == 2) { // 2 columns
    $portfolio_layout = 'col-sm-6';
    $item_thumbnail_layout = 'portfolio-2col';
  }
  elseif ($pf_layout == 3) { // 3 columns
    $portfolio_layout = 'col-md-4 col-xs-6';
    $item_thumbnail_layout = 'portfolio-3col';
  }
  elseif ($pf_layout == 4) { // 4 columns
    $portfolio_layout = 'col-md-3 col-xs-4';
    $item_thumbnail_layout = 'portfolio-4col';
  }
  elseif ($pf_layout == 5) { // masonry
    $item_thumbnail_layout = 'portfolio-masonry pm-hidden';
  }

$elise_portfolio_custom = '';
if ($portfolio_gap != 30) {
  $custom_id = rand(0,9999);

  $elise_portfolio_custom = 'elise-ps-custom-' .$custom_id. ' ';

  $custom_css = '<style type="text/css" scoped>
      .elise-ps-custom-' .$custom_id. ' {
        margin: '. intval($portfolio_gap) / 2 .'px 0 !important;
      }

      .elise-ps-custom-' .$custom_id. ' .portfolio-items--container .row {
        margin-right: '. - intval($portfolio_gap) / 2 .'px;
        margin-left: '. - intval($portfolio_gap) / 2 .'px;
      }

      .elise-ps-custom-' .$custom_id. ' .portfolio-items--container .row [class*="col-"] {
        padding-left: '. intval($portfolio_gap) / 2 .'px;
        padding-right: '. intval($portfolio_gap) / 2 .'px;
        margin-bottom: '. intval($portfolio_gap) / 2 .'px;
        margin-top: '. intval($portfolio_gap) / 2 .'px;
      }
    </style>
  ';

  echo $custom_css;
}


$custom_filtering_id = '';
if (!empty($f_active_bg) || !empty($f_active_color) || !empty($f_color)) {
  $custom_id = rand(0,9999);
  $custom_filtering_id = 'elise-filtering-custom-' .$custom_id. ' ';

  $custom_filtering_style = '<style type="text/css" scoped>';
  if (!empty($f_active_bg)) {
    $custom_filtering_style .= '
      .elise-filtering-custom-' .intval($custom_id). ' .filters li .active {
        background: '. $f_active_bg .';
      }
    ';
  }
  if (!empty($f_active_color)) {
    $custom_filtering_style .= '
      .elise-filtering-custom-' .intval($custom_id). ' .filters li .active {
        color: '. $f_active_color .';
      }
    ';
  }
  if (!empty($f_color)) {
    $custom_filtering_style .= '
      .elise-filtering-custom-' .intval($custom_id). ' .filters li a {
        color: '. $f_color .';
      }
    ';
  }
  $custom_filtering_style .= '</style>';

  echo $custom_filtering_style;
}

?>


<div class="elise-portfolio-shortcode <?php echo esc_attr($elise_portfolio_custom) . esc_attr($el_class); ?>">

  <?php 
  // $portfolio_cats = 'Case Study, Web';

  if (!empty($portfolio_cats)) {
    $selected_categories = explode(',', $portfolio_cats);
  } else {
    $selected_categories = '';
  }
  ?>
<?php 
  if ($pf_filtering == 1) { ?>

  <div class="project-filtering <?php echo esc_attr($custom_filtering_id) . esc_attr($filtering_position) ?>">
    <?php 

    $filter_categories = get_terms('portfolio_category'); 
    // $selected_categories = $elise_options['opt-portfolio-categories'];

    echo '<ul class="filters">';
    //echo '<li><small class="filter-title">'. __('Filter Projects', 'Elise') .'</small></li>';
    echo '<li><a data-filter="*" class="active">'.__("All", "Elise").'</a></li>';

    if (!empty($selected_categories)) {
      foreach ($selected_categories as $selected_category) {
        $filter_category = get_term_by('slug',$selected_category,'portfolio_category');
        echo '<li><a data-filter=".'. esc_attr($filter_category->slug) .'">'. esc_html(ucfirst($filter_category->name)) . '</a></li>';
      }
    } else {
      foreach ($filter_categories as $filter_category) {
         echo '<li><a data-filter=".'. esc_attr($filter_category->slug) .'">'. esc_html(ucfirst($filter_category->name)) . '</a></li>';
      }
    }

    echo '</ul>';
    ?>
  </div>

<?php 
  } // project filtering if
?>


    <!-- Portfolio items container -->
    <div class="portfolio-items--container <?php echo esc_attr($portfolio_item_style); ?> <?php echo esc_attr($item_thumbnail_layout) ?>">

      <div class="row">

        <?php

         // if (empty($selected_categories)) {
         //   $terms_ids = get_terms( 'portfolio_category' ); 
         //   $selected_categories = wp_list_pluck( $terms_ids, 'term_id' );
         // }
        if ($pf_items == -1) {
          $no_found_rows = false;

          if ($portfolio_pagination == 'paged') {
           $projects_per_page = intval($items_per_page);
          } else {
           $projects_per_page = -1;
          }

        } else {
          $projects_per_page = intval($pf_items);
          $no_found_rows = true;
        }

        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );
        $portfolio = array(
          'posts_per_page' => intval($projects_per_page),
          'post_type' => 'portfolio',
          'no_found_rows'   => $no_found_rows,
          'paged'=> $paged
        );

        if(!empty($selected_categories)) {
          $portfolio['tax_query'] = array(
            array(
              'taxonomy' => 'portfolio_category',
              'field' => 'slug',
              'terms' => sanitize_text_field($selected_categories)
            )
          );
  
        }

        $p_query = new WP_Query($portfolio);

        if( $p_query->have_posts()) : while( $p_query->have_posts()) :  $p_query->the_post(); ?>

            <?php 
              $content_width = intval($elise_options['opt-content-width']);

              if ($pf_layout == 1) { // 1 column
                $image_w = round($content_width - 30, 0);
                $image_h = round(($content_width - 30) / 3, 0);
                $ratio = '3x1';
              }
              elseif ($pf_layout == 2) { // 2 columns
                $image_w = round(($content_width - 30) / 2, 0);
                $image_h = round((($content_width - 30) / 2) / 1.5, 0);
                $ratio = '3x2';
              }
              elseif ($pf_layout == 3) { // 3 columns
                $image_w = round(($content_width - 30) / 3, 0);
                $image_h = round((($content_width - 30) / 3) / 1.333333333, 0);
                $ratio = '4x3';
              }
              elseif ($pf_layout == 4) { // 4 columns
                $image_w = round(($content_width - 30) / 4, 0);
                $image_h = round(($content_width - 30) / 4, 0);
                $ratio = '1x1';
              }
              elseif ($pf_layout == 5) { // masonry

                $masonry_thumb_size = get_post_meta( $post->ID, 'opt-masonry-thumb-size', true );
                // redux_post_meta( "elise_options", $post->ID, "opt-masonry-thumb-size" );
                // echo $masonry_thumb_size;

                if ($masonry_thumb_size == 1 || empty($masonry_thumb_size)) { // small
                  $portfolio_layout = 'col-md-4 col-sm-6 col-xs-6 portfolio-masonry-small';
                  $image_w = round((($content_width - 30) / 3), 0);
                  $image_h = round((($content_width - 30) / 3) / 1.333333333, 0);
                  $ratio = '4x3';
                }
                elseif ($masonry_thumb_size == 2) { // wide
                  $portfolio_layout = 'col-md-8 col-xs-12 portfolio-masonry-wide';
                  $image_w = round(((($content_width - 30 )  / 3) * 2 ), 0);
                  $image_h = round((($content_width - 30) / 3) / 1.333333333, 0);
                  $ratio = '8x3';
                }
                elseif ($masonry_thumb_size == 3) { // tall
                  $portfolio_layout = 'col-md-4 col-sm-6 col-xs-6 portfolio-masonry-tall';
                  $image_w = round((($content_width - 30) / 3), 0);
                  $image_h = round((((($content_width - 30) / 3) * 2) / 1.333333333), 0);
                  $ratio = '2x3';
                }
                elseif ($masonry_thumb_size == 4) { // big
                  $portfolio_layout = 'col-md-8 col-xs-12 portfolio-masonry-big';
                  $image_w = round(((($content_width - 30 ) / 3) * 2 ), 0);
                  $image_h = round((((($content_width - 30) / 3) * 2) / 1.333333333), 0);
                  $ratio = '4x3';
                }

              }
            ?>

            <?php
              $terms = get_the_terms( $post->ID , 'portfolio_category' );

              $isotope_item_categories = '';
              if (!empty($terms)) {

                foreach ( $terms as $term ) {
                  $isotope_item_categories .= $term->slug .' ';
                }

              }
            ?>

          <!-- Portfolio item -->
          <div class="<?php echo esc_attr($portfolio_layout) ?> <?php echo esc_attr($isotope_item_categories) ?>">

            <div class="item portfolio-item">
              <a href="<?php the_permalink(); ?>" <?php echo ($elise_options['opt-portfolio1-gradient'] == 1) ? 'class="gradient"' : '' ?> >
                <?php

                if (has_post_thumbnail()) { 
                  $thumb = get_post_thumbnail_id();
                  $img_url = wp_get_attachment_url( $thumb, 'full' ); //get full URL to image (use "large" or "medium" if the images too big)
                  $image = aq_resize( $img_url, $image_w, $image_h, true, false, true ); //resize & crop the image
                  
                  if($image) {
                    echo '<img class="project-thmb" src="'. esc_url($image[0]) .'" width="'. esc_attr($image[1]) .'" height="'. esc_attr($image[2]) .'" alt="">';
                  }

                } else {
                   echo '<img src="'.get_template_directory_uri().'/img/blank/blank-'.$ratio.'.png" alt="'. __('No thumbnail yet.', 'Elise') .'" />';
                }

                ?>
                <figure>
                  
                  <?php if ($elise_options['opt-portfolio-style'] == 1 && $elise_options['opt-porfolio-item-categories'] == false ) {}
                  else { ?>
                  <small><?php
                    if (!empty($terms)) {
                      foreach ( $terms as $term ) {
                        echo '<span class="portfolio-cat">'. esc_html($term->name) . '</span>';
                      }
                    } 
                    elseif ($elise_options['opt-portfolio-style'] == 2) {
                      _e('Uncategorized', 'Elise');
                    }
                    ?></small>
                  <?php } ?>
                  
                  <h3><?php the_title(); ?></h3>
                </figure>
                <div class="overlay">
                  <span class="see-more"><?php _e("See more", "Elise") ?></span>
                </div>
              </a>
              <?php 
              $project_gallery = get_post_meta( $post->ID, 'opt-meta-project-gallery', true );

              if ($show_quick_view == 'show' && (has_post_thumbnail() || !empty($project_gallery))) {

                $content_width = intval($elise_options['opt-content-width']);

                if ($elise_options['opt-project-layout'] == 1 || $elise_options['opt-project-layout'] == 4 ) {
                  $image_w = round($content_width - 30, 0);
                  $image_h = '';
                } 
                elseif ($elise_options['opt-project-layout'] == 2) {
                  $image_w = round($content_width * .75 - 45, 0);
                  $image_h = '';
                } 
                elseif ($elise_options['opt-project-layout'] == 3) {
                  $image_w = round($content_width - 30, 0);
                  $image_h = intval($elise_options['opt-gallery-wide-height']);
                } 
                // redux_post_meta( "elise_options", $post->ID, "opt-meta-project-gallery" );
                $project_attachments = explode(',', $project_gallery);
                $project_attachment = wp_prepare_attachment_for_js( $project_attachments[0] );

                if ($project_gallery) {
                  if ($project_attachment['caption'] == '[video]') {
                    $project_src = $project_attachment['alt'];
                    $project_class = 'mfp-iframe';
                  }
                  else {
                    //$project_src = $project_attachment['url'];                
                    $img_url = wp_get_attachment_url($project_attachments[0], 'full');
                    $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );

                    //$get_project_src = wp_get_attachment_image_src( $project_attachments[0], $quick_view_img_size );
                    $project_src = $image[0];
                    $project_class = 'mfp-image';
                  }
                } 
                else {        
                  $thumb = get_post_thumbnail_id();
                  $img_url = wp_get_attachment_url( $thumb, 'full' ); //get full URL to image (use "large" or "medium" if the images too big)
                  $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );
                  $project_src = $image[0];
                  $project_class = 'mfp-image';
                }

              ?>

              <a href="<?php echo esc_url($project_src) ?>" class="<?php echo esc_attr($project_class) ?> quick-view" data-effect="mfp-zoom-in" title="<?php the_title() ?>"><?php _e("Quick view", "Elise") ?></a>

              <?php } ?>
            </div>
          </div><!-- Portfolio item END -->
            
         <?php endwhile; 
          wp_reset_postdata();
         else : ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('no-posts'); ?>>

          <h2><?php _e('No projects were found.', 'Elise'); ?></h2>
        
        </article>

        <?php endif; ?>

        
      </div>

    </div><!-- Portfolio items container END-->

    <?php
    if ( function_exists('elise_pagination') ) {
      elise_pagination($p_query);
    }
    ?>

</div>