<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class'  => '',
    'title' 	=> '',
    'date'		=> '',
    'tb_bg' => '',
    // 'tb_bcolor' => '',
    'tb_title_color' => '',
), $atts));


if (!empty($tb_bg) ) {
	$border_bg_style = 'background: '. esc_attr($tb_bg) .'; ';
	$arrow_style = 'color: '. esc_attr($tb_bg) .'; ';
} else {
	$border_bg_style = '';
	$arrow_style = '';
}

// if (!empty($tb_bcolor) ) {
// 	// $border_color_style = 'border: 1px solid '. $tb_bcolor .'; ';
// 	$arrow_style = 'color: '. $tb_bcolor .'; ';
// } else {
// 	// $border_color_style = '';
// 	$arrow_style = '';
// }

?>
<div class="elise_timeline_block_wrap tl_block_left <?php echo esc_attr($el_class) ?>">
<span class="tl-arrow" style="<?php echo $arrow_style ?>"></span>
	<div class="elise_timeline_block" style="<?php echo $border_bg_style ?>">
		<?php 
			if (!empty($title) || !empty($date)) {
				echo '<h4 class="timeline_block_title" '. (!empty($tb_title_color) ? 'style="color:'. esc_attr($tb_title_color) .';"' : '') .' >';
				if (!empty($title)) {
					echo '<span class="tl-title">';
					echo esc_html($title);
					echo '</span>';
				}
				if (!empty($date)) {
					echo '<span class="tl-date">';
					echo esc_html($date);
					echo '</span>';
				}
				echo '</h4>';
			}
		?>
		<?php 
			if (!empty($content)) { ?>
				<div class="elise_timeline_content">
					<p>
					<?php echo wpb_js_remove_wpautop($content); ?>
					</p>
				</div>
			<?php }
		?>
	</div>
</div>