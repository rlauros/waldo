<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'avatar' => '',
    'author' => '',
    'function' => '',
    'testi_style' => 'standard',
    'author_color' => '',
    'border_color' => '',
    'border_bg' => '',
    'css_animation' => '',
), $atts));

if (empty($avatar) && !empty($author) || !empty($avatar) && empty($author)) {
	$footer_class = 'class="f_full"';
	$avatar_size = 'full';
}
elseif (!empty($avatar) && !empty($author)) {
	$footer_class = '';
	$avatar_size = 'thumbnail';
}

if ($testi_style == 'standard') {
	$testi_style_class = 'blockquote-standard';
	$border_bg_style = '';

	if (!empty($border_color)) {
		$border_color_style = 'style="border-top: 1px solid '. $border_color .'; color:'. $border_color .'"';
	} else {
		$border_color_style = '';
	}
} 
elseif ($testi_style == 'bordered') {
	$testi_style_class = 'blockquote-bordered';
	// $border_color_style = '';

	if (!empty($border_bg)) {
		$border_bg_style = 'background:'. $border_bg .';';
		$border_color_style = 'style="color:'. $border_bg .'"';
	} else {
		$border_bg_style = '';
		$border_color_style = '';
	}

	// if (!empty($border_color)) {
	// 	$border_color_style = 'style="color:'. $border_color .'"';
	// 	// $border_bg_style .= 'border: 1px solid '. $border_color .';';
	// } else {
	// 	$border_color_style = '';
	// }
}

if (!empty($author_color)) {
	$author_color_style = 'style="color:'. $author_color .'" ';
} else {
	$author_color_style = '';
}

?>
<div class="elise-shortcode-testimonial <?php echo $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>">

	<blockquote class="<?php echo esc_attr($testi_style_class) ?>">
		<p style="<?php echo esc_attr($border_bg_style) ?>">
			<?php echo wpb_js_remove_wpautop($content); ?>
		</p>
		<footer <?php echo $footer_class ?> <?php echo $border_color_style ?>>
			<?php 
				if (!empty($avatar)) { ?>
					<div class="avatar"><?php echo wp_get_attachment_image($avatar, $avatar_size); ?></div>
				<?php }
			?>

			<?php 
				if (!empty($author)) { ?>
					<div class="author" <?php echo $author_color_style; ?>>
						<?php echo esc_html($author) ?>
						<?php 
							if (!empty($function)) { ?>
								<small <?php echo $author_color_style; ?>><?php echo esc_html($function); ?></small>
							<?php }
						?>
						
					</div>
				<?php }
			?>
			
		</footer>
	</blockquote>
</div>