<?php
global $elise_options;

$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $bg_style = $bg_overlay = $bg_overlay_color = $font_color = $padding = $margin_bottom = $css = '';
extract(shortcode_atts(array(
    'el_class'        => '',
    'bg_image'        => '',
    'bg_color'        => '',
    'bg_image_repeat' => '',
    'bg_style'	    => 'standard',
    'background_position' => '',
    'bg_attachment_fixed'      => '',
    'bg_video_source' => '',
    'bg_overlay'      => '',
    'bg_overlay_color'      => '',
    'bg_shadow'      => '',
    'row_height'     => '',
    'vertical_align' => '',
    'font_color'      => '',
    'padding'         => '',
    'margin_bottom'   => '',
    'textarea_video_urls'      => '',
    'css' => '',
    'full_width_content' => 'standard',
), $atts));

// wp_enqueue_style( 'js_composer_front' );
wp_enqueue_script( 'wpb_composer_front_js' );
// wp_enqueue_style('js_composer_custom_css');

$el_class = $this->getExtraClass($el_class);

if ($this->settings('base') != 'vc_row_inner' ) {
    if ($bg_style == 'standard') {
        if ($bg_attachment_fixed == 'bg-fixed' ) {
            $bg_style_class = 'vc_bg_standard vc_bg_fixed ';
        } else {
            $bg_style_class = 'vc_bg_standard ';
        }
    }
    elseif ($bg_style == 'parallax') {
        $bg_style_class = 'vc_bg_has_parallax ';
    }
    elseif ($bg_style == 'video') {
        $bg_style_class = 'vc_bg_has_video ';
    }

    if ($bg_overlay == 'show-overlay' ) {
        $bg_overlay_class = 'vc_bg_has_overlay ';
    } else {
        $bg_overlay_class = '';
    }

    if ($bg_shadow == 'bg-top-shadow' ) {
        $bg_shadow_class = 'vc_bg_shadow ';
    } else {
        $bg_shadow_class = '';
    }

} else {
    $bg_style_class = '';
    $bg_overlay_class = '';
    $bg_shadow_class = '';
    $row_height_class = '';
    $min_height_style = '';
    $vertical_align_class = '';
}


if (!empty($row_height)) {

    if (is_numeric($row_height)) {
        $row_height_class = 'vc_row_custom_height ';   
        $row_bg_pos_attr = (!empty($background_position)) ? ' background-position:'. esc_attr($background_position) .' !important; ' : '';
        $min_height_style = 'style="height: '. intval($row_height) .'px; '. $row_bg_pos_attr .'" data-basic-height="'. intval($row_height) .'"';
    } 
    elseif ($row_height == 'window') {
        $row_height_class = 'vc_window_height ';
        $min_height_style = '';
    }

    if ($vertical_align == 'vertical') {
        $vertical_align_class = 'vc_row_vertical_align ';
    } else {
        $vertical_align_class = '';
    }
} else {
    $row_height_class = '';
    $min_height_style = '';
    $vertical_align_class = '';
}

$fwc_container_class = 'container ';
$fwc_content_class = 'vc_row_stcontent ';
// if ( is_page_template( 'template-full-width.php') ) {

// }
if ($full_width_content == 'full-width') {
    $fwc_container_class = 'full-width-container ';
    $fwc_content_class = 'vc_row_fullwidthcontent ';
}

$row_bg_pos = (!empty($background_position)) ? ' style="background-position:'. esc_attr($background_position) .' !important" ' : '';
$row_parallax_bg_pos = (!empty($background_position)) ? ' background-position:'. esc_attr($background_position) .' !important; ' : '';



$css_get_url =  strpos($css, 'url');
// if ($css_get_url !== false ) {
//     $css_cut_from_url = substr($css, $css_get_url);
//     $css_bg_values = explode(';', $css_cut_from_url);
// }

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. $fwc_content_class . $bg_style_class . $bg_overlay_class . $bg_shadow_class . $row_height_class . $vertical_align_class .''. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner rsContent ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$parallax_css = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_bg_parallax '. $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );


$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);

$output .= '<div class="'.$css_class.'"'.$style.' '. $min_height_style . $row_bg_pos. ' >';
if (is_page_template( 'template-full-width.php') || vc_is_frontend_editor() || (is_singular('portfolio') && $elise_options['opt-project-layout'] == 4)) {
    if ($this->settings('base') != 'vc_row_inner') {

        if ($bg_style == 'parallax' && $css_get_url !== false) {
            $output .= '<div class="vc_bg_parallax_wrap"><div style="border: 0 !important; padding: 0 !important; margin: 0 !important; '. $row_parallax_bg_pos .' " class="'. $parallax_css .'" data-bottom-top="transform: translate3d(0px, -25%, 0px);" data-top-bottom="transform: translate3d(0px, 0%, 0px);"></div></div>';
        }

        if ($bg_style == 'video') {
            $bg_video_ids = explode(',' , $textarea_video_urls);

            if ($bg_video_ids) {
                $output .= '<div class="bg_video_wrap"><video class="vc_bg_video" muted loop autoplay>';
                foreach ($bg_video_ids as $bg_video_id) {
                    // $video_url =  wp_get_attachment_url($bg_video_id);
                    $filetype_ext = wp_check_filetype($bg_video_id);

                    if (!empty($filetype_ext['ext'])) {
                        $output .= '<source type="'. esc_attr($filetype_ext['type']) .'" src="'. esc_url($bg_video_id) .'">';
                    }
                }
                $output .= '</video></div>';
            }

        }

        if ($bg_overlay == 'show-overlay' ) {
            $output .= '<div class="vc_bg_overlay" style="background:'. esc_attr($bg_overlay_color) .'"></div>';
        }

        $output .= '<div class="vc_container_inner '. esc_attr($fwc_container_class) .'"><div class="row">';
    }
}
$output .= wpb_js_remove_wpautop($content);
if (is_page_template( 'template-full-width.php') || vc_is_frontend_editor() || (is_singular('portfolio') && $elise_options['opt-project-layout'] == 4)) {
    if ($this->settings('base') != 'vc_row_inner') {
        $output .= '</div></div>';
    }
}
$output .= '</div>'.$this->endBlockComment('row');

echo $output;