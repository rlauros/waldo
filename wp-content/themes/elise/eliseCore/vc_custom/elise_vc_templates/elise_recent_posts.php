<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'categories' => '',
    'rp_style' => 'grid',
    'columns' => 4,
    'posts_number' => 4,
    'bg_color' => '',
    'text_color' => '',
    'carousel_arrows_pos' => 'sides',
    'css_animation' => '',
), $atts));

if ($columns == 2) {
	$column_class = 'col-md-6';
}
elseif ($columns == 3) {
	$column_class = 'col-md-4';
}
elseif ($columns == 4) {
	$column_class = 'col-md-3';
}
elseif ($columns == 5) {
	$column_class = 'col-md-25';
}

if ($rp_style == 'grid') {
	$rp_style_class = 'recentposts-grid ';
}
elseif ($rp_style == 'carousel') {
	if ($carousel_arrows_pos == 'sides') {
		$arrow_pos_class = 'rsNavOuter ';
	} 
	elseif ($carousel_arrows_pos == 'top' ) {
		$arrow_pos_class = 'rsNavTop ';
	}
	$rp_style_class = 'recentposts-carousel content-carousel royalSlider rsElise '. esc_attr($arrow_pos_class) .' ';
}


?>
<div class="elise-recentposts <?php echo esc_attr($rp_style_class) . $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>" data-carousel-nav="bullets" data-autoplay="0">
<div class="row recentposts-row rsContent">
<?php         
$r = new WP_Query( apply_filters( 'elise_blog_shortcode', array(
	'posts_per_page'      => intval($posts_number),
	'no_found_rows'       => true,
	'post_status'         => 'publish',
	'category_name'		  => sanitize_text_field($categories),
	'ignore_sticky_posts' => true
) ) );
$i = 1;
if ($r->have_posts()) : while ( $r->have_posts() ) : $r->the_post();

  ?>
  <div class="<?php echo esc_attr($column_class) ?>">  		
	  	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  			<a href="<?php the_permalink(); ?>" class="recentposts-a">
  			<div class="recentposts-post" <?php echo (!empty($bg_color) ? 'style="background:'. esc_attr($bg_color) .'"' : '') ?>>
	  		<div class="recentpost-date">
	  			<small <?php echo (!empty($text_color) ? 'style="color:'. esc_attr($text_color) .'"' : '') ?>><?php the_time(get_option('date_format')); ?></small>
	  		</div>
	  		<div class="recentpost-title">
	 			<h3 <?php echo (!empty($text_color) ? 'style="color:'. esc_attr($text_color) .'"' : '') ?>><?php the_title(); ?></h3>
	  		</div>
	 		<div class="recentpost-readmore">
	 			<span class="btn btn-link btn-sm" <?php echo (!empty($text_color) ? 'style="color:'. $text_color .'"' : '') ?>><?php _e('Read more', 'Elise') ?></span>
	 		</div>	
 			</div>
 			</a>
	 	</article>
  </div>

<?php if($i % $columns == 0 && $i != $posts_number  ) : ?>
	</div>
	<div class="row recentposts-row">
<?php endif; $i++; ?>   
<?php endwhile;
	wp_reset_query();
 else : ?>

<div class="col-md-12">
	<article id="post-<?php the_ID(); ?>" <?php post_class('no-posts'); ?>>

	  <h2><?php _e('No posts were found.', 'Elise'); ?></h2>

	</article>
</div>

<?php endif; ?>
</div>
</div>