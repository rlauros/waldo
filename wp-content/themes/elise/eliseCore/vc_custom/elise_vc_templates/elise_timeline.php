<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'timeline_style' => 'center',
    'css_animation' => '',
), $atts));

// if(vc_is_frontend_editor()) {
// 	$tl_hidd = ' timeline-visible';
// } else {
// 	$tl_hidd = ' timeline-hidden';
// }

$timeline_class = '';
if ($timeline_style == 'left') {
	$timeline_class = 'timeline-left ';
}
elseif ($timeline_style == 'center') {
	$timeline_class = 'timeline-center timeline-hidden ';
}
elseif ($timeline_style == 'right') {
	$timeline_class = 'timeline-right ';
}

?>
<div class="elise_timeline_wrapper <?php echo esc_attr($timeline_class) . $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>">
	<div class="timeline_line"></div>
	<div class="timeline-stamp"></div>
	<?php echo wpb_js_remove_wpautop($content); //Parse inner shortcodes ?>
</div> <!-- Timeline End -->