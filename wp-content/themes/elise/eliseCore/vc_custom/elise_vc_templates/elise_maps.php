<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'address' => '1111 5th Avenue, New York',
    'zoom' => '12',
    'map_type' => 'ROADMAP',
    'marker' => '',
    'style' => '',
    'height' => '',
    'css_animation' => '',
), $atts));

if (!empty($style)) {
	$style_code = rawurldecode(base64_decode(strip_tags($style)));
} else {
	$style_code = '""';
}

$height_style = '';
if (!empty($height)) {
	$height_style = 'style="height:'. esc_attr($height) .'px"';
}

wp_enqueue_script( 'google-maps' );

$map_id = rand(0, 9999);
?>

<div id="map-canvas-<?php echo esc_attr($map_id) ?>" class="elise-google-maps <?php echo $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>" <?php echo $height_style ?>></div> 
<script type="text/javascript">
function map_<?php echo $map_id ?>() {
	var geocoder;
	var map;
	var address = "<?php echo esc_js($address) ?>";
	var customStyle = <?php echo $style_code ?>;
	geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
	var mapOptions = {
		zoom: <?php echo esc_js($zoom) ?>,
		center: latlng,
		scrollwheel: false,
		mapTypeControl: true,
		streetViewControl: false,
		panControl: false,
		zoomControl: true,
		zoomControlOptions: {
		    style: google.maps.ZoomControlStyle.SMALL
		},
        mapTypeId: google.maps.MapTypeId.<?php echo esc_js($map_type) ?>,
		styles: customStyle,
		};
	var map = new google.maps.Map(document.getElementById('map-canvas-<?php echo esc_js($map_id) ?>'),
	  mapOptions);
		<?php 
			if (!empty($marker)) { 
				$marker_src = wp_get_attachment_image_src( $marker, 'full' );
				?>
				var customMarker = {
					url: '<?php echo esc_url($marker_src[0]) ?>',
				};
			<?php } ?>
	if (geocoder) {
	  geocoder.geocode( { 'address': address}, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
	      map.setCenter(results[0].geometry.location);

	        var infowindow = new google.maps.InfoWindow(
	            { content: '<b>'+address+'</b>',
	              size: new google.maps.Size(150,50)
	            });

	        var marker = new google.maps.Marker({
	            position: results[0].geometry.location,
	            map: map, 
	            title:address,
				<?php echo (!empty($marker)) ? 'icon: customMarker,' : '' ?>
	        }); 
	        google.maps.event.addListener(marker, 'click', function() {
	            infowindow.open(map,marker);
	        });

	      } else {
	        alert("No results found");
	      }
	    } else {
	      alert("Geocode was not successful for the following reason: " + status);
	    }
	  });
	}		
}
google.maps.event.addDomListener(window, 'load', map_<?php echo esc_js($map_id) ?>);
</script>