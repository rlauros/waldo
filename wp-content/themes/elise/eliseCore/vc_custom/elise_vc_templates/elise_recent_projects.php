<?php
global $elise_options, $post;
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'rp_columns' => 3,
    'rp_items' => 6,
    'rp_style' => 'carousel',
    'portfolio_item_style' => 'portfolio-style-overlay',
    'carousel_arrows_pos' => 'sides',
    'portfolio_cats' => '',
    'items_gap' => 30,
    'css_animation' => '',
), $atts));
?>


<?php 
  $content_width = $elise_options['opt-content-width'];

  if ($rp_columns == 1) { // 1 column
    $portfolio_layout = 'col-md-12';
    $item_thumbnail_layout = 'portfolio-1col';
  }
  elseif ($rp_columns == 2) { // 2 columns
    $portfolio_layout = 'col-sm-6';
    $item_thumbnail_layout = 'portfolio-2col';
  }
  elseif ($rp_columns == 3) { // 3 columns
    $portfolio_layout = 'col-md-4 col-xs-6';
    $item_thumbnail_layout = 'portfolio-3col';
  }
  elseif ($rp_columns == 4) { // 4 columns
    $portfolio_layout = 'col-md-3 col-xs-4';
    $item_thumbnail_layout = 'portfolio-4col';
  }
  elseif ($rp_columns == 5) { // 4 columns
    $portfolio_layout = 'col-md-25 col-xs-4';
    $item_thumbnail_layout = 'portfolio-5col';
  }
  // elseif ($elise_options['opt-portfolio-layout'] == 5) { // masonry
  //   $item_thumbnail_layout = 'portfolio-masonry pm-hidden';
  // }

  // if ($elise_options['opt-portfolio-style'] == 1) {
  //   $portfolio_item_style = 'portfolio-style-overlay';
  // }
  // elseif ($elise_options['opt-portfolio-style'] == 2) {
  //   $portfolio_item_style = 'portfolio-style-bottom';
  // }



if ($rp_style == 'standard') {
	$rp_style_class = 'recentprojects-grid ';
}
elseif ($rp_style == 'carousel') {
	if ($carousel_arrows_pos == 'sides') {
		$arrow_pos_class = 'rsNavOuter ';
	} 
	elseif ($carousel_arrows_pos == 'top' ) {
		$arrow_pos_class = 'rsNavTop ';
	}
	$rp_style_class = 'recentprojects-carousel royalSlider rsElise '. $arrow_pos_class .' ';
}
?>

<?php 
  if ($items_gap != 30) {
    $gap_row_style = 'style="margin-left: '. - intval($items_gap) / 2 .'px;margin-right: '. - intval($items_gap) / 2 .'px;"';
    $gap_row_col_style = 'style="padding-left: '. intval($items_gap) / 2 .'px;padding-right: '. intval($items_gap) / 2 .'px;margin-bottom: '. intval($items_gap) / 2 .'px;margin-top: '. intval($items_gap) / 2 .'px;"';
  } else {
    $gap_row_style = '';
    $gap_row_col_style = '';
  }
?>

<div class="elise_shortcode_recent_projects <?php echo $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>">
	
    <!-- Portfolio items container -->
    <div class="portfolio-recent-posts--container <?php echo esc_attr($rp_style_class) . esc_attr($portfolio_item_style); ?> <?php echo esc_attr($item_thumbnail_layout) ?>" data-items-gap="<?php echo intval($items_gap) ?>">

      <div class="row recentprojects-row rsContent" <?php echo $gap_row_style ?>>

        <?php

         // if (empty($selected_categories)) {
         //   $terms_ids = get_terms( 'portfolio_category' ); 
         //   $selected_categories = wp_list_pluck( $terms_ids, 'term_id' );
         // }
        // if ($pf_items == -1) {
        //   $no_found_rows = false;

        //   if ($elise_options['opt-portfolio-pagination'] == 1 ) {
        //    $projects_per_page = $elise_options['opt-portfolio-pper-page'];
        //   } else {
        //    $projects_per_page = -1;
        //   }

        // } else {
        // }


		if (!empty($portfolio_cats)) {
			$selected_categories = explode(',', $portfolio_cats);
		} else {
			$selected_categories = '';
		}

        if ($rp_style == 'standard') {
			$projects_per_page = intval($rp_columns);
			$no_found_rows = true;
        }
        elseif ($rp_style == 'carousel') {
			$projects_per_page = intval($rp_items);
			$no_found_rows = true;
        }

        $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );
        $portfolio = array(
          'posts_per_page' => intval($projects_per_page),
          'post_type' => 'portfolio',
          'no_found_rows'   => $no_found_rows,
          'paged'=> $paged
        );

        if(!empty($selected_categories)) {
          $portfolio['tax_query'] = array(
            array(
              'taxonomy' => 'portfolio_category',
              'field' => 'slug',
              'terms' => sanitize_text_field($selected_categories)
            )
          );
  
        }

        $rp_query = new WP_Query($portfolio);
		$i = 1;

        if( $rp_query->have_posts()) : while( $rp_query->have_posts()) :  $rp_query->the_post(); ?>

            <?php 
              $content_width = $elise_options['opt-content-width'];

              if ($rp_columns == 1) { // 1 column
                $image_w = round($content_width - 30, 0);
                $image_h = round(($content_width - 30) / 3, 0);
                $ratio = '3x1';
              }
              elseif ($rp_columns == 2) { // 2 columns
                $image_w = round(($content_width - 30) / 2, 0);
                $image_h = round((($content_width - 30) / 2) / 1.5, 0);
                $ratio = '3x2';
              }
              elseif ($rp_columns == 3) { // 3 columns
                $image_w = round(($content_width - 30) / 3, 0);
                $image_h = round((($content_width - 30) / 3) / 1.333333333, 0);
                $ratio = '4x3';
              }
              elseif ($rp_columns == 4 || $rp_columns == 5 ) { // 4 columns
                $image_w = round(($content_width - 30) / 4, 0);
                $image_h = round(($content_width - 30) / 4, 0);
                $ratio = '1x1';
              }
            ?>

            <?php
              $terms = get_the_terms( $post->ID , 'portfolio_category' );

              $isotope_item_categories = '';
              if (!empty($terms)) {

                foreach ( $terms as $term ) {
                  $isotope_item_categories .= $term->slug .' ';
                }

              }
            ?>

          <!-- Portfolio item -->
          <div class="<?php echo esc_attr($portfolio_layout) ?> <?php echo esc_attr($isotope_item_categories) ?>" <?php echo $gap_row_col_style ?>>

            <div class="item portfolio-item">
              <a href="<?php the_permalink(); ?>" <?php echo ($elise_options['opt-portfolio1-gradient'] == 1) ? 'class="gradient"' : '' ?> >
                <?php

                if (has_post_thumbnail()) { 
                  $thumb = get_post_thumbnail_id();
                  $img_url = wp_get_attachment_url( $thumb, 'full' ); //get full URL to image (use "large" or "medium" if the images too big)
                  $image = aq_resize( $img_url, $image_w, $image_h, true, false, true ); //resize & crop the image
                  
                  if($image) {
                    echo '<img class="project-thmb" src="'. esc_url($image[0]) .'" width="'. esc_attr($image[1]) .'" height="'. esc_attr($image[2]) .'" alt="">';
                  }
                } else {
                   echo '<img src="'.get_template_directory_uri().'/img/blank/blank-'.$ratio.'.png" alt="'. __('No thumbnail yet.', 'Elise') .'" />';
                } 

                ?>
                <figure>
                  
                  <?php if ($elise_options['opt-portfolio-style'] == 1 && $elise_options['opt-porfolio-item-categories'] == false ) {}
                  else { ?>
                  <small><?php
                    if (!empty($terms)) {
                      foreach ( $terms as $term ) {
                        echo '<span class="portfolio-cat">'. $term->name . '</span>';
                      }
                    } 
                    elseif ($elise_options['opt-portfolio-style'] == 2) {
                      _e('Uncategorized', 'Elise');
                    }
                    ?></small>
                  <?php } ?>
                  
                  <h3><?php the_title(); ?></h3>
                </figure>
                <div class="overlay">
                  <span class="see-more"><?php _e("See more", "Elise") ?></span>
                </div>
              </a>
              <?php 
              $project_gallery = get_post_meta( $post->ID, 'opt-meta-project-gallery', true );

              if ($elise_options['opt-quick-view'] == 1 && (has_post_thumbnail() || !empty($project_gallery))) {

                $content_width = $elise_options['opt-content-width'];

                if ($elise_options['opt-project-layout'] == 1 || $elise_options['opt-project-layout'] == 4 ) {
                  $image_w = round($content_width - 30, 0);
                  $image_h = '';
                } 
                elseif ($elise_options['opt-project-layout'] == 2) {
                  $image_w = round($content_width * .75 - 45, 0);
                  $image_h = '';
                } 
                elseif ($elise_options['opt-project-layout'] == 3) {
                  $image_w = round($content_width - 30, 0);
                  $image_h = intval($elise_options['opt-gallery-wide-height']);
                } 
                // redux_post_meta( "elise_options", $post->ID, "opt-meta-project-gallery" );
                $project_attachments = explode(',', $project_gallery);
                $project_attachment = wp_prepare_attachment_for_js( $project_attachments[0] );

                if ($project_gallery) {
                  if ($project_attachment['caption'] == '[video]') {
                    $project_src = $project_attachment['alt'];
                    $project_class = 'mfp-iframe';
                  }
                  else {
                    //$project_src = $project_attachment['url'];                
                    $img_url = wp_get_attachment_url($project_attachments[0], 'full');
                    $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );

                    //$get_project_src = wp_get_attachment_image_src( $project_attachments[0], $quick_view_img_size );
                    $project_src = $image[0];
                    $project_class = 'mfp-image';
                  }
                } 
                else {        
                  $thumb = get_post_thumbnail_id();
                  $img_url = wp_get_attachment_url( $thumb, 'full' ); //get full URL to image (use "large" or "medium" if the images too big)
                  $image = aq_resize( $img_url, $image_w, $image_h, true, false, true );
                  $project_src = $image[0];
                  $project_class = 'mfp-image';
                }

              ?>

              <a href="<?php echo esc_url($project_src) ?>" class="<?php echo esc_attr($project_class) ?> quick-view" data-effect="mfp-zoom-in" title="<?php the_title() ?>"><?php _e("Quick view", "Elise") ?></a>

              <?php } ?>
            </div>
          </div><!-- Portfolio item END -->

         
		<?php
		if ($rp_style == 'carousel') {
		 if($i % $rp_columns == 0 && $i != $rp_items ) : ?>
			</div>
			<div class="row recentprojects-row" <?php echo $gap_row_style ?>>
		<?php endif; $i++; 
     	} ?> 
            
         <?php endwhile; 
          wp_reset_postdata();
         else : ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('no-posts'); ?>>

          <h2><?php _e('No projects were found.', 'Elise'); ?></h2>
        
        </article>

        <?php endif; ?>

        
      </div>

    </div><!-- Portfolio items container END-->
</div>