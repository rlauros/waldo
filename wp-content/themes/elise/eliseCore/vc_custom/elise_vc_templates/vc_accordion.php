<?php
wp_enqueue_script('jquery-ui-accordion');
$output = $title = $interval = $el_class = $collapsible = $active_tab = '';
//
extract(shortcode_atts(array(
    'title' => '',
    'interval' => 0,
    'el_class' => '',
    'collapsible' => 'no',
    'active_tab' => '1',
    'acc_bg' => '',
    'acc_color' => '',
    'disable_keyboard' => 'no',
    'css_animation' => '',
), $atts));

if (!empty($acc_bg) || !empty($acc_color)) {
	$random_class = 'accordion-style-'.rand(0, 9999);

	$output .= '<style type="text/css" scoped>';
	if (!empty($acc_bg)) {
		$output .= '.'.$random_class.' .wpb_accordion_section {background:'. esc_attr($acc_bg) .';}';
	}
	if (!empty($acc_color)) {
		$output .= '.'.$random_class.' .wpb_accordion_section h3 a {color:'. esc_attr($acc_color) .';}';
	}
	$output .= '</style>';

} else {
	$tab_bg_style = '';
	$random_class = '';
}

$el_class = $this->getExtraClass($el_class);
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_accordion wpb_content_element '. $random_class . $el_class . ' not-column-inherit', $this->settings['base'], $atts );
$css_class .= $this->getCSSAnimation($css_animation);

$output .= "\n\t".'<div class="'.$css_class.'" data-collapsible="'.$collapsible.'" data-vc-disable-keydown="' . ( esc_attr( ( 'yes' == $disable_keyboard ? 'true' : 'false' ) ) ) . '" data-active-tab="'.$active_tab.'">'; //data-interval="'.$interval.'"
$output .= "\n\t\t".'<div class="wpb_wrapper elise-accordion wpb_accordion_wrapper ui-accordion">';
$output .= wpb_widget_title(array('title' => $title, 'extraclass' => 'wpb_accordion_heading'));

$output .= "\n\t\t\t".wpb_js_remove_wpautop($content);
$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= "\n\t".'</div> '.$this->endBlockComment('.wpb_accordion');

echo $output;