<?php
// extract(shortcode_atts(array(
//     'color' => 'wpb_button',
//     'icon' => 'none',
//     'size' => '',
//     'target' => '',
//     'href' => '',
//     'title' => __('Text on the button', "js_composer"),
//     'call_text' => '',
//     'position' => 'cta_align_right',
//     'el_class' => ''
// ), $atts));
extract(shortcode_atts(array(
    // 'h2' => '',
    // 'h4' => '',
    'position' => '',
    'el_width' => '',
    'style' => '',
    'txt_align' => '',
    'accent_color' => '',
    'link' => '',
    'title' => __('Text on the button', "js_composer"),
    'color' => '',
    'size' => '',
    'btn_style' => '',
    'el_class' => '',
    'css_animation' => ''
), $atts));

$class = "elise-call-to-action wpb_content_element";
// $position = 'left';
// $width = '90';
// $style = '';
// $txt_align = 'right';
$link = ($link=='||') ? '' : $link;

$class .= ($position!='') ? ' elise-cta-btn-pos-'.$position : '';
$class .= ($el_width!='') ? ' vc_el_width_'.$el_width : '';
$class .= ($color!='') ? ' vc_cta_'.$color : '';
$class .= ($style!='') ? ' vc_cta_'.$style : '';
$class .= ($txt_align!='') ? ' vc_txt_align_'.$txt_align : '';

$inline_css = ($accent_color!='') ? ' style="'.vc_get_css_color('background-color', $accent_color).'"' : '';

$class .= $this->getExtraClass($el_class);
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class, $this->settings['base'], $atts );
$css_class .= $this->getCSSAnimation($css_animation);
?>
<div<?php echo $inline_css; ?> class="<?php echo esc_attr(trim($css_class)); ?>">
    <?php if ($link!='' && $position!='bottom') echo do_shortcode('[vc_button2 link="'.$link.'" title="'.$title.'" color="'.$color.'" size="'.$size.'" style="'.$btn_style.'" el_class="cta-btn"]'); ?>
    <?php 
    echo '<div class="cta-content">';
    echo wpb_js_remove_wpautop($content, true); 
    echo '</div>';
    ?>
    <?php if ($link!='' && $position=='bottom') echo do_shortcode('[vc_button2 link="'.$link.'" title="'.$title.'" color="'.$color.'" size="'.$size.'" style="'.$btn_style.'" el_class="cta-btn"]'); ?>
</div>
<?php $this->endBlockComment('.vc_call_to_action') . "\n";