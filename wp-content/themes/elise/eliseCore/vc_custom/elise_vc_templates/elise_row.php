<?php
$output = $el_class = $bg_image = $bg_color = $bg_image_repeat = $bg_style = $bg_overlay = $bg_overlay_color = $font_color = $padding = $margin_bottom = $css = '';
extract(shortcode_atts(array(
    'el_class'        => '',
    'bg_image'        => '',
    'bg_color'        => '',
    'bg_image_repeat' => '',
    'bg_style'	    => 'standard',
    'bg_attachment_fixed'      => false,
    'bg_video_source' => '',
    'bg_overlay'      => false,
    'bg_overlay_color'      => '',
    'bg_shadow'      => false,
    'row_height'     => '',
    'vertical_align' => false,
    'font_color'      => '',
    'padding'         => '',
    'margin_bottom'   => '',
    'textarea_video_urls'      => '',
    'css' => ''
), $atts));

// wp_enqueue_style( 'js_composer_front' );
wp_enqueue_script( 'wpb_composer_front_js' );
// wp_enqueue_style('js_composer_custom_css');

$el_class = $this->getExtraClass($el_class);

if ($this->settings('base') != 'vc_row_inner' ) {
    if ($bg_style == 'standard') {
        if ($bg_attachment_fixed == true ) {
            $bg_style_class = 'vc_bg_standard vc_bg_fixed ';
        } else {
            $bg_style_class = 'vc_bg_standard ';
        }
    }
    elseif ($bg_style == 'parallax') {
        $bg_style_class = 'vc_bg_has_parallax ';
    }
    elseif ($bg_style == 'video') {
        $bg_style_class = 'vc_bg_has_video ';
    }

    if ($bg_overlay == true ) {
        $bg_overlay_class = 'vc_bg_has_overlay ';
    } else {
        $bg_overlay_class = '';
    }

    if ($bg_shadow == true ) {
        $bg_shadow_class = 'vc_bg_shadow ';
    } else {
        $bg_shadow_class = '';
    }

    if (!empty($row_height)) {

        if (is_numeric($row_height)) {
            $row_height_class = 'vc_row_custom_height ';
            $min_height_style = 'style="height: '. $row_height .'px " basic-height='. $row_height .'';
        } 
        elseif ($row_height == 'window') {
            $row_height_class = 'vc_window_height ';
            $min_height_style = '';
        }

        if ($vertical_align == true) {
            $vertical_align_class = 'vc_row_vertical_align ';
        } else {
            $vertical_align_class = '';
        }
    } else {
        $row_height_class = '';
        $min_height_style = '';
        $vertical_align_class = '';
    }

} else {
    $bg_style_class = '';
    $bg_overlay_class = '';
    $bg_shadow_class = '';
    $row_height_class = '';
    $min_height_style = '';
    $vertical_align_class = '';
}


$css_get_url =  strpos($css, 'url');
// if ($css_get_url !== false ) {
//     $css_cut_from_url = substr($css, $css_get_url);
//     $css_bg_values = explode(';', $css_cut_from_url);
// }

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_row wpb_row '. $bg_style_class . $bg_overlay_class . $bg_shadow_class . $row_height_class . $vertical_align_class .''. ( $this->settings('base')==='vc_row_inner' ? 'vc_inner ' : '' ) . get_row_css_class() . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$parallax_css = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_bg_parallax '. $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );


$style = $this->buildStyle($bg_image, $bg_color, $bg_image_repeat, $font_color, $padding, $margin_bottom);


$output .= '<div class="'.$css_class.'"'.$style.' '. $min_height_style .' >';
if ($this->settings('base') != 'vc_row_inner') {

    if ($bg_style == 'parallax' && $css_get_url !== false) {
        $output .= '<div class="vc_bg_parallax_wrap"><div style="border: 0 !important; padding: 0 !important; margin: 0 !important" class="'. $parallax_css .'" data-bottom-top="transform: translate3d(0px, -25%, 0px);" data-top-bottom="transform: translate3d(0px, 0%, 0px);"></div></div>';
    }

    if ($bg_style == 'video') {
        $bg_video_ids = explode(',' , $textarea_video_urls);

        if ($bg_video_ids) {
            $output .= '<div class="bg_video_wrap"><video class="vc_bg_video" muted loop autoplay>';
            foreach ($bg_video_ids as $bg_video_id) {
                // $video_url =  wp_get_attachment_url($bg_video_id);
                $filetype_ext = wp_check_filetype($bg_video_id);

                if (!empty($filetype_ext['ext'])) {
                    $output .= '<source type="'. $filetype_ext['type'] .'" src="'. $bg_video_id .'">';
                }
            }
            $output .= '</div></video>';
        }

    }

    if ($bg_overlay == true ) {
        $output .= '<div class="vc_bg_overlay" style="background:'. $bg_overlay_color .'"></div>';
    }

    $output .= '<div class="vc_container_inner container">';
}
$output .= wpb_js_remove_wpautop($content);
if ($this->settings('base') != 'vc_row_inner' ) {
    $output .= '</div>';
}
$output .= '</div>'.$this->endBlockComment('row');

echo $output;