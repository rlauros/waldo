<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'carousel_arrows_pos' => 'top',
    'navigation' => 'bullets',
    'autoplay' => '0',
    'carousel_bg' => '',
    'css_animation' => '',
), $atts));

if ($carousel_arrows_pos == 'sides') {
	$arrow_pos_class = 'rsNavOuter ';
} 
elseif ($carousel_arrows_pos == 'top' ) {
	$arrow_pos_class = 'rsNavTop ';
}
elseif ($carousel_arrows_pos = 'none') {
    $arrow_pos_class = 'rsNoArrows ';
}

$navigation_attr = 'data-carousel-nav="none" ';
if ($navigation == 'bullets') {
	$navigation_attr = 'data-carousel-nav="bullets"';
}

$carousel_custom_bg = '';
if (!empty($carousel_bg)) {
    $carousel_id = rand(0,9999);
    $carousel_custom_bg = 'carousel_custom_'. intval($carousel_id). ' ';

	echo '<style type="text/css" scoped>
		.carousel_custom_'. $carousel_id .' .rsSlide {
			background: '. esc_attr($carousel_bg) .' !important;
		}
        </style>
	';
}

// $has_bullets_class = ($carousel_arrows_pos == 'sides' && $navigation == 'bullets') ? 'rsHasBullets ' : '';

?>


<div class="elise_shortcode_carousel content-carousel royalSlider rsElise <?php echo $carousel_custom_bg . esc_attr($arrow_pos_class) . $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>" <?php echo $navigation_attr ?> data-autoplay="<?php echo intval($autoplay) ?>">
	<?php echo wpb_js_remove_wpautop($content); //Parse inner shortcodes ?>
</div>


