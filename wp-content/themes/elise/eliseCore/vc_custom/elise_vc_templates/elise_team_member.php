<?php
$el_class = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'photo' => '',
    'name' => '',
    'function' => '',
    'name_color' => '',
    'function_color' => '',
    'css_animation' => '',
), $atts));

if (!empty($name_color)) {
	$name_color_style = 'style="color:'. esc_attr($name_color) .'"';
} else {
	$name_color_style = '';
}

if (!empty($function_color)) {
	$function_color_style = 'style="color:'. esc_attr($function_color) .'"';
} else {
	$function_color_style = '';
}


?>
<div class="elise-team-member <?php echo $this->getCSSAnimation($css_animation) .' '. esc_attr($el_class); ?>">
	<?php 
		if (!empty($photo)) { ?>
			<div class="tm-photo">
				<?php echo wp_get_attachment_image($photo, 'full'); ?>
			</div>
		<?php }
	?>
	<?php 
		if (!empty($name)) { ?>
			<header class="tm-name">
				<h3 <?php echo $name_color_style; ?>><?php echo esc_html($name) ?></h3>
				<?php echo (!empty($function)) ? '<small '. $function_color_style .'>'. esc_html($function) .'</small>' : '' ?>
			</header>
		<?php }
	?>
	<?php 
		if (!empty($content)) { ?>
			<div class="tm-desc">
				<p>
					<?php echo wpb_js_remove_wpautop($content); //Parse inner shortcodes ?>
				</p>
			</div>
		<?php }
	?>
</div>