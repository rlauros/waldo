<?php

$output = $el_class = $image = $img_size = $img_link = $img_link_target = $img_link_large = $title = $alignment = $css_animation = $css = '';

extract( shortcode_atts( array(
	'title' => '',
	'image' => $image,
	'img_size' => 'full',
	'img_link_large' => false,
	'img_link' => '',
	'link' => '',
	'img_link_target' => '_self',
	'alignment' => 'left',
	'el_class' => '',
	'css_animation' => '',
	'style' => '',
	'border_color' => '',
	'overlay' => '',
	'drop_shadow' => '',
	// 'caption' => '',
	// 'caption_position' => 'c-bottom-left',
	'css' => ''
), $atts ) );

$style = ( $style != '' ) ? $style : '';
$border_color = ( $border_color != '' ) ? '" style="border: 1px solid ' . $border_color.' ' : '';

$img_id = preg_replace( '/[^\d]/', '', $image );
$img = wpb_getImageBySize( array( 'attach_id' => $img_id, 'thumb_size' => $img_size, 'class' => $style . $border_color) );
if ( $img == NULL ) $img['thumbnail'] = '<img class="' . $style. '" src="' . vc_asset_url( 'vc/no_image.png' ) . '" />'; //' <small>'.__('This is image placeholder, edit your page to replace it.', 'js_composer').'</small>';

$el_class = $this->getExtraClass( $el_class );

// $image_overlay = '';
if ($overlay == 'yes') {
	$style .= ' si_overlay ';
}

$a_class = '';
$mfp_data = '';
if ($img_link_target == '_self' && empty($img_link)) {
	$a_class = ' class="magnpopup '.$style.'"';
	$mfp_data = 'data-effect="mfp-zoom-in" ';
}

$link_to = '';
if ( $img_link_large == true ) {
	$link_to = wp_get_attachment_image_src( $img_id, 'full' );
	$link_to = $link_to[0];
} else if ( strlen($link) > 0 ) {
	$link_to = $link;
	$a_class = ' class="img-link '.$style.'"';
	$mfp_data = '';
} else if ( ! empty( $img_link ) ) {
	$link_to = $img_link;
	$a_class = ' class="img-link '.$style.'"';
	$mfp_data = '';
	if ( ! preg_match( '/^(https?\:\/\/|\/\/)/', $link_to ) ) $link_to = 'http://' . $link_to;
}
//to disable relative links uncomment this..

$drop_shadow_class = '';
if ($drop_shadow == 'yes') {
	$drop_shadow_class = 'single-img_shadow ';
}


$img_output = $img['thumbnail'];
$image_string = ! empty( $link_to ) ? '<a' . $a_class . ' '. $mfp_data .' href="' . esc_url($link_to) . '"' . ' target="' . esc_attr($img_link_target) . '"'. '>' . $img_output . '</a>' : $img_output;
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_single_image wpb_content_element ' . $drop_shadow_class . $el_class . vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$css_class .= $this->getCSSAnimation( $css_animation );

$css_class .= ' vc_align_' . $alignment;

$output .= "\n\t" . '<div class="' . $css_class . '">';
$output .= "\n\t\t" . '<div class="wpb_wrapper single-img-wrapper">';
$output .= "\n\t\t\t" . wpb_widget_title( array( 'title' => $title, 'extraclass' => 'wpb_singleimage_heading' ) );
// if ($style != 'img_rounded' && $style != 'img_rounded-bordered' && !empty($caption)) {
// 	$output .= "\n\t\t\t" . '<div class="img-caption '. $caption_position .'">';
// 	$output .= "\n\t\t\t\t <p>" . wpb_js_remove_wpautop($caption) . '</p>';
// 	$output .= "\n\t\t\t" . '</div>';
// }
$output .= "\n\t\t\t" . $image_string;
$output .= "\n\t\t" . '</div> ' . $this->endBlockComment( '.wpb_wrapper' );
$output .= "\n\t" . '</div> ' . $this->endBlockComment( '.wpb_single_image' );

echo $output;