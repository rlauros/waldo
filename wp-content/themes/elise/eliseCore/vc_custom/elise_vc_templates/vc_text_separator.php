<?php
$el_class = $icon = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypoicons = $icon_linecons = '';
extract(shortcode_atts(array(
    'title' => '',  

    'icon' => '',
    'icon_fontawesome' => '',
    'icon_openiconic' => '',
    'icon_typicons' => '',
    'icon_entypoicons' => '',
    'icon_linecons' => '',
    'icon_entypo' => '',

    'title_align' => '',
    'el_width' => 'wide',
    'sep_position' => 'center',
    'style' => '',
    'color' => '',
    'accent_color' => '',
    'text_color' => '',
    'el_class' => '',
    'back_to_top' => '',
    'margin_bottom' => '',
), $atts));
$class = "elise_separator wpb_content_element";

// $el_width = "50";
//$style = 'double';
//$title = '';
//$color = 'blue';

vc_icon_element_fonts_enqueue( $icon );

$class .= ($title_align!='') ? ' elise_'.$title_align : '';
$class .= ($el_width!='') ? ' elise_el_width_'.$el_width : '';
$class .= ($el_width!='wide') ? ' elise_sep_position_'.$sep_position : '';
$class .= ($style!='') ? ' elise_sep_'.$style : '';
$class .= ($back_to_top!='') ? ' elise_sep_btt' : '';

$inline_css = ( $accent_color!='') ? ' style="'.vc_get_css_color('border-color', $accent_color).'"' : '';
$text_inline_css = ( $text_color!='') ? ' style="'.vc_get_css_color('color', $text_color).'"' : '';
$margin_inline_css = ( $margin_bottom!='') ? ' style="'.vc_get_css_color('margin-bottom', intval($margin_bottom).'px !important').'"' : '';

$class .= $this->getExtraClass($el_class);
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class, $this->settings['base'], $atts );

?>
<div class="<?php echo esc_attr(trim($css_class)); ?>" <?php echo $margin_inline_css; ?>>
    <div class="elise_sep_wrapper">
        <div class="elise_sep_container">
	       <span class="elise_sep_holder elise_sep_holder_l"><span<?php echo $inline_css; ?> class="elise_sep_line"></span></span>
	       <?php if($title!='' || $icon!=''): ?><span class="elise_sep_text" <?php echo $text_inline_css; ?>><?php echo ( $icon!='' ? '<i class="'. esc_attr( ${"icon_" . $icon} ) .'"></i> ' : ''); ?><?php echo ( $title!='' ? '<span class="sep_title">'.esc_html($title).'</span>' : ''); ?></span><?php endif; ?>
	       <span class="elise_sep_holder elise_sep_holder_r"><span<?php echo $inline_css; ?> class="elise_sep_line"></span></span>
        </div>
    </div>
</div>
<?php echo $this->endBlockComment('separator')."\n";