<?php
$el_class = $icon = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypoicons = $icon_linecons = '';
extract(shortcode_atts(array(
    'el_class' => '',
    'title' => '',

    'icon' => '',
    'icon_fontawesome' => '',
    'icon_openiconic' => '',
    'icon_typicons' => '',
    'icon_entypoicons' => '',
    'icon_linecons' => '',
    'icon_entypo' => '',

    'price' => '',
    'billing' => '',
    'features_list' => '',
    'choose_btn' => '',
    'featured' => '',
    'featured_text' => '',
    'header_bg' => '',
    'header_colors' => '',
    // 'price_color' => '',
    'features_bg' => '',
    'features_color' => '',
), $atts));

vc_icon_element_fonts_enqueue( $icon );

if ($featured == 'true') {
	$featured_class = 'pt-featured ';
} else {
	$featured_class = '';
}

if (!empty($header_bg)) {
	$header_bg_style = 'style="background:'. esc_attr($header_bg) .'"';
} else {
	$header_bg_style = '';	
}
if (!empty($header_colors)) {
	$header_colors_style = 'style="color:'. esc_attr($header_colors) .'"';
} else {
	$header_colors_style = '';
}

if (!empty($features_bg)) {
	$features_bg_style = 'style="background:'. esc_attr($features_bg) .'"';
} else {
	$features_bg_style = '';
}
if (!empty($features_color)) {
	$features_colors_style = 'style="color:'. esc_attr($features_color) .'"';
} else {
	$features_colors_style = '';
}



// $values = '5.00 zł';
// preg_match("/([^0-9.,]*)([0-9.,]*)([^0-9.,]*)/", $values, $matches);
// print_r($matches);


?>
<div class="elise-pricing-column <?php echo esc_attr($featured_class) . esc_attr($el_class); ?>" <?php echo $features_bg_style ?>>
	<?php 
		if ($featured == 'true' && !empty($featured_text)) { ?>
			<div class="pt-featured-text">
				<small>
					<?php echo esc_html($featured_text); ?>
				</small>
			</div>
		<?php }
	?>
	<header class="pt-header" <?php echo $header_bg_style ?>>
		<?php 
			if (!empty($icon)) { ?>
				<div class="pt-icon">
					<i class="<?php echo esc_attr( ${"icon_" . $icon} ) ?>"></i>
				</div>
			<?php }
		?>
		<?php 
			if (!empty($title)) { ?>
				<small class="pt-title" <?php echo $header_colors_style ?>>
					<?php echo esc_html($title) ?>
				</small>
			<?php }
		?>
		<?php 
			$value = $price;
			preg_match("/^(\D*)\s*([\d,\.]+)\s*(\D*)$/", $value, $price_c);

			if (!empty($price)) { ?>
				<div class="pt-price" <?php echo $header_colors_style ?>>
					<h2 class="hero" <?php echo $header_colors_style ?>><?php echo (!empty($price_c[1]) ? '<span class="currency-bef">'. esc_html($price_c[1]) .'</span>' : '') ?><?php echo esc_html($price_c[2]); ?><?php echo (!empty($price_c[3]) ? '<span class="currency-aft">'. esc_html($price_c[3]) .'</span>' : '') ?></h2>
					<?php echo (!empty($billing)) ? '<span>'. esc_html($billing) .'</span>' : '' ?>
				</div>
			<?php }
		?>
	</header>
	<div class="pt-features" <?php echo $features_colors_style ?>>
		<?php 
			if (!empty($features_list)) {
				$get_features = explode(',', $features_list);

				echo '<ul>';
				foreach ($get_features as $feature) {
					if (strpos($feature,'|') !== false) {
					    $feature_sep = explode('|', $feature);
					} else {
						$feature_sep[0] = '';
						$feature_sep[1] = $feature;
					}

					echo '<li>'. (!empty($feature_sep[0]) ? '<span class="qty">'. esc_html($feature_sep[0]) .' </span>' : '') .''. esc_html($feature_sep[1]) .'</li>';
				}
				echo '</ul>';
			}
		?>	
	</div>
	<footer>
		<?php 
			$href = vc_build_link($choose_btn);

			if (!empty($href['url'])) { ?>
				<div class="pt-choose">
					<a href="<?php echo esc_url($href['url']) ?>" class="btn btn-default btn-sm" <?php echo (!empty($href['target']) ? 'target='. esc_attr($href['target']) .'' : '') ?>><?php echo esc_html($href['title']) ?></a>
				</div>
			<?php }
		?>
	</footer>
</div>