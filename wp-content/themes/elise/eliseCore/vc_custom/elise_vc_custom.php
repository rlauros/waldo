<?php

$vc_is_wp_version_3_6_more = version_compare( preg_replace( '/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo( 'version' ) ), '3.6' ) >= 0;


// Reorganize Elements --------------------


vc_remove_element("vc_tabs");
vc_remove_element("vc_tab");
vc_remove_element("vc_tour");
vc_remove_element("vc_row");
vc_remove_element("vc_row_inner");
vc_remove_element("vc_accordion");
vc_remove_element("vc_accordion_tab");
vc_remove_element("vc_single_image");
vc_remove_element("vc_gallery");
vc_remove_element("vc_images_carousel");
vc_remove_element("vc_button");
vc_remove_element("vc_button2");
vc_remove_element("vc_basic_grid");
vc_remove_element("vc_carousel");
vc_remove_element("vc_posts_slider");
vc_remove_element("vc_custom_heading");
vc_remove_element("vc_facebook");
vc_remove_element("vc_tweetmeme");
vc_remove_element("vc_googleplus");
vc_remove_element("vc_pinterest");
vc_remove_element("vc_message");
vc_remove_element("vc_cta_button");
vc_remove_element("vc_cta_button2");
vc_remove_element("vc_separator");
vc_remove_element("vc_text_separator");
vc_remove_element("vc_pie");
vc_remove_element("vc_progress_bar");
vc_remove_element("vc_gmaps");
vc_remove_element("vc_masonry_grid");
vc_remove_element("vc_media_grid");
vc_remove_element("vc_masonry_media_grid");
vc_remove_element("vc_icon");







include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); // Require plugin.php to use is_plugin_active() below


// VC MAP ------------------------------- //

$target_arr = array(
    __( 'Same window', 'js_composer' ) => '_self',
    __( 'New window', 'js_composer' ) => "_blank"
);

$add_css_animation = array(
    'type' => 'dropdown',
    'heading' => __( 'CSS Animation', 'js_composer' ),
    'param_name' => 'css_animation',
    'admin_label' => true,
    'value' => array(
        __( 'No', 'js_composer' ) => '',
        __( 'Top to bottom', 'js_composer' ) => 'top-to-bottom',
        __( 'Bottom to top', 'js_composer' ) => 'bottom-to-top',
        __( 'Left to right', 'js_composer' ) => 'left-to-right',
        __( 'Right to left', 'js_composer' ) => 'right-to-left',
        __( 'Appear from center', 'js_composer' ) => "appear"
    ),
    'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'js_composer' )
);


// Row --------------------
vc_map( array(
    'name' => __( 'Row', 'js_composer' ),
    'base' => 'vc_row',
    'weight' => 10,
    'is_container' => true,
    'icon' => 'icon-wpb-row',
    'show_settings_on_create' => false,
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Place content elements inside the row', 'js_composer' ),
    'params' => array(
        // array(
        //     'type' => 'colorpicker',
        //     'heading' => __( 'Font Color', 'js_composer' ),
        //     'param_name' => 'font_color',
        //     'description' => __( 'Select font color', 'js_composer' ),
        //     'edit_field_class' => 'vc_col-md-6 vc_column'
        // ),
        /*
   array(
        'type' => 'colorpicker',
        'heading' => __( 'Custom Background Color', 'js_composer' ),
        'param_name' => 'bg_color',
        'description' => __( 'Select backgound color for your row', 'js_composer' ),
        'edit_field_class' => 'col-md-6'
  ),
  array(
        'type' => 'textfield',
        'heading' => __( 'Padding', 'js_composer' ),
        'param_name' => 'padding',
        'description' => __( 'You can use px, em, %, etc. or enter just number and it will use pixels.', 'js_composer' ),
        'edit_field_class' => 'col-md-6'
  ),
  array(
        'type' => 'textfield',
        'heading' => __( 'Bottom margin', 'js_composer' ),
        'param_name' => 'margin_bottom',
        'description' => __( 'You can use px, em, %, etc. or enter just number and it will use pixels.', 'js_composer' ),
        'edit_field_class' => 'col-md-6'
  ),
  array(
        'type' => 'attach_image',
        'heading' => __( 'Background Image', 'js_composer' ),
        'param_name' => 'bg_image',
        'description' => __( 'Select background image for your row', 'js_composer' )
  ),
  array(
        'type' => 'dropdown',
        'heading' => __( 'Background Repeat', 'js_composer' ),
        'param_name' => 'bg_image_repeat',
        'value' => array(
                          __( 'Default', 'js_composer' ) => '',
                          __( 'Cover', 'js_composer' ) => 'cover',
                      __('Contain', 'js_composer') => 'contain',
                      __('No Repeat', 'js_composer') => 'no-repeat'
                    ),
        'description' => __( 'Select how a background image will be repeated', 'js_composer' ),
        'dependency' => array( 'element' => 'bg_image', 'not_empty' => true)
  ),
  */

        array(
            'type' => 'dropdown',
            'heading' => __( 'Row Content', 'Elise' ),
            'param_name' => 'full_width_content',
            'value' => array(
                __('Standard with Container', 'Elise') => 'standard',
                __('Full Window Width', 'Elise') => 'full-width',
            ),
            'std' => 'standard',
            // 'group' => __( 'Design options', 'js_composer' ),
            // "admin_label" => true,
        ),

        array(
            'type' => 'textfield',
            'heading' => __( 'Row Min Height', 'Elise' ),
            'param_name' => 'row_height',
            'description' => __( 'Type minimum height of the row in pixels or type "window" for full window height. (E.g. 600)', 'Elise' ),
            // 'group' => __( 'Design options', 'Elise' )
        ),

        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'vertical_align',
            'value' => array(
                              __( 'Content Vertical Align', 'js_composer' ) => 'vertical',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            'dependency' => array( 'element' => 'row_height', 'not_empty' => true),
            // 'group' => __( 'Design options', 'Elise' )
        ),

        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
        ),

        array(
            'type' => 'css_editor',
            'heading' => __( 'Css', 'js_composer' ),
            'param_name' => 'css',
            // 'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
            'group' => __( 'Design options', 'js_composer' )
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Background Style', 'Elise' ),
            'param_name' => 'bg_style',
            'value' => array(
                              __( 'Standard Background', 'Elise' ) => 'standard',
                              __( 'Parallax Background', 'Elise' ) => 'parallax',
                              __( 'Video Background', 'Elise' ) => 'video',
                        ),
            'description' => __( 'Select background style.', 'Elise' ),
            //'dependency' => array( 'element' => 'bg_image', 'not_empty' => true),
            'group' => __( 'Design options', 'js_composer' )
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Background Position', 'Elise' ),
            'param_name' => 'background_position',
            'value' => array(
                              '-' => '',
                              __( 'Left Top', 'Elise' ) => 'left top',
                              __( 'Left Center ', 'Elise' ) => 'left center',
                              __( 'Left Bottom ', 'Elise' ) => 'left bottom',
                              __( 'Center Top ', 'Elise' ) => 'center top',
                              __( 'Center Center ', 'Elise' ) => 'center center',
                              __( 'Center Bottom ', 'Elise' ) => 'center bottom',
                              __( 'Right Top ', 'Elise' ) => 'right top',
                              __( 'Right Center ', 'Elise' ) => 'right center',
                              __( 'Right Bottom ', 'Elise' ) => 'right bottom',
                        ),
            'std' => '',
            'dependency' => array( 'element' => 'bg_style', 'value' => array('standard', 'parallax')),
            'group' => __( 'Design options', 'js_composer' )
        ),

        // array(
        //     'type' => 'attach_images',
        //     'heading' => __( 'Video Sources', 'js_composer' ),
        //     'param_name' => 'bg_video_source',
        //     'description' => __( 'Select .mp4/.webm/.ogg videos.', 'js_composer' ),
        //     'dependency' => array( 'element' => 'bg_style', 'value' => 'video'),
        //     'group' => __( 'Design options', 'js_composer' )
        // ),

        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'bg_attachment_fixed',
            'value' => array(
                              __( 'Background Attachment: Fixed', 'Elise' ) => 'bg-fixed',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            'dependency' => array( 'element' => 'bg_style', 'value' => 'standard'),
            'group' => __( 'Design options', 'js_composer' )
        ),

        array(
            'type' => 'exploded_textarea',
            'heading' => __( 'Video Sources', 'js_composer' ),
            'param_name' => 'textarea_video_urls',
            'description' => __( 'Paste here .mp4/.webm/.ogg video urls. One url in one line. E.g.:<br>http://localhost/elise-wp/wp-content/uploads/2014/10/Explore4.mp4<br>http://localhost/elise-wp/wp-content/uploads/2014/10/Explore4.webm', 'Elise' ),
            'dependency' => array( 'element' => 'bg_style', 'value' => 'video'),
            'group' => __( 'Design options', 'js_composer' )
        ),

        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'bg_overlay',
            'value' => array(
                              __( 'Show Overlay', 'Elise' ) => 'show-overlay',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            'dependency' => array( 'element' => 'bg_image', 'not_empty' => true),
            'group' => __( 'Design options', 'js_composer' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Overlay Color', 'Elise' ),
            'param_name' => 'bg_overlay_color',
            'description' => __( 'Select overlay layer color', 'Elise' ),
            'dependency' => array( 'element' => 'bg_overlay', 'not_empty' => true),
            'group' => __( 'Design options', 'js_composer' )
        ),

        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'bg_shadow',
            'value' => array(
                              __( 'Inner Shadow', 'Elise' ) => 'bg-top-shadow',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            //'dependency' => array( 'element' => 'bg_image', 'not_empty' => true),
            'group' => __( 'Design options', 'Elise' )
        ),
    ),
    'js_view' => 'VcRowView'
) );

vc_map( array(
    'name' => __( 'Row', 'js_composer' ), //Inner Row
    'base' => 'vc_row_inner',
    'content_element' => false,
    'is_container' => true,
    'icon' => 'icon-wpb-row',
    'weight' => 1000,
    'show_settings_on_create' => false,
    'description' => __( 'Place content elements inside the row', 'js_composer' ),
    'params' => array(
        // array(
        //     'type' => 'colorpicker',
        //     'heading' => __( 'Font Color', 'js_composer' ),
        //     'param_name' => 'font_color',
        //     'description' => __( 'Select font color', 'js_composer' ),
        //     'edit_field_class' => 'vc_col-md-6 vc_column'
        // ),

        array(
            'type' => 'textfield',
            'heading' => __( 'Row Min Height', 'Elise' ),
            'param_name' => 'row_height',
            'description' => __( 'Type minimum height of the row in pixels or type "window" for full window height. (E.g. 600)', 'Elise' ),
            // 'group' => __( 'Design options', 'Elise' )
        ),

        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'vertical_align',
            'value' => array(
                              __( 'Content Vertical Align', 'js_composer' ) => 'vertical',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            'dependency' => array( 'element' => 'row_height', 'not_empty' => true),
            // 'group' => __( 'Design options', 'Elise' )
        ),

        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),
        array(
            'type' => 'css_editor',
            'heading' => __( 'Css', 'js_composer' ),
            'param_name' => 'css',
            // 'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
            'group' => __( 'Design options', 'js_composer' )
        ),
    ),
    'js_view' => 'VcRowView'
) );

// Timeline --------------------
vc_map( array(
    "name" => __("Timeline", "Elise"),
    "base" => "elise_timeline",
    "icon" => "elise-icon-timeline",
    "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => true,
    'description' => __( 'A list of events in chronological order.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element

        array(
            'type' => 'dropdown',
            'heading' => __( 'Timeline Axis Position', 'js_composer' ),
            'param_name' => 'timeline_style',
            'value' => array(
                              __( 'Left', 'js_composer' ) => 'left',
                              __( 'Center', 'js_composer' ) => 'center',
                              __( 'Right', 'js_composer' ) => 'right',
                        ),
            'std' => 'center',
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        )
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Timeline Block", "Elise"),
    "base" => "elise_timeline_block",
    "icon" => "elise-icon-timeline",
    "content_element" => true,
    "as_child" => array('only' => 'elise_timeline'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "Elise"),
            "param_name" => "title",
            'value' => __('Timeline Block Title.', 'Elise'),
            // "admin_label" => true,
            "holder"    => "h4",
            "description" => __("Timeline Block Title", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Date", "Elise"),
            "param_name" => "date",
            'value' => __('November 2014', 'Elise'),
            "admin_label" => true,
            "description" => __("Timeline Block Date", "Elise")
        ),
        array(
            "type" => "textarea_html",
            "heading" => __("Timeline Block Content", "Elise"),
            "param_name" => "content",
            'value' => 'Morbi facilisis risus id malesuada accumsan. Ut at augue condimentum, blandit ante in, luctus eros.',
            "holder"    => "p",
            // "admin_label" => true,
        ),


        array(
            'type' => 'colorpicker',
            'heading' => __( 'Title Color', 'Elise' ),
            'param_name' => 'tb_title_color',
            // 'value' => '#262626',
            // 'description' => __( 'Select background color', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Background Color', 'Elise' ),
            'param_name' => 'tb_bg',
            // 'value' => '#ffffff',
            // 'description' => __( 'Select background color', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        // array(
        //     'type' => 'colorpicker',
        //     'heading' => __( 'Border Color', 'Elise' ),
        //     'param_name' => 'tb_bcolor',
        //     // 'value' => '#cccccc',
        //     // 'description' => __( 'Select border color', 'Elise' ),
        //     // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
        //     'group' => __( 'Design options', 'Elise' )
        // ),
    )
) );
//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_elise_timeline extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_timeline_block extends WPBakeryShortCode {
    }
}




// Icon Box -----------------------------
vc_map( array(
    "name" => __("Icon Box", "Elise"),
    "base" => "elise_iconbox",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-iconbox",
    "show_settings_on_create" => true,
    'description' => __( 'Content block with icon.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element

        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Select Icons', 'Elise' ),
        //     'param_name' => 'icon_image_sel',
        //     "admin_label" => true,
        //     'value' => array(
        //                 __( 'WebFonts', 'Elise' ) => 'icon',
        //                 __( 'Image', 'Elise' ) => 'image',
        //                 ),
        //     // 'description' => __( 'Select style of iconbox title.', 'Elise' ),
        //     'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
        //     // 'group' => __( 'Design options', 'Elise' )
        // ),
        // array(
        //     "type" => "iconpicker",
        //     "heading" => __("Icon", "Elise"),
        //     "param_name" => "icon",
        //     'value' => 'fa fa-times',
        //     // "admin_label" => true,
        //     "holder"    => "h6",
        //     'settings' => array(
        //         'emptyIcon' => false, // default true, display an "EMPTY" icon?
        //         'type' => 'all',
        //         'iconsPerPage' => 200, // default 100, how many icons per/page to display
        //     ),
        //     // 'dependency' => array( 'element' => 'icon_image_sel', 'value' => 'icon'),
        //     // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        // ),


        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon library', 'js_composer' ),
            "admin_label" => true,
            'value' => array(
                __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
                __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                __( 'Typicons', 'js_composer' ) => 'typicons',
                __( 'Entypo', 'js_composer' ) => 'entypo',
                __( 'Linecons', 'js_composer' ) => 'linecons',
                __( 'Upload Custom Image', 'js_composer' ) => 'upload_image',
            ),
            'param_name' => 'icon',
            'description' => __( 'Select icon library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_fontawesome',
            // "admin_label" => true,
            'value' => 'fa fa-info-circle',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'fontawesome',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_openiconic',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'openiconic',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'openiconic',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_typicons',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'typicons',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
            'element' => 'icon',
            'value' => 'typicons',
        ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_entypo',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'entypo',
                'iconsPerPage' => 300, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'entypo',
            ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_linecons',
            'settings' => array(
                'emptyIcon' => false, // default true, display an "EMPTY" icon?
                'type' => 'linecons',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'linecons',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),

        array(
            'type' => 'attach_image',
            'heading' => __( 'Upload Image', 'Elise' ),
            'param_name' => 'icon_img',
            "admin_label" => true,
            'description' => __( 'Upload your own image instead icon.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'upload_image',
            ),
            // 'group' => __( 'Design options', 'js_composer' )
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "Elise"),
            "param_name" => "iconbox_title",
            // "admin_label" => true,
            'value' => '',
            "holder"    => "h4",
            //"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textarea_html",
            "heading" => __("Content", "Elise"),
            "param_name" => "content",
            'value' => '',
            "holder"    => "p",
            // "admin_label" => true,
        ),
        array(
            "type" => "vc_link",
            "heading" => __("Button Link", "Elise"),
            "param_name" => "iconbox_readmore_link",
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            // "admin_label" => true,
            // "holder"    => "a",
            //"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Button Text", "Elise"),
            "param_name" => "iconbox_readmore_text",
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            // "admin_label" => true,
            'value' => __('Read more', 'Elise'),
            // "holder"    => "h4",
            //"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),


        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Box Style', 'Elise' ),
            'param_name' => 'iconbox_title_style',
            "admin_label" => true,
            'edit_field_class' => 'vc_col-sm-6',
            'value' => array(
                        __( 'Block', 'Elise' ) => 'block',
                        __( 'Inline', 'Elise' ) => 'inline',
                        ),
            // 'description' => __( 'Select style of iconbox title.', 'Elise' ),
            // 'dependency' => array( 'element' => 'bg_image', 'not_empty' => true),
            'group' => __( 'Design options', 'Elise' )
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Box Layout', 'js_composer' ),
            'param_name' => 'iconbox_border',
            "admin_label" => true,
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'value' => array(
                              __( 'Simple', 'Elise' ) => '',
                              __( 'Boxed', 'Elise' ) => 'border',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            // 'dependency' => array( 'element' => 'row_height', 'not_empty' => true),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Iconbox Align', 'Elise' ),
            'param_name' => 'iconbox_align',
            // "admin_label" => true,
            'value' => array(
                        __( 'Left', 'Elise' ) => 'left',
                        __( 'Center', 'Elise' ) => 'center',
                        __( 'Right', 'Elise' ) => 'right',
                        ),
            'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            'group' => __( 'Design options', 'Elise' )
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Iconbox Inline Align', 'Elise' ),
            'param_name' => 'iconbox_inline_align',
            // "admin_label" => true,
            'value' => array(
                        __( 'Left', 'Elise' ) => 'left',
                        __( 'Right', 'Elise' ) => 'right',
                        ),
            'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            'std' => 'left',
            'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'inline'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Iconbox Header Size', 'Elise' ),
            'param_name' => 'ib_header_size',
            // 'edit_field_class' => 'vc_col-sm-8 vc_column',
            // "admin_label" => true,
            'value' => array(
                        // __( 'H1', 'Elise' ) => 'h1',
                        __( 'H2', 'Elise' ) => 'h2',
                        __( 'H3', 'Elise' ) => 'h3',
                        __( 'H4', 'Elise' ) => 'h4',
                        __( 'H5', 'Elise' ) => 'h5',
                        __( 'H6', 'Elise' ) => 'h6',
                        ),
            'std' => 'h4',
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Header Color', 'Elise' ),
            'param_name' => 'ib_title_color',
            // 'edit_field_class' => 'vc_col-sm-4 vc_column',
            // 'value' => '#262626',
            // 'description' => __( 'Select background color', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Style', 'Elise' ),
            'param_name' => 'ib_icon_style',
            // "admin_label" => true,
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'value' => array(
                        __( 'No Background', 'Elise' ) => 'no-bg',
                        __( 'Square Background', 'Elise' ) => 'bg-square',
                        __( 'Circle Background', 'Elise' ) => 'bg-circle',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Size', 'Elise' ),
            'param_name' => 'ib_icon_size',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            // "admin_label" => true,
            'value' => array(
                        __( 'Normal', 'Elise' ) => 'normal',
                        __( 'Large', 'Elise' ) => 'large',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Icon Color', 'Elise' ),
            'param_name' => 'ib_icon_color',
            'edit_field_class' => 'vc_col-sm-4 vc_column',
            // 'value' => '#262626',
            // 'description' => __( 'Select background color', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Icon BG Color', 'Elise' ),
            'param_name' => 'ib_icon_bg',
            'edit_field_class' => 'vc_col-sm-4 vc_column',
            // 'value' => '#262626',
            // 'description' => __( 'Select background color', 'Elise' ),
            'dependency' => array( 'element' => 'ib_icon_style', 'value' => array('bg-square', 'bg-circle') ),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'ib_icon_shadow',
            'edit_field_class' => 'vc_col-sm-4 vc_column',
            'value' => array(
                              __( 'Icon BG Shadow', 'js_composer' ) => 'icon-drop-shadow',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            'dependency' => array( 'element' => 'ib_icon_style', 'value' => array('bg-square', 'bg-circle') ),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Background Color', 'Elise' ),
            'param_name' => 'ib_border_bg',
            // 'value' => '#ffffff',
            // 'description' => __( 'Select background color', 'Elise' ),
            'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        // array(
        //     'type' => 'colorpicker',
        //     'heading' => __( 'Border Color', 'Elise' ),
        //     'param_name' => 'ib_border_bcolor',
        //     // 'value' => '#cccccc',
        //     // 'description' => __( 'Select border color', 'Elise' ),
        //     'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
        //     'group' => __( 'Design options', 'Elise' )
        // ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_iconbox extends WPBakeryShortCode {
    }
}





// Blog -----------------------------

$get_blog_categories = get_categories();
$blog_categories = array();
foreach ($get_blog_categories as $type) {
    $blog_categories[$type->name] = $type->slug;

}
vc_map( array(
    "name" => __("Blog", "Elise"),
    "base" => "elise_blog",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "show_settings_on_create" => false,
    'description' => __( 'Place Blog content.', 'Elise' ),
    "icon" => "elise-icon-blog",
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("<h3>To Customize this Field go to 'Elise Options -> Blog'</h3>Custom Class Here:", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "checkbox",
            "heading" => __("Blog Categories", "Elise"),
            "param_name" => "blog_categories",
            "value" => $blog_categories,
            "description" => __("For All Categories leave this fields unchecked.", "Elise")
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_blog extends WPBakeryShortCode {
    }
}

// Recent Posts -----------------------------
vc_map( array(
    "name" => __("Recent Posts", "Elise"),
    "base" => "elise_recent_posts",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-recentposts",
    "show_settings_on_create" => true,
    'description' => __( 'Recent blog posts.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "checkbox",
            "heading" => __("Categories", "Elise"),
            "param_name" => "categories",
            "value" => $blog_categories,
            "description" => __("For All Categories leave this fields unchecked.", "Elise")
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Columns', 'Elise' ),
            'param_name' => 'columns',
            "admin_label" => true,
            'value' => array(
                        __( '2 Columns', 'Elise' ) => 2,
                        __( '3 Columns', 'Elise' ) => 3,
                        __( '4 Columns', 'Elise' ) => 4,
                        __( '5 Columns', 'Elise' ) => 5,
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            // 'group' => __( 'Design options', 'Elise' ),
            "std"    => 4,
        ),
        array(
            "type" => "textfield",
            "heading" => __("Number of Posts", "Elise"),
            "param_name" => "posts_number",
            "admin_label" => true,
            'value' => 4,
            // "holder"    => "h4",
            "description" => __("Value '-1' Shows all posts.", "Elise")
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Recent Posts Style', 'Elise' ),
            'param_name' => 'rp_style',
            "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Grid', 'Elise' ) => 'grid',
                        __( 'Carousel', 'Elise' ) => 'carousel',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            // 'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Carousel Arrows Position', 'Elise' ),
            'param_name' => 'carousel_arrows_pos',
            // "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Sides', 'Elise' ) => 'sides',
                        __( 'Top Right', 'Elise' ) => 'top',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 'side',
            // 'group' => __( 'Design options', 'Elise' )
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Custom Class:", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Background Color', 'Elise' ),
            'param_name' => 'bg_color',
            // 'value' => '#cccccc',
            'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Text Color', 'Elise' ),
            'param_name' => 'text_color',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_recent_posts extends WPBakeryShortCode {
    }
}


//Portfolio
if ( is_plugin_active( 'portfolio-post-type/portfolio-post-type.php' ) ) {

    // $portfolio_terms = get_terms('portfolio_category');

    // $portfolio_categories = array();
    // foreach ($portfolio_terms as $term) {
    //     $portfolio_categories[$term->name] = $term->slug;
    // }

    vc_map( array(
        "name" => __("Portfolio", "Elise"),
        "base" => "elise_portfolio",
        // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
        // "content_element" => true,
        "show_settings_on_create" => true,
        "icon" => "elise-icon-portfolio",
        'description' => __( 'Place Portfolio content.', 'Elise' ),
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "textfield",
                "heading" => __("Portfolio Categories", "Elise"),
                "param_name" => "portfolio_cats",
                "value" => '',
                "description" => __("Select specific categories names. Separate with comas. Empty field show all categories. (E.g Case Study, Website etc.)", "Elise")
            ),
            array(
                "type" => "textfield",
                "heading" => __("Portfolio Items", "Elise"),
                "param_name" => "pf_items",
                'value' => -1,
                "admin_label" => true,
                "description" => __("Type number of porfolio items that will show up. ('-1' Shows all items)", "Elise")
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Layout', 'Elise' ),
                'param_name' => 'pf_layout',
                "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( '1 Column', 'Elise' ) => 1,
                            __( '2 Columns', 'Elise' ) => 2,
                            __( '3 Columns', 'Elise' ) => 3,
                            __( '4 Columns', 'Elise' ) => 4,
                            __( 'Masonry', 'Elise' ) => 5,
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 3,
                // 'group' => __( 'Design options', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Pagination', 'Elise' ),
                'param_name' => 'portfolio_pagination',
                "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Enabled', 'Elise' ) => 'paged',
                            __( 'Disabled', 'Elise' ) => 'no-paged',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 'no-paged',
                // 'group' => __( 'Design options', 'Elise' )
            ),
            array(
                "type" => "textfield",
                "heading" => __("Items Per Page", "Elise"),
                "param_name" => "items_per_page",
                'value' => 4,
                // "admin_label" => true,
                'dependency' => array( 'element' => 'portfolio_pagination', 'value' => 'paged'),
                "description" => __("Type number of porfolio items per page.", "Elise")
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Filtering', 'Elise' ),
                'param_name' => 'pf_filtering',
                // "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Enabled', 'Elise' ) => 1,
                            __( 'Disabled', 'Elise' ) => 0,
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 0,
                'group' => __( 'Portfolio Filtering', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Filtering Position', 'Elise' ),
                'param_name' => 'filtering_position',
                // "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Left', 'Elise' ) => 'filtering-left',
                            __( 'Center', 'Elise' ) => 'filtering-center',
                            __( 'Right', 'Elise' ) => 'filtering-right',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                'dependency' => array( 'element' => 'pf_filtering', 'value' => '1'),
                "std"    => 1,
                'group' => __( 'Portfolio Filtering', 'Elise' )
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __( 'Filtering: Active Category Background', 'Elise' ),
                'param_name' => 'f_active_bg',
                // 'value' => '#cccccc',
                // 'description' => __( 'Post Background color.', 'Elise' ),
                'dependency' => array( 'element' => 'pf_filtering', 'value' => '1'),
                'group' => __( 'Portfolio Filtering', 'Elise' )
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __( 'Filtering: Active Category Color', 'Elise' ),
                'param_name' => 'f_active_color',
                // 'value' => '#cccccc',
                // 'description' => __( 'Post Background color.', 'Elise' ),
                'dependency' => array( 'element' => 'pf_filtering', 'value' => '1'),
                'group' => __( 'Portfolio Filtering', 'Elise' )
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __( 'Filtering: Categories Color', 'Elise' ),
                'param_name' => 'f_color',
                // 'value' => '#cccccc',
                // 'description' => __( 'Post Background color.', 'Elise' ),
                'dependency' => array( 'element' => 'pf_filtering', 'value' => '1'),
                'group' => __( 'Portfolio Filtering', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Style', 'Elise' ),
                'param_name' => 'portfolio_item_style',
                // "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Title Overlay', 'Elise' ) => 'portfolio-style-overlay',
                            __( 'Title Under Image', 'Elise' ) => 'portfolio-style-bottom',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 'portfolio-style-overlay',
                'group' => __( 'Design options', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Items Gap', 'Elise' ),
                'param_name' => 'portfolio_gap',
                // "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( '0px - No Gap', 'Elise' ) => 0,
                            __( '6px', 'Elise' ) => 6,
                            __( '10px', 'Elise' ) => 10,
                            __( '16px', 'Elise' ) => 16,
                            __( '20px', 'Elise' ) => 20,
                            __( '26px', 'Elise' ) => 26,
                            __( '30px', 'Elise' ) => 30,
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 30,
                'group' => __( 'Design options', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Quick View Button', 'Elise' ),
                'param_name' => 'show_quick_view',
                // "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Enabled', 'Elise' ) => 'show',
                            __( 'Disabled', 'Elise' ) => 'hide',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 'show',
                'group' => __( 'Design options', 'Elise' )
            ),
            // array(
            //     'type' => 'dropdown',
            //     'heading' => __( 'Portfolio Style', 'Elise' ),
            //     'param_name' => 'pf_style',
            //     "admin_label" => true,
            //     // "holder"    => "h3",
            //     'value' => array(
            //                 __( 'Overlayed Title', 'Elise' ) => 1,
            //                 __( 'Bottom Title', 'Elise' ) => 2,
            //                 ),
            //     // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            //     // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            //     "std"    => 1,
            //     // 'group' => __( 'Design options', 'Elise' )
            // ),
            // array(
            //     'type' => 'dropdown',
            //     'heading' => __( 'Portfolio Items Gap', 'Elise' ),
            //     'param_name' => 'portfolio_items_gap',
            //     "admin_label" => true,
            //     // "holder"    => "h3",
            //     'value' => array(
            //                 __( '0px - No Gap', 'Elise' ) => 0,
            //                 __( '5px', 'Elise' ) => 5,
            //                 __( '10px', 'Elise' ) => 10,
            //                 __( '15px', 'Elise' ) => 15,
            //                 __( '20px', 'Elise' ) => 20,
            //                 __( '25px', 'Elise' ) => 25,
            //                 __( '30px', 'Elise' ) => 30,
            //                 ),
            //     // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            //     // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
            //     "std"    => 30,
            //     // 'group' => __( 'Design options', 'Elise' )
            // ),
            array(
                "type" => "textfield",
                "heading" => __("<h4>For More Customize Options of this Field go to 'Elise Options -> Portfolio' and 'Elise Options -> Styling'</h4><br>Custom Class Here:", "Elise"),
                "param_name" => "el_class",
                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
            ),
        )
    ) );
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_elise_portfolio extends WPBakeryShortCode {
        }
    }

    //Recent Projects
    vc_map( array(
        "name" => __("Recent Projects", "Elise"),
        "base" => "elise_recent_projects",
        // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
        // "content_element" => true,
        "icon" => "elise-icon-recprojects",
        'description' => __( 'recent portfolio content.', 'Elise' ),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "textfield",
                "heading" => __("Categories", "Elise"),
                "param_name" => "portfolio_cats",
                "value" => '',
                "description" => __("Select specific categories names. Separate with comas. Empty field show all categories. (E.g Case Study, Website etc.)", "Elise")
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Recent Portfolio Columns', 'Elise' ),
                'param_name' => 'rp_columns',
                "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( '1 Column', 'Elise' ) => 1,
                            __( '2 Columns', 'Elise' ) => 2,
                            __( '3 Columns', 'Elise' ) => 3,
                            __( '4 Columns', 'Elise' ) => 4,
                            __( '5 Columns', 'Elise' ) => 5,
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 3,
                // 'group' => __( 'Design options', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Recent Projects Style', 'Elise' ),
                'param_name' => 'rp_style',
                "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Standard', 'Elise' ) => 'standard',
                            __( 'Carousel', 'Elise' ) => 'carousel',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 1,
                // 'group' => __( 'Design options', 'Elise' )
            ),
            array(
                "type" => "textfield",
                "heading" => __("Recent Project Number", "Elise"),
                "param_name" => "rp_items",
                'value' => 6,
                // "admin_label" => true,
                'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
                "description" => __("Type number of porfolio items that will show up. ('-1' Shows all items)", "Elise")
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Carousel Arrows Position', 'Elise' ),
                'param_name' => 'carousel_arrows_pos',
                // "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Sides', 'Elise' ) => 'sides',
                            __( 'Top Right', 'Elise' ) => 'top',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
                "std"    => 'side',
                // 'group' => __( 'Design options', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Style', 'Elise' ),
                'param_name' => 'portfolio_item_style',
                "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( 'Overlayed Title', 'Elise' ) => 'portfolio-style-overlay',
                            __( 'Bottom Title', 'Elise' ) => 'portfolio-style-bottom',
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 'portfolio-style-overlay',
                // 'group' => __( 'Design options', 'Elise' )
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Portfolio Items Gap', 'Elise' ),
                'param_name' => 'items_gap',
                "admin_label" => true,
                // "holder"    => "h3",
                'value' => array(
                            __( '0px - No Gap', 'Elise' ) => 0,
                            __( '6px', 'Elise' ) => 6,
                            __( '10px', 'Elise' ) => 10,
                            __( '16px', 'Elise' ) => 16,
                            __( '20px', 'Elise' ) => 20,
                            __( '26px', 'Elise' ) => 26,
                            __( '30px', 'Elise' ) => 30,
                            ),
                // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
                // 'dependency' => array( 'element' => 'iconbox_title_style', 'value' => 'block'),
                "std"    => 30,
                // 'group' => __( 'Design options', 'Elise' )
            ),
            $add_css_animation,
            array(
                "type" => "textfield",
                "heading" => __("<h4>For More Customize Options of this Field go to 'Elise Options -> Portfolio' and 'Elise Options -> Styling'</h4><br>Custom Class Here:", "Elise"),
                "param_name" => "el_class",
                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
            ),
        )
    ) );
    if ( class_exists( 'WPBakeryShortCode' ) ) {
        class WPBakeryShortCode_elise_recent_projects extends WPBakeryShortCode {
        }
    }
    
} // endif portfolio post type


// Carousel --------------------
vc_map( array(
    "name" => __("Carousel", "Elise"),
    "base" => "elise_carousel",
    "as_parent" => array('only' => 'vc_row'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "icon" => "elise-icon-carousel",
    "show_settings_on_create" => true,
    'description' => __( 'Place content inside carousel row. One row = one slide.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element
        array(
            'type' => 'dropdown',
            'heading' => __( 'Carousel Arrows Position', 'Elise' ),
            'param_name' => 'carousel_arrows_pos',
            // "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Sides', 'Elise' ) => 'sides',
                        __( 'Top Right', 'Elise' ) => 'top',
                        __( 'None', 'Elise' ) => 'none',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 'top',
            // 'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Carousel Bottom Navigation', 'Elise' ),
            'param_name' => 'navigation',
            // "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Bullets', 'Elise' ) => 'bullets',
                        __( 'None', 'Elise' ) => 'none',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 'bullets',
            // 'group' => __( 'Design options', 'Elise' )
        ),
        array(
            "type" => "textfield",
            "heading" => __("Autoplay", "Elise"),
            "param_name" => "autoplay",
            'value' => 0,
            "description" => __("Autoplay Delay in ms. (E.g. '2000' = 2seconds, '0' Disable autoplay).", "Elise")
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Slides Background', 'Elise' ),
            'param_name' => 'carousel_bg',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
    ),
    // 'custom_markup' => '<h1>Slide</h1>',
    "js_view" => 'VcColumnView'
) );
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_elise_carousel extends WPBakeryShortCodesContainer {
    }
}

// Testimonials -------------
vc_map( array(
    "name" => __("Testimonial", "Elise"),
    "base" => "elise_testimonials",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-testimonial",
    'description' => __( 'Simple or bubbled testimonial block.', 'Elise' ),
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textarea_html",
            "heading" => __("Quote", "Elise"),
            "param_name" => "content",
            'value' => 'The natural desire of good men is knowledge.',
            "holder"    => "p",
            // "admin_label" => true,
        ),
        array(
            "type" => "textfield",
            "heading" => __("Author", "Elise"),
            "param_name" => "author",
            // 'value' => 'Leonardo da Vinci.',
            "holder"    => "h5",
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Function", "Elise"),
            "param_name" => "function",
            // 'value' => 'Artist',
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'attach_image',
            'heading' => __( 'Author Avatar', 'Elise' ),
            'param_name' => 'avatar',
            'description' => __( 'Select avatar/photo/logo for quote author.', 'Elise' )
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Custom Class Here:", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Testimonial Style', 'Elise' ),
            'param_name' => 'testi_style',
            // "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Standard', 'Elise' ) => 'standard',
                        __( 'Bordered/Bubble', 'Elise' ) => 'bordered',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 'top',
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Author Color', 'Elise' ),
            'param_name' => 'author_color',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'iconbox_border', 'value' => 'border'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Bottom Line Color', 'Elise' ),
            'param_name' => 'border_color',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            'dependency' => array( 'element' => 'testi_style', 'value' => 'standard'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Backgound Color', 'Elise' ),
            'param_name' => 'border_bg',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            'dependency' => array( 'element' => 'testi_style', 'value' => 'bordered'),
            'group' => __( 'Design options', 'Elise' )
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_testimonials extends WPBakeryShortCode {
    }
}

// Pricing Table --------------------
vc_map( array(
    "name" => __("Pricing Table", "Elise"),
    "base" => "elise_pricing_table",
    "as_parent" => array('only' => 'elise_pricing_column'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "icon" => "elise-icon-pt",
    "show_settings_on_create" => true,
    'description' => __( '1-5 Columns Pricing Table', 'Elise' ),
    "params" => array(
        // add params same as with any other content element
        array(
            'type' => 'dropdown',
            'heading' => __( 'Pricing Columns', 'Elise' ),
            'param_name' => 'columns',
            // "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( '1 Column', 'Elise' ) => 1,
                        __( '2 Columns', 'Elise' ) => 2,
                        __( '3 Columns', 'Elise' ) => 3,
                        __( '4 Columns', 'Elise' ) => 4,
                        __( '5 Columns', 'Elise' ) => 5,
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 4,
            // 'group' => __( 'Design options', 'Elise' )
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
    ),
    // 'custom_markup' => '<h1>Slide</h1>',
    "js_view" => 'VcColumnView'
) );

$features_list_values = '10|Pages
1GB|Storage
100|Products
Feature #4';

vc_map( array(
    "name" => __("Pricing Column", "Elise"),
    "base" => "elise_pricing_column",
    "content_element" => true,
    "icon" => "elise-icon-pt",
    "as_child" => array('only' => 'elise_pricing_table'), // Use only|except attributes to limit parent (separate multiple values with comma)
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),

        array(
            'type' => 'checkbox',
            // 'heading' => __( 'Show Overlay', 'js_composer' ),
            'param_name' => 'featured',
            'value' => array(
                              __( 'Featured Column', 'js_composer' ) => 'true',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            // 'dependency' => array( 'element' => 'bg_style', 'value' => 'standard'),
            // 'std' => false,
            // 'group' => __( 'Design options', 'js_composer' )
        ),
        array(
            "type" => "textfield",
            "heading" => __("Featured Text", "Elise"),
            "param_name" => "featured_text",
            'value' => 'Most Popular!',
            'dependency' => array( 'element' => 'featured', 'value' => 'true'),
            // "holder"    => "h4",
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "Elise"),
            "param_name" => "title",
            'value' => 'Personal',
            "holder"    => "h4",
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Background Icon', 'js_composer' ),
            // 'group' => __( 'Text separator', 'Elise' ),
            // "admin_label" => true,
            'holder' => 'div',
            'value' => array(
                __( 'No Icon', 'Elise' ) => '',
                __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
                __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                __( 'Typicons', 'js_composer' ) => 'typicons',
                __( 'Entypo', 'js_composer' ) => 'entypo',
                __( 'Linecons', 'js_composer' ) => 'linecons',
            ),
            'param_name' => 'icon',
            'std' => '',
            'description' => __( 'Select icon library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_fontawesome',
            // 'group' => __( 'Text separator', 'Elise' ),
            // "admin_label" => true,
            // 'holder' => 'div',
            'value' => '',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'fontawesome',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_openiconic',
            // 'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'openiconic',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'openiconic',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_typicons',
            // 'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'typicons',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
            'element' => 'icon',
            'value' => 'typicons',
        ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_entypo',
            // 'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'entypo',
                'iconsPerPage' => 300, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'entypo',
            ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_linecons',
            // 'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'linecons',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'linecons',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),

        array(
            "type" => "textfield",
            "heading" => __("Price", "Elise"),
            "param_name" => "price",
            'value' => '$19',
            "holder"    => "span",
            "description" => __("E.g. $19, 19 EUR, 19zl, etc.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Billing", "Elise"),
            "param_name" => "billing",
            'value' => '/month',
            "holder"    => "span",
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'exploded_textarea',
            'heading' => __( 'Features List', 'Elise' ),
            'param_name' => 'features_list',
            'value' => $features_list_values,
            "holder"    => "p",
            'description' => __( 'Quantity|Feature Name', 'Elise' ),
            // 'dependency' => array( 'element' => 'bg_style', 'value' => 'video'),
            // 'group' => __( 'Design options', 'js_composer' )
        ),
        array(
            "type" => "vc_link",
            "heading" => __("Choose/Sign up Button", "Elise"),
            "param_name" => "choose_btn",
            'value' => 'url:#|title:'.__('Choose Plan', 'Elise').'',
            // "admin_label" => true,
            // "holder"    => "a",
            //"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),

        array(
            'type' => 'colorpicker',
            'heading' => __( 'Header Background Color', 'Elise' ),
            'param_name' => 'header_bg',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'testi_style', 'value' => 'bordered'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Header Colors', 'Elise' ),
            'param_name' => 'header_colors',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'testi_style', 'value' => 'bordered'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Features Background Color', 'Elise' ),
            'param_name' => 'features_bg',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'testi_style', 'value' => 'bordered'),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Features Color', 'Elise' ),
            'param_name' => 'features_color',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            // 'dependency' => array( 'element' => 'testi_style', 'value' => 'bordered'),
            'group' => __( 'Design options', 'Elise' )
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_elise_pricing_table extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_pricing_column extends WPBakeryShortCode {
    }
}

// Team Member --------------------
vc_map( array(
    "name" => __("Team Member", "Elise"),
    "base" => "elise_team_member",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-team",
    "show_settings_on_create" => true,
    'description' => __( 'Simple team member block with photo and description.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element
        array(
            'type' => 'attach_image',
            'heading' => __( 'Member Photography', 'Elise' ),
            'param_name' => 'photo',
            // 'description' => __( 'Select avatar/photo/logo for quote author.', 'Elise' )
        ),
        array(
            "type" => "textfield",
            "heading" => __("Name", "Elise"),
            "param_name" => "name",
            'value' => 'John Doe',
            "holder"    => "h4",
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Function", "Elise"),
            "param_name" => "function",
            'value' => 'Chief Executive Officer',
            "holder"    => "small",
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "textarea_html",
            "heading" => __("Member Bio/Description", "Elise"),
            "param_name" => "content",
            'value' => 'Donec blandit magna ante, ac luctus magna maximus vel. Integer pretium metus a augue viverra, eu feugiat arcu iaculis. Phasellus accumsan ac purus sit amet lobortis. Duis sit amet nisi nec mi laoreet pulvinar a eu ipsum.',
            "holder"    => "p",
            // "admin_label" => true,
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Custom Class:", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Name Color', 'Elise' ),
            'param_name' => 'name_color',
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            'dependency' => array( 'element' => 'name', 'not_empty' => true),
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Function Color', 'Elise' ),
            'param_name' => 'function_color',
            'dependency' => array( 'element' => 'function', 'not_empty' => true),
            // 'value' => '#cccccc',
            // 'description' => __( 'Post Background color.', 'Elise' ),
            'group' => __( 'Design options', 'Elise' )
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_team_member extends WPBakeryShortCode {
    }
}

// Social Icons --------------------
vc_map( array(
    "name" => __("Social Icons", "Elise"),
    "base" => "elise_social_icons",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-social",
    "show_settings_on_create" => true,
    'description' => __( 'Social websites linked icons.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icons Size', 'Elise' ),
            'param_name' => 'icons_size',
            "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Normal', 'Elise' ) => 'si-normal ',
                        __( 'Large', 'Elise' ) => 'si-large ',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 'si-normal ',
            // 'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icons Position', 'Elise' ),
            'param_name' => 'icons_position',
            "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Left', 'Elise' ) => 'si-left ',
                        __( 'Center', 'Elise' ) => 'si-center ',
                        __( 'Right', 'Elise' ) => 'si-right ',
                        ),
            // 'description' => __( 'Select alignment of elements in iconbox.', 'Elise' ),
            // 'dependency' => array( 'element' => 'rp_style', 'value' => 'carousel'),
            "std"    => 'si-left ',
            // 'group' => __( 'Design options', 'Elise' )
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Custom Class:", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Facebook Profile URL", "Elise"),
            "param_name" => "url_facebook",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Twitter Profile URL", "Elise"),
            "param_name" => "url_twitter",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Google Plus Profile URL", "Elise"),
            "param_name" => "url_google_plus",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Flickr Profile URL", "Elise"),
            "param_name" => "url_flickr",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("LinkedIn Profile URL", "Elise"),
            "param_name" => "url_linkedin",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Pinterest Profile URL", "Elise"),
            "param_name" => "url_pinterest",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Instagram Profile URL", "Elise"),
            "param_name" => "url_instagram",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Behance Profile URL", "Elise"),
            "param_name" => "url_behance",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Dribbble Profile URL", "Elise"),
            "param_name" => "url_dribbble",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Tumblr Profile URL", "Elise"),
            "param_name" => "url_tumblr",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("YouTube Profile URL", "Elise"),
            "param_name" => "url_youtube",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Vimeo Profile URL", "Elise"),
            "param_name" => "url_vimeo",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Vine Profile URL", "Elise"),
            "param_name" => "url_vine",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("LastFM Profile URL", "Elise"),
            "param_name" => "url_lastfm",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("DeviantArt Profile URL", "Elise"),
            "param_name" => "url_deviantart",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Digg Profile URL", "Elise"),
            "param_name" => "url_digg",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Dropbox Profile URL", "Elise"),
            "param_name" => "url_dropbox",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("FourSquare Profile URL", "Elise"),
            "param_name" => "url_foursquare",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("GitHub Profile URL", "Elise"),
            "param_name" => "url_github",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Reddit Profile URL", "Elise"),
            "param_name" => "url_reddit",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Skype Profile URL", "Elise"),
            "param_name" => "url_skype",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("SoundCloud Profile URL", "Elise"),
            "param_name" => "url_soundcloud",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Spotify Profile URL", "Elise"),
            "param_name" => "url_spotify",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("Steam Profile URL", "Elise"),
            "param_name" => "url_steam",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("StumbleUpon Profile URL", "Elise"),
            "param_name" => "url_stumbleupon",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("VK Profile URL", "Elise"),
            "param_name" => "url_vk",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            "type" => "href",
            "heading" => __("WordPress Profile URL", "Elise"),
            "param_name" => "url_wordpress",
            "admin_label" => true,
            // "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Color', 'Elise' ),
            'param_name' => 'color',
            // 'dependency' => array( 'element' => 'function', 'not_empty' => true),
            // 'value' => '#cccccc',
            'description' => __( 'Border and icon color.', 'Elise' ),
            'group' => __( 'Design options', 'Elise' )
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_social_icons extends WPBakeryShortCode {
    }
}

// Counter --------------------
vc_map( array(
    "name" => __("Counter", "Elise"),
    "base" => "elise_counter",
    // "content_element" => true,
    "icon" => "elise-icon-counter",
    'description' => __( 'Animated counter.', 'Elise' ),
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Start Value", "Elise"),
            "param_name" => "start_value",
            "value" => '0',
            "admin_label" => true,
            "description" => __("E.g. 0, 10.45 etc.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("End Value", "Elise"),
            "param_name" => "end_value",
            "admin_label" => true,
            "value" => '1337',
        ),
        array(
            "type" => "textfield",
            "heading" => __("Decimals", "Elise"),
            "param_name" => "decimals",
            "admin_label" => true,
            "value" => '0',
            "description" => __("Decimals Number. Important: start value must be different than 0 (E.g 0.01)", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Prefix", "Elise"),
            "param_name" => "prefix",
            "admin_label" => true,
            "description" => __("Text before counter number. E.g. $, Number:, etc.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Suffix", "Elise"),
            "param_name" => "suffix",
            "admin_label" => true,
            "description" => __("Text after counter number. E.g. %, Countries, etc.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Subtitle", "Elise"),
            "admin_label" => true,
            "param_name" => "subtitle",
        ),
        array(
            "type" => "textfield",
            "heading" => __("Counter Speed", "Elise"),
            "param_name" => "speed",
            "value" => '1500',
            "admin_label" => true,
            "description" => __("Counter speed in miliseconds.", "Elise")
        ),
        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Custom Class:", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Counter Align', 'Elise' ),
            'param_name' => 'align',
            // "admin_label" => true,
            // "holder"    => "h3",
            'value' => array(
                        __( 'Left', 'Elise' ) => 'left',
                        __( 'Center', 'Elise' ) => 'center',
                        __( 'Right', 'Elise' ) => 'right',
                        ),
            "std"    => 'left',
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Number Color', 'Elise' ),
            'param_name' => 'number_color',
            'group' => __( 'Design options', 'Elise' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Subtitle Color', 'Elise' ),
            'param_name' => 'subtitle_color',
            'group' => __( 'Design options', 'Elise' ),
            'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_counter extends WPBakeryShortCode {
    }
}



/* Tabs
---------------------------------------------------------- */
$tab_id_1 = time() . '-1-' . rand( 0, 100 );
$tab_id_2 = time() . '-2-' . rand( 0, 100 );
vc_map( array(
    "name" => __( 'Tabs', 'js_composer' ),
    'base' => 'vc_tabs',
    'show_settings_on_create' => false,
    'is_container' => true,
    'icon' => 'icon-wpb-ui-tab-content',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Tabbed content', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'dropdown',
            'heading' => __( 'Auto rotate tabs', 'js_composer' ),
            'param_name' => 'interval',
            'value' => array( __( 'Disable', 'js_composer' ) => 0, 3, 5, 10, 15 ),
            'std' => 0,
            'description' => __( 'Auto rotate tabs each X seconds.', 'js_composer' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Widget title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Background Color', 'Elise' ),
            'param_name' => 'tab_bg',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Active Tab Text Color', 'Elise' ),
            'param_name' => 'tab_active_color',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Tabs Background', 'Elise' ),
            'param_name' => 'tabs_bg',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
    ),
    'custom_markup' => '
<div class="wpb_tabs_holder wpb_holder vc_container_for_children">
<ul class="tabs_controls">
</ul>
%content%
</div>'
,
    'default_content' => '
[vc_tab title="' . __( 'Tab 1', 'js_composer' ) . '" tab_id="' . $tab_id_1 . '"][/vc_tab]
[vc_tab title="' . __( 'Tab 2', 'js_composer' ) . '" tab_id="' . $tab_id_2 . '"][/vc_tab]
',
    'js_view' => $vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35'
) );

/* Tour section
---------------------------------------------------------- */
$tab_id_1 = time() . '-1-' . rand( 0, 100 );
$tab_id_2 = time() . '-2-' . rand( 0, 100 );
WPBMap::map( 'vc_tour', array(
    'name' => __( 'Tour', 'js_composer' ),
    'base' => 'vc_tour',
    'show_settings_on_create' => false,
    'is_container' => true,
    'container_not_allowed' => true,
    'icon' => 'icon-wpb-ui-tab-content-vertical',
    'category' => __( 'Content', 'js_composer' ),
    'wrapper_class' => 'vc_clearfix',
    'description' => __( 'Vertical tabbed content', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Widget title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Auto rotate slides', 'js_composer' ),
            'param_name' => 'interval',
            'value' => array( __( 'Disable', 'js_composer' ) => 0, 3, 5, 10, 15 ),
            'std' => 0,
            'description' => __( 'Auto rotate slides each X seconds.', 'js_composer' )
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),
        // array(
        //     'type' => 'colorpicker',
        //     'heading' => __( 'Content/Active Tab Background Color', 'Elise' ),
        //     'param_name' => 'tab_bg',
        //     'group' => __( 'Design options', 'Elise' ),
        //     // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        // ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Active Tab Text Color', 'Elise' ),
            'param_name' => 'tab_active_color',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Tabs Border Colors', 'Elise' ),
            'param_name' => 'tour_border',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
    ),
    'custom_markup' => '
<div class="wpb_tabs_holder wpb_holder vc_clearfix vc_container_for_children">
<ul class="tabs_controls">
</ul>
%content%
</div>'
,
    'default_content' => '
[vc_tab title="' . __( 'Tab 1', 'js_composer' ) . '" tab_id="' . $tab_id_1 . '"][/vc_tab]
[vc_tab title="' . __( 'Tab 2', 'js_composer' ) . '" tab_id="' . $tab_id_2 . '"][/vc_tab]
',
    'js_view' => $vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35'
) );

vc_map( array(
    'name' => __( 'Tab', 'js_composer' ),
    'base' => 'vc_tab',
    'allowed_container_element' => 'vc_row',
    'is_container' => true,
    'content_element' => false,
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Tab title.', 'js_composer' )
        ),
        array(
            'type' => 'tab_id',
            'heading' => __( 'Tab ID', 'js_composer' ),
            'param_name' => "tab_id"
        )
    ),
    'js_view' => $vc_is_wp_version_3_6_more ? 'VcTabView' : 'VcTabView35'
) );

/* Accordion block
---------------------------------------------------------- */
vc_map( array(
    'name' => __( 'Accordion', 'js_composer' ),
    'base' => 'vc_accordion',
    'show_settings_on_create' => false,
    'is_container' => true,
    'icon' => 'icon-wpb-ui-accordion',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Collapsible content panels', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Widget title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Active section', 'js_composer' ),
            'param_name' => 'active_tab',
            'description' => __( 'Enter section number to be active on load or enter false to collapse all sections.', 'js_composer' )
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Allow collapsible all', 'js_composer' ),
            'param_name' => 'collapsible',
            'description' => __( 'Select checkbox to allow all sections to be collapsible.', 'js_composer' ),
            'value' => array( __( 'Allow', 'js_composer' ) => 'yes' )
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Disable keyboard interactions', 'js_composer' ),
            'param_name' => 'disable_keyboard',
            'description' => __( 'Disables keyboard arrows interactions LEFT/UP/RIGHT/DOWN/SPACES keys.', 'js_composer' ),
            'value' => array( __( 'Disable', 'js_composer' ) => 'yes' )
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Accordion Background', 'Elise' ),
            'param_name' => 'acc_bg',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Accordion Header Color', 'Elise' ),
            'param_name' => 'acc_color',
            'group' => __( 'Design options', 'Elise' ),
            // 'dependency' => array( 'element' => 'subtitle', 'not_empty' => true),
        ),
    ),
    'custom_markup' => '
<div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
%content%
</div>
<div class="tab_controls">
    <a class="add_tab" title="' . __( 'Add section', 'js_composer' ) . '"><span class="vc_icon"></span> <span class="tab-label">' . __( 'Add section', 'js_composer' ) . '</span></a>
</div>
',
    'default_content' => '
    [vc_accordion_tab title="' . __( 'Section 1', 'js_composer' ) . '"][/vc_accordion_tab]
    [vc_accordion_tab title="' . __( 'Section 2', 'js_composer' ) . '"][/vc_accordion_tab]
',
    'js_view' => 'VcAccordionView'
) );
vc_map( array(
    'name' => __( 'Section', 'js_composer' ),
    'base' => 'vc_accordion_tab',
    'allowed_container_element' => 'vc_row',
    'is_container' => true,
    'content_element' => false,
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Accordion section title.', 'js_composer' )
        ),
    ),
    'js_view' => 'VcAccordionTabView'
) );

/* Single image */
vc_map( array(
    'name' => __( 'Single Image', 'js_composer' ),
    'base' => 'vc_single_image',
    'icon' => 'icon-wpb-single-image',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Simple image with CSS animation', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Widget title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' )
        ),
        array(
            'type' => 'attach_image',
            'heading' => __( 'Image', 'js_composer' ),
            'param_name' => 'image',
            'value' => '',
            'description' => __( 'Select image from media library.', 'js_composer' )
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Image size', 'js_composer' ),
            'param_name' => 'img_size',
            'description' => __( 'Enter image size. Example: "thumbnail", "medium", "large", "full" or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "full" size.', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Image alignment', 'js_composer' ),
            'param_name' => 'alignment',
            'value' => array(
                __( 'Align left', 'js_composer' ) => '',
                __( 'Align right', 'js_composer' ) => 'right',
                __( 'Align center', 'js_composer' ) => 'center'
            ),
            'description' => __( 'Select image alignment.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Image style', 'js_composer' ),
            'param_name' => 'style',
            'value' => array(
                __( 'Regular', 'Elise' ) => '',
                __( 'Bordered', 'Elise' ) => 'img_bordered',
                __( 'Rounded', 'Elise' ) => 'img_rounded',
                __( 'Rounded Bordered', 'Elise' ) => 'img_rounded-bordered',
            ),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Border color', 'js_composer' ),
            'param_name' => 'border_color',
            'dependency' => array(
                'element' => 'style',
                'value' => array( 'img_bordered', 'img_rounded-bordered' )
            ),
            'description' => __( 'Border color.', 'js_composer' ),
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Image Drops Shadow', 'Elise' ),
            'param_name' => 'drop_shadow',
            'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' )
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Link to large image?', 'js_composer' ),
            'param_name' => 'img_link_large',
            'description' => __( 'If selected, image will be linked to the larger image.', 'js_composer' ),
            'value' => array( __( 'Yes, please', 'js_composer' ) => 'yes' )
        ),
        array(
            'type' => 'href',
            'heading' => __( 'Image link', 'js_composer' ),
            'param_name' => 'link',
            'description' => __( 'Enter URL if you want this image to have a link.', 'js_composer' ),
            'dependency' => array(
                'element' => 'img_link_large',
                'is_empty' => true,
                'callback' => 'wpb_single_image_img_link_dependency_callback'
            )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Link Target', 'js_composer' ),
            'param_name' => 'img_link_target',
            'value' => $target_arr,
            'dependency' => array(
                'element' => 'img_link',
                'not_empty' => true
            )
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Show Linked image overlay', 'Elise' ),
            'param_name' => 'overlay',
            'description' => __( 'If linked, show image overlay', 'Elise' ),
            'value' => array( __( 'Yes, please', 'Elise' ) => 'yes' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),
        // array(
        //     "type" => "textarea_html",
        //     "heading" => __("Image Caption", "Elise"),
        //     "param_name" => "caption",
        //     // 'value' => '',
        //     // "holder"    => "p",
        //     // "admin_label" => true,
        //     'group' => __( 'Caption', 'js_composer' )
        // ),
        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Image style', 'js_composer' ),
        //     'param_name' => 'caption_position',
        //     'value' => array(
        //         __( 'Bottom Left', 'Elise' ) => 'c-bottom-left',
        //         __( 'Bottom Right', 'Elise' ) => 'c-bottom-right',
        //         __( 'Top Left', 'Elise' ) => 'c-top-left',
        //         __( 'Top Right', 'Elise' ) => 'c-top-right',
        //     ),
        //     'std' => 'c-bottom-left',
        //     'group' => __( 'Caption', 'js_composer' )
        // ),
        array(
            'type' => 'css_editor',
            'heading' => __( 'Css', 'js_composer' ),
            'param_name' => 'css',
            // 'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
            'group' => __( 'Design options', 'js_composer' )
        ),
    )
) );

/* Gallery/Slideshow
---------------------------------------------------------- */
vc_map( array(
    'name' => __( 'Image Gallery', 'js_composer' ),
    'base' => 'vc_gallery',
    'icon' => 'icon-wpb-images-stack',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Responsive image gallery', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Widget title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Gallery type', 'js_composer' ),
            'param_name' => 'type',
            'value' => array(
                __( 'Carousel', 'Elise' ) => 'carousel',
                __( 'Image grid', 'Elise' ) => 'image_grid'
            ),
            'description' => __( 'Select gallery type.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Carousel Navigation', 'js_composer' ),
            'param_name' => 'navigation',
            'value' => array(
                __( 'Thumbnails', 'Elise' ) => 'nav_thumbs',
                __( 'Bullets', 'Elise' ) => 'nav_bullets',
                __( 'None', 'Elise' ) => 'nav_none'
            ),
            'dependency' => array(
                'element' => 'type',
                'value' => 'carousel',
            ),
            'std' => 'nav_thumbs',
            'description' => __( 'Select gallery type.', 'js_composer' )
        ),
        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Auto rotate slides', 'js_composer' ),
        //     'param_name' => 'interval',
        //     'value' => array( 3, 5, 10, 15, __( 'Disable', 'js_composer' ) => 0 ),
        //     'description' => __( 'Auto rotate slides each X seconds.', 'js_composer' ),
        //     'dependency' => array(
        //         'element' => 'type',
        //         'value' => array( 'flexslider_fade', 'flexslider_slide', 'nivo' )
        //     )
        // ),
        array(
            'type' => 'attach_images',
            'heading' => __( 'Images', 'js_composer' ),
            'param_name' => 'images',
            'value' => '',
            'description' => __( 'Select images from media library.', 'js_composer' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Image size', 'js_composer' ),
            'param_name' => 'img_size',
            'description' => __( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'On click', 'js_composer' ),
            'param_name' => 'onclick',
            'value' => array(
                __( 'Open Full Image/Magnific Popup', 'Elise' ) => 'link_image',
                __( 'Do nothing', 'js_composer' ) => 'link_no',
                __( 'Open custom link', 'js_composer' ) => 'custom_link'
            ),
            'description' => __( 'Define action for onclick event if needed.', 'js_composer' )
        ),
        array(
            'type' => 'exploded_textarea',
            'heading' => __( 'Custom links', 'js_composer' ),
            'param_name' => 'custom_links',
            'description' => __( 'Enter links for each slide here. Divide links with linebreaks (Enter) . ', 'js_composer' ),
            'dependency' => array(
                'element' => 'onclick',
                'value' => array( 'custom_link' )
            )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Custom link target', 'js_composer' ),
            'param_name' => 'custom_links_target',
            'description' => __( 'Select where to open  custom links.', 'js_composer' ),
            'dependency' => array(
                'element' => 'onclick',
                'value' => array( 'custom_link' )
            ),
            'value' => $target_arr
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        )
    )
) );

vc_map( array(
    'name' => __( 'Button', 'js_composer' ),
    'base' => 'vc_button2',
    'icon' => 'icon-wpb-ui-button',
    'category' => array(
        __( 'Content', 'js_composer' )),
    'description' => __( 'Eye catching button', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'js_composer' ),
            'param_name' => 'link',
            'description' => __( 'Button link.', 'js_composer' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Text on the button', 'js_composer' ),
            'holder' => 'button',
            'class' => 'vc_btn',
            'param_name' => 'title',
            'value' => __( 'Text on the button', 'js_composer' ),
            'description' => __( 'Text on the button.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Style', 'js_composer' ),
            'param_name' => 'style',
            'value' => array(
                __( 'Standard (Rounded: 3px)', 'Elise' ) => 'btn-standard',
                __( 'Rounded', 'Elise' ) => 'btn-rounded',
                __( 'Squared', 'Elise' ) => 'btn-squared',
                __( 'Outlined', 'Elise' ) => 'btn-outlined',
                __( 'Rounded Outlined', 'Elise' ) => 'btn-outlined btn-rounded',
            ),
            'description' => __( 'Button style.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Color', 'js_composer' ),
            'param_name' => 'color',
            'value' => array(
                __( 'Default (Accent Color)', 'Elise' ) => 'btn-default',
                __( 'Success', 'Elise' ) => 'btn-success',
                __( 'Info', 'Elise' ) => 'btn-info',
                __( 'Warning', 'Elise' ) => 'btn-warning',
                __( 'Danger', 'Elise' ) => 'btn-danger',
                __( 'Light', 'Elise' ) => 'btn-light',
                __( 'Dark', 'Elise' ) => 'btn-dark',
            ),
            'description' => __( 'Button color.', 'js_composer' ),
            // 'param_holder_class' => 'vc_colored-dropdown'
        ),
        /*array(
        'type' => 'dropdown',
        'heading' => __( 'Icon', 'js_composer' ),
        'param_name' => 'icon',
        'value' => getVcShared( 'icons' ),
        'description' => __( 'Button icon.', 'js_composer' )
  ),*/
        array(
            'type' => 'dropdown',
            'heading' => __( 'Size', 'js_composer' ),
            'param_name' => 'size',
            'value' => array(
                __( 'Large', 'Elise' ) => 'btn-lg',
                __( 'Standard', 'Elise' ) => 'btn-md',
                __( 'Small', 'Elise' ) => 'btn-sm',
                __( 'Extra small', 'Elise' ) => 'btn-xs',
            ),
            'std' => 'btn-md',
            'description' => __( 'Button size.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Alignment', 'Elise' ),
            'param_name' => 'alignment',
            'value' => array(
                __( 'Left', 'Elise' ) => ' btn-left ',
                __( 'Center', 'Elise' ) => ' btn-center ',
                __( 'Right', 'Elise' ) => ' btn-right ',
            ),
            'std' => ' btn-left ',
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        )
    ),
    'js_view' => 'VcButton2View'
) );


vc_map( array(
    'name' => __( 'Message Box', 'js_composer' ),
    'base' => 'vc_message',
    'icon' => 'icon-wpb-information-white',
    'wrapper_class' => 'alert',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Notification box', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'dropdown',
            'heading' => __( 'Message box type', 'js_composer' ),
            'param_name' => 'color',
            'value' => array(
                __( 'Informational', 'js_composer' ) => 'alert-info',
                __( 'Warning', 'js_composer' ) => 'alert-warning',
                __( 'Success', 'js_composer' ) => 'alert-success',
                __( 'Error', 'js_composer' ) => "alert-danger"
            ),
            'description' => __( 'Select message type.', 'js_composer' ),
            'param_holder_class' => 'vc_message-type'
        ),
        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Style', 'js_composer' ),
        //     'param_name' => 'style',
        //     'value' => getVcShared( 'alert styles' ),
        //     'description' => __( 'Alert style.', 'js_composer' )
        // ),
        array(
            'type' => 'textarea_html',
            'holder' => 'div',
            'class' => 'messagebox_text',
            'heading' => __( 'Message text', 'js_composer' ),
            'param_name' => 'content',
            'value' => __( '<p>I am message box. Click edit button to change this text.</p>', 'js_composer' )
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Background Color', 'Elise' ),
            'param_name'  => 'bg_color',
            // 'description' => __( 'Custom separator color for your element.', 'js_composer' ),
            // 'dependency'  => array(
            //  'element' => 'color',
            //  'value'   => array( 'custom' )
            // ),
            'group' => __( 'Design options', 'js_composer' )
        ),
    ),
    'js_view' => 'VcMessageView'
) );

vc_map( array(
    'name' => __( 'Call to Action Box', 'Elise' ),
    'base' => 'vc_cta_button2',
    'icon' => 'icon-wpb-call-to-action',
    'category' => array( __( 'Content', 'js_composer' ) ),
    'description' => __( 'Catch visitors attention with CTA block', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Custom Background Color', 'js_composer' ),
            'param_name' => 'accent_color',
            'description' => __( 'Select background color for your element.', 'js_composer' )
        ),
        array(
            'type' => 'textarea_html',
            //holder' => 'div',
            //'admin_label' => true,
            'heading' => __( 'Promotional text', 'js_composer' ),
            'holder' => 'p',
            'param_name' => 'content',
            'value' => __( 'I am promo text. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'js_composer' )
        ),
        array(
            'type' => 'vc_link',
            'heading' => __( 'URL (Link)', 'js_composer' ),
            'param_name' => 'link',
            'description' => __( 'Button link.', 'js_composer' )
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Text on the button', 'js_composer' ),
            //'holder' => 'button',
            //'class' => 'wpb_button',
            'param_name' => 'title',
            'value' => __( 'Text on the button', 'js_composer' ),
            'description' => __( 'Text on the button.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Button style', 'js_composer' ),
            'param_name' => 'btn_style',
            'value' => array(
                __( 'Standard (Rounded: 3px)', 'Elise' ) => 'btn-standard',
                __( 'Rounded', 'Elise' ) => 'btn-rounded',
                __( 'Squared', 'Elise' ) => 'btn-squared',
                __( 'Outlined', 'Elise' ) => 'btn-outlined',
                __( 'Rounded Outlined', 'Elise' ) => 'btn-outlined btn-rounded',
            ),
            'std' => 'btn-standard',
            'description' => __( 'Button style.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Color', 'js_composer' ),
            'param_name' => 'color',
            'value' => array(
                __( 'Default (Accent Color)', 'Elise' ) => 'btn-default',
                __( 'Success', 'Elise' ) => 'btn-success',
                __( 'Info', 'Elise' ) => 'btn-info',
                __( 'Warning', 'Elise' ) => 'btn-warning',
                __( 'Danger', 'Elise' ) => 'btn-danger',
                __( 'Light', 'Elise' ) => 'btn-light',
                __( 'Dark', 'Elise' ) => 'btn-dark',
            ),
            'description' => __( 'Button color.', 'js_composer' ),
            'std' => 'btn-default',
            'param_holder_class' => 'vc_colored-dropdown'
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Size', 'js_composer' ),
            'param_name' => 'size',
            'value' => array(
                __( 'Large', 'Elise' ) => 'btn-lg',
                __( 'Standard', 'Elise' ) => 'btn-md',
                __( 'Small', 'Elise' ) => 'btn-sm',
                __( 'Extra small', 'Elise' ) => 'btn-xs',
            ),
            'std' => 'btn-md',
            'description' => __( 'Button size.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Button position', 'js_composer' ),
            'param_name' => 'position',
            'value' => array(
                __( 'Align right', 'js_composer' ) => 'right',
                __( 'Align left', 'js_composer' ) => 'left',
                __( 'Align bottom', 'js_composer' ) => 'bottom'
            ),
            'std' => 'bottom',
            'description' => __( 'Select button alignment.', 'js_composer' )
        ),
        $add_css_animation,
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        )
    )
) );

/* Textual block
---------------------------------------------------------- */
vc_map( array(
    'name' => __( 'Separator', 'Elise' ),
    'base' => 'vc_text_separator',
    'icon' => 'icon-wpb-ui-separator-label',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Horizontal separator line with heading', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'dropdown',
            'heading' => __( 'Style', 'js_composer' ),
            'param_name' => 'style',
            'value' => array(
                __('Border', 'Elise') => '',
                __('Dashed', 'Elise') => 'dashed',
                __('Dotted', 'Elise') => 'dotted',
                __('Double', 'Elise') => 'double',
                __('Wave', 'Elise') => 'wave',
                __('Shadow', 'Elise') => 'shadow',
            ),
            'description' => __( 'Separator style.', 'js_composer' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Separator width', 'Elise' ),
            'param_name' => 'el_width',
            'value' => array(
                __('Wide (100% width)', 'Elise') => 'wide',
                __('Medium (50% width)', 'Elise') => 'medium',
                __('Small (20% width)', 'Elise') => 'small',
            ),
            'description' => __( 'Separator element width.', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Separator alignment', 'Elise' ),
            'param_name' => 'sep_position',
            'value' => array(
                __('Left', 'Elise') => 'left',
                __('Center', 'Elise') => 'center',
                __('Right', 'Elise') => 'right',
            ),
            'std' => 'center',
            'dependency' => array( 'element' => 'el_width', 'value' => array('medium', 'small')),
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon library', 'js_composer' ),
            'group' => __( 'Text separator', 'Elise' ),
            // "admin_label" => true,
            'holder' => 'div',
            'value' => array(
                __( 'No Icon', 'Elise' ) => '',
                __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
                __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                __( 'Typicons', 'js_composer' ) => 'typicons',
                __( 'Entypo', 'js_composer' ) => 'entypo',
                __( 'Linecons', 'js_composer' ) => 'linecons',
            ),
            'param_name' => 'icon',
            'description' => __( 'Select icon library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_fontawesome',
            'group' => __( 'Text separator', 'Elise' ),
            // "admin_label" => true,
            // 'holder' => 'div',
            'value' => '',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'fontawesome',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_openiconic',
            'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'openiconic',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'openiconic',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_typicons',
            'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'typicons',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
            'element' => 'icon',
            'value' => 'typicons',
        ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_entypo',
            'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'entypo',
                'iconsPerPage' => 300, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'entypo',
            ),
        ),
        array(
            'type' => 'iconpicker',
            'heading' => __( 'Icon', 'js_composer' ),
            'param_name' => 'icon_linecons',
            'group' => __( 'Text separator', 'Elise' ),
            // 'holder' => 'div',
            'settings' => array(
                'emptyIcon' => true, // default true, display an "EMPTY" icon?
                'type' => 'linecons',
                'iconsPerPage' => 200, // default 100, how many icons per/page to display
            ),
            'dependency' => array(
                'element' => 'icon',
                'value' => 'linecons',
            ),
            'description' => __( 'Select icon from library.', 'js_composer' ),
        ),

        array(
            'type' => 'textfield',
            'heading' => __( 'Title', 'js_composer' ),
            'param_name' => 'title',
            'holder' => 'div',
            // 'value' => __( 'Title', 'js_composer' ),
            'description' => __( 'Separator title.', 'js_composer' ),
            'group' => __( 'Text separator', 'Elise' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Title position', 'js_composer' ),
            'param_name' => 'title_align',
            'value' => array(
                __( 'Align center', 'js_composer' ) => 'separator_align_center',
                __( 'Align left', 'js_composer' ) => 'separator_align_left',
                __( 'Align right', 'js_composer' ) => "separator_align_right"
            ),
            'description' => __( 'Select title location.', 'js_composer' ),
            'group' => __( 'Text separator', 'Elise' )
        ),

        array(
            'type' => 'checkbox',
            'heading' => __( 'Text/Icon as Back to Top Button', 'Elise' ),
            'param_name' => 'back_to_top',
            'value' => array(
                              __( 'Yes, Please', 'Elise' ) => 'btt-btn',
                        ),
            // 'description' => __( 'Select background style.', 'js_composer' ),
            // 'dependency' => array( 'element' => 'bg_style', 'value' => 'standard'),
            'group' => __( 'Text separator', 'Elise' )
        ),
        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Color', 'js_composer' ),
        //     'param_name' => 'color',
        //     'value' => array_merge( getVcShared( 'colors' ), array( __( 'Custom color', 'js_composer' ) => 'custom' ) ),
        //     'std' => 'grey',
        //     'description' => __( 'Separator color.', 'js_composer' ),
        //     'param_holder_class' => 'vc_colored-dropdown'
        // ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Line Color', 'js_composer' ),
            'param_name'  => 'accent_color',
            'description' => __( 'Custom separator color for your element.', 'js_composer' ),
            'group' => __( 'Design options', 'js_composer' ),
            // 'dependency'  => array(
            //     'element' => 'color',
            //     'value'   => array( 'custom' )
            // ),
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Text Color', 'js_composer' ),
            'param_name'  => 'text_color',
            'description' => __( 'Custom text color.', 'Elise' ),
            'group' => __( 'Design options', 'js_composer' ),
            'dependency'  => array(
                'element' => array('title', 'icon'),
                'not_empty'   => true
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Margin Bottom', 'Elise' ),
            'param_name' => 'margin_bottom',
            // 'holder' => 'div',
            // 'value' => __( 'Title', 'js_composer' ),
            'description' => __( 'Margin Bottom Height in px. E.g. 50', 'Elise' ),
            'group' => __( 'Design options', 'js_composer' ),
        ),
    ),
    'js_view' => 'VcTextSeparatorView'
) );



// Icon Box -----------------------------
vc_map( array(
    "name" => __("Pie Chart", "Elise"),
    "base" => "elise_piechart",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-piechart",
    'description' => __( 'Animated pie chart.', 'Elise' ),
    "show_settings_on_create" => true,
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Value", "Elise"),
            "param_name" => "value",
            "value" => '90',
            'admin_label' => true,
            "description" => __("Pie Value. (0-100 Integer)", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Name", "Elise"),
            "param_name" => "value_text",
            'admin_label' => true,
            "value" => __('Done', 'Elise'),
            "description" => __("Text under value.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Duration", "Elise"),
            "param_name" => "duration",
            'admin_label' => true,
            "value" => '2000',
            // "description" => __("Pie Value. (0-100 Integer)", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Size", "Elise"),
            "param_name" => "size",
            'admin_label' => true,
            "value" => '160',
            "description" => __("Pie Size in pixels.", "Elise")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Pie Thickness", "Elise"),
            "param_name" => "thickness",
            'admin_label' => true,
            "value" => '5',
            "description" => __("Pie Thickness in pixels", "Elise")
        ),
        array(
            'type' => 'checkbox',
            'heading' => __( 'Show "%" Unit', 'Elise' ),
            'param_name' => 'show_unit',
            'value' => array(
                __( 'Yes, please', 'Elise' ) => 'show',
            ),
            'std' => '',
        ),

        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Pie Color options', 'Elise' ),
            'param_name' => 'color_fill',
            'value' => array(
                __( 'Solid Color', 'Elise' ) => 'color',
                __( 'Gradient Color', 'Elise' ) => 'gradient',
            ),
            'std' => 'color',
            'group' => __( 'Design options', 'js_composer' )
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Pie Color', 'Elise' ),
            'param_name'  => 'color_1',
            'value' => '#8dc73f',
            'group' => __( 'Design options', 'js_composer' ),
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Pie Second Color', 'Elise' ),
            'param_name'  => 'color_2',
            'value' => '#3FC7B9',
            'group' => __( 'Design options', 'js_composer' ),
            'dependency' => array( 'element' => 'color_fill', 'value' => 'gradient'),
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Pie Background Color', 'Elise' ),
            'param_name'  => 'color_bg',
            'value' => 'rgba(0,0,0,.1)',
            'group' => __( 'Design options', 'js_composer' ),
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Pie value Color', 'Elise' ),
            'param_name'  => 'value_color',
            'value' => '',
            'group' => __( 'Design options', 'js_composer' ),
        ),

        array(
            'type' => 'dropdown',
            'heading' => __( 'Pie Alignment', 'Elise' ),
            'param_name' => 'align',
            'value' => array(
                __( 'Left', 'Elise' ) => 'left',
                __( 'Center', 'Elise' ) => 'center',
                __( 'Right', 'Elise' ) => 'right',
            ),
            'std' => 'center',
            'group' => __( 'Design options', 'js_composer' )
        ),

    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_piechart extends WPBakeryShortCode {
    }
}





// Google Maps -----------------------------
vc_map( array(
    "name" => __("Google Map", "Elise"),
    "base" => "elise_maps",
    // "as_parent" => array('only' => 'elise_timeline_block'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    // "content_element" => true,
    "icon" => "elise-icon-maps",
    "show_settings_on_create" => true,
    'description' => __( 'Custom google map.', 'Elise' ),
    "params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Address", "Elise"),
            "param_name" => "address",
            "holder" => 'p',
            "value" => '1111 5th Avenue, New York',
            "description" => __("Address or Coordinates here. (E.g. 1111 5th Avenue, New York or 40.785439,-73.959254)", "Elise")
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Map Zoom', 'Elise' ),
            'param_name' => 'zoom',
            'value' => array(
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
                '6' => 6,
                '7' => 7,
                '8' => 8,
                '9' => 9,
                '10' => 10,
                '11' => 11,
                '12' => 12,
                '13' => 13,
                '14' => 14,
                '15' => 15,
                '16' => 16,
                '17' => 17,
                '18' => 18,
                '19' => 19,
                '20' => 20,
            ),
            'std' => 12,
            "admin_label" => true,
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Map Type', 'Elise' ),
            'param_name' => 'map_type',
            'value' => array(
                __('ROADMAP', 'Elise') => 'ROADMAP',
                __('HYBRID', 'Elise') => 'HYBRID',
                __('SATELLITE', 'Elise') => 'SATELLITE',
                __('TERRAIN', 'Elise') => 'TERRAIN',
            ),
            'std' => 'ROADMAP',
            "admin_label" => true,
        ),
        array(
            "type" => "textfield",
            "heading" => __("Map Height", "Elise"),
            "param_name" => "height",
            "value" => 500,
            "admin_label" => true,
            "description" => __('Map height in pixels. Default: 500', 'Elise'),
            // 'group' => __( 'Design options', 'js_composer' ),
        ),
        array(
            'type' => 'attach_image',
            'heading' => __( 'Custom Marker', 'Elise' ),
            'param_name' => 'marker',
            'description' => __( 'Select marker .png image', 'Elise' )
        ),

        array(
            "type" => "textarea_raw_html",
            "heading" => __("Map Style", "Elise"),
            "param_name" => "style",
            "description" => __("JSON style code here. Go to <a href=\"http://snazzymaps.com/\">http://snazzymaps.com/</a> for map styles or create your own.", "Elise"),
            // "holder"    => "p",
            // "admin_label" => true,
            'group' => __( 'Design options', 'js_composer' )
        ),

        $add_css_animation,
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Elise"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Elise")
        ),

    )
) );
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_elise_maps extends WPBakeryShortCode {
    }
}
vc_map( array(
    'name' => __( 'Progress Bar', 'js_composer' ),
    'base' => 'vc_progress_bar',
    'icon' => 'icon-wpb-graph',
    'category' => __( 'Content', 'js_composer' ),
    'description' => __( 'Animated progress bar', 'js_composer' ),
    'params' => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Widget title', 'js_composer' ),
            'param_name' => 'title',
            'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'js_composer' )
        ),
        array(
            'type' => 'exploded_textarea',
            'heading' => __( 'Graphic values', 'js_composer' ),
            'param_name' => 'values',
            'description' => __( 'Input graph values, titles and color here. Divide values with linebreaks (Enter). Example: 90|Development|#e75956', 'js_composer' ),
            'value' => "90|Development,80|Design,70|Marketing"
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Units', 'js_composer' ),
            'param_name' => 'units',
            'value' => '%',
            'description' => __( 'Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.', 'js_composer' )
        ),
        // array(
        //     'type' => 'dropdown',
        //     'heading' => __( 'Bar color', 'js_composer' ),
        //     'param_name' => 'bgcolor',
        //     'value' => array(
        //         __( 'Grey', 'js_composer' ) => 'bar_grey',
        //         __( 'Blue', 'js_composer' ) => 'bar_blue',
        //         __( 'Turquoise', 'js_composer' ) => 'bar_turquoise',
        //         __( 'Green', 'js_composer' ) => 'bar_green',
        //         __( 'Orange', 'js_composer' ) => 'bar_orange',
        //         __( 'Red', 'js_composer' ) => 'bar_red',
        //         __( 'Black', 'js_composer' ) => 'bar_black',
        //         __( 'Custom Color', 'js_composer' ) => 'custom'
        //     ),
        //     'description' => __( 'Select bar background color.', 'js_composer' ),
        //     'admin_label' => true
        // ),
        array(
            'type' => 'colorpicker',
            'heading' => __( 'Bar custom color', 'js_composer' ),
            'param_name' => 'custombgcolor',
            'description' => __( 'Select custom background color for bars.', 'js_composer' ),
            // 'dependency' => array( 'element' => 'bgcolor', 'value' => array( 'custom' ) )
        ),
        // array(
        //     'type' => 'checkbox',
        //     'heading' => __( 'Options', 'js_composer' ),
        //     'param_name' => 'options',
        //     'value' => array(
        //         __( 'Add Stripes?', 'js_composer' ) => 'striped',
        //         __( 'Add animation? Will be visible with striped bars.', 'js_composer' ) => 'animated'
        //     )
        // ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Extra class name', 'js_composer' ),
            'param_name' => 'el_class',
            'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
        )
    )
) );








?>