<?php 

function minify( $css ) {
	// Normalize whitespace
	$css = preg_replace( '/\s+/', ' ', $css );
	// Remove spaces before and after comment
	$css = preg_replace( '/(\s+)(\/\*(.*?)\*\/)(\s+)/', '$2', $css );
	// Remove comment blocks, everything between /* and */, unless
	// preserved with /*! ... */ or /** ... */
	$css = preg_replace( '~/\*(?![\!|\*])(.*?)\*/~', '', $css );
	// Remove ; before }
	$css = preg_replace( '/;(?=\s*})/', '', $css );
	// Remove space after , : ; { } */ >
	$css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );
	// Remove space before , ; { } ( ) >
	$css = preg_replace( '/ (,|;|\{|}|\(|\)|>)/', '$1', $css );
	// Strips leading 0 on decimal values (converts 0.5px into .5px)
	$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );
	// Strips units if value is 0 (converts 0px to 0)
	$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );
	// Converts all zeros value into short-hand
	$css = preg_replace( '/0 0 0 0/', '0', $css );
	// Shortern 6-character hex color codes to 3-character where possible
	$css = preg_replace( '/#([a-f0-9])\\1([a-f0-9])\\2([a-f0-9])\\3/i', '#\1\2\3', $css );
	return trim( $css );
}

function checkHex( $hex ) {
    // Strip # sign is present
    $color = str_replace("#", "", $hex);
    // Make sure it's 6 digits
    if( strlen($color) == 3 ) {
        $color = $color[0].$color[0].$color[1].$color[1].$color[2].$color[2];
    } else if( strlen($color) != 6 ) {
        // throw new Exception("You leave some color field with empty value!");
    }
    return $color;
}


function elise_hexConvert( $color, $alpha = NULL, $cssoutput = NULL, $hueshift = NULL ) {

    // Sanity check
    $color = checkHex($color);

    // Convert HEX to DEC
    $R = hexdec($color[0].$color[1]);
    $G = hexdec($color[2].$color[3]);
    $B = hexdec($color[4].$color[5]);

    $HSLA = array();
    $RGBA = array();

    $var_R = ($R / 255);
    $var_G = ($G / 255);
    $var_B = ($B / 255);

    $var_Min = min($var_R, $var_G, $var_B);
    $var_Max = max($var_R, $var_G, $var_B);
    $del_Max = $var_Max - $var_Min;

    $L = ($var_Max + $var_Min)/2;

    if ($del_Max == 0)
    {
        $H = 0;
        $S = 0;
    }
    else
    {
        if ( $L < 0.5 ) $S = $del_Max / ( $var_Max + $var_Min );
        else            $S = $del_Max / ( 2 - $var_Max - $var_Min );

        $del_R = ( ( ( $var_Max - $var_R ) / 6 ) + ( $del_Max / 2 ) ) / $del_Max;
        $del_G = ( ( ( $var_Max - $var_G ) / 6 ) + ( $del_Max / 2 ) ) / $del_Max;
        $del_B = ( ( ( $var_Max - $var_B ) / 6 ) + ( $del_Max / 2 ) ) / $del_Max;

        if      ($var_R == $var_Max) $H = $del_B - $del_G;
        else if ($var_G == $var_Max) $H = ( 1 / 3 ) + $del_R - $del_B;
        else if ($var_B == $var_Max) $H = ( 2 / 3 ) + $del_G - $del_R;

        if ($H<0) $H++;
        if ($H>1) $H--;
    }

    if ( !empty($alpha) ) {
    	$A = $alpha;
    } else {
    	$A = 1;
    }

    $RGBA['R'] = $R;
    $RGBA['G'] = $G;
    $RGBA['B'] = $B;
    $RGBA['A'] = $A;

    $HSLA['H'] = (!empty($hueshift) ? round($H*360, 0) + $hueshift : round($H*360, 0));
    $HSLA['S'] = round($S, 2) * 100 . '%';
    $HSLA['L'] = round($L, 2) * 100 . '%';
    $HSLA['A'] = $A;

    if ($cssoutput == true || !empty($cssoutput)) {
    	$output = 'hsla('.implode(', ', $HSLA).')';
    } 
    else {
    	$output = 'rgba('.implode(', ', $RGBA).')';
    }
    	
    return $output;  
}

// Styles
function custom_style_inline() {
	global $elise_options;


	$c_css = '';

	if ($elise_options['opt-navbar-height'] != 90) {
		// if ($elise_options['opt-hover-style'] == 1 && $elise_options['opt-navbar-transparent'] != 1) {
		// 	$c_css .= '
		// 		#navbar { height: '. $elise_options['opt-navbar-height'] .'px; }
		// 		.main-nav ul li a, .nav-container {	line-height: '. $elise_options['opt-navbar-height'] .'px; }
		// 		';
		// }
		// elseif ($elise_options['opt-hover-style'] == 1 || $elise_options['opt-navbar-transparent'] == 1) {
		// 	$c_css .= '
		// 		#navbar { height: '. $elise_options['opt-navbar-height'] .'px; }
		// 	';
		// }
		$c_css .= '
			#navbar { height: '. $elise_options['opt-navbar-height'] .'px; }
			.main-nav ul li:not(.nav-icons) a {	line-height: '. $elise_options['opt-navbar-height'] .'px; }
			';
	} // edit
	
	if ($elise_options['opt-navbar-style'] == 2) {
		if ($elise_options['opt-header-height'] != 150) {
			$c_css .= '
				.header-centered #navbar { height: '. $elise_options['opt-header-height'] .'px; }
			';
		}
	}

	if ($elise_options['opt-navbar-transparent'] == 1 && $elise_options['opt-title-bar'] == 0) {
		if ($elise_options['opt-navbar-style'] != 3) {
			$c_css .= '
				.page-navigation { display: none; }
			';
		}
	}

	if ($elise_options['opt-navbar-style'] == 4 && $elise_options['opt-title-bar'] == 0) {
		$c_css .= '
			.page-navigation { display: none; }
		';
	}

	// title bar
	// -- title bar height
	if ($elise_options['opt-title-bar'] == 1) {
	
		// if ($elise_options['opt-title-bar-height'] != 120 && $elise_options['opt-navbar-transparent'] == 0 ) {
		// 	$c_css .= '
		// 		.page-title-container { height: '. $elise_options['opt-title-bar-height'] .'px !important }
		// 	';
		// }

		$title_bar_padding = $elise_options['opt-title-bar-padding'];

		// print_r($title_bar_padding);

		if ($title_bar_padding['padding-top'] != '45px' || $title_bar_padding['padding-bottom'] != '45px') {
			$c_css .= '
				.page-title .title-wrap {
					padding-top: '. $title_bar_padding['padding-top'] .';
					padding-bottom: '. $title_bar_padding['padding-bottom'] .';
				}
			';
		}

		if ($elise_options['opt-navbar-transparent'] == 1) {
			if ($elise_options['opt-navbar-style'] == 1 && $elise_options['opt-navbar-height'] != 90) {
				$c_css .= '
					.header-standard.header-transparent .page-title-container, 
					.header-fullwidth.header-transparent .page-title-container {
						padding-top: '. $elise_options['opt-navbar-height'] .'px;
					}
				';
			}
			elseif ($elise_options['opt-navbar-style'] == 2 && $elise_options['opt-header-height'] != 150) {
				$c_css .= '
					.header-centered.header-transparent .page-title-container {
						padding-top: '. $elise_options['opt-header-height'] .'px;
					}
				';
			}
		}

		// if ($elise_options['opt-title-bar-height'] != 120 && ($elise_options['opt-navbar-transparent'] == 1 || $elise_options['opt-navbar-style'] == 4 ) && $elise_options['opt-navbar-style'] != 3 ) {
		// 	if ($elise_options['opt-navbar-style'] != 2) { 
		// 	$nav_head_height = 'opt-navbar-height'; }
		// 	else { 
		// 	$nav_head_height = 'opt-header-height'; }

		// 	$bar_extra_padding = 0;
		// 	if ($elise_options['opt-navbar-style'] == 4) { $bar_extra_padding = 10; }

		// 	$c_css .= '
		// 		.page-title-container { height: '. ($elise_options['opt-title-bar-height'] + $elise_options[$nav_head_height] + $bar_extra_padding ).'px !important;
		// 			padding-top: '. ( $elise_options[$nav_head_height] + $bar_extra_padding ) .'px !important; }
		// 	';
		// }

	}

	if ($elise_options['opt-content-width'] != 1176) {
		if ($elise_options['opt-responsive'] == true ) {
			$c_css .= '
				@media (min-width: 1200px) {
					.container,
					.vc_elise_row_fullwidth,
					.header-standard.header-full-width .nav-menu-left .nav-container {
						width: '. $elise_options['opt-content-width'] .'px;
					}
				}
			';
		} 
	}

	if ($elise_options['opt-responsive'] == false) {
		$c_css .= '
			.elise-non-responsive {
				min-width: '. $elise_options['opt-content-width'] .'px;
			}
			.container,
			.vc_elise_row_fullwidth,
			.header-standard.header-full-width .nav-menu-left .nav-container {
				width: '. $elise_options['opt-content-width'] .'px;
			}
			@media (min-width: 992px) {
				.container,
				.vc_elise_row_fullwidth,
				.header-standard.header-full-width .nav-menu-left .nav-container {
					width: '. $elise_options['opt-content-width'] .'px !important;
				}
			}
			@media (min-width: 768px) {
				.container,
				.vc_elise_row_fullwidth,
				.header-standard.header-full-width .nav-menu-left .nav-container {
					width: '. $elise_options['opt-content-width'] .'px !important;
				}
			}
			@media (min-width: 1200px) {
				.container,
				.vc_elise_row_fullwidth,
				.header-standard.header-full-width .nav-menu-left .nav-container {
					width: '. $elise_options['opt-content-width'] .'px !important;
				}
			}
		';
	}


	// Content padding
	$content_padding = $elise_options['opt-content-padding'];

	if($content_padding['padding-top'] != '55px' || $content_padding['padding-bottom'] != '60px') {
		$c_css .= '
			.section {
				padding: '. $content_padding['padding-top'] .' 0 '. $content_padding['padding-bottom'] .' 0;
			}
		';
	}



	// portfolio items gap
	$portfolio_items_gap = $elise_options['opt-portfolio-items-gap'];

	if ($portfolio_items_gap != 30) {
		$c_css .= '
			.portfolio-items--container .row {
				margin-right: '. - $portfolio_items_gap / 2 .'px;
				margin-left: '. - $portfolio_items_gap / 2 .'px;
			}

			.portfolio-items--container .row [class*="col-"] {
				padding-left: '. $portfolio_items_gap / 2 .'px;
				padding-right: '. $portfolio_items_gap / 2 .'px;
				margin-bottom: '. $portfolio_items_gap / 2 .'px;
				margin-top: '. $portfolio_items_gap / 2 .'px;
			}
		';

		if ($elise_options['opt-portfolio-fullwidth'] == true) {
			$c_css .= '
				.portfolio-fullwidth {
					padding: '. $portfolio_items_gap / 2 .'px '. $portfolio_items_gap .'px;
				}
			';
		}
	}

	if (is_page_template('portfolio.php') && $elise_options['opt-portfolio-fullwidth'] == false) {
		if($content_padding['padding-top'] != '55px' || $content_padding['padding-bottom'] != '60px' || $portfolio_items_gap != 30) {
			$c_css .= '
				.section.portfolio {
					padding: '. ($content_padding['padding-top'] - $portfolio_items_gap / 2) .'px 0 '. ($content_padding['padding-bottom'] - $portfolio_items_gap / 2) .'px 0;
				}
			';
		}
	}

	// Custom Typography
	function body_typography() {
		global $elise_options;

		//Body
		$body_typography = $elise_options['opt-typo-body'];
		
		$body_c_css = '';
		if ($body_typography['font-family'] != 'Raleway') {
			$body_c_css .= '
					font-family: "'.$body_typography['font-family'].'";
				';
		}
		if ($body_typography['font-weight'] != '400' && !empty($body_typography['font-weight'])) {
			$body_c_css .= '
					font-weight: '.$body_typography['font-weight'].';
				';
		}
		if ($body_typography['font-size'] != '14px' && !empty($body_typography['font-size'])) {
			$body_c_css .= '
					font-size: '.$body_typography['font-size'].';
				';
		}
		if ($body_typography['line-height'] != '24px' && !empty($body_typography['line-height'])) {
			$body_c_css .= '
					line-height: '.$body_typography['line-height'].';
				';
		}

		return $body_c_css;
	}

	if (body_typography()) {
		$c_css .= '
			body {
				'. body_typography() .'
			}
		';
	}

	// Headings Typography Basic
	function headings_typo_basic($typo_attr) {
		global $elise_options;

		//Headings Basic
		$headings_typo_basic = $elise_options['opt-typo-basic-headings'];

		$h_c_css = '';
		if ($typo_attr == 'font-family' || $typo_attr == 'all' ) {
			if ($headings_typo_basic['font-family'] != 'Raleway') {
				$h_c_css .= '
						font-family: "'.$headings_typo_basic['font-family'].'";
					';
			}
		}
		if ($typo_attr == 'font-weight' || $typo_attr == 'all' ) {
			if ($headings_typo_basic['font-weight'] != '300' && !empty($headings_typo_basic['font-weight'])) {
				$h_c_css .= '
						font-weight: '.$headings_typo_basic['font-weight'].';
					';
			}
		}
		if ($typo_attr == 'font-style' || $typo_attr == 'all' ) {
			if ($headings_typo_basic['font-style'] != 'normal' && !empty($headings_typo_basic['font-style'])) {
				$h_c_css .= '
						font-style: '.$headings_typo_basic['font-style'].';
					';
			}
		}
		if ($typo_attr == 'text-transform' || $typo_attr == 'all' ) {
			if (isset($headings_typo_basic['text-transform']) && $headings_typo_basic['text-transform']) {
				$h_c_css .= '
						text-transform: '.$headings_typo_basic['text-transform'].';
					';
			}
		}
		if ($typo_attr == 'letter-spacing' ) {
			if ($headings_typo_basic['letter-spacing'] != '-1px' && !empty($headings_typo_basic['letter-spacing'])) {
				$h_c_css .= '
						letter-spacing: '.$headings_typo_basic['letter-spacing'].';
					';
			}
		}
		return $h_c_css;
	}

	if (headings_typo_basic('all')) {
		$c_css .= '
			h1, h2, h3, h4, h5, .elise-counter {
				'. headings_typo_basic('font-family') .'
				'. headings_typo_basic('font-weight') .'
				'. headings_typo_basic('font-style') .'
				'. headings_typo_basic('text-transform') .'
			}

			h6, small, label 
			.format-link .link,
			.format-quote blockquote footer {
				'. headings_typo_basic('font-family') .'
			}

			.widget_recent_posts_tab .recent_posts li a, 
			.widget_recent_posts_tab .popular_posts li a, 
			.widget_recent_posts_tab .recent_comments li a,
			.widget_recent_posts_tab .recent_posts li a, 
			.widget_recent_posts_tab .popular_posts li a, 
			.widget_recent_posts_tab .recent_comments li a,
			.widget_calendar #wp-calendar caption,
			.widget_recent_comments ul li .url, 
			.widget_recent_entries ul li .url, 
			.widget_rss ul li .url,
			.widget_rss ul li a,
			.widget_recent_comments ul li a, 
			.widget_recent_entries ul li a, .widget_rss ul li a {
				'. headings_typo_basic('font-family') .'
				'. headings_typo_basic('text-transform') .'
			}
		';
	}
	if (headings_typo_basic('letter-spacing')) {
		$c_css .= '
			h1, h2, h3 {
				'. headings_typo_basic('letter-spacing') .'
			}
		';
	}

	function headings_basic_single($hsize) {
		global $elise_options;

		$h_basic = $elise_options['opt-typo-basic-'.$hsize.''];

		if ($hsize == 'h1' ) {
			$h_fs_def = '40px';
			$h_lh_def = '48px';
		}
		elseif ($hsize == 'h2' ) {
			$h_fs_def = '32px';
			$h_lh_def = '38px';
		}
		elseif ($hsize == 'h3' ) {
			$h_fs_def = '26px';
			$h_lh_def = '30px';
		}
		elseif ($hsize == 'h4' ) {
			$h_fs_def = '18px';
			$h_lh_def = '28px';
		}
		elseif ($hsize == 'h5' ) {
			$h_fs_def = '15px';
			$h_lh_def = '24px';
		}
		elseif ($hsize == 'h6' ) {
			$h_fs_def = '13px';
			$h_lh_def = '21px';
		}

		$hsingle_c_css = '';
		if ($h_basic['font-size'] != $h_fs_def && !empty($h_basic['font-size'])) {
			$hsingle_c_css .= '
					font-size: '. $h_basic['font-size'] .';
			';
		}
		if ($h_basic['line-height'] != $h_lh_def && !empty($h_basic['line-height'])) {
			$hsingle_c_css .= '
					line-height: '. $h_basic['line-height'] .';
			';
		}

		return $hsingle_c_css;
	}

	if (headings_basic_single('h1')) {
		$c_css .= 'h1, .elise-counter, .hero {'.headings_basic_single('h1'). '}';
	}
	if (headings_basic_single('h2')) {
		$c_css .= 'h2 {'.headings_basic_single('h2'). '}';
	}
	if (headings_basic_single('h3')) {
		$c_css .= 'h3 {'.headings_basic_single('h3'). '}';
	}
	if (headings_basic_single('h4')) {
		$c_css .= 'h4 {'.headings_basic_single('h4'). '}';
	}
	if (headings_basic_single('h5')) {
		$c_css .= 'h5 {'.headings_basic_single('h5'). '}';
	}
	if (headings_basic_single('h6')) {
		$c_css .= 'h6, small {'.headings_basic_single('h6'). '}';
	}

	// Main Nav Typo 
	function main_nav_typography() {
		global $elise_options;

		$main_nav_typo = $elise_options['opt-typo-main-nav'];
		$mn_css = '';

		if ($main_nav_typo['font-family'] != 'Raleway') {
			$mn_css .= 'font-family: '. $main_nav_typo['font-family'] .'; ';
		}
		if ($main_nav_typo['font-size'] != '14px') {
			$mn_css .= 'font-size: '. $main_nav_typo['font-size'] .'; ';
		}
		if ($main_nav_typo['font-weight'] != '400' && !empty($main_nav_typo['font-weight'])) {
			$mn_css .= 'font-weight: '. $main_nav_typo['font-weight'] .'; ';
		}
		if (!empty($main_nav_typo['text-transform'])) {
			$mn_css .= 'text-transform: '. $main_nav_typo['text-transform'] .'; ';
		}
		if ($main_nav_typo['letter-spacing'] != '0px') {
			$mn_css .= 'letter-spacing: '. $main_nav_typo['letter-spacing'] .'; ';
		}

		return $mn_css;
	}

	if (main_nav_typography()) {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) a,
			.sidebar-nav ul li a {
				'.main_nav_typography().'
			}
		';
	}

	function main_nav_submenu_typography() {
		global $elise_options;

		$main_nav_sub_typo = $elise_options['opt-typo-main-submenu'];
		$main_nav_typo = $elise_options['opt-typo-main-nav'];
		$mn_css = '';

		if ($main_nav_sub_typo['font-size'] != '12px') {
			$mn_css .= 'font-size: '. $main_nav_sub_typo['font-size'] .'; ';
		}
		if (!empty($main_nav_sub_typo['font-weight'])) {
			$mn_css .= 'font-weight: '. $main_nav_sub_typo['font-weight'] .'; ';
		}
		if (!empty($main_nav_sub_typo['text-transform'])) {
			$mn_css .= 'text-transform: '. $main_nav_sub_typo['text-transform'] .'; ';
		}
		if ($main_nav_sub_typo['letter-spacing'] != $main_nav_typo['letter-spacing']) {
			$mn_css .= 'letter-spacing: '. $main_nav_sub_typo['letter-spacing'] .'; ';
		}

		return $mn_css;
	}


	if (main_nav_submenu_typography()) {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) ul li a {
				'.main_nav_submenu_typography().'
			}
		';
	}
	// Main Nav Typo 
	function full_width_typo() {
		global $elise_options;

		$full_width_typo = $elise_options['opt-typo-fullw-nav'];
		$fw_css = '';

		if ($full_width_typo['font-family'] != 'Raleway') {
			$fw_css .= 'font-family: '. $full_width_typo['font-family'] .'; ';
		}
		if ($full_width_typo['font-size'] != '30px') {
			$fw_css .= 'font-size: '. $full_width_typo['font-size'] .'; ';
		}
		if ($full_width_typo['line-height'] != '18px') {
			$fw_css .= 'line-height: '. $full_width_typo['line-height'] .'; ';
		}
		if ($full_width_typo['font-weight'] != '300' && !empty($full_width_typo['font-weight'])) {
			$fw_css .= 'font-weight: '. $full_width_typo['font-weight'] .'; ';
		}
		if (!empty($full_width_typo['text-transform'])) {
			$fw_css .= 'text-transform: '. $full_width_typo['text-transform'] .'; ';
		}
		if ($full_width_typo['letter-spacing'] != '-1px') {
			$fw_css .= 'letter-spacing: '. $full_width_typo['letter-spacing'] .'; ';
		}

		return $fw_css;
	}

	if (full_width_typo()) {
		$c_css .= '
			.full-nav ul li a {
				'.full_width_typo().'
			}
		';
	}

	// Page Title
	if ($elise_options['opt-custom-page-title-typo'] == true ) {
		function page_title_typo() {
			global $elise_options;

			$page_title_typo = $elise_options['opt-page-title-heading'];
			$fw_css = '';

			if ($page_title_typo['font-family'] != 'Raleway') {
				$fw_css .= 'font-family: "'. $page_title_typo['font-family'] .'"; ';
			}
			if ($page_title_typo['font-weight'] != '300' && !empty($page_title_typo['font-weight'])) {
				$fw_css .= 'font-weight: '. $page_title_typo['font-weight'] .'; ';
			}
			if (!empty($page_title_typo['font-style'])) {
				$fw_css .= 'font-style: '. $page_title_typo['font-style'] .'; ';
			}
			if (!empty($page_title_typo['text-transform'])) {
				$fw_css .= 'text-transform: '. $page_title_typo['text-transform'] .'; ';
			}
			if ($page_title_typo['letter-spacing'] != '-1px') {
				$fw_css .= 'letter-spacing: '. $page_title_typo['letter-spacing'] .'; ';
			}
			if ($page_title_typo['font-size'] != '26px') {
				$fw_css .= 'font-size: '. $page_title_typo['font-size'] .'; ';
			}
			if ($page_title_typo['line-height'] != '30px') {
				$fw_css .= 'line-height: '. $page_title_typo['line-height'] .'; ';
			}

			return $fw_css;
		}

		if (page_title_typo()) {
			$c_css .= '
				.elise-title {
					'.page_title_typo().'
				}

			';
		}

		// Page Subtitle
		function page_subtitle_typo() {
			global $elise_options;

			$page_subtitle_typo = $elise_options['opt-page-subtitle-heading'];
			$fw_css = '';

			if ($page_subtitle_typo['font-size'] != '15px') {
				$fw_css .= 'font-size: '. $page_subtitle_typo['font-size'] .'; ';
			}
			if ($page_subtitle_typo['line-height'] != '24px') {
				$fw_css .= 'line-height: '. $page_subtitle_typo['line-height'] .'; ';
			}
			if (!empty($page_subtitle_typo['font-weight'])) {
				$fw_css .= 'font-weight: '. $page_subtitle_typo['font-weight'] .'; ';
			}
			if (!empty($page_subtitle_typo['font-style'])) {
				$fw_css .= 'font-style: '. $page_subtitle_typo['font-style'] .'; ';
			}
			if (!empty($page_subtitle_typo['text-transform'])) {
				$fw_css .= 'text-transform: '. $page_subtitle_typo['text-transform'] .'; ';
			}
			if ($page_subtitle_typo['letter-spacing'] != '-1px') {
				$fw_css .= 'letter-spacing: '. $page_subtitle_typo['letter-spacing'] .'; ';
			}

			return $fw_css;
		}

		if (page_subtitle_typo()) {
			$c_css .= '
				.elise-subtitle {
					'.page_subtitle_typo().'
				}
			';
		}
	}


	// Blog Title
	if ($elise_options['opt-custom-post-title'] == true ) {
		function post_title_typo() {
			global $elise_options;

			$post_title_typo = $elise_options['opt-post-title'];
			$fw_css = '';

			if ($post_title_typo['font-family'] != 'Raleway') {
				$fw_css .= 'font-family: "'. $post_title_typo['font-family'] .'"; ';
			}
			if ($post_title_typo['font-weight'] != '300' && !empty($post_title_typo['font-weight'])) {
				$fw_css .= 'font-weight: '. $post_title_typo['font-weight'] .'; ';
			}
			if (!empty($post_title_typo['font-style'])) {
				$fw_css .= 'font-style: '. $post_title_typo['font-style'] .'; ';
			}
			if (!empty($post_title_typo['text-transform'])) {
				$fw_css .= 'text-transform: '. $post_title_typo['text-transform'] .'; ';
			}
			if ($post_title_typo['letter-spacing'] != '-1px') {
				$fw_css .= 'letter-spacing: '. $post_title_typo['letter-spacing'] .'; ';
			}
			if ($post_title_typo['font-size'] != '28px') {
				$fw_css .= 'font-size: '. $post_title_typo['font-size'] .'; ';
			}
			if ($post_title_typo['line-height'] != '34px') {
				$fw_css .= 'line-height: '. $post_title_typo['line-height'] .'; ';
			}

			return $fw_css;
		}

		if (post_title_typo()) {
			$c_css .= '
				.blog-header h3,
				.format-quote blockquote p {
					'.post_title_typo( true ).'
				}
			';
		}
	}

	// Portfolio Item Title
	if ($elise_options['opt-custom-portfolio-title'] == true ) {
		function portfolio_item_title_typo() {
			global $elise_options;

			$portfolio_item_title_typo = $elise_options['opt-portfolio-title'];
			$fw_css = '';

			if ($portfolio_item_title_typo['font-family'] != 'Raleway') {
				$fw_css .= 'font-family: "'. $portfolio_item_title_typo['font-family'] .'"; ';
			}
			if ($portfolio_item_title_typo['font-weight'] != '300' && !empty($portfolio_item_title_typo['font-weight'])) {
				$fw_css .= 'font-weight: '. $portfolio_item_title_typo['font-weight'] .'; ';
			}
			if (!empty($portfolio_item_title_typo['font-style'])) {
				$fw_css .= 'font-style: '. $portfolio_item_title_typo['font-style'] .'; ';
			}
			if (!empty($portfolio_item_title_typo['text-transform'])) {
				$fw_css .= 'text-transform: '. $portfolio_item_title_typo['text-transform'] .'; ';
			}
			if ($portfolio_item_title_typo['letter-spacing'] != '-1px') {
				$fw_css .= 'letter-spacing: '. $portfolio_item_title_typo['letter-spacing'] .'; ';
			}
			if ($portfolio_item_title_typo['font-size'] != '24px') {
				$fw_css .= 'font-size: '. $portfolio_item_title_typo['font-size'] .'; ';
			}
			if ($portfolio_item_title_typo['line-height'] != '28px') {
				$fw_css .= 'line-height: '. $portfolio_item_title_typo['line-height'] .'; ';
			}

			return $fw_css;
		}

		if (portfolio_item_title_typo()) {
			$c_css .= '
				.portfolio-style-bottom .portfolio-item a figure h3,
				.portfolio-style-overlay .portfolio-item a figure h3 {
					'.portfolio_item_title_typo( true ).'
				}
			';
		}
	}








	// Colors ---------------------------------------------- /
	// Layout Settings
	if ($elise_options['opt-layout'] != 1) {
		function elise_boxbor_bg() {
			global $elise_options;

			$body_background = $elise_options['opt-body-bg'];
			$c_css = '';

			if (!empty($body_background) && $body_background['background-color'] != '#282828') {
				$c_css .= '
					background-color: '. $body_background['background-color'] .';
				';
			}
			if (!empty($body_background['background-image'])) {
				$c_css .= '
					background-image: url('. $body_background['background-image'] .');
				';

				if (!empty($body_background['background-repeat'])) {
					$c_css .= '
						background-repeat: '. $body_background['background-repeat'] .';
					';
				}
				if (!empty($body_background['background-size'])) {
					$c_css .= '
						background-size: '. $body_background['background-size'] .';
					';
				}
				if (!empty($body_background['background-attachment'])) {
					$c_css .= '
						background-attachment: '. $body_background['background-attachment'] .';
					';
				}
				if (!empty($body_background['background-position'])) {
					$c_css .= '
						background-position: '. $body_background['background-position'] .';
					';
				}
			}

			return $c_css;
		}

		if (elise_boxbor_bg()) {
			$c_css .= '
				.layout-boxed, .layout-bordered  {
					'. elise_boxbor_bg() .'
				}
			';
		}

		if ($elise_options['opt-layout'] == 2) {
			if ($elise_options['opt-boxed-gap'] != 60) {
				if ($elise_options['opt-responsive'] == 1) {
					$boxed_width = ($elise_options['opt-content-width'] + ($elise_options['opt-boxed-gap'] * 2));
					$c_css .= '
						@media (min-width: '. $boxed_width .'px) { .layout-boxed .layout-wrapper,
							.layout-boxed .layout-wrapper #navbar {
								width: '. $boxed_width .'px;
							}
						}
					';
				} else {
					$c_css .= '
						.layout-boxed .layout-wrapper,
						.layout-boxed .layout-wrapper #navbar {
							width: '. ($elise_options['opt-content-width'] + ($elise_options['opt-boxed-gap'] * 2)) .'px;
						}
					';
				}
			}
		}

		if ($elise_options['opt-layout'] == 3) {
			if ($elise_options['opt-border-size'] != 20) {
				if ($elise_options['opt-responsive'] == 1) {
					$c_css .= '
						@media (min-width: '. ($elise_options['opt-content-width'] + 60 + $elise_options['opt-border-size']) .'px) {
							.layout-bordered .layout-wrapper {
								margin: '. ($elise_options['opt-border-size']) .'px;
							}
						}
					';
				} else {
					$c_css .= '
						.layout-bordered .layout-wrapper {
							margin: '. ($elise_options['opt-border-size']) .'px;
						}
					';
				}
			}
		}
	}

	//$elise_options['opt-content-width'] + 30 + $elise_options['opt-border-size']

	// Basic Colors
	$body_content_bg = $elise_options['opt-color-body-bg'];
	$body_content_text = $elise_options['opt-color-body-text'];
	$body_content_headings = $elise_options['opt-color-headings'];
	$body_content_links = $elise_options['opt-link-colors'];
	$accent_color = $elise_options['opt-color-accent'];

	if (!empty($body_content_bg) && $body_content_bg != '#ffffff') {
		$c_css .= '
			.content {
				background: '. $body_content_bg .';
			}
		';
	}

	if (!empty($body_content_text) && $body_content_text != '#747474') {
		$c_css .= '
			body {
				color: '. $body_content_text .';
			}
		';
	}

	if (!empty($body_content_headings) && $body_content_headings != '#262626') {
		$c_css .= '
			h1, h2, h3, h4, h5, h6, small, label, .elise-counter {
				color: '. $body_content_headings .';
			}
		';
	}

	if (!empty($body_content_links['regular']) && $body_content_links['regular'] != '#8dc73f') {
		$c_css .= '
			a {
				color: '. $body_content_links['regular'] .';
			}
		';
	}
	if (!empty($body_content_links['hover']) && $body_content_links['hover'] != '#8dc73f') {
		$c_css .= '
			a:hover,
			a:focus,
			.widget_calendar #wp-calendar tfoot td a:hover {
				color: '. $body_content_links['hover'] .';
			}
		';
	}

	$woo_colors = '';
	$woo_bg = '';
	$woo_bg_alpha = '';
	if (function_exists('is_woocommerce')) {
		$woo_colors = ',.elise_woo_title .price .amount,
			.woocommerce .elise_woo_sp_pricing .amount, .woocommerce-page .elise_woo_sp_pricing .amount,
			.woocommerce .star-rating span, .woocommerce-page .star-rating span,
			.woocommerce .widget_price_filter .price_slider_amount .button, 
			.woocommerce-page .widget_price_filter .price_slider_amount .button,
			.nav-shopping-bag .nav-shopping-cart .cart_list li .quantity .amount,
			.woocommerce .widget_shopping_cart ul li .quantity .amount, 
			.woocommerce .widget_products ul li .quantity .amount, 
			.woocommerce .widget_recently_viewed_products ul li .quantity .amount, 
			.woocommerce .widget_recent_reviews ul li .quantity .amount, 
			.woocommerce .widget_top_rated_products ul li .quantity .amount, 
			.woocommerce-page .widget_shopping_cart ul li .quantity .amount, 
			.woocommerce-page .widget_products ul li .quantity .amount, 
			.woocommerce-page .widget_recently_viewed_products ul li .quantity .amount, 
			.woocommerce-page .widget_recent_reviews ul li .quantity .amount, 
			.woocommerce-page .widget_top_rated_products ul li .quantity .amount,
			.woocommerce .widget_shopping_cart ul li .amount, 
			.woocommerce .widget_products ul li .amount, 
			.woocommerce .widget_recently_viewed_products ul li .amount, 
			.woocommerce .widget_recent_reviews ul li .amount, 
			.woocommerce .widget_top_rated_products ul li .amount, 
			.woocommerce-page .widget_shopping_cart ul li .amount, 
			.woocommerce-page .widget_products ul li .amount, 
			.woocommerce-page .widget_recently_viewed_products ul li .amount, 
			.woocommerce-page .widget_recent_reviews ul li .amount, 
			.woocommerce-page .widget_top_rated_products ul li .amount,
			.add_to_cart_inline .amount, .add_to_cart_inline ins .amount,
			.woocommerce-pagination .page-numbers .next, 
			.woocommerce-pagination .page-numbers .prev,
			.single-product .product-type-variable .single_variation .amount';
		$woo_bg = ',.woocommerce-pagination .page-numbers li .current, 
			.woocommerce-pagination .page-numbers li .current:hover, 
			.woocommerce-pagination .page-numbers li span.current,
			.woocommerce #respond input#submit,
			.woocommerce .widget_price_filter .ui-slider .ui-slider-handle, 
			.woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle,
			.woocommerce nav.woocommerce-pagination ul li span.current, 
			.woocommerce-page nav.woocommerce-pagination ul li span.current,
			.woocommerce .widget_layered_nav ul li.chosen a, .woocommerce-page .widget_layered_nav ul li.chosen a,
			.woocommerce .widget_layered_nav_filters ul li a, .woocommerce-page .widget_layered_nav_filters ul li a,
			.woocommerce input.button.alt, 
			.woocommerce button.button.alt, 
			.woocommerce a.button.alt, 
			.woocommerce-page input.button.alt, 
			.woocommerce-page button.button.alt, 
			.woocommerce-page a.button.alt,
			.nav-shopping-bag .nav-shopping-cart .buttons a.button.checkout,
			.woocommerce .widget_shopping_cart .buttons a.button.checkout, 
			.woocommerce-page .widget_shopping_cart .buttons a.button.checkout';
		$woo_bg_alpha = ',
			.woocommerce-pagination .page-numbers .next:hover, 
			.woocommerce-pagination .page-numbers .prev:hover,
			.woocommerce #respond input#submit:hover,
			.woocommerce .widget_price_filter .price_slider_amount .button:hover, 
			.woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
			.woocommerce input.button.alt:hover, 
			.woocommerce button.button.alt:hover, 
			.woocommerce a.button.alt:hover, 
			.woocommerce-page input.button.alt:hover, 
			.woocommerce-page button.button.alt:hover, 
			.woocommerce-page a.button.alt:hover,
			.woocommerce .cart-collaterals .shipping_calculator .shipping-calculator-button:hover, 
			.woocommerce-page .cart-collaterals .shipping_calculator .shipping-calculator-button:hover,
			.nav-shopping-bag .nav-shopping-cart .buttons a.button.checkout:hover,
			.woocommerce .widget_shopping_cart .buttons a.button.checkout:hover, 
			.woocommerce-page .widget_shopping_cart .buttons a.button.checkout:hover,
			.woocommerce #respond input#submit:hover, 
			.woocommerce #respond input#submit:focus,
			.add_to_cart_inline .add_to_cart_button:hover';
	}


	if (!empty($accent_color) && $accent_color != '#8dc73f') {
		$c_css .= '
			.pagination .next, 
			.pagination .prev, 
			.widget_rss h6 .rsswidget:first-child::before,
			.back-to-top-btn,
			.project-name span,
			.format-quote blockquote footer,
			.format-quote blockquote footer::before,
			.vc_label_units,
			.timeline_line::before,
			.elise_timeline_block:hover::before,
			.elise-iconbox .elise-iconbox-icon,
			.rsElise .rsArrow:hover .rsArrowIcn,
			.elise-shortcode-testimonial blockquote:hover footer::before,
			.color-accent,
			.blog-content .more-link,
			.elise-toggle::after,
			.btn-outlined
			'. $woo_colors .' {
				color:  '. $accent_color .';
			}

			.elise_timeline_block_wrap:hover .tl-arrow {
				color:  '. $accent_color .' !important;
			}

			.page-navigation .buttons:hover li a:hover {
				color:  '. elise_hexConvert($accent_color, 0.8) .';
			}

			.nav-shopping-bag .woo-cart-items-count,
			.jp-volume-bar-value,
			.jp-play-bar,
			.pagination li .current, 
			.pagination li .current:hover, 
			.pagination li span.current, 
			.btn-default,
			input[type="submit"], 
			.elise_timeline_block:hover::after,
			.back-to-top-btn:hover,
			.add_to_cart_inline .add_to_cart_button,
			.rsElise .rsBullet:not(.rsNavSelected) span:hover,
			.filters li .active,
			.pt-featured-text
			'. $woo_bg .' {
				background:  '. $accent_color .';
			}

			.btn-outlined {
				background: transparent;
			}

			.vc_progress_bar .vc_single_bar .vc_bar,
			.elise_timeline_block_wrap:hover .tl-arrow::after,
			.btn-outlined:hover {
				background:  '. $accent_color .' !important;
			}

			.blog-content .more-link:hover,
			.blog-content .more-link:focus,
			.pagination .next:hover, 
			.pagination .prev:hover, 
			.btn-default:hover, 
			.btn:hover, 
			.btn:focus, 
			input[type="submit"]:hover, 
			input[type="submit"]:focus, 
			input[type="submit"]:hover, 
			input[type="submit"]:focus
			.recentposts-a:hover .btn-link,
			.elise-iconbox-more .btn-link:hover
			'. $woo_bg_alpha .' {
				background:  '. elise_hexConvert($accent_color, 0.8) .';
			}

			.main-nav ul li:not(.nav-icons) ul {
				-webkit-box-shadow: -2 15px 71px rgba(0, 0, 0, 0.05), 0 1px 1px rgba(0, 0, 0, 0.15), 0 -2px 0 '. $accent_color .';
				-moz-box-shadow: 0 15px 71px rgba(0, 0, 0, 0.05), 0 1px 1px rgba(0, 0, 0, 0.15), 0 -2px 0 '. $accent_color .';
				box-shadow: 0 15px 71px rgba(0, 0, 0, 0.05), 0 1px 1px rgba(0, 0, 0, 0.15), 0 -2px 0 '. $accent_color .';
			}

			.nav-tabs > li.active > a, 
			.nav-tabs > li.active > a:hover, 
			.nav-tabs > li.active > a:focus, 
			.elise-tabs-nav > li.ui-tabs-active > a,
			.elise-call-to-action  {
				-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), inset 0px 2px 0 '. $accent_color .';
				-moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), inset 0px 2px 0 '. $accent_color .';
				box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), inset 0px 2px 0 '. $accent_color .';
			}

			.wpb_tour .nav-tabs > li.active > a, 
			.wpb_tour .nav-tabs > li.active > a:hover, 
			.wpb_tour .nav-tabs > li.active > a:focus, 
			.wpb_tour .elise-tabs-nav > li.ui-tabs-active > a {
				-webkit-box-shadow: inset 2px 0 0 '. $accent_color .';
				-moz-box-shadow: inset 2px 0 0 '. $accent_color .';
				box-shadow: inset 2px 0 0 '. $accent_color .';
			}

			.elise-accordion .elise-accordion-header a {
				-webkit-box-shadow: inset 0 2px 0 '. $accent_color .';
				-moz-box-shadow: inset 0 2px 0 '. $accent_color .';
				box-shadow: inset 0 2px 0 '. $accent_color .';
			}

			.pagination .next, 
			.pagination .prev, 
			.woocommerce-pagination .page-numbers .next, 
			.woocommerce-pagination .page-numbers .prev,
			.woocommerce .widget_price_filter .price_slider_amount .button, 
			.woocommerce-page .widget_price_filter .price_slider_amount .button,
			.btn-outlined {
				-webkit-box-shadow: inset 0 0 0 1px '. $accent_color .';
				-moz-box-shadow: inset 0 0 0 1px '. $accent_color .';
				box-shadow: inset 0 0 0 1px '. $accent_color .';
			}

			.project-name,
			.project-info .info-item:hover {
				-webkit-box-shadow: -1px 0 0 '. $accent_color .';
				-moz-box-shadow: -1px 0 0 '. $accent_color .';
				box-shadow: -1px 0 0 '. $accent_color .';
			}

			.project-meta-fullwidth .project-info ul .info-item:hover {
				-webkit-box-shadow: 0px -1px 0 '. $accent_color .';
				-moz-box-shadow: 0px -1px 0 '. $accent_color .';
				box-shadow: 0px -1px 0 '. $accent_color .';
			}

			.woocommerce span.onsale, .woocommerce-page span.onsale,
			.woo_elise_thumb-overlay,
			.highlight-accent,
			.single-img-wrapper .img-caption {
				background:  '. elise_hexConvert($accent_color, 0.9) .';
			}

			.elise_timeline_block_wrap:hover .elise_timeline_block,
			.rsElise .rsArrow:hover .rsArrowIcn,
			.elise-shortcode-testimonial blockquote.blockquote-bordered:hover p {
				border: 1px solid '. $accent_color .' !important;
			}

			.elise-shortcode-testimonial blockquote.blockquote-standard:hover footer,
			.elise-iconbox p::before {
				border-top: 1px solid '. $accent_color .' !important;
			}

			.pt-header {
				border-top: 1px solid '. $accent_color .';
			}

			.recentposts-post,
			.iconbox-border {
				border-top: 2px solid '. $accent_color .';
			}

			a.magnpopup:not(.carousel-view).si_overlay:hover:before, 
			a.img-link:not(.carousel-view).si_overlay:hover:before,
			.woo_elise_thumb-overlay {
				background: -webkit-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
				background: -moz-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
				background: -o-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
				background: linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .');
			}

			.bypostauthor {
				-webkit-box-shadow: inset 2px 0 0 '. $accent_color .';
				-moz-box-shadow: inset 2px 0 0 '. $accent_color .';
				box-shadow: inset 2px 0 0 '. $accent_color .';
			}
		';
	}



	// Page Title
	if ($elise_options['opt-title-bar'] == 1) {
		function elise_page_title_bg() {
			global $elise_options;

			$page_title_background = $elise_options['opt-pt-bg'];
			$c_css = '';

			if (!empty($page_title_background['background-color']) && $page_title_background['background-color'] != '#f5f5f5') {
				$c_css .= '
					background-color: '. $page_title_background['background-color'] .';
				';
			}
			if (!empty($page_title_background['background-image'])) {
				$c_css .= '
					background-image: url('. $page_title_background['background-image'] .');
				';

				if (!empty($page_title_background['background-repeat'])) {
					$c_css .= '
						background-repeat: '. $page_title_background['background-repeat'] .';
					';
				}
				if (!empty($page_title_background['background-size'])) {
					$c_css .= '
						background-size: '. $page_title_background['background-size'] .';
					';
				}
				if (!empty($page_title_background['background-attachment'])) {
					$c_css .= '
						background-attachment: '. $page_title_background['background-attachment'] .';
					';
				}
				if (!empty($page_title_background['background-position'])) {
					$c_css .= '
						background-position: '. $page_title_background['background-position'] .';
					';
				}
			}

			return $c_css;
		}

		if (elise_page_title_bg()) {
			$c_css .= '
				.page-title-container {
					'. elise_page_title_bg() .'
				}
			';
		}

		if ($elise_options['opt-pt-overlay'] == 1) {
			$pt_overlay = $elise_options['opt-pt-overlay-bg'];

			// print_r($pt_overlay);

			if (!empty($pt_overlay) && $pt_overlay['color'] != '#000000' || $pt_overlay['alpha'] != 0.8) {
				$c_css .= '
					.page-title-container .pt-overlay {
						background: '. $pt_overlay['rgba'] .';
					}
				';
			}
		}
	}

	
	$pt_color = $elise_options['opt-pagetitle-color'];

	if (!empty($pt_color) && $pt_color != '#262626') {
		$c_css .= '
			.elise-title {
				color: '. $pt_color .';
			}
		';

		if (!empty($elise_options['opt-page-subtitle'])) {
			$c_css .= '
				.elise-subtitle {
					color: '. $pt_color .';
				}
			';
		}
	}

	$pt_text_bg = $elise_options['opt-titletext-bg'];

	// print_r($pt_text_bg);
	// print_r($elise_options['opt-titletext-bg']);

	if (!empty($pt_text_bg) && $pt_text_bg['color'] != '#000000' || $pt_text_bg['alpha'] != 0) {
		// $pt_title_padding = (!isset($pt_text_bg['alpha'])) ? 'padding: 0' : '';

		$c_css .= '
			.elise-title {
				background: '. $pt_text_bg['rgba'] .';
				padding: 3px 5px;
			}
		';

		if (!empty($elise_options['opt-page-subtitle'])) {
			$c_css .= '
				.elise-subtitle {
					margin-top: 0;
				}
				.elise-subtitle span {
					background: '. $pt_text_bg['rgba'] .';
					padding: 1px 5px;
				}
			';
		}
	}


	// Navigation
	$nav_bg = $elise_options['opt-navigation-color-bg'];
	if ($nav_bg != '#ffffff' && !empty($nav_bg)) {
		$c_css .= '
			#navbar, .header-bar #navbar .header-style-bar {
				background: '. $nav_bg .';
			}

			.navbar-sticky {
				background: '. elise_hexConvert($nav_bg, 0.96) .';
			}
		';
	}

	$nav_shadow = $elise_options['opt-navigation-shadow'];
	if ($nav_shadow == false) {
		$c_css .= '
			#navbar, 
			.header-bar #navbar .header-style-bar,
			.navbar-sticky {
				-webkit-box-shadow: none; 
				-moz-box-shadow: none; 
				box-shadow: none; 
			}
			.main-nav ul li:not(.nav-icons) ul {
				-webkit-box-shadow: 0 -2px 0 '. $accent_color .'; 
				-moz-box-shadow: 0 -2px 0 '. $accent_color .'; 
				box-shadow: 0 -2px 0 '. $accent_color .'; 
			}
		';
	}

	if ($elise_options['opt-navbar-transparent'] == true) {
		$nav_trasp_bg = $elise_options['opt-nav-transparent-bg'];

		if (!empty($nav_trasp_bg) && $nav_trasp_bg['color'] != '#000000' || $nav_trasp_bg['alpha'] != '0.0') {
			$c_css .= '
				.header-standard.header-transparent #navbar, 
				.header-fullwidth.header-transparent #navbar {
					background: '. $nav_trasp_bg['rgba'] .';
				}
			';
		}
	}

	$nav_bar_border = $elise_options['opt-navbar-border'];
	// print_r($nav_bar_border);
	if (!empty($nav_bar_border) && $nav_bar_border['color'] != '#000000' || $nav_bar_border['alpha'] != '0.0') {
		$c_css .= '
			#navbar,
			.header-standard.header-transparent #navbar, 
			.header-fullwidth.header-transparent #navbar {
				border-bottom: 1px solid '. $nav_bar_border['rgba'] .';
			}
		';
	}

	// main-nav
	$nav_menu_colors = $elise_options['opt-navigation-color'];
	if (!empty($nav_menu_colors['regular']) && $nav_menu_colors['regular'] != '#262626') {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) a,
			.search-bar,
			.search-bar .close-btn a {
				color: '.$nav_menu_colors['regular'].';
			}
		';
	}
	if (!empty($nav_menu_colors['hover']) && $nav_menu_colors['hover'] != '#8dc73f') {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) a:hover,
			.main-nav ul li:not(.nav-icons):hover > a,
			.main-nav ul .current-menu-item:not(.nav-icons) > a, 
			.main-nav ul .current-menu-ancestor:not(.nav-icons) > a
			 {
				color: '.$nav_menu_colors['hover'].';
			}
		';
	}

	// submenu
	$nav_submenu_bg = $elise_options['opt-navigation-submenu-color-bg'];
	if (!empty($nav_submenu_bg) && $nav_submenu_bg != '#ffffff') {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) ul {
				background: '. $nav_submenu_bg .';
			}
		';
	}

	$nav_submenu_colors = $elise_options['opt-navigation-submenu-color'];
	if (!empty($nav_submenu_colors['regular']) && $nav_submenu_colors['regular'] != '#262626') {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) ul li a:not(.button),
			.main-nav ul .current-menu-item:not(.nav-icons) ul li a, 
			.main-nav ul .current-menu-ancestor:not(.nav-icons) ul li a,
			.main-nav ul .nav-icons .nav-shopping-cart,
			.nav-shopping-bag .nav-shopping-cart .cart_list li a,
			.nav-shopping-bag .nav-shopping-cart .total,
			.nav-shopping-bag .nav-shopping-cart .cart_list li .quantity,
			.nav-shopping-bag .nav-shopping-cart .total .amount,
			.main-nav ul .elise-mega-menu:not(.nav-icons):hover ul > li:hover > a {
				color: '.$nav_submenu_colors['regular'].';
			}

			.nav-shopping-bag .nav-shopping-cart .cart_list li a {
				color: '.$nav_submenu_colors['regular'].' !important;
			}
		';
	}
	if (!empty($nav_submenu_colors['hover']) && $nav_submenu_colors['hover'] != '#8dc73f') {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) ul li a:hover,
			.main-nav ul li:not(.nav-icons):hover ul > li:hover > a {
				color: '.$nav_submenu_colors['hover'].';
			}

			.main-nav ul .elise-mega-menu:not(.nav-icons) > ul > li ul li a:hover {
				color: '.$nav_submenu_colors['hover'].' !important;

			}
		';
	}

	$nav_submenu_border = $elise_options['opt-navigation-submenu-border'];
	if (!empty($nav_submenu_border) && $nav_submenu_border['border-bottom'] != '1px' || $nav_submenu_border['border-style'] != 'solid' || $nav_submenu_border['border-color'] != '#e9e9e9') {
		$c_css .= '
			.main-nav ul li:not(.nav-icons) ul li a,
			.main-nav ul li:not(.nav-icons) ul li:last-child ul li a,
			.main-nav .nav-shopping-bag .nav-shopping-cart .cart_list li:not(:last-child),
			.main-nav ul .elise-mega-menu:not(.nav-icons) > ul > li a span {
				border-bottom: '. $nav_submenu_border['border-bottom'] .' '. $nav_submenu_border['border-style'] .' '. $nav_submenu_border['border-color'] .';
			}
		';
	}

	// $nav_arrow = $elise_options['opt-nav-arrows'];
	// if ($nav_arrow != '#D3D3D3') {
	// 	$c_css .= '
	// 		.main-nav ul .menu-item-has-children a span::after,
	// 		.main-nav ul .menu-item-has-children ul .menu-item-has-children > a span::after {
	// 			color: '. $nav_arrow .';
	// 		}
	// 	';
	// }

	$nav_icons = $elise_options['opt-navigation-icons-color'];
	if (!empty($nav_icons) && $nav_icons != '#888888') {
		$c_css .= '
			.nav-icons a:not(.button) {
				color: '. $nav_icons .' !important;
			}
		';
	}

	$nav_ext_border = $elise_options['opt-navigation-menu-border'];
	if (!empty($nav_ext_border) && $nav_ext_border['border-top'] != '1px' || $nav_ext_border['border-style'] != 'solid' || $nav_ext_border['border-color'] != '#e9e9e9') {
		$c_css .= '
			.header-extended #navbar .nav-bottom-bar-wrap,
			.header-centered .nav-centered-bar-wrap {
				border-top: '. $nav_ext_border['border-top'] .' '. $nav_ext_border['border-style'] .' '. $nav_ext_border['border-color'] .';
			}

			.header-extended #navbar .logo-desc {
				border-left: '. $nav_ext_border['border-top'] .' '. $nav_ext_border['border-style'] .' '. $nav_ext_border['border-color'] .';
			}
		';
	}  

	// Sticky Header
	if ($elise_options['opt-show-sticky-header'] == 1) {
		if ($elise_options['opt-stickyh-settings'] == 2) {
			$stickyh_bg = $elise_options['opt-stickyheader-bg'];
			$stickyh_links = $elise_options['opt-stickyh-color'];

			// print_r($stickyh_bg);

			// if ($stickyh_bg['color'] != '#ffffff' || $stickyh_bg['alpha'] != 0.96) {
			// }
			$c_css .= '
				.navbar-sticky {
					background: '. $stickyh_bg['rgba'] .';
				}
			';

			if (!empty($stickyh_links)) {
				$c_css .= '

					.navbar-sticky .main-nav ul li:not(.nav-icons) a,
					.navbar-sticky .search-bar,
					.navbar-sticky .search-bar .close-btn a,
					.navbar-sticky .main-nav ul .current-menu-item:not(.nav-icons) ul li a, 
					.navbar-sticky .main-nav ul .current-menu-ancestor:not(.nav-icons) ul li a {
						color: '. $stickyh_links['regular'] .'
					}

					.navbar-sticky .nav-icons a:not(.button),
					.navbar-sticky .main-nav ul .nav-icons a:not(.button) {
						color: #888888 !important;
					}

					.navbar-sticky .main-nav ul li:not(.nav-icons) a:hover,
					.navbar-sticky .main-nav ul li:not(.nav-icons):hover > a,
					.navbar-sticky .main-nav ul .current-menu-item:not(.nav-icons) a, 
					.navbar-sticky .main-nav ul .current-menu-ancestor:not(.nav-icons) a {
						color: '. $stickyh_links['hover'] .'
					}

					.navbar-sticky .main-nav ul li:not(.nav-icons) ul li a:not(.button),
					.navbar-sticky .main-nav ul .current-menu-item:not(.nav-icons) ul li a, 
					.navbar-sticky .main-nav ul .current-menu-ancestor:not(.nav-icons) ul li a,
					.navbar-sticky .main-nav ul .nav-icons .nav-shopping-cart,
					.navbar-sticky .nav-shopping-bag .nav-shopping-cart .cart_list li a,
					.navbar-sticky .nav-shopping-bag .nav-shopping-cart .total,
					.navbar-sticky .nav-shopping-bag .nav-shopping-cart .cart_list li .quantity,
					.navbar-sticky .nav-shopping-bag .nav-shopping-cart .total .amount,
					.navbar-sticky .main-nav ul .elise-mega-menu:not(.nav-icons):hover ul > li:hover > a,
					.cart_list a {
						color: '.$nav_submenu_colors['regular'].';
					}

					.navbar-sticky .nav-shopping-bag .nav-shopping-cart .cart_list li a {
						color: '.$nav_submenu_colors['regular'].' !important;
					}

					.navbar-sticky .main-nav ul li:not(.nav-icons) ul li a:hover,
					.navbar-sticky .main-nav ul li:not(.nav-icons):hover ul > li:hover > a {
						color: '.$nav_submenu_colors['hover'].';
					}

					.navbar-sticky .main-nav ul .elise-mega-menu:not(.nav-icons) > ul > li ul li a:hover {
						color: '.$nav_submenu_colors['hover'].' !important;

					}
				';
			}

			$sticky_icons = $elise_options['opt-stickyh-nav-icons'];

			if ($sticky_icons != '#888888') {
				$c_css .= '
					.navbar-sticky .nav-icons a:not(.button),
					.navbar-sticky .main-nav ul .nav-icons a:not(.button) {
						color: '. $sticky_icons .' !important;
					}
				';
			}



		}
	}

	//Has submenu arrow 
	if ($elise_options['opt-nav-subarrow'] == false) {
		$c_css .= '
			.main-nav ul .menu-item-has-children a span::after {
				content: "";
			}
		';
	}


	// Full Width Navigation

	$fwnav_overlay = $elise_options['opt-fwnav-overlay'];

	if ($fwnav_overlay == 1) {
		$fwoverlay_color = $elise_options['opt-fwnav-overlay-solid'];

		if (!empty($fwoverlay_color) && $fwoverlay_color['color'] != '#000000' || $fwoverlay_color['alpha'] != 0.8 ) {
			$c_css .= '
				.sec-nav-overlay {
					background: '. $fwoverlay_color['rgba'] .';
				}
			';
		} else {
			$c_css .= '
				.sec-nav-overlay {
					background: rgba(0,0,0,.8);
				}
			';
		}
	} 
	elseif ($fwnav_overlay == 2) {
		if (!empty($accent_color) && $accent_color != '#8dc73f') {
			$c_css .= '
				.sec-nav-overlay {
					background: -webkit-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: -moz-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: -o-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .');
				}
			';
		}
	}

	$fw_style = $elise_options['opt-secondary-nav-style'];

	if ($fw_style == 1) {
		$fw_sidebar_bg = $elise_options['opt-fwnav-sidebar-background'];
		if (!empty($fw_sidebar_bg) && $fw_sidebar_bg != '#ffffff') {
			$c_css .= '
				.sidebar-nav-wrap {
					background: '. $fw_sidebar_bg .';
				}
			';
		}

		$fw_sidebar_links = $elise_options['opt-fwnav-sidebar-links'];
		if (!empty($fw_sidebar_links['regular']) && $fw_sidebar_links['regular'] != '#262626') {
			$c_css .= '
				.sidebar-nav ul li a,
				.sidebar-nav-wrap .sec-nav-close-btn a,
				.sidebar-nav-wrap .widget_pages ul li a, 
				.sidebar-nav-wrap .widget_meta ul li a, 
				.sidebar-nav-wrap .widget_nav_menu ul li a,
				.sidebar-nav-wrap .social-icons li a  {
					color: '. $fw_sidebar_links['regular'] .';
				}
			';
		}
		if (!empty($fw_sidebar_links['hover']) && $fw_sidebar_links['hover'] != '#262626') {
			$c_css .= '
				.sidebar-nav ul li a:hover {
					color: '. $fw_sidebar_links['hover'] .';
				}
			';
		}
	}
	elseif ($fw_style == 2) {
		$fw_fw_links = $elise_options['opt-fwnav-fw-links'];
		$fw_fw_link_hover = $elise_options['opt-fwnav-fw-background'];
		if (!empty($fw_fw_links['regular']) && $fw_fw_links['regular'] != '#ffffff') {
			$c_css .= '
				.full-nav ul li a,
				.full-nav-wrap .sec-nav-close-btn a {
					color: '. $fw_fw_links['regular'] .';
				}
			';
		}
		if ((!empty($fw_fw_links['hover']) && $fw_fw_links['hover'] != '#8dc73f') || (!empty($fw_fw_link_hover) && $fw_fw_link_hover != '#ffffff')) {
			$c_css .= '
				.full-nav ul li a:hover {
					color: '. $fw_fw_links['hover'] .';
					background: '. $fw_fw_link_hover .';
				}
			';
		}
	}
	

	// Top Bar
	if ($elise_options['opt-show-top-bar'] == 1) {
		$top_bar_bg = $elise_options['opt-topbar-background'];
		if (!empty($top_bar_bg) && $top_bar_bg != '#ffffff') {
			$c_css .= '
				#top-bar {
					background: '. $top_bar_bg .';
				}
			';
		}

		$top_bar_color = $elise_options['opt-topbar-color'];
		if (!empty($top_bar_color) && $top_bar_color != '#b3b3b3') {
			$c_css .= '
				#top-bar {
					color: '. $top_bar_color .';
				}
			';
		}

		$top_bar_links = $elise_options['opt-topbar-links'];
		if (!empty($top_bar_links['regular']) && $top_bar_links['regular'] != '#b3b3b3') {
			$c_css .= '
				#top-bar a {
					color: '. $top_bar_links['regular'] .';
				}
			';
		}
		if (!empty($top_bar_links['hover']) && $top_bar_links['hover'] != '#b3b3b3') {
			$c_css .= '
				#top-bar a:hover
				{
					color: '. $top_bar_links['hover'] .';
				}
			';
		}

		$top_bar_social = $elise_options['opt-topbar-social'];
		if (!empty($top_bar_social) && $top_bar_social != '#b3b3b3') {
			$c_css .= '
				#top-bar .social-icons li a  {
					color: '. $top_bar_social .';
				}
			';
		}

		$top_bar_borders = $elise_options['opt-topbar-border'];
		if ($top_bar_borders['border-bottom'] != '1px' || $top_bar_borders['border-style'] != 'solid' || $top_bar_borders['border-color'] != '#f0f0f0') {
			$c_css .= '
				#top-bar {
					border-bottom: '. $top_bar_borders['border-bottom'] .' '. $top_bar_borders['border-style'] .' '. $top_bar_borders['border-color'] .';
				}

				#top-bar .nav-social-icons,
				#top-bar .woo-settings {
					border-left: '. $top_bar_borders['border-bottom'] .' '. $top_bar_borders['border-style'] .' '. $top_bar_borders['border-color'] .';
				}

				#top-bar .top-bar-content {
					border-right: '. $top_bar_borders['border-bottom'] .' '. $top_bar_borders['border-style'] .' '. $top_bar_borders['border-color'] .';
				}
			';
		} 
	}

	// Breadcrumbs Bar
	if ($elise_options['opt-breadcrumbs-bar'] == 1) {
		$breadcrumbs_bar_bg = $elise_options['opt-breadcrumbs-background'];
		if (!empty($breadcrumbs_bar_bg) && $breadcrumbs_bar_bg != '#ffffff') {
			$c_css .= '
				.page-navigation {
					background: '. $breadcrumbs_bar_bg .';
				}
			';
		}

		$breadcrumbs_bar_color = $elise_options['opt-breadcrumbs-color'];
		if ($breadcrumbs_bar_color != '#656565') {
			$c_css .= '
				.page-navigation,
				.breadcrumb li:last-child,
				.breadcrumb .fa {
					color: '. $breadcrumbs_bar_color .';
				}
			';
		}

		$breadcrumbs_bar_links = $elise_options['opt-breadcrumbs-links'];
		if ($breadcrumbs_bar_links['regular'] != '#262626') {
			$c_css .= '
				.page-navigation a,
				.breadcrumb li a,
				.page-navigation .buttons li a,
				.btn-share:hover > a {
					color: '. $breadcrumbs_bar_links['regular'] .';
				}
			';
		}
		if ($breadcrumbs_bar_links['hover'] != '#262626') {
			$c_css .= '
				.page-navigation a:hover,
				.page-navigation .buttons:hover li a:hover {
					color: '. $breadcrumbs_bar_links['hover'] .';
				}
			';
		}

		$breadcrumbs_bar_borders = $elise_options['opt-breadcrumbs-border'];
		if ($breadcrumbs_bar_borders['border-bottom'] != '1px' || $breadcrumbs_bar_borders['border-style'] != 'solid' || $breadcrumbs_bar_borders['border-color'] != '#f0f0f0') {
			$c_css .= '
				.page-navigation {
					border-bottom: '. $breadcrumbs_bar_borders['border-bottom'] .' '. $breadcrumbs_bar_borders['border-style'] .' '. $breadcrumbs_bar_borders['border-color'] .';
				}

				.page-navigation .buttons li {
					border-left: '. $breadcrumbs_bar_borders['border-bottom'] .' '. $breadcrumbs_bar_borders['border-style'] .' '. $breadcrumbs_bar_borders['border-color'] .';
				}
			';
		}
	}


	// Blog
	$blog_header_links = $elise_options['opt-blog-heading-color'];

	if ($blog_header_links['regular'] != '#262626') {
		$c_css .= '
			.blog-header h3 a {
				color: '. $blog_header_links['regular'] .';
			}
		';
	}
	if ($blog_header_links['hover'] != '#8dc73f') {
		$c_css .= '
			.blog-header h3 a:hover {
				color: '. $blog_header_links['hover'] .';
			}
		';
	}

	$blog_content_color = $elise_options['opt-blog-content-color'];

	if ($blog_content_color != '#747474') {
		$c_css .= '
			.blog-content p {
				color: '. $blog_content_color .';
			}
		';
	}

	$blog_thumb_overlay = $elise_options['opt-blog-thumb-overlay'];

	if ($blog_thumb_overlay == 1) {
		$blog_thumb_color = $elise_options['opt-blog-th-over'];

		if (!empty($blog_thumb_color) && $blog_thumb_color['color'] != '#000000' || $blog_thumb_color['alpha'] != 0.8 ) {
			$c_css .= '
				.format-standard .blog-media a::after, 
				.format-image .blog-media a::after {
					background: '. $blog_thumb_color['rgba'] .';
				}
			';
		} else {
			$c_css .= '
				.format-standard .blog-media a::after, 
				.format-image .blog-media a::after {
					background: rgba(0,0,0,.8);
				}
			';
		}
	} 
	elseif ($blog_thumb_overlay == 2) {
		if (!empty($accent_color) && $accent_color != '#8dc73f') {
			$c_css .= '
				.format-standard .blog-media a::after, 
				.format-image .blog-media a::after {
					background: -webkit-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: -moz-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: -o-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .');
				}
			';
		}
	}

	$blog_time_color = $elise_options['opt-blog-time-color'];
	if ($blog_time_color != '#8dc73f') {
		$c_css .= '
			.blog-header .time a {
				color: '. $blog_time_color .';
			}
		';
	}

	// $blog_thumb_border = $elise_options['opt-blog-th-border'];
	// if ($blog_thumb_border['border-top'] != '1px' || $blog_thumb_border['border-style'] != 'solid' || $blog_thumb_border['border-color'] != '#e9e9e9') {
	// 	$c_css .= '
	// 		.blog-media {
	// 			border: '. $blog_thumb_border['border-top'] .' '. $blog_thumb_border['border-style'] .' '. $blog_thumb_border['border-color'] .';
	// 		}
	// 	';
	// } 
	// if ($blog_thumb_border['border-style'] == 'none') {
	// 	$c_css .= '
	// 		.blog-media {
	// 			padding: 0;
	// 		}
	// 	';
	// }

	if ($elise_options['opt-blog-style'] == 3) {
		$blog_masonry_border = $elise_options['opt-blog-masonry-border'];

		if ($blog_masonry_border['border-top'] != '1px' || $blog_masonry_border['border-style'] != 'solid' || $blog_masonry_border['border-color'] != '#eeeeee') {
			$c_css .= '
				.blog-masonry .post {
					border: '. $blog_masonry_border['border-top'] .' '. $blog_masonry_border['border-style'] .' '. $blog_masonry_border['border-color'] .';
				}
			';
		}
	} 

	if (is_singular(array('post', 'portfolio'))) {
		$blog_comment_bg = $elise_options['opt-blog-comment-bg'];

		if ($blog_comment_bg != '#f3f3f3') {
			$c_css .= '
				.the-comment {
					background: '. $blog_comment_bg .';
				}
			';
		}
	}

	// Portfolio
	if ($elise_options['opt-portfolio-style'] == 1) {
		 $portfolio_s1_heading_color = $elise_options['opt-portfolio1-heading-color'];

		 if ($portfolio_s1_heading_color != '#ffffff') {
		 	$c_css .= '
		 		.portfolio-style-overlay .portfolio-item a figure h3,
		 		.portfolio-style-overlay .portfolio-item a figure small {
		 			color: '. $portfolio_s1_heading_color .';
		 		}
		 	';
		 }
	} 
	elseif ($elise_options['opt-portfolio-style'] == 2) {
		$portfolio_s2_heading_color = $elise_options['opt-portfolio2-heading-color'];

		if ($portfolio_s2_heading_color['regular'] != '#262626') {
		 	$c_css .= '
		 		.portfolio-style-bottom .portfolio-item a figure h3,
		 		.portfolio-style-bottom .portfolio-item a figure small {
		 			color: '. $portfolio_s2_heading_color['regular'] .';
		 		}
		 	';
		}
		if ($portfolio_s2_heading_color['hover'] != '#262626') {
		 	$c_css .= '
		 		.portfolio-style-bottom .portfolio-item a:hover figure h3 {
		 			color: '. $portfolio_s2_heading_color['hover'] .';
		 		}
		 	';
		}
	}

	$portfolio_thumb_overlay = $elise_options['opt-portfolio-thumb-overlay'];
	if ($portfolio_thumb_overlay == 1) {
		$portfolio_thumb_color = $elise_options['opt-portfolio-th-over'];

		if (!empty($portfolio_thumb_color['color']) && $portfolio_thumb_color['color'] != '#000000' || $portfolio_thumb_color['alpha'] != 0.8 ) {
			$c_css .= '
				.portfolio-item a .overlay,
				.widget-portfolio-item a .overlay {
					background: '. $portfolio_thumb_color['rgba'] .';
				}
			';
		} else {
			$c_css .= '
				.portfolio-item a .overlay,
				.widget-portfolio-item a .overlay {
					background: rgba(0,0,0,.8);
				}
			';
		}
	} 
	elseif ($portfolio_thumb_overlay == 2) {
		if (!empty($accent_color) && $accent_color != '#8dc73f') {
			$c_css .= '
				.portfolio-item a .overlay,
				.widget-portfolio-item a .overlay {
					background: -webkit-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: -moz-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: -o-linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .'); 
					background: linear-gradient(-45deg, '. elise_hexConvert($accent_color, 1, true, 0) .', '. elise_hexConvert($accent_color, 0.8) .');
				}
			';
		}
	}

	////////// ----------------------- EDIT H5 FOR VALIDATE

	$portfolio_thumb_overlay_color = $elise_options['opt-portfolio-overlay-color'];
	if ($portfolio_thumb_overlay_color != '#ffffff') {
		$c_css .= '
			.portfolio-style-overlay .portfolio-item:hover a .overlay .see-more,
			.portfolio-style-bottom .portfolio-item:hover a .overlay .see-more,
			.portfolio-item .quick-view,
			.widget-portfolio-item a .overlay .see-more h5 {
				color: '. $portfolio_thumb_overlay_color .';
			}
		';
	}

	if (is_page_template('portfolio.php')) {
		$portfolio_filter_bg = $elise_options['opt-portfolio-filter-bg'];
		if ($portfolio_filter_bg != '#333333') {
			$c_css .= '
				.project-filtering-wrap {
					background: '. $portfolio_filter_bg .';
				}
			';
		}

		$portfolio_filter_links = $elise_options['opt-portfolio-filter-links'];
		$portfolio_filter_active_bg = $elise_options['opt-portfolio-filter-active-bg'];
		if ($portfolio_filter_links['regular'] != '#dddddd') {
			$c_css .= '
				.filters li a {
					color: '. $portfolio_filter_links['regular'] .';
				}
			';
		}
		if ($portfolio_filter_links['active'] != '#ffffff') {
			$c_css .= '
				.filters li .active {
					color: '. $portfolio_filter_links['active'] .';
				}
			';
		}
		if ($portfolio_filter_active_bg != '#8dc73f') {
			$c_css .= '
				.filters li .active {
					background: '. $portfolio_filter_active_bg .';
				}
			';
		}
	}

	if ($elise_options['opt-project-layout'] == 3) {
		$project_wide_bg = $elise_options['opt-portfolio-project-wide'];

		if ($project_wide_bg != '#333333') {
			$c_css .= '
				.project-gallery-wide-wrapper {
					background: '. $project_wide_bg .';
				}
			';
		}
	}

	// Footer 
	if ($elise_options['opt-footer-widget-area'] == 1) { 
		$footer_w_bg = $elise_options['opt-footer-widgets-bg'];
		if ($footer_w_bg != '#252525') {
			$c_css .= '
				#footer-widget-area {
					background: '. $footer_w_bg .';
				}
			';
		}

		$footer_w_color = $elise_options['opt-footer-widgets-text'];
		if ($footer_w_color != '#e6e6e6') {
			$c_css .= '
				#footer-widget-area {
					color: '. $footer_w_color .';
				}
			';
		}

		$footer_w_links = $elise_options['opt-footer-widgets-links'];
		if ($footer_w_links['regular'] != '#e6e6e6') {
			$c_css .= '
				#footer-widget-area a,
				#footer-widget-area .widget_pages ul li a, 
				#footer-widget-area .widget_meta ul li a, 
				#footer-widget-area .widget_nav_menu ul li a,
				#footer-widget-area .social-icons li a,
				#footer-widget-area .widget_recent_comments ul li a, 
				#footer-widget-area .widget_recent_entries ul li a, 
				#footer-widget-area .widget_rss ul li a {
					color: '. $footer_w_links['regular'] .';
				}
			';
		}
		if ($footer_w_links['hover'] != '#e6e6e6') {
			$c_css .= '
				#footer-widget-area a:hover,
				#footer-widget-area .widget_pages ul li a:hover, 
				#footer-widget-area .widget_meta ul li a:hover, 
				#footer-widget-area .widget_nav_menu ul li a:hover,
				#footer-widget-area .widget_recent_comments ul li a:hover, 
				#footer-widget-area .widget_recent_entries ul li a:hover, 
				#footer-widget-area .widget_rss ul li a:hover {
					color: '. $footer_w_links['hover'] .';
				}
			';
		}
		$footer_ws_bg = $elise_options['opt-footer-bgwidgets'];
		if (!empty($footer_ws_bg['color']) && $footer_ws_bg['color'] != '#ffffff' || $footer_ws_bg['alpha'] != 0.05) {
			$c_css .= '
				#footer-widget-area .widget_recent_comments ul li, 
				#footer-widget-area .widget_recent_entries ul li, 
				#footer-widget-area .widget_rss ul li {
					background: '. $footer_ws_bg['rgba'] .';
				}
			';

		}

		$footer_w_borders = $elise_options['opt-footer-widgets-border'];
		if ($footer_w_borders['border-bottom'] != '1px' || $footer_w_borders['border-top'] != '0' || $footer_w_borders['border-style'] != 'solid' || $footer_w_borders['border-color'] != '#3a3a3a' ) {
			$c_css .= '
				#footer-widget-area {
					border-bottom: '. $footer_w_borders['border-bottom'] .' '. $footer_w_borders['border-style'] .' '. $footer_w_borders['border-color'] .';
					border-top: '. $footer_w_borders['border-top'] .' '. $footer_w_borders['border-style'] .' '. $footer_w_borders['border-color'] .';
				}
			';
		}
	}

	$footer_c_bg = $elise_options['opt-footer-copyrights-bg'];
	if ($footer_c_bg != '#2d2d2d') {
		$c_css .= '
			#copyrights,
			#lang_sel_footer  {
				background: '. $footer_c_bg .';
			}
		';
	}

	$footer_c_color = $elise_options['opt-footer-copyrights-text'];
	if ($footer_c_color != '#999999') {
		$c_css .= '
			#copyrights,
			#lang_sel_footer  {
				color: '. $footer_c_color .';
			}
		';
	}

	$footer_c_links = $elise_options['opt-footer-copyrights-links'];
	if ($footer_c_links['regular'] != '#cccccc') {
		$c_css .= '
			#copyrights a,
			#lang_sel_footer a {
				color: '. $footer_c_links['regular'] .';
			}
		';
	}
	if ($footer_c_links['hover'] != '#cccccc') {
		$c_css .= '
			#copyrights a:hover,
			#lang_sel_footer a:hover {
				color: '. $footer_c_links['hover'] .';
			}
		';
	}

	// Widgets 
	$widget_title_color = $elise_options['opt-widget-h-color'];
	if ($widget_title_color != '#656565') {
		$c_css .= '
			.widget h6,
			#footer-widget-area .widget_rss h6 .rsswidget {
				color: '. $widget_title_color .';
			}
		';
	}
	if ($elise_options['opt-footer-widget-area'] == 1) {
		$widget_footer_title_color = $elise_options['opt-widget-h-footer-color'];
		if ($widget_footer_title_color != '#999999') {
			$c_css .= '
				#footer-widget-area .widget h6,
				#footer-widget-area .widget_rss h6 .rsswidget {
					color: '. $widget_footer_title_color .';
				}
			';
		}
	}



	// Misc 
	$misc_hr = $elise_options['opt-misc-hr'];
	if ($misc_hr['border-bottom'] != '1px' || $misc_hr['border-style'] != 'solid' || $misc_hr['border-color'] != '#e9e9e9' ) {
		$c_css .= '
			hr,
			.post,
			.commentslist ul li::before,
			.widget_categories ul > li a, 
			.widget_archive ul > li a, 
			.woocommerce.widget_product_categories ul > li a,
			.woocommerce .widget_layered_nav ul li, 
			.woocommerce-page .widget_layered_nav ul li,
			.woocommerce.widget_product_categories ul li:last-child a,
			.woocommerce .elise_woo_sp_pricing, 
			.woocommerce-page .elise_woo_sp_pricing,
			.woocommerce .woocommerce-product-rating, 
			.woocommerce-page .woocommerce-product-rating {
				border-bottom: '. $misc_hr['border-bottom'] .' '. $misc_hr['border-style'] .' '. $misc_hr['border-color'] .';
			}

			.woocommerce nav.woocommerce-pagination, 
			.woocommerce-page nav.woocommerce-pagination,
			.page-template-portfolio-php .pagination-wrap,
			.project-meta-fullwidth .project-info,
			.post-author,
			.comment-respond,
			.woocommerce .cart-collaterals .cart_totals tr td, 
			.woocommerce .cart-collaterals .cart_totals tr th, 
			.woocommerce-page .cart-collaterals .cart_totals tr td, 
			.woocommerce-page .cart-collaterals .cart_totals tr th,
			.woocommerce .elise_woo_sp_pricing, 
			.woocommerce-page .elise_woo_sp_pricing,
			.woocommerce .woocommerce-product-rating, 
			.woocommerce-page .woocommerce-product-rating,
			.woocommerce #reviews h3, .woocommerce-page #reviews h3 {
				border-top: '. $misc_hr['border-bottom'] .' '. $misc_hr['border-style'] .' '. $misc_hr['border-color'] .';
			}

			.project-info,
			.woocommerce .woocommerce-product-rating, 
			.woocommerce-page .woocommerce-product-rating,
			.commentslist ul li {
				border-left: '. $misc_hr['border-bottom'] .' '. $misc_hr['border-style'] .' '. $misc_hr['border-color'] .';
			}

			.addresses .address,
			.the-comment .avatar img,
			.post-author .avatar img,
			.format-link .bc-wrap,
			.format-quote .bc-wrap {
				border: '. $misc_hr['border-bottom'] .' '. $misc_hr['border-style'] .' '. $misc_hr['border-color'] .';
			}
		';
	}

	$form_text = $elise_options['opt-misc-form-text'];
	if ($form_text != '#656565') {
		$c_css .= '
			input, textarea, select, select option, textarea,
			.input-group-addon {
				color: '. $form_text .';
			}
		';
	}

	$form_bg = $elise_options['opt-misc-form-bg'];
	if ($form_bg != '#ffffff') {
		$c_css .= '
			input, textarea, select, select option, textarea,
			.input-group-addon {
				background: '. $form_bg .';
			}
		';
	}

	$form_border = $elise_options['opt-misc-form-border'];
	if ($form_border['border-top'] != '1px' || $form_border['border-style'] != 'solid' || $form_border['border-color'] != '#cccccc') {
		$c_css .= '
			input, textarea, select, select option, textarea,
			.input-group-addon {
				border: '. $form_border['border-top'] .' '. $form_border['border-style'] .' '. $form_border['border-color'] .';
			}
		';
	}

	if (is_page_template('template-side-navigation.php')) {

		$side_nav_temp_bg = $elise_options['opt-misc-sn-bg'];
		if ($side_nav_temp_bg != '#f7f7f7') {
			$c_css .= '
				.side-navigation .current_page_item a {
					background: '. $side_nav_temp_bg .';
				}
			';
		}

		$side_nav_temp_text = $elise_options['opt-misc-sn-text'];
		if ($side_nav_temp_text != '#8dc73f') {
			$c_css .= '
				.side-navigation .current_page_item a {
					color: '. $side_nav_temp_text .';
				}
			';
		}

	}

























	// options custom css
	$elise_custom_css = $elise_options['opt-ace-editor-css'];
	if ($elise_custom_css) {
		$c_css .= $elise_custom_css;
	}

	// meta options custom css
	$elise_custom_meta_css = $elise_options['opt-meta-ace-editor-css'];
	if ($elise_custom_meta_css) {
		$c_css .= $elise_custom_meta_css;
	}


	return minify(sanitize_text_field($c_css));
}






function my_styles_method() {
	wp_enqueue_style(
		'custom-style',
		get_template_directory_uri() . '/css/custom-style.css'
	);

$custom_css = custom_style_inline();

        wp_add_inline_style( 'custom-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'my_styles_method', 999 );

?>