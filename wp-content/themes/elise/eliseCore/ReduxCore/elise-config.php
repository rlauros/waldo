<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('Redux_Framework_sample_config')) {

    class Redux_Framework_sample_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );
            
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);
            
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.

         * */
        function compiler_action($elise_options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
            //print_r($elise_options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

            /*
              // Demo of how to use the dynamic CSS and write your own static CSS file
              $filename = dirname(__FILE__) . '/style' . '.css';
              global $wp_filesystem;
              if( empty( $wp_filesystem ) ) {
                require_once( ABSPATH .'/wp-admin/includes/file.php' );
              WP_Filesystem();
              }

              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             */
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => __('Section via hook', 'redux-framework-demo'),
                'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redux-framework-demo'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                <?php endif; ?>

                <h4><?php echo $this->theme->display('Name'); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'redux-framework-demo'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'redux-framework-demo'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'redux-framework-demo') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redux-framework-demo'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                Redux_Functions::initWpFilesystem();
                
                global $wp_filesystem;

                $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
            }

            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'title'     => __('General', 'Elise'),
                'desc'      => __('General Theme Settings.', 'Elise'),
                'icon'      => 'el-icon-cogs',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-logo',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Logo Image', 'Elise'),
                        'subtitle'  => __('Main Logo Image.', 'Elise'),
                        'default'   => '',
                        'mode'      => 'image',
                    ),

                    array(
                        'id'        => 'opt-logo-stickyh',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Sticky Header Logo Image', 'Elise'),
                        'subtitle'  => __('Sticky Header Custom Logo Image (overwrite main logo).', 'Elise'),
                        'default'   => '',
                        'required'  => array('opt-show-sticky-header', "=", 1),
                        'mode'      => 'image',
                    ),

                    array(
                        'id'        => 'opt-logo-mobile',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Mobile Navigation Logo Image', 'Elise'),
                        'subtitle'  => __('Mobile/Full Width Custom Logo Image (overwrite main logo).', 'Elise'),
                        'default'   => '',
                        'mode'      => 'image',
                    ),

                    array(
                        'id'        => 'opt-favicon',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Favicon Image', 'Elise'),
                        'default'   => '',
                        'subtitle'  => __('Favicon 16x16px .png image.', 'Elise'),
                        'mode'      => 'image',
                    ),

                    array(
                        'id'        => 'opt-apple-icons',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => __('Apple Icons', 'Elise'),
                        'default'   => '',
                        'subtitle'  => __('Icons for all apple devices (180x180px - important!)', 'Elise'),
                    ),

                    // array(
                    //     'id'        => 'opt-analytics',
                    //     'type'      => 'ace_editor',
                    //     'title'     => __('Tracking Code', 'Elise'),
                    //     'subtitle'  => __('Paste your Google Analytics (or other) tracking code here. This will be added immediately before &lt;/head&gt; tag.', 'Elise'),
                    //     'mode'      => 'html',
                    //     'theme'     => 'chrome',
                    //     'default'   => ""
                    // ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Layout', 'Elise'),
                'desc'      => __('Layout Settings', 'Elise'),
                'icon'      => 'el-icon-website',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Layout', 'Elise'),
                        'subtitle'  => __('Theme layout.', 'Elise'),
                        'options'   => array(
                            '1' => array('title'=>'Wide', 'alt' => 'Wide',       'img' => ReduxFramework::$_url . 'assets/img/layout-wide.jpg'),
                            '2' => array('title'=>'Boxed', 'alt' => 'Boxed', 'img' => ReduxFramework::$_url . 'assets/img/layout-boxed.jpg'),
                            '3' => array('title'=>'Bordered', 'alt' => 'Bordered','img' => ReduxFramework::$_url . 'assets/img/layout-bordered.jpg'),
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'            => 'opt-content-width',
                        'type'          => 'slider',
                        //'required'      => array('opt-title-bar', "=", 1),
                        'title'         => __('Content Width', 'Elise'),
                        'subtitle'      => __('Content Container Width.', 'Elise'),
                        'desc'          => __('Min: 960, max: 1200, default value: 1176', 'Elise'),
                        'default'       => 1176,
                        'min'           => 960,
                        'step'          => 12,
                        'max'           => 1200,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'            => 'opt-boxed-gap',
                        'type'          => 'slider',
                        //'required'      => array('opt-title-bar', "=", 1),
                        'title'         => __('Boxed Container Gap', 'Elise'),
                        'required'      => array('opt-layout','=','2'),
                        'subtitle'      => __('Gap between content and background.', 'Elise'),
                        'desc'          => __('Min: 30, max: 150, default value: 60', 'Elise'),
                        'default'       => 60,
                        'min'           => 30,
                        'step'          => 5,
                        'max'           => 150,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'            => 'opt-border-size',
                        'type'          => 'slider',
                        //'required'      => array('opt-title-bar', "=", 1),
                        'title'         => __('Layout Border Size', 'Elise'),
                        'required'      => array('opt-layout','=','3'),
                        'subtitle'      => __('Bordered layout - border padding.', 'Elise'),
                        'desc'          => __('Min: 10, max: 60, default value: 20', 'Elise'),
                        'default'       => 20,
                        'min'           => 10,
                        'step'          => 5,
                        'max'           => 60,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'       => 'opt-content-padding',
                        'type'     => 'spacing',
                        // An array of CSS selectors to apply this font style to
                        'mode'     => 'padding',
                        // absolute, padding, margin, defaults to padding
                        'all'      => false,
                        // Have one field that applies to all
                        //'top'           => false,     // Disable the top
                        'right'         => false,     // Disable the right
                        //'bottom'        => false,     // Disable the bottom
                        'left'          => false,     // Disable the left
                        'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                        //'units_extended'=> 'true',    // Allow users to select any type of unit
                        // 'display_units' => true,   // Set to false to hide the units if the units are specified
                        'title'    => __( 'Content Padding', 'Elise' ),
                        'subtitle' => __( 'Content Padding top and bottom dimensions. (default: top: 55px, bottom: 60px)', 'Elise' ),
                        'default'  => array(
                            'padding-top'    => '55px',
                            'padding-bottom' => '60px'
                        )
                    ),

                    array(
                        'id'        => 'opt-body-bg',
                        'type'      => 'background',
                        'title'     => __('Body Background', 'Elise'),
                        'required'      => array('opt-layout','!=','1'),
                        'subtitle'  => __('Body background color or image.', 'Elise'),
                        //'preview_height' => '100px',
                        'preview'   => true,
                        'preview_media' => true,
                        'transparent' => false,
                        'default'   => array(
                            'background-color' => "#282828"
                        ),
                    ),

                    array(
                        'id'        => 'opt-layout-wrapper-shadow',
                        'type'      => 'switch',
                        'required'      => array('opt-layout','!=','1'),
                        'title'     => __('Drop Shadow', 'Elise'),
                        'subtitle'  => __('Layout Drops Shadow.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id' => 'section-usability-start',
                        'type' => 'section',
                        'title' => __('Usability', 'Elise'),
                        //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-smoothscroll',
                        'type'      => 'switch',
                        'title'     => __('Smooth Scroll', 'Elise'),
                        'subtitle'  => __('Enable/Disable smooth scrolling.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-responsive',
                        'type'      => 'switch',
                        'title'     => __('Responsive', 'Elise'),
                        'subtitle'  => __('Enable/Disable responsive layout.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-back-to-top',
                        'type'      => 'switch',
                        'title'     => __('Back to Top Button', 'Elise'),
                        'subtitle'  => __('Enable/Disable Back to Top button.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id' => 'section-usability-end',
                        'type' => 'section',
                        'indent' => false 
                    ),
                ),
            );


            $this->sections[] = array(
                'title'     => __('Header', 'Elise'),
                'desc'      => __('Header Settings', 'Elise'),
                'icon'      => 'el-icon-chevron-up',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-navbar-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Navbar Style', 'Elise'),
                        'subtitle'  => __('Main Navbar style.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Header Standard',       'img' => ReduxFramework::$_url . 'assets/img/elise-hs.png'),
                            // '2' => array('alt' => 'Header Full Width',  'img' => ReduxFramework::$_url . 'assets/img/elise-hfw.png'),
                            '2' => array('alt' => 'Header Centered', 'img' => ReduxFramework::$_url . 'assets/img/elise-hlt.png'),
                            '3' => array('alt' => 'Header Extended','img' => ReduxFramework::$_url . 'assets/img/elise-hsh.png'),
                            '4' => array('alt' => 'Header Bar',  'img' => ReduxFramework::$_url . 'assets/img/elise-hb.png')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-nav-full-width',
                        'type'      => 'switch',
                        'required'      => array('opt-navbar-style','!=','4'),
                        'title'     => __('Nav Full Width', 'Elise'),
                        'subtitle'  => __('Enable/Disable Full Width navbar.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-navbar-transparent',
                        'type'      => 'switch',
                        'required' => array( 
                                        array('opt-navbar-style','!=','3'), 
                                        array('opt-navbar-style','!=','4') 
                        ),      
                        'title'     => __('Transparent Navbar background', 'Elise'),
                        'subtitle'  => __('Enable/Disable transparent navbar background.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-menu-position',
                        'type'      => 'button_set',
                        'title'     => __('Menu Position', 'Elise'),
                        'subtitle'  => __('Menu position on navbar.', 'Elise'),                   
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'Left', 
                            '2' => 'Center', 
                            '3' => 'Right'
                        ), 
                        'default'   => '3'
                    ),

                    array(
                        'id'            => 'opt-navbar-height',
                        'type'          => 'slider',
                        'title'         => __('Navbar Height', 'Elise'),
                        'required' => array( 
                                        array('opt-navbar-style','!=','2'), 
                                        array('opt-navbar-style','!=','3'), 
                                        array('opt-navbar-style','!=','4') 
                        ),                        
                        'subtitle'      => __('Navbar height in pixels.', 'Elise'),
                        'desc'          => __('Navbar height. Min: 50, max: 300, default value: 90', 'Elise'),
                        'default'       => 90,
                        'min'           => 50,
                        'step'          => 5,
                        'max'           => 300,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'            => 'opt-header-height',
                        'type'          => 'slider',
                        'required'      => array('opt-navbar-style','=','2'),
                        'title'         => __('Header Height', 'Elise'),
                        'subtitle'      => __('Header height in pixels.', 'Elise'),
                        'desc'          => __('Navbar height. Min: 120, max: 500, default value: 150', 'Elise'),
                        'default'       => 150,
                        'min'           => 120,
                        'step'          => 5,
                        'max'           => 500,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'        => 'opt-hover-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'required' => array( 
                                        array('opt-navbar-style','!=','3'), 
                                        array('opt-navbar-style','!=','4'),
                        ),          
                        'title'     => __('Hover Style', 'Elise'),
                        'subtitle'  => __('Main Menu hover style.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Hover Block',       'img' => ReduxFramework::$_url . 'assets/img/hover-block.jpg'),
                            '2' => array('alt' => 'Hover Boxed',  'img' => ReduxFramework::$_url . 'assets/img/hover-boxed.jpg')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-nav-search',
                        'type'      => 'switch',
                        'title'     => __('Nav search', 'Elise'),
                        'subtitle'  => __('Search icon in Navbar', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-nav-subarrow',
                        'type'      => 'switch',
                        'title'     => __('Submenu Arrow', 'Elise'),
                        'subtitle'  => __('Enable/Disable menu item arrows if menu item has submenu.', 'Elise'),
                        'default'   => true,
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Sticky Header', 'Elise'),
                'desc'      => __('Sticky Header Settings.', 'Elise'),
                'icon'      => 'el-icon-download-alt',
                'subsection' => true,
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-show-sticky-header',
                        'type'      => 'switch',
                        // 'required'  => array('opt-title-bar', "=", 1),
                        'title'     => __('Show Sticky Header', 'Elise'),
                        'subtitle'  => __('Show sticky header navigation.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-sticky-header-style',
                        'type'      => 'button_set',
                        'title'     => __('Sticky Header Style', 'Elise'),
                        'required'  => array('opt-show-sticky-header', "=", 1),
                        'subtitle'  => __('Sticky header is visible on:', 'Elise'),                   
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => __('Scroll', 'Elise'), 
                            '2' => __('Scroll Up', 'Elise')
                        ), 
                        'default'   => '2'
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Top Bar', 'Elise'),
                'desc'      => __('Top Bar Settings', 'Elise'),
                'icon'      => 'el-icon-eject',
                'subsection' => true,
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-show-top-bar',
                        'type'      => 'switch',
                        'required'  => array('opt-navbar-style', "!=", 3),
                        'title'     => __('Show Top Bar', 'Elise'),
                        'subtitle'  => __('Enable/disable bar above header.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-top-bar-text',
                        'type'      => 'editor',
                        'title'     => __('Top Bar Text', 'Elise'),
                        'required'  => array(
                                array('opt-show-top-bar', "=", 1),
                                array('opt-navbar-style', "!=", 3),
                            ),
                        'subtitle'  => __('Additional information text.', 'Elise'),
                        'default'   => '<p>Contact us - (33) 2222-33-24</p>',
                            'args'   => array(
                                'wpautop'          => true,
                                'textarea_rows'    => 4,
                                'media_buttons'    => false
                            )
                    ),

                    array(
                        'id'     => 'opt-topbar-info2',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-navbar-style', "=", 3),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Top Bar is Not Available for this Header.', 'Elise' ),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Full Screen/Mobile Navigation', 'Elise'),
                'desc'      => __('Full Screen Navigation Settings.', 'Elise'),
                'icon'      => 'el-icon-lines',
                'subsection' => true,
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-secondary-nav',
                        'type'      => 'switch',
                        // 'required'  => array('opt-title-bar', "=", 1),
                        'title'     => __('Show Hamburger Icon', 'Elise'),
                        'subtitle'  => __('Show full screen navigation Icon in Main Menu.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-secondary-nav-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Secondary/Mobile Navigation Style', 'Elise'),
                        'subtitle'  => __('Full Screen/Mobile navigation style.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar', 'title' => 'Sidebar Nav',       'img' => ReduxFramework::$_url . 'assets/img/secnav-sidebar.jpg'),
                            '2' => array('alt' => 'Full', 'title' => 'Full Width Nav',   'img' => ReduxFramework::$_url . 'assets/img/secnav-full.jpg'),
                        ),
                        'default'   => '1'
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Title Bar', 'Elise'),
                'desc'      => __('Title Bar Settings.', 'Elise'),
                'icon'      => 'el-icon-fontsize',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    // array(
                    //     'id'   =>'divider_1',
                    //     // 'desc' => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                    //     'type' => 'divide',
                    //     'class' => 'hr2'
                    // ),

                    // array(
                    //    'id' => 'section-title-bar-start',
                    //    'type' => 'section',
                    //    'title' => __('Title Bar Options', 'redux-framework-demo'),
                    //    //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                    //    'indent' => false 
                    //  ),

                    array(
                        'id'        => 'opt-title-bar',
                        'type'      => 'switch',
                        'title'     => __('Title Bar', 'Elise'),
                        'subtitle'  => __('Enable/Disable title bar on every page.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-title-bar-centered',
                        'type'      => 'button_set',
                        'title'     => __('Page Title Position', 'Elise'),
                        'subtitle'  => __('Position of page title on title bar.', 'Elise'),
                        'required'  => array('opt-title-bar', "=", 1),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'Left', 
                            '2' => 'Center'
                        ), 
                        'default'   => '1'
                    ),

                    // array(
                    //     'id'            => 'opt-title-bar-height',
                    //     'type'          => 'slider',
                    //     'required'      => array('opt-title-bar', "=", 1),
                    //     'title'         => __('Title Bar Height', 'Elise'),
                    //     'subtitle'      => __('Title Bar height in pixels.', 'Elise'),
                    //     'desc'          => __('Navbar height. Min: 60, max: 800, default value: 120', 'Elise'),
                    //     'default'       => 120,
                    //     'min'           => 60,
                    //     'step'          => 5,
                    //     'max'           => 800,
                    //     'display_value' => 'text'
                    // ),

                    array(
                        'id'       => 'opt-title-bar-padding',
                        'type'     => 'spacing',
                        'required'  => array('opt-title-bar', "=", 1),
                        // An array of CSS selectors to apply this font style to
                        'mode'     => 'padding',
                        // absolute, padding, margin, defaults to padding
                        'all'      => false,
                        // Have one field that applies to all
                        //'top'           => false,     // Disable the top
                        'right'         => false,     // Disable the right
                        //'bottom'        => false,     // Disable the bottom
                        'left'          => false,     // Disable the left
                        'units'         => 'px',      // You can specify a unit value. Possible: px, em, %
                        //'units_extended'=> 'true',    // Allow users to select any type of unit
                        // 'display_units' => true,   // Set to false to hide the units if the units are specified
                        'title'    => __( 'Title Bar Padding', 'Elise' ),
                        'subtitle' => __( 'Title Bar top and bottom padding size. (default: top: 45px, bottom: 45px)', 'Elise' ),
                        'default'  => array(
                            'padding-top'    => '45px',
                            'padding-bottom' => '45px'
                        )
                    ),

                    array(
                        'id' => 'section-bread-start',
                        'type' => 'section',
                        'title' => __('Breadcrumbs Bar', 'Elise'),
                        //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-breadcrumbs-bar',
                        'type'      => 'switch',
                        'title'     => __('Show Breadcrumbs', 'Elise'),
                        'subtitle'  => __('Enable/Disable breadcrumbs with Title Bar.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-breadcrumbs-style',
                        'type'      => 'button_set',
                        'title'     => __('Breadcrumbs Style', 'Elise'),
                        'subtitle'  => __('Style of breadcrumbs.', 'Elise'),
                        'required'  => array('opt-breadcrumbs-bar', "=", 1),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'Under Title Bar', 
                            '2' => 'On Title Bar'
                        ), 
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-breadcrumbs-share',
                        'type'      => 'switch',
                        'required'  => array(
                            array('opt-breadcrumbs-bar', "=", 1),
                            array('opt-breadcrumbs-style', "=", 1),
                        ),
                        'title'     => __('Show Share Buttons', 'Elise'),
                        'subtitle'  => __('Enable/Disable share buttons on Breadcrumbs Bar.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id' => 'section-bread-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    array(
                        'id'     => 'opt-notice-info',
                        'type'   => 'info',
                        'notice' => false,
                        'style'  => 'info',
                        'style'  => 'warning',
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Information', 'Elise' ),
                        'desc'   => __( 'Title Bar styling settings like Background, Color and more you will find in "Styling" Section.', 'Elise' )
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Footer', 'Elise'),
                'desc'      => __('Footer Settings.', 'Elise'),
                'icon'      => 'el-icon-chevron-down',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-footer-widget-area',
                        'type'      => 'switch',
                        'title'     => __('Show Widget Area', 'Elise'),
                        'subtitle'  => __('Enable/Disable Widget Area in Footer.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-footer-widget-columns',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Widget Columns', 'Elise'),
                        'subtitle'  => __('Widget Area columns style.', 'Elise'),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'options'   => array(
                            '1' => array('title'=>'2 Columns', 'alt' => '2 Columns',       'img' => ReduxFramework::$_url . 'assets/img/footer-2col.jpg'),
                            '2' => array('title'=>'3 Columns - First Wide', 'alt' => '3 Columns Wide',  'img' => ReduxFramework::$_url . 'assets/img/footer-3col-wide.jpg'),
                            '3' => array('title'=>'3 Columns', 'alt' => '3 Columns', 'img' => ReduxFramework::$_url . 'assets/img/footer-3col.jpg'),
                            '4' => array('title'=>'4 Columns', 'alt' => '4 Columns', 'img' => ReduxFramework::$_url . 'assets/img/footer-4col.jpg')
                        ),
                        'default'   => '3'
                    ),

                    array(
                        'id'        => 'opt-copyrights',
                        'type'      => 'editor',
                        'title'     => __('Copyright Section Text', 'Elise'),
                        'subtitle'  => __('Copyright section text here.', 'Elise'),
                        'default'   => '<p>&copy; Copyrights 2015 by Elise. All rights reserved.</p><p>Powered by <a href="http://wordpress.org">WordPress.</a></p>',
                            'args'   => array(
                                'wpautop'          => false,
                                'textarea_rows'    => 4,
                                'media_buttons'    => false
                            )
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Blog', 'Elise'),
                'desc'      => __('Blog Settings.', 'Elise'),
                'icon'      => 'el-icon-book',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-blog-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Blog Layout', 'Elise'),
                        'subtitle'  => __('Blog Page Style.', 'Elise'),
                        // 'desc'      => __('Only Blog page -- fix', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Standard Big Thumbnail',       'img' => ReduxFramework::$_url . 'assets/img/blog-large.jpg'),
                            '2' => array('alt' => 'Standard Small Thumbnail',  'img' => ReduxFramework::$_url . 'assets/img/blog-small.jpg'),
                            '3' => array('alt' => 'Masonry', 'img' => ReduxFramework::$_url . 'assets/img/blog-masonry.jpg'),
                            '4' => array('alt' => 'Full Width', 'img' => ReduxFramework::$_url . 'assets/img/blog-wide.jpg')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-show-sidebar',
                        'type'      => 'switch',
                        'required' => array('opt-blog-style','!=','4'),
                        'title'     => __('Show Sidebar', 'Elise'),
                        'subtitle'  => __('Enable/Disable Blog Sidebar.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-show-thumbnail',
                        'type'      => 'switch',
                        'required' => array('opt-blog-style','=','4'),
                        'title'     => __('Show Thumbnail', 'Elise'),
                        'subtitle'  => __('Enable/Disable posts thumbnails.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-blog-sidebar-position',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Sidebar Position', 'Elise'),
                        'subtitle'  => __('Position of sidebar on blog page.', 'Elise'),
                        'required' => array('opt-show-sidebar','=','1'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/elise-sr.png'),
                            '2' => array('alt' => 'Sidebar Left', 'img' => ReduxFramework::$_url . 'assets/img/elise-sl.png')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-thumbnail-on-top',
                        'type'      => 'switch',
                        'required' => array(
                            array('opt-blog-style','!=','2'),
                            array('opt-blog-style','!=','3'),
                        ),
                        'title'     => __('Thumbnail on Top', 'Elise'),
                        'subtitle'  => __('Set thumbnail above post title.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-excerpts',
                        'type'      => 'switch',
                        'required' => array('opt-blog-style','!=','2'),
                        'title'     => __('Automatic Post Excerpts', 'Elise'),
                        'subtitle'  => __('Enable/Disable automation posts excerpts.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'            => 'opt-excerpt-lenght',
                        'type'          => 'slider',
                        'title'         => __('Excerpt Lenght', 'Elise'),
                        'required'      => array(
                                array('opt-blog-style','!=','2'),
                                array('opt-excerpts','=','1'),
                            ),
                        'subtitle'      => __('Number of words.', 'Elise'),
                        'desc'          => __('Min: 10, max: 200, default value: 50', 'Elise'),
                        'default'       => 50,
                        'min'           => 10,
                        'max'           => 200,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'        => 'opt-blog-welcome',
                        'type'      => 'text',
                        'title'     => __('Welcome Title Text', 'Elise'),
                        'subtitle'  => __('Welcome text when Blog is Front Page.', 'Elise'),
                        //'desc'      => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                        // 'validate'  => 'str_replace',
                        // 'str'       => array(
                        //     'search'        => ' ', 
                        //     'replacement'   => '-'
                        // ),
                        'validate' => 'no_html',
                        'default'   => __('Welcome to our Blog', 'Elise'),
                    ),
                ),
            );



            $this->sections[] = array(
                'title'     => __('Single Blog Page', 'Elise'),
                'desc'      => __('Single Blog Page Settings.', 'Elise'),
                'icon'      => 'el-icon-fontsize',
                'subsection' => true,
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-blog-page-style-custom',
                        'type'      => 'switch',
                        'on'        => __('Yes', 'Elise'),
                        'off'        => __('No', 'Elise'),
                        'title'     => __('Custom Single Post Layout', 'Elise'),
                        'subtitle'  => __('Set Single Post Style different than Blog Style', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-blog-page-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'required' => array('opt-blog-page-style-custom','=','1'),
                        'title'     => __('Single Post Layout', 'Elise'),
                        'subtitle'  => __('Blog page style.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/single-sidebar-right.jpg'),
                            '2' => array('alt' => 'Sidebar Left', 'img' => ReduxFramework::$_url . 'assets/img/single-sidebar-left.jpg'),
                            '3' => array('alt' => 'Full Width', 'img' => ReduxFramework::$_url . 'assets/img/single-fullwidth.jpg'),
                            '4' => array('alt' => 'Full Width Thin', 'img' => ReduxFramework::$_url . 'assets/img/single-wide.jpg')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-share-buttons',
                        'type'      => 'switch',
                        'title'     => __('Share Buttons', 'Elise'),
                        'subtitle'  => __('Enable/Disable Share buttons on post page.', 'Elise'),
                        'default'   => true,
                    ),

                    // array(
                    //     'id'        => 'opt-share-buttons-check',
                    //     'type'      => 'checkbox',
                    //     'required' => array('opt-share-buttons','=','1'),
                    //     'title'     => __('Share Buttons Select', 'Elise'),
                    //     // 'subtitle'  => __('Share buttons .', 'Elise'),
                    //     'options'   => array(
                    //         '1' => 'Facebook Button', 
                    //         '2' => 'Twitter Button', 
                    //         '3' => 'Google Plus Button', 
                    //         '4' => 'Pinterest Button'
                    //     ),
                    //     'default'   => array(
                    //         '1' => '1', 
                    //         '2' => '1', 
                    //         '3' => '1', 
                    //         '4' => '1'
                    //     )
                    // ),

                    array(
                        'id'        => 'opt-tags',
                        'type'      => 'switch',
                        'title'     => __('Display Tags', 'Elise'),
                        'subtitle'  => __('Enable/Disable Tags on post page.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-author-bio',
                        'type'      => 'switch',
                        'title'     => __('Author\'s Bio', 'Elise'),
                        'subtitle'  => __('Enable/Disable Author\'s Bio on post.', 'Elise'),
                        'default'   => true,
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Portfolio', 'Elise'),
                'desc'      => __('Portfolio Settings.', 'Elise'),
                'icon'      => 'el-icon-brush',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-portfolio-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Portfolio Layout', 'Elise'),
                        'subtitle'  => __('Main Portfolio layout.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => '1 Column', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-1col.jpg'),
                            '2' => array('alt' => '2 Columns',  'img' => ReduxFramework::$_url . 'assets/img/portfolio-2col.jpg'),
                            '3' => array('alt' => '3 Columns',  'img' => ReduxFramework::$_url . 'assets/img/portfolio-3col.jpg'),
                            '4' => array('alt' => '4 Columns', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-4col.jpg'),
                            '5' => array('alt' => 'Masonry', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-masonry.jpg')
                        ),
                        'default'   => '3'
                    ),

                    array(
                        'id'        => 'opt-portfolio-fullwidth',
                        'type'      => 'switch',
                        'title'     => __('Full Width Portfolio', 'Elise'),
                        'subtitle'  => __('Enable/Disable Full Width portfolio layout.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-portfolio-filtering',
                        'type'      => 'switch',
                        'title'     => __('Portfolio Filtering', 'Elise'),
                        'subtitle'  => __('Enable/Disable portfolio filtering.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-portfolio-pagination',
                        'type'      => 'switch',
                        'title'     => __('Portfolio Pagination', 'Elise'),
                        'subtitle'  => __('Paginate Projects on Portfolio page.', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'            => 'opt-portfolio-pper-page',
                        'type'          => 'slider',
                        'required'      => array('opt-portfolio-pagination', "=", 1),
                        'title'         => __('Projects per Page', 'Elise'),
                        'subtitle'      => __('Number of projects that will show on Page.', 'Elise'),
                        'desc'          => __('Navbar height. Min: 1, max: 20, default value: 5', 'Elise'),
                        'default'       => 5,
                        'min'           => 1,
                        'step'          => 1,
                        'max'           => 20,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'        => 'opt-portfolio-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Portfolio Items Style', 'Elise'),
                        'subtitle'  => __('Portfolio items layout.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Portfolio Item Overlay', 'img' => ReduxFramework::$_url . 'assets/img/portfolio-style-overlay.jpg'),
                            '2' => array('alt' => 'Portfolio Item Bottom',  'img' => ReduxFramework::$_url . 'assets/img/portfolio-style-bottom.jpg')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-porfolio-item-categories',
                        'type'      => 'switch',
                        'required'      => array('opt-portfolio-style', "=", 1),
                        'title'     => __('Categories Names', 'Elise'),
                        'subtitle'  => __('Enable/Disale portfolio categories above portfolio title.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-quick-view',
                        'type'      => 'switch',
                        'title'     => __('Quick View Button', 'Elise'),
                        'subtitle'  => __('Enable/Disable quick view button on portfolio item.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'            => 'opt-portfolio-items-gap',
                        'type'          => 'slider',
                        'title'         => __('Portfolio Items Gap', 'Elise'),                      
                        'subtitle'      => __('Gap between portfolio items in pixels.', 'Elise'),
                        'desc'          => __('Gap size. Min: 0, max: 30, default value: 30', 'Elise'),
                        'default'       => 30,
                        'min'           => 0,
                        'step'          => 2,
                        'max'           => 30,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'        => 'opt-portfolio-custom-slug',
                        'type'      => 'text',
                        'title'     => __('Custom Slug', 'Elise'),
                        'subtitle'  => __('Your custom slug (e.g portfolio, elise-works etc.)', 'Elise'),
                        //'desc'      => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                        'validate'  => 'str_replace',
                        'str'       => array(
                            'search'        => ' ', 
                            'replacement'   => '-'
                        ),
                        'default'   => 'portfolio',
                    ),
                ),
            );
            
            // $vc_ct = get_option( 'wpb_js_content_types' );
            // $vc_sp_layout = array(
            //     '1' => array('title' => 'Large Images', 'alt' => 'Large',       'img' => ReduxFramework::$_url . 'assets/img/project-large.jpg'),
            //     '2' => array('title' => 'Medium Images', 'alt' => 'Medium',  'img' => ReduxFramework::$_url . 'assets/img/project-medium.jpg'),
            //     '3' => array('title' => 'Wide Images', 'alt' => 'Wide',  'img' => ReduxFramework::$_url . 'assets/img/project-wide.jpg'),
            // );

            // if (in_array('portfolio', $vc_ct)) {
            //     $vc_sp_layout = array(
            //         '1' => array('title' => 'Large Images', 'alt' => 'Large',       'img' => ReduxFramework::$_url . 'assets/img/project-large.jpg'),
            //         '2' => array('title' => 'Medium Images', 'alt' => 'Medium',  'img' => ReduxFramework::$_url . 'assets/img/project-medium.jpg'),
            //         '3' => array('title' => 'Wide Images', 'alt' => 'Wide',  'img' => ReduxFramework::$_url . 'assets/img/project-wide.jpg'),
            //         '4' => array('title' => 'VC Clean', 'alt' => 'Wide',  'img' => ReduxFramework::$_url . 'assets/img/project-wide.jpg'),
            //     );
            // }

            $this->sections[] = array(
                'title'     => __('Project Page', 'Elise'),
                'desc'      => __('Project Page Settings.', 'Elise'),
                'icon'      => 'el-icon-picture',
                'subsection' => true,
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-project-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Project page Layout', 'Elise'),
                        'subtitle'  => __('Single project page layout.', 'Elise'),
                        'options'   => array(
                                '1' => array('title' => 'Large Images', 'alt' => 'Large',       'img' => ReduxFramework::$_url . 'assets/img/project-large.jpg'),
                                '2' => array('title' => 'Medium Images', 'alt' => 'Medium',  'img' => ReduxFramework::$_url . 'assets/img/project-medium.jpg'),
                                '3' => array('title' => 'Wide Images', 'alt' => 'Wide',  'img' => ReduxFramework::$_url . 'assets/img/project-wide.jpg'),
                            ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-project-image-gallery',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'required' => array(
                                array('opt-project-layout', "!=", 3),
                                array('opt-project-layout', "!=", 4)
                            ),
                        'title'     => __('Project Image Gallery', 'Elise'),
                        'subtitle'  => __('Project images gallery Layout.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Inline',  'img' => ReduxFramework::$_url . 'assets/img/project-gallery-slider.jpg'),
                            '2' => array('alt' => 'Slider',       'img' => ReduxFramework::$_url . 'assets/img/project-gallery-inline.jpg')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'            => 'opt-gallery-wide-height',
                        'type'          => 'slider',
                        'title'         => __('Gallery Images Height', 'Elise'),
                        'required'      => array('opt-project-layout', "=", 3),                      
                        'subtitle'      => __('Wide Gallery images height.', 'Elise'),
                        'desc'          => __('Min: 300, max: 1000, default value: 700', 'Elise'),
                        'default'       => 700,
                        'min'           => 300,
                        'step'          => 5,
                        'max'           => 1000,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'       => 'opt-project-desc-position',
                        'type'     => 'select',
                        'required' => array('opt-project-layout', "=", 1),
                        'title'    => __('Project Description Position', 'Elise'), 
                        'select2'  => array( 'allowClear' => false, 'minimumResultsForSearch' => -1),
                        'subtitle' => __('Choose position where project description and meta will show up.', 'Elise'),
                        //'desc'     => __('This is the description field, again good for additional info.', 'Elise'),
                        // Must provide key => value pairs for select options
                        'options'  => array(
                            '1' => 'Above the Project Gallery',
                            '2' => 'Below the Project Gallery'
                        ),
                        'default'  => '1',
                    ),

                    array(
                        'id'        => 'opt-project-desc-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'required' => array(
                                array('opt-project-layout', "!=", 2),
                                array('opt-project-layout', "!=", 4)
                            ),
                        'title'     => __('Project Description Style', 'Elise'),
                        'subtitle'  => __('Choose style of description.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Description Full Width',       'img' => ReduxFramework::$_url . 'assets/img/project-desc-full.jpg'),
                            '2' => array('alt' => 'Description with Sidebar',  'img' => ReduxFramework::$_url . 'assets/img/project-desc-sidebar.jpg')
                        ),
                        'default'   => '2'
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('WooCommerce', 'Elise'),
                'desc'      => __('WooCommerce Settings.', 'Elise'),
                'icon'      => ' el-icon-shopping-cart',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-woo-shop-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Shop Layout', 'Elise'),
                        // 'subtitle'  => __('Shop Layout.', 'Elise'),
                        'options'   => array(
                            '1' => array('alt' => 'Full Width',  'img' => ReduxFramework::$_url . 'assets/img/woo_fw.jpg'),
                            '2' => array('alt' => 'Sidebar',       'img' => ReduxFramework::$_url . 'assets/img/woo_side.jpg')
                        ),
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-woo-shop-sidebar-position',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Sidebar Position', 'Elise'),
                        // 'subtitle'  => __('Select sidebar position.', 'Elise'),
                        'required' => array('opt-woo-shop-layout','=','2'),
                        'options'   => array(
                            '1' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/elise-sr.png'),
                            '2' => array('alt' => 'Sidebar Left', 'img' => ReduxFramework::$_url . 'assets/img/elise-sl.png')
                        ),
                        'default'   => '2'
                    ),

                    array(
                        'id'            => 'opt-woo-shop-items-columns',
                        'type'          => 'slider',
                        'title'         => __('Products Column Number', 'Elise'),                     
                        // 'subtitle'      => __('Columns number', 'Elise'),
                        'desc'          => __('Min: 2, max: 5, default value: 3', 'Elise'),
                        'default'       => 3,
                        'min'           => 2,
                        'step'          => 1,
                        'max'           => 5,
                        'display_value' => 'text'
                    ),

                    array(
                        'id'        => 'opt-woo-shop-nav-icon',
                        'type'      => 'switch',
                        'title'     => __('Shopping Bag Icon / Cart', 'Elise'),
                        'subtitle'  => __('Show shopping bag icon and cart in main navigation.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-shop-welcome',
                        'type'      => 'text',
                        'title'     => __('Shop Title Text', 'Elise'),
                        'subtitle'  => __('Shop page title text.', 'Elise'),
                        //'desc'      => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                        // 'validate'  => 'str_replace',
                        // 'str'       => array(
                        //     'search'        => ' ', 
                        //     'replacement'   => '-'
                        // ),
                        'validate' => 'no_html',
                        'default'   => __('Shop', 'Elise'),
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Styling', 'Elise'),
                //'desc'      => __('Styling settings.', 'Elise'),
                'icon'      => ' el-icon-tint',
                'class'     => 'elise_opt_styling',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'       => 'opt-color-body-bg',
                        'type'     => 'color',
                        'title'    => __( 'Body Background Color', 'Elise' ),
                        'subtitle' => __( 'Pick a body background color (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-color-body-text',
                        'type'     => 'color',
                        'title'    => __( 'Body Text Color', 'Elise' ),
                        'subtitle' => __( 'Pick a body text color (default: #747474)', 'Elise' ),
                        'default'  => '#747474',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-color-headings',
                        'type'     => 'color',
                        'title'    => __( 'Headings Color', 'Elise' ),
                        'subtitle' => __( 'Pick a headings color (default: #262626)', 'Elise' ),
                        'default'  => '#262626',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-color-accent',
                        'type'     => 'color',
                        'title'    => __( 'Primary/Accent Color', 'Elise' ),
                        'subtitle' => __( 'Pick a accent color for the theme (default: #8dc73f)', 'Elise' ),
                        'default'  => '#8dc73f',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-link-colors',
                        'type'     => 'link_color',
                        'title'    => __( 'Link Colors', 'Elise' ),
                        'subtitle' => __( 'Pick link colors. (default: #8dc73f/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#8dc73f',
                            'hover'   => '#8dc73f',
                        )
                    ),

                    // Navigation ------------------------------------------- /
                    array(
                        'id' => 'section-h-start',
                        'type' => 'section',
                        'title' => __('Navigation Bar', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-navigation-color-bg',
                        'type'     => 'color',
                        'title'    => __( 'Navigation Background Color', 'Elise' ),
                        'subtitle' => __( 'Pick a navigation bar background color (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-navigation-shadow',
                        'type'      => 'switch',
                        'title'     => __('Navigation Bar Shadow', 'Elise'),
                        'subtitle'  => __('Show shadow under navbar and submenus.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'opt-nav-transparent-bg',
                        'type'      => 'color_rgba',
                         'required'      => array('opt-navbar-transparent','=','1'),
                        'title'    => __( 'Transparent Navigation Background', 'Elise' ),
                        'subtitle' => __( 'Pick trasparency settings for Tranparent Header. (default: rgba(0,0,0,0))', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => 0),
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'        => 'opt-navbar-border',
                        'type'      => 'color_rgba',
                        'title'    => __( 'Navigation Bar Bottom Border Color', 'Elise' ),
                        'subtitle' => __( 'Bottom border color (Regular and Transparent Header).', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => 0),
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'       => 'opt-navigation-color',
                        'type'     => 'link_color',
                        'title'    => __( 'Menu Links Colors', 'Elise' ),
                        'subtitle' => __( 'Pick menu link colors. (default: #262626/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#8dc73f',
                        )
                    ),

                    array(
                        'id'       => 'opt-navigation-submenu-color-bg',
                        'type'     => 'color',
                        'title'    => __( 'Submenu Background Color', 'Elise' ),
                        'subtitle' => __( 'Pick a submenu background color (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-navigation-submenu-color',
                        'type'     => 'link_color',
                        'title'    => __( 'Submenu Links Colors', 'Elise' ),
                        'subtitle' => __( 'Pick submenu link colors. (default: #262626/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#8dc73f',
                        )
                    ),

                    array(
                        'id'        => 'opt-navigation-submenu-border',
                        'type'      => 'border',
                        'title'     => __('Submenu Bottom Border Line', 'Elise'),
                        'subtitle'  => __('Pick submenu border bottom settings. (default: 1px solid #e9e9e9)', 'Elise'),
                        'all'       => false,
                        'bottom'    => true,
                        'top'       => false,
                        'left'       => false,
                        'right'       => false,
                        'default'   => array(
                            'border-color'  => '#e9e9e9', 
                            'border-style'  => 'solid', 
                            'border-bottom' => '1px',
                        )
                    ),

                    // array(
                    //     'id'       => 'opt-nav-arrows',
                    //     'type'     => 'color',
                    //     'title'    => __( 'Submenu Arrow Icon', 'Elise' ),
                    //     'subtitle' => __( 'Pick submenu arrow color. (default: #D3D3D3)', 'Elise' ),
                    //     'default'  => '#D3D3D3',
                    //     'transparent' => false,
                    //     'validate' => 'color',
                    // ),

                    array(
                        'id'       => 'opt-navigation-icons-color',
                        'type'     => 'color',
                        'title'    => __( 'Nav Icons Color', 'Elise' ),
                        'subtitle' => __( 'Pick nav icons link color. (default: #888888)', 'Elise' ),
                        'default'  => '#888888',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-navigation-menu-border',
                        'type'      => 'border',
                        'title'     => __('Menu top border', 'Elise'),
                        'subtitle'  => __('Border settings for centered and extended header style. (default: 1px solid #e9e9e9)', 'Elise'),
                        'all'       => false,
                        'bottom'    => false,
                        'top'       => true,
                        'left'       => false,
                        'right'       => false,
                        'default'   => array(
                            'border-color'  => '#e9e9e9', 
                            'border-style'  => 'solid', 
                            'border-top' => '1px',
                        )
                    ),

                    array(
                        'id' => 'section-h-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    

                    // Sticky Header -------------------------------------- /
                    array(
                        'id' => 'section-sticky-h-start',
                        'type' => 'section',
                        'title' => __('Sticky Header', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-stickyh-settings',
                        'type'      => 'button_set',
                        'title'     => __('Sticky Header Colors', 'Elise'),
                        'required'  => array('opt-show-sticky-header','=','1'),
                        // 'subtitle'  => __('Select overlay backgroud color style.', 'Elise'),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => __('Same As Navigation Bar', 'Elise'), 
                            '2' => __('Custom Colors', 'Elise')
                        ), 
                        'default'   => '1'
                    ),

                    array(
                        'id'        => 'opt-stickyheader-bg',
                        'type'      => 'color_rgba',
                        'required'      => array(
                            array('opt-stickyh-settings','=','2'),
                            array('opt-show-sticky-header','=','1'),
                        ),
                        'title'    => __( 'Sticky Header Background', 'Elise' ),
                        'subtitle' => __( 'Pick color settings for Sticky Header BG. (default: rgba(255,255,255,0.96))', 'Elise' ),
                        'default'   => array('color' => '#ffffff', 'alpha' => '0.96'),
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'       => 'opt-stickyh-color',
                        'type'     => 'link_color',
                        'title'    => __( 'Sticky Header Menu Colors', 'Elise' ),
                        'required'      => array(
                            array('opt-stickyh-settings','=','2'),
                            array('opt-show-sticky-header','=','1'),
                        ),
                        'subtitle' => __( 'Pick menu link colors. (default: #262626/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#8dc73f',
                        )
                    ),

                    array(
                        'id'       => 'opt-stickyh-nav-icons',
                        'type'     => 'color',
                        'title'    => __( 'Nav Icons Color', 'Elise' ),
                        'required'      => array(
                            array('opt-stickyh-settings','=','2'),
                            array('opt-show-sticky-header','=','1'),
                        ),
                        'subtitle' => __( 'Pick sticky header nav icons color. (default: #888888)', 'Elise' ),
                        'default'  => '#888888',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'     => 'opt-sticky-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-show-sticky-header','=','0'),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Sticky Header is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Header -> Sticky Header Section to Enabled Sticky Header.', 'Elise' )
                    ),

                    array(
                        'id' => 'section-sticky-h-end',
                        'type' => 'section',
                        'indent' => false 
                    ),



                    // Full Width Navigation -------------------------------------- /
                    array(
                        'id' => 'section-fwnav-start',
                        'type' => 'section',
                        'title' => __('Full Width/Mobile Navigation', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-fwnav-overlay',
                        'type'      => 'button_set',
                        'title'     => __('Overlay Background', 'Elise'),
                        'subtitle'  => __('Select overlay backgroud color style.', 'Elise'),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => __('Custom Solid Color', 'Elise'), 
                            '2' => __('Accent Color Gradient', 'Elise')
                        ), 
                        'default'   => '2'
                    ),

                    array(
                        'id'        => 'opt-fwnav-overlay-solid',
                        'type'      => 'color_rgba',
                        'required'  => array('opt-fwnav-overlay', "=", 1),
                        'title'    => __( 'Overlay Background - Solid Color', 'Elise' ),
                        'subtitle' => __( 'Overlay Solid Color (default: #000000, alpha: 0.8)', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => '0.8'),
                        'transparent' => true,
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    // array(
                    //     'id'        => 'opt-fwnav-style-choose',
                    //     'type'      => 'button_set',
                    //     'title'     => __('Full Nav/Mobile Style Settings', 'Elise'),
                    //     'subtitle'  => __('Select settings for style:', 'Elise'),
                    //     //Must provide key => value pairs for radio options
                    //     'options'   => array(
                    //         '1' => 'Sidebar Nav', 
                    //         '2' => 'Full Width Nav'
                    //     ), 
                    //     'default'   => '1'
                    // ),

                    array(
                        'id'       => 'opt-fwnav-sidebar-background',
                        'type'     => 'color',
                        'title'    => __( 'Sidebar Background Color', 'Elise' ),
                        'required'  => array('opt-secondary-nav-style', "=", 1),
                        'subtitle' => __( 'Pick full width - sidebar nav background color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-fwnav-sidebar-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Sidebar Link Colors', 'Elise' ),
                        'required'  => array('opt-secondary-nav-style', "=", 1),
                        'subtitle' => __( 'Pick full width - sidebar nav link colors. (default: #262626/#262626)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#262626',
                        )
                    ),

                    array(
                        'id'       => 'opt-fwnav-fw-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Full Width Link Colors', 'Elise' ),
                        'required'  => array('opt-secondary-nav-style', "=", 2),
                        'subtitle' => __( 'Pick full width nav link colors. (default: #ffffff/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#ffffff',
                            'hover'   => '#8dc73f',
                        )
                    ),

                    array(
                        'id'       => 'opt-fwnav-fw-background',
                        'type'     => 'color',
                        'title'    => __( 'Full Width Hover Background Color', 'Elise' ),
                        'required'  => array('opt-secondary-nav-style', "=", 2),
                        'subtitle' => __( 'Pick full width nav hover background color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),


                    array(
                        'id' => 'section-fwnav-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // Top Bar ------------------------------------------- /
                    array(
                        'id' => 'section-topbar-start',
                        'type' => 'section',
                        'title' => __('Top Bar', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-topbar-background',
                        'type'     => 'color',
                        'title'    => __( 'Top Bar Background', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar background color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-topbar-color',
                        'type'     => 'color',
                        'title'    => __( 'Top Bar Text Color', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar text color. (default: #b3b3b3)', 'Elise' ),
                        'default'  => '#b3b3b3',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-topbar-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Top Bar Links', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar link colors. (default: #b3b3b3/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#b3b3b3',
                            'hover'   => '#b3b3b3',
                        )
                    ),

                    array(
                        'id'       => 'opt-topbar-social',
                        'type'     => 'color',
                        'title'    => __( 'Top Bar Social Icons Color', 'Elise' ),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar social icons color. (default: #b3b3b3)', 'Elise' ),
                        'default'  => '#b3b3b3',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-topbar-border',
                        'type'      => 'border',
                        'title'     => __('Top Bar Borders', 'Elise'),
                        'required'  => array('opt-show-top-bar', "=", 1),
                        'subtitle'  => __('Pick top bar borders settings. (default: 1px solid #f0f0f0)', 'Elise'),
                        'all'       => false,
                        'bottom'    => true,
                        'top'       => false,
                        'left'       => false,
                        'right'       => false,
                        'default'   => array(
                            'border-color'  => '#f0f0f0', 
                            'border-style'  => 'solid', 
                            'border-bottom' => '1px',
                        )
                    ),

                    array(
                        'id'     => 'opt-topbar-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-show-top-bar', "=", 0),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Top Bar is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Header -> Top Bar Section to Enabled Top Bar', 'Elise' )
                    ),

                    array(
                        'id' => 'section-topbar-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // Page Title ------------------------------------------- /
                    array(
                        'id' => 'section-pt-start',
                        'type' => 'section',
                        'class' => 'aaa',
                        'title' => __('Title Bar', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-pagetitle-color',
                        'type'     => 'color',
                        'title'    => __( 'Page Title Text Color', 'Elise' ),
                        'required'      => array('opt-title-bar', "=", 1),
                        'subtitle' => __( 'Pick page title text color. (default: #262626)', 'Elise' ),
                        'default'  => '#262626',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    // array(
                    //     'id'        => 'opt-pt-text-bg',
                    //     'type'      => 'color_rgba',
                    //     'title'    => __( 'Page Title Text BG Color', 'Elise' ),
                    //     'required'      => array('opt-title-bar', "=", 1),
                    //     'subtitle' => __( 'Pick page title text bg color. (default: rgba(0, 0, 0, 0))', 'Elise' ),
                    //     'default'   => array('color' => '#000000', 'alpha' => 0),
                    //     'options' => array(
                    //         // 'allow_empty' => false,
                    //         'clickout_fires_change' => true,
                    //     ),
                    //     'validate'  => 'colorrgba',
                    // ),

                    array(
                        'id'        => 'opt-titletext-bg',
                        'type'      => 'color_rgba',
                        'title'    => __( 'Page Title Text BG Color', 'Elise' ),
                        'required'      => array('opt-title-bar', "=", 1),
                        'subtitle' => __( 'Pick page title text bg color. (default: rgba(0, 0, 0, 0))', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => 0),
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'        => 'opt-pt-bg',
                        'type'      => 'background',
                        'title'     => __('Page Title Background', 'Elise'),
                        'required'  => array('opt-title-bar', "=", 1),
                        'subtitle'  => __('Pick Page Title background color or image.', 'Elise'),
                        'preview_height' => '110px',
                        'preview'   => true,
                        'preview_media' => true,
                        'default'   => array(
                            'background-color' => "#f5f5f5"
                        ),
                    ),

                    array(
                        'id'        => 'opt-pt-overlay',
                        'type'      => 'switch',
                        'title'     => __('Page Title image bg Overlay', 'Elise'),
                        'required'  => array('opt-title-bar', "=", 1),
                        'subtitle'  => __('Show overlay on title bar image background. (default: #000000, alpha: 0.8)', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'opt-pt-overlay-bg',
                        'type'      => 'color_rgba',
                        'title'    => __( 'Page Title image bg Overlay Color', 'Elise' ),
                        'required'  => array( 
                            array('opt-pt-overlay', "=", 1),
                            array('opt-title-bar', "=", 1)
                        ),
                        'subtitle' => __( 'Page Title image overlay color.', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => '0.8'),
                        'transparent' => true,
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'     => 'opt-titlebar-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required' => array('opt-title-bar', "=", 0),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Title Bar is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Title Bar Section to Enabled Title Bar', 'Elise' )
                    ),

                    array(
                        'id' => 'section-pt-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    array(
                        'id' => 'section-breadcrumbs-start',
                        'type' => 'section',
                        'title' => __('Breadcrumbs Bar', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-breadcrumbs-background',
                        'type'     => 'color',
                        'title'    => __( 'Breadcrumbs Bar Background', 'Elise' ),
                        'required'  => array('opt-breadcrumbs-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar background color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-breadcrumbs-color',
                        'type'     => 'color',
                        'title'    => __( 'Breadcrumbs Bar Text Color', 'Elise' ),
                        'required'  => array('opt-breadcrumbs-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar text color. (default: #656565)', 'Elise' ),
                        'default'  => '#656565',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-breadcrumbs-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Breadcrumbs Bar Links Color', 'Elise' ),
                        'required'  => array('opt-breadcrumbs-bar', "=", 1),
                        'subtitle' => __( 'Pick top bar link colors. (default: #262626/#262626)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#262626',
                        )
                    ),

                    array(
                        'id'        => 'opt-breadcrumbs-border',
                        'type'      => 'border',
                        'title'     => __('Breadcrumbs Bar Borders', 'Elise'),
                        'required'  => array('opt-breadcrumbs-bar', "=", 1),
                        'subtitle'  => __('Pick top bar borders settings. (default: 1px solid #f0f0f0)', 'Elise'),
                        'all'       => false,
                        'bottom'    => true,
                        'top'       => false,
                        'left'       => false,
                        'right'       => false,
                        'default'   => array(
                            'border-color'  => '#f0f0f0', 
                            'border-style'  => 'solid', 
                            'border-bottom' => '1px',
                        )
                    ),

                    array(
                        'id'     => 'opt-breadbar-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-breadcrumbs-bar', "=", 0),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Breadcrumbs Bar is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Title Bar Section to Enabled Breadcrumbs Bar', 'Elise' )
                    ),

                    array(
                        'id' => 'section-breadcrumbs-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // BLOG ------------------------------------------- /
                    array(
                        'id' => 'section-blog-start',
                        'type' => 'section',
                        'title' => __('Blog', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-blog-heading-color',
                        'type'     => 'link_color',
                        'title'    => __( 'Blog Post Title Colors', 'Elise' ),
                        'subtitle' => __( 'Pick blog heading colors. (default: #262626/#8dc73f)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#8dc73f',
                        )
                    ),

                    array(
                        'id'       => 'opt-blog-content-color',
                        'type'     => 'color',
                        'title'    => __( 'Blog Post Content Text Color', 'Elise' ),
                        'subtitle' => __( 'Pick content text color. (default: #747474)', 'Elise' ),
                        'default'  => '#747474',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-blog-thumb-overlay',
                        'type'      => 'button_set',
                        'title'     => __('Blog Post Thumnbnail Overlay', 'Elise'),
                        'subtitle'  => __('Select Blog thumbnail vverlay style', 'Elise'),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'Custom Solid Color', 
                            '2' => 'Accent Color Gradient'
                        ), 
                        'default'   => '2'
                    ),

                    array(
                        'id'        => 'opt-blog-th-over',
                        'type'      => 'color_rgba',
                        'required'  => array('opt-blog-thumb-overlay', "=", 1),
                        'title'    => __( 'Blog Post Thumnbnail - Overlay Color', 'Elise' ),
                        'subtitle' => __( 'Page Title image overlay color. (default: rgba(0,0,0,0.8)).', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => '0.8'),
                        'transparent' => true,
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    // array(
                    //     'id'        => 'opt-blog-th-border',
                    //     'type'      => 'border',
                    //     'title'     => __('Blog Post Thumnbnail - Border', 'Elise'),
                    //     'subtitle'  => __('Pick blog thumbnail border settings. (default: 1px solid #e9e9e9)', 'Elise'),
                    //     'default'   => array(
                    //         'border-color'  => '#e9e9e9', 
                    //         'border-style'  => 'solid', 
                    //         'border-top'    => '1px', 
                    //         'border-right'  => '1px', 
                    //         'border-bottom' => '1px', 
                    //         'border-left'   => '1px'
                    //     )
                    // ),

                    array(
                        'id'       => 'opt-blog-time-color',
                        'type'     => 'color',
                        'title'    => __( 'Post Date Color', 'Elise' ),
                        'subtitle' => __( 'Pick date text color. (default: #8dc73f)', 'Elise' ),
                        'default'  => '#8dc73f',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-blog-masonry-border',
                        'type'      => 'border',
                        'title'     => __('Blog Masonry - Post Border', 'Elise'),
                        'subtitle'  => __('Pick blog masonry post border settings. (default: 1px solid #eeeeee)', 'Elise'),
                        'default'   => array(
                            'border-color'  => '#eeeeee', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px', 
                            'border-right'  => '1px', 
                            'border-bottom' => '1px', 
                            'border-left'   => '1px'
                        )
                    ),

                    array(
                        'id'       => 'opt-blog-comment-bg',
                        'type'     => 'color',
                        'title'    => __( 'Blog Comment Background Color', 'Elise' ),
                        'subtitle' => __( 'Pick comment bg color. (default: #f3f3f3)', 'Elise' ),
                        'default'  => '#f3f3f3',
                        'transparent' => true,
                        'validate' => 'color',
                    ),

                    array(
                        'id' => 'section-blog-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // Portfolio ------------------------------------------- /
                    array(
                        'id' => 'section-portfolio-start',
                        'type' => 'section',
                        'title' => __('Portfolio', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-portfolio-style-choose',
                        'type'      => 'button_set',
                        'title'     => __('Portfolio Item Style Settings', 'Elise'),
                        'subtitle'  => __('Select settings for style:', 'Elise'),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'Style 1', 
                            '2' => 'Style 2'
                        ), 
                        'default'   => '1'
                    ),

                    array(
                        'id'       => 'opt-portfolio1-heading-color',
                        'type'     => 'color',
                        'required'  => array('opt-portfolio-style-choose', "=", 1),
                        'title'    => __( 'Portfolio Item "Style 1" Heading Color', 'Elise' ),
                        'subtitle' => __( 'Pick portfolio item heading text color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-portfolio1-gradient',
                        'type'      => 'switch',
                        'required'  => array('opt-portfolio-style-choose', "=", 1),
                        'title'     => __('Portfolio Item "Style 1" Gradient', 'Elise'),
                        'subtitle'  => __('Show gradient under Portfolio Item Title.', 'Elise'),
                        'default'   => true,
                    ),

                    array(
                        'id'       => 'opt-portfolio2-heading-color',
                        'type'     => 'link_color',
                        'required'  => array('opt-portfolio-style-choose', "=", 2),
                        'title'    => __( 'Portfolio Item "Style 2" Heading Colors', 'Elise' ),
                        'subtitle' => __( 'Pick portfolio item heading colors. (default: #262626/#262626)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#262626',
                            'hover'   => '#262626',
                        )
                    ),

                    array(
                        'id'        => 'opt-portfolio-thumb-overlay',
                        'type'      => 'button_set',
                        'title'     => __('Portfolio Thumnbnail Overlay', 'Elise'),
                        'subtitle'  => __('Select Portfolio Thumbnail overlay style', 'Elise'),
                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            '1' => 'Custom Solid Color', 
                            '2' => 'Accent Color Gradient'
                        ), 
                        'default'   => '2'
                    ),

                    array(
                        'id'        => 'opt-portfolio-th-over',
                        'type'      => 'color_rgba',
                        'required'  => array('opt-portfolio-thumb-overlay', "=", 1),
                        'title'    => __( 'Overlay Color', 'Elise' ),
                        'subtitle' => __( 'Portfolio Thumbnail overlay color. (default: rgba(0,0,0,0.8))', 'Elise' ),
                        'default'   => array('color' => '#000000', 'alpha' => '0.8'),
                        'transparent' => true,
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'       => 'opt-portfolio-overlay-color',
                        'type'     => 'color',
                        'title'    => __( 'Portoflio Overlay Text Color', 'Elise' ),
                        'subtitle' => __( 'Pick portfolio overlay text color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-portfolio-filter-bg',
                        'type'     => 'color',
                        'title'    => __( 'Portfolio Filtering Background', 'Elise' ),
                        'subtitle' => __( 'Pick portfolio filtering background. (default: #333333)', 'Elise' ),
                        'default'  => '#333333',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-portfolio-filter-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Portfolio Filtering Links', 'Elise' ),
                        'subtitle' => __( 'Pick portfolio filtering links. (default: #dddddd/#ffffff)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        'hover'     => false, // Disable Hover Color
                        'active'    => true, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#dddddd',
                            'active'  => '#ffffff',
                        )
                    ),

                    array(
                        'id'       => 'opt-portfolio-filter-active-bg',
                        'type'     => 'color',
                        'title'    => __( 'Portfolio Filtering Active BG', 'Elise' ),
                        'subtitle' => __( 'Pick portfolio filtering active item background. (default: #333333)', 'Elise' ),
                        'default'  => '#8dc73f',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-portfolio-project-wide',
                        'type'     => 'color',
                        'title'    => __( 'Project Style Wide BG', 'Elise' ),
                        'subtitle' => __( 'Pick project style wide gallery background. (default: #333333)', 'Elise' ),
                        'default'  => '#333333',
                        'required'  => array('opt-project-layout','=','3'),
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id' => 'section-portfolio-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // Footer ------------------------------------------- /
                    array(
                        'id' => 'section-footer-start',
                        'type' => 'section',
                        'title' => __('Footer', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-footer-widgets-bg',
                        'type'     => 'color',
                        'title'    => __( 'Footer Widget Area Background', 'Elise' ),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'subtitle' => __( 'Pick footer widget area background. (default: #252525)', 'Elise' ),
                        'default'  => '#252525',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-footer-widgets-text',
                        'type'     => 'color',
                        'title'    => __( 'Footer Widget Area Text Color', 'Elise' ),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'subtitle' => __( 'Pick footer widget area text color. (default: #e6e6e6)', 'Elise' ),
                        'default'  => '#e6e6e6',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-footer-widgets-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Footer Widgets Area Link Colors', 'Elise' ),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'subtitle' => __( 'Pick widget area link colors (default: #e6e6e6/#e6e6e6)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#e6e6e6',
                            'hover'   => '#e6e6e6',
                        )
                    ),

                    array(
                        'id'       => 'opt-footer-bgwidgets',
                        'type'     => 'color_rgba',
                        'title'    => __( 'Footer Widgets Background', 'Elise' ),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'subtitle' => __( 'Pick background color for widgets with custom backgroud - "Latest news, RSS etc. (default: rgba(255,255,255,0.05))', 'Elise' ),
                        'default'   => array('color' => '#ffffff', 'alpha' => '0.05'),
                        'transparent' => false,
                        'options' => array(
                            'allow_empty' => false,
                            'clickout_fires_change' => true,
                        ),
                        'validate'  => 'colorrgba',
                    ),

                    array(
                        'id'        => 'opt-footer-widgets-border',
                        'type'      => 'border',
                        'title'     => __('Footer Widget Area Borders', 'Elise'),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'subtitle'  => __('Pick borders settings for footer widget area. (default: 1px solid #3a3a3a)', 'Elise'),
                        'all'       => false,
                        'bottom'    => true,
                        'top'       => true,
                        'left'       => false,
                        'right'       => false,
                        'default'   => array(
                            'border-color'  => '#3a3a3a', 
                            'border-style'  => 'solid', 
                            'border-bottom' => '1px', 
                            'border-top' => '0px', 
                        )
                    ),

                    array(
                        'id'     => 'opt-footer-widget-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required'  => array('opt-footer-widget-area','=','0'),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Widget Area is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Footer Section to Enabled Widget Area.', 'Elise' )
                    ),

                    array(
                        'id'       => 'opt-footer-copyrights-bg',
                        'type'     => 'color',
                        'title'    => __( 'Footer Copyright Area Background', 'Elise' ),
                        'subtitle' => __( 'Pick footer widget area background. (default: #2d2d2d)', 'Elise' ),
                        'default'  => '#2d2d2d',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-footer-copyrights-text',
                        'type'     => 'color',
                        'title'    => __( 'Footer Copyright Area Text Color', 'Elise' ),
                        'subtitle' => __( 'Pick footer widget area text color. (default: #999999)', 'Elise' ),
                        'default'  => '#999999',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-footer-copyrights-links',
                        'type'     => 'link_color',
                        'title'    => __( 'Footer Copyright Area Link Colors', 'Elise' ),
                        'subtitle' => __( 'Pick copyrights widget area link colors (default: #cccccc/#cccccc)', 'Elise' ),
                        //'regular'   => false, // Disable Regular Color
                        //'hover'     => false, // Disable Hover Color
                        'active'    => false, // Disable Active Color
                        //'visited'   => true,  // Enable Visited Color
                        'default'  => array(
                            'regular' => '#cccccc',
                            'hover'   => '#cccccc',
                        )
                    ),

                    array(
                        'id' => 'section-footer-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // Widgets ------------------------------------------- /
                    array(
                        'id' => 'section-widgets-start',
                        'type' => 'section',
                        'title' => __('Widgets', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'       => 'opt-widget-h-color',
                        'type'     => 'color',
                        'title'    => __( 'Widget Title Color', 'Elise' ),
                        'subtitle' => __( 'Pick widget title color. (default: #656565)', 'Elise' ),
                        'default'  => '#656565',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-widget-h-footer-color',
                        'type'     => 'color',
                        'title'    => __( 'Footer Widget Title Color', 'Elise' ),
                        'required'  => array('opt-footer-widget-area','=','1'),
                        'subtitle' => __( 'Pick footer widget title color. (default: #999999)', 'Elise' ),
                        'default'  => '#999999',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id' => 'section-widgets-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    // Misc ------------------------------------------- /
                    array(
                        'id' => 'section-misc-start',
                        'type' => 'section',
                        'title' => __('Misc', 'Elise'),
                        'indent' => false 
                    ),

                    array(
                        'id'        => 'opt-misc-hr',
                        'type'      => 'border',
                        'title'     => __('HR and Lines', 'Elise'),
                        'all'       => false,
                        'bottom'    => true,
                        'top'       => false,
                        'left'       => false,
                        'right'       => false,
                        'subtitle'  => __('Pick hr line settings. (default: 1px solid #e9e9e9)', 'Elise'),
                        'default'   => array(
                            'border-color'  => '#e9e9e9', 
                            'border-style'  => 'solid', 
                            'border-bottom' => '1px', 
                        )
                    ),

                    array(
                        'id'       => 'opt-misc-form-text',
                        'type'     => 'color',
                        'title'    => __( 'Form Text Color', 'Elise' ),
                        'subtitle' => __( 'Pick form text color. (default: #656565)', 'Elise' ),
                        'default'  => '#656565',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-misc-form-bg',
                        'type'     => 'color',
                        'title'    => __( 'Form Background', 'Elise' ),
                        'subtitle' => __( 'Pick form background color. (default: #ffffff)', 'Elise' ),
                        'default'  => '#ffffff',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'        => 'opt-misc-form-border',
                        'type'      => 'border',
                        'title'     => __('Form Border', 'Elise'),
                        'subtitle'  => __('Pick form border settings. (default: 1px solid #cccccc)', 'Elise'),
                        'default'   => array(
                            'border-color'  => '#cccccc', 
                            'border-style'  => 'solid', 
                            'border-top'    => '1px', 
                            'border-right'  => '1px', 
                            'border-bottom' => '1px', 
                            'border-left'   => '1px'
                        )
                    ),

                    array(
                        'id'       => 'opt-misc-sn-bg',
                        'type'     => 'color',
                        'title'    => __( 'Template Side Navigation - Menu Current BG', 'Elise' ),
                        'subtitle' => __( 'Pick side navigation template menu active item bg. (default: #f7f7f7)', 'Elise' ),
                        'default'  => '#f7f7f7',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'opt-misc-sn-text',
                        'type'     => 'color',
                        'title'    => __( 'Template Side Navigation - Menu Current Color', 'Elise' ),
                        'subtitle' => __( 'Pick side navigation template menu active item bg. (default: #8dc73f)', 'Elise' ),
                        'default'  => '#8dc73f',
                        'transparent' => false,
                        'validate' => 'color',
                    ),

                    array(
                        'id' => 'section-misc-end',
                        'type' => 'section',
                        'indent' => false 
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Typography', 'Elise'),
                //'desc'      => __('Typography settings.', 'Elise'),
                'icon'      => 'el-icon-font',
                'class'     => 'elise_opt_styling',
                'subsection' => true,
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'          => 'opt-typo-body',
                        'type'        => 'typography', 
                        'title'       => __('Body', 'Elise'),
                        'subtitle'    => __('Defaults: font-family: "Raleway", font-weight: 400, font-size: 14px, line-height: 24px', 'Elise'),
                        'google'      => true, 
                        'units'       =>'px',
                        'color'       => false,
                        'font-weight' => true,
                        'font-style' => false,
                        'text-align'  => false,
                        'font-backup' => false,
                        'font_family_clear' => false,
                        'all_styles'  => true,
                        'preview'     => array(
                                'always_display' => false,
                                'text'           => 'Grumpy wizards make toxic... / 0123456789',
                            ),
                        'default'     => array(
                            //'color'       => '#333', 
                            'font-weight'  => '400', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'font-size'   => '14px', 
                            'line-height' => '24px',
                        ),
                    ),

                    array(
                        'id' => 'section-mainnav-typo-start',
                        'type' => 'section',
                        'title' => __('Main Navigation Menu', 'Elise'),
                        //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                        'indent' => false 
                    ),

                    array(
                        'id'          => 'opt-typo-main-nav',
                        'type'        => 'typography', 
                        'title'       => __('Main Menu', 'Elise'),
                        'google'      => true, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'line-height' => false,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Home Portfolio Blog Contact',
                        ),
                        'subtitle'    => __('Main Navigation typography. (defaults: font-family: "Raleway", font-size: 14px, font-weight: 400, letter-spacing: 0px, text-transform: NULL)', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '400', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'font-size'   => '14px',
                            'text-transform' => '',
                            'letter-spacing' => '0px',
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-main-submenu',
                        'type'        => 'typography', 
                        'title'       => __('Sub Menu', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'font-family' => false,
                        'font-style'  => false,
                        'font-weight' => true,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'line-height' => false,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Home Portfolio Blog Contact',
                        ),
                        'subtitle'    => __('Main Navigation Submenu typography. (defaults: font-size: 12px, font-weight: NULL = Semibold 600, letter-spacing: 0px, text-transform: NULL)', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '', 
                            'font-size'   => '12px',
                            'text-transform' => '',
                            'letter-spacing' => '0px',
                        ),
                    ),

                    array(
                        'id' => 'section-mainnav-typo-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    array(
                        'id' => 'section-fw_nav-typo-start',
                        'type' => 'section',
                        'title' => __('Full Width Navigation', 'Elise'),
                        //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                        'indent' => false 
                    ),

                    array(
                        'id'          => 'opt-typo-fullw-nav',
                        'type'        => 'typography', 
                        'title'       => __('Full Width Menu', 'Elise'),
                        'required'      => array('opt-secondary-nav-style','=','2'),
                        'google'      => true, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'line-height' => true,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Home<br><br>Portfolio<br><br>Blog<br><br>Contact',
                        ),
                        'subtitle'    => __('Full Width Navigation typography. (defaults: font-family: "Raleway", font-size: 30px, line-height: 18px, font-weight: 300, letter-spacing: -1px, text-transform: NULL )', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '300', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'font-size'   => '30px',
                            'line-height'   => '18px',
                            'text-transform' => '',
                            'letter-spacing' => '-1px',
                        ),
                    ),

                    array(
                        'id'     => 'opt-fwnav-info',
                        'type'   => 'info',
                        'notice' => true,
                        'style'  => 'info',
                        'required' => array('opt-secondary-nav-style','!=','2'),
                        'icon'   => 'el-icon-info-sign',
                        'title'  => __( 'Full Width Navigation is Disabled.', 'Elise' ),
                        'desc'   => __( 'Go to Header -> Full Screen/Mobile Navigation to Enabled Full Width Navigation.', 'Elise' )
                    ),

                    array(
                        'id' => 'section-fw_nav-typo-end',
                        'type' => 'section',
                        'indent' => false 
                    ),

                    array(
                        'id' => 'section-heading-typo-start',
                        'type' => 'section',
                        'title' => __('Headings', 'Elise'),
                        //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
                        'indent' => false 
                    ),

                    array(
                        'id'          => 'opt-typo-basic-headings',
                        'type'        => 'typography', 
                        'title'       => __('Headings', 'Elise'),
                        'subtitle'    => __('Defaults: font-family: "Raleway", font-weight: 300, text-transform: none(null), letter-spacing: -1px', 'Elise'),
                        'google'      => true, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font-size'   => false,
                        'line-height' => false,
                        'font_family_clear' => false,
                        'all_styles'  => true,
                        'preview'     => array(
                            'always_display' => true,
                            'font-size'      => '30px',
                            'text'           => 'Grumpy wizards make toxic... / 0123456789',
                        ),
                        'default'     => array(
                            'font-weight'  => '300', 
                            'font-style'  => 'normal', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'letter-spacing' => '-1px',
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-basic-h1',
                        'type'        => 'typography', 
                        'title'       => __('H1', 'Elise'),
                        'subtitle'        => __('Defaults: 40px/48px', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'font-style' => false,
                        'font_family_clear' => false,
                        'default'     => array(
                            'font-size' => '40px',
                            'line-height' => '48px'
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-basic-h2',
                        'type'        => 'typography', 
                        'title'       => __('H2', 'Elise'),
                        'subtitle'        => __('Defaults: 32px/38px', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'font-style' => false,
                        'font_family_clear' => false,
                        'default'     => array(
                            'font-size' => '32px',
                            'line-height' => '38px'
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-basic-h3',
                        'type'        => 'typography', 
                        'title'       => __('H3', 'Elise'),
                        'subtitle'        => __('Defaults: 26px/30px', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'font-style' => false,
                        'font_family_clear' => false,
                        'default'     => array(
                            'font-size' => '26px',
                            'line-height' => '30px'
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-basic-h4',
                        'type'        => 'typography', 
                        'title'       => __('H4', 'Elise'),
                        'subtitle'        => __('Defaults: 18px/28px', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'font-style' => false,
                        'font_family_clear' => false,
                        'default'     => array(
                            'font-size' => '18px',
                            'line-height' => '28px'
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-basic-h5',
                        'type'        => 'typography', 
                        'title'       => __('H5', 'Elise'),
                        'subtitle'        => __('Defaults: 15px/24px', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'font-style' => false,
                        'font_family_clear' => false,
                        'default'     => array(
                            'font-size' => '15px',
                            'line-height' => '24px'
                        ),
                    ),

                    array(
                        'id'          => 'opt-typo-basic-h6',
                        'type'        => 'typography', 
                        'title'       => __('H6 / Small', 'Elise'),
                        'subtitle'        => __('Defaults: 13px/21px', 'Elise'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'font-family' => false,
                        'font-weight' => false,
                        'font-style' => false,
                        'font_family_clear' => false,
                        'default'     => array(
                            'font-size' => '13px',
                            'line-height' => '21px'
                        ),
                    ),

                    array(
                        'id'   =>'divider_1',
                        // 'desc' => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
                        'type' => 'divide',
                        'class' => 'hr'
                    ),

                    array(
                        'id'        => 'opt-custom-page-title-typo',
                        'type'      => 'switch',
                        'title'     => __('Customize Page Title', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'          => 'opt-page-title-heading',
                        'type'        => 'typography', 
                        'title'       => __('Page Title Heading', 'Elise'),
                        'required'      => array('opt-custom-page-title-typo','=','1'),
                        'google'      => true, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'line-height' => true,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Page Title',
                        ),
                        'subtitle'    => __('Page Title typography. (defaults: font-family: "Raleway", font-size: 26px, line-height: 30px, font-weight: 300, letter-spacing: -1px, text-transform: NULL)', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '300', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'font-size'   => '26px',
                            'line-height' => '30px',
                            'text-transform' => '',
                            'letter-spacing' => '-1px',
                        ),
                    ),

                    array(
                        'id'          => 'opt-page-subtitle-heading',
                        'type'        => 'typography', 
                        'title'       => __('Page Subtitle Heading', 'Elise'),
                        'required'      => array('opt-custom-page-title-typo','=','1'),
                        'google'      => false, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'font-family' => false,
                        'text-align'  => false,
                        'line-height' => true,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Page Subtitle. Nulla facilisi. Donec suscipit ullamcorper accumsan.',
                        ),
                        'subtitle'    => __('Page SubTitle typography. (defaults: font-size: 15px, line-height: 24px, font-weight: 300, letter-spacing: -1px, text-transform: NULL)', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '300', 
                            // 'font-family' => 'Raleway', 
                            // 'google'      => true,
                            'font-size'   => '15px',
                            'line-height' => '24px',
                            'text-transform' => '',
                            'letter-spacing' => '-1px',
                        ),
                    ),

                    array(
                        'id'        => 'opt-custom-post-title',
                        'type'      => 'switch',
                        'title'     => __('Customize Blog Post Title', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'          => 'opt-post-title',
                        'type'        => 'typography', 
                        'title'       => __('Blog Post Title', 'Elise'),
                        'required'      => array('opt-custom-post-title','=','1'),
                        'google'      => true, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'line-height' => true,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Hello World!',
                        ),
                        'subtitle'    => __('Blog Post Title typography. (defaults: font-family: "Raleway", font-size: 28px, line-height: 34px, font-weight: 300, letter-spacing: -1px, text-transform: NULL)', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '300', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'font-size'   => '28px',
                            'line-height' => '34px',
                            'text-transform' => '',
                            'letter-spacing' => '-1px',
                        ),
                    ),

                    array(
                        'id'        => 'opt-custom-portfolio-title',
                        'type'      => 'switch',
                        'title'     => __('Customize Portfolio Item Title', 'Elise'),
                        'default'   => false,
                    ),

                    array(
                        'id'          => 'opt-portfolio-title',
                        'type'        => 'typography', 
                        'title'       => __('Portfolio Item Title', 'Elise'),
                        'required'      => array('opt-custom-portfolio-title','=','1'),
                        'google'      => true, 
                        'font-backup' => false,
                        'subsets'     => false,
                        'units'       =>'px',
                        'color'       => false,
                        'text-align'  => false,
                        'line-height' => true,
                        'letter-spacing' => true,
                        'text-transform' => true,
                        'font_family_clear' => false,
                        'preview'     => array(
                            'always_display' => false,
                            'text'           => 'Portfolio Item',
                        ),
                        'subtitle'    => __('Portfolio Item typography. (defaults: font-family: "Raleway", font-size: 24px, line-height: 24px, font-weight: 300, letter-spacing: -1px, text-transform: NULL)', 'Elise'),
                        'default'     => array(
                            'font-weight'  => '300', 
                            'font-family' => 'Raleway', 
                            'google'      => true,
                            'font-size'   => '24px',
                            'line-height' => '28px',
                            'text-transform' => '',
                            'letter-spacing' => '-1px',
                        ),
                    ),

                    array(
                        'id' => 'section-heading-typo-end',
                        'type' => 'section',
                        'indent' => false 
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Social Sharing Links', 'Elise'),
                // 'desc'      => __('', 'Elise'),
                'icon'      => 'el-icon-share',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-social-facebook-url',
                        'type'      => 'text',
                        'title'     => __('Facebook', 'Elise'),
                        'subtitle'  => __('Your facebook profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-twitter-url',
                        'type'      => 'text',
                        'title'     => __('Twitter', 'Elise'),
                        'subtitle'  => __('Your twitter profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-google-plus-url',
                        'type'      => 'text',
                        'title'     => __('Google+', 'Elise'),
                        'subtitle'  => __('Your google+ profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-flickr-url',
                        'type'      => 'text',
                        'title'     => __('Flickr', 'Elise'),
                        'subtitle'  => __('Your flickr profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-linkedin-url',
                        'type'      => 'text',
                        'title'     => __('LinkedIn', 'Elise'),
                        'subtitle'  => __('Your linkedin profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-pinterest-url',
                        'type'      => 'text',
                        'title'     => __('Pinterest', 'Elise'),
                        'subtitle'  => __('Your pinterest profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-instagram-url',
                        'type'      => 'text',
                        'title'     => __('Instagram', 'Elise'),
                        'subtitle'  => __('Your instagram profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-behance-url',
                        'type'      => 'text',
                        'title'     => __('Behance', 'Elise'),
                        'subtitle'  => __('Your behance profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-dribbble-url',
                        'type'      => 'text',
                        'title'     => __('Dribbble', 'Elise'),
                        'subtitle'  => __('Your dribbble profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-tumblr-url',
                        'type'      => 'text',
                        'title'     => __('Tumblr', 'Elise'),
                        'subtitle'  => __('Your tumblr profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-youtube-url',
                        'type'      => 'text',
                        'title'     => __('YouTube', 'Elise'),
                        'subtitle'  => __('Your youtube profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-vimeo-url',
                        'type'      => 'text',
                        'title'     => __('Vimeo', 'Elise'),
                        'subtitle'  => __('Your vimeo profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-vine-url',
                        'type'      => 'text',
                        'title'     => __('Vine', 'Elise'),
                        'subtitle'  => __('Your vine profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-lastfm-url',
                        'type'      => 'text',
                        'title'     => __('LastFM', 'Elise'),
                        'subtitle'  => __('Your LastFM profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-deviantart-url',
                        'type'      => 'text',
                        'title'     => __('deviantART', 'Elise'),
                        'subtitle'  => __('Your deviantart profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-digg-url',
                        'type'      => 'text',
                        'title'     => __('Digg', 'Elise'),
                        'subtitle'  => __('Your digg profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-dropbox-url',
                        'type'      => 'text',
                        'title'     => __('Dropbox', 'Elise'),
                        'subtitle'  => __('Your dropbox profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-foursquare-url',
                        'type'      => 'text',
                        'title'     => __('Foursquare', 'Elise'),
                        'subtitle'  => __('Your foursquare profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-github-url',
                        'type'      => 'text',
                        'title'     => __('GitHub', 'Elise'),
                        'subtitle'  => __('Your github profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-reddit-url',
                        'type'      => 'text',
                        'title'     => __('Reddit', 'Elise'),
                        'subtitle'  => __('Your reddit profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-skype-url',
                        'type'      => 'text',
                        'title'     => __('Skype', 'Elise'),
                        'subtitle'  => __('Your skype profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-soundcloud-url',
                        'type'      => 'text',
                        'title'     => __('SoundCloud', 'Elise'),
                        'subtitle'  => __('Your soundcloud profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-spotify-url',
                        'type'      => 'text',
                        'title'     => __('Spotify', 'Elise'),
                        'subtitle'  => __('Your spotify profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-steam-url',
                        'type'      => 'text',
                        'title'     => __('Steam', 'Elise'),
                        'subtitle'  => __('Your steam profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-stumbleupon-url',
                        'type'      => 'text',
                        'title'     => __('StumbleUpon', 'Elise'),
                        'subtitle'  => __('Your stumbleupon profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-vk-url',
                        'type'      => 'text',
                        'title'     => __('VK', 'Elise'),
                        'subtitle'  => __('Your vk profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),

                    array(
                        'id'        => 'opt-social-wordpress-url',
                        'type'      => 'text',
                        'title'     => __('WordPress', 'Elise'),
                        'subtitle'  => __('Your wordpress profile url', 'Elise'),
                        'validate'  => 'url',
                        'default'   => ''
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => __('Custom CSS', 'Elise'),
                'desc'      => __('Advanced Settings.', 'Elise'),
                'icon'      => 'el-icon-css',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields'    => array(

                    array(
                        'id'        => 'opt-ace-editor-css',
                        'type'      => 'ace_editor',
                        'title'     => __('Custom CSS Code', 'Elise'),
                        'subtitle'  => __('Add your CSS code here.', 'Elise'),
                        'mode'      => 'css',
                        'theme'     => 'monokai',
                        'default'   => ""
                    ),

                    // array(
                    //     'id'        => 'opt-ace-editor-js',
                    //     'type'      => 'ace_editor',
                    //     'title'     => __('Custom JS Code', 'Elise'),
                    //     'subtitle'  => __('Add your JS code here.', 'Elise'),
                    //     'mode'      => 'javascript',
                    //     'theme'     => 'monokai',
                    //     'default'   => ""
                    // ),

                    // array(
                    //     'id'        => 'opt-ace-editor-head',
                    //     'type'      => 'ace_editor',
                    //     'title'     => __('&lt;head&gt Code', 'Elise'),
                    //     'subtitle'  => __('Add code before &lt;/head&gt tag.', 'Elise'),
                    //     'mode'      => 'html',
                    //     'theme'     => 'chrome',
                    //     'default'   => ""
                    // ),

                    // array(
                    //     'id'        => 'opt-ace-editor-footer',
                    //     'type'      => 'ace_editor',
                    //     'title'     => __('Footer Code', 'Elise'),
                    //     'subtitle'  => __('Add code before &lt;/body&gt tag.', 'Elise'),
                    //     'mode'      => 'html',
                    //     'theme'     => 'chrome',
                    //     'default'   => ""
                    // ),
                ),
            );

            $theme_info  = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'redux-framework-demo') . '<a href="' . $this->theme->get('ThemeURI') . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'redux-framework-demo') . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'redux-framework-demo') . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'redux-framework-demo') . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            if (file_exists(dirname(__FILE__) . '/../README.md')) {
                $this->sections['theme_docs'] = array(
                    'icon'      => 'el-icon-list-alt',
                    'title'     => __('Documentation', 'redux-framework-demo'),
                    'fields'    => array(
                        array(
                            'id'        => '17',
                            'type'      => 'raw',
                            'markdown'  => true,
                            'content'   => file_get_contents(dirname(__FILE__) . '/../README.md')
                        ),
                    ),
                );
            }
            $this->sections[] = array(
                'icon'              => 'el-icon-list-alt',
                'title'             => __('Customizer Only', 'redux-framework-demo'),
                'desc'              => __('<p class="description">This Section should be visible only in Customizer</p>', 'redux-framework-demo'),
                'customizer_only'   => true,
                'fields'    => array(
                    array(
                        'id'        => 'opt-customizer-only',
                        'type'      => 'select',
                        'title'     => __('Customizer Only Option', 'redux-framework-demo'),
                        'subtitle'  => __('The subtitle is NOT visible in customizer', 'redux-framework-demo'),
                        'desc'      => __('The field desc is NOT visible in customizer.', 'redux-framework-demo'),
                        'customizer_only'   => true,

                        //Must provide key => value pairs for select options
                        'options'   => array(
                            '1' => 'Opt 1',
                            '2' => 'Opt 2',
                            '3' => 'Opt 3'
                        ),
                        'default'   => '2',
                    ),
                )
            );            
            
            $this->sections[] = array(
                'title'     => __('Import / Export', 'redux-framework-demo'),
                'desc'      => __('Import and Export your Redux Framework settings from file, text or URL.', 'redux-framework-demo'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );                     
                    
            $this->sections[] = array(
                'type' => 'divide',
            );

            $this->sections[] = array(
                'icon'      => 'el-icon-info-sign',
                'title'     => __('Theme Information', 'redux-framework-demo'),
                'desc'      => '',
                'fields'    => array(
                    array(
                        'id'        => 'opt-raw-info',
                        'type'      => 'raw',
                        'content'   => $item_info,
                    )
                ),
            );

            if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
                $tabs['docs'] = array(
                    'icon'      => 'el-icon-book',
                    'title'     => __('Documentation', 'redux-framework-demo'),
                    'content'   => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
                );
            }
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => __('Theme Information', 'redux-framework-demo'),
                'content'   => ''
            );

            // $this->args['help_tabs'][] = array(
            //     'id'        => 'redux-help-tab-2',
            //     'title'     => __('Theme Information 2', 'redux-framework-demo'),
            //     'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
            // );

            // Set the help sidebar
            $this->args['help_sidebar'] = '';
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'elise_options',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Theme Options', 'Elise'),
                'page_title'        => __('Theme Options', 'Elise'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE
                'ajax_save'             => true,

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            // $this->args['share_icons'][] = array(
            //     'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
            //     'title' => 'Visit us on GitHub',
            //     'icon'  => 'el-icon-github'
            //     //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
            // );
            // $this->args['share_icons'][] = array(
            //     'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
            //     'title' => 'Like us on Facebook',
            //     'icon'  => 'el-icon-facebook'
            // );
            // $this->args['share_icons'][] = array(
            //     'url'   => 'http://twitter.com/reduxframework',
            //     'title' => 'Follow us on Twitter',
            //     'icon'  => 'el-icon-twitter'
            // );
            // $this->args['share_icons'][] = array(
            //     'url'   => 'http://www.linkedin.com/company/redux-framework',
            //     'title' => 'Find us on LinkedIn',
            //     'icon'  => 'el-icon-linkedin'
            // );

            // Panel Intro text -> before the form
            // if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
            //     if (!empty($this->args['global_variable'])) {
            //         $v = $this->args['global_variable'];
            //     } else {
            //         $v = str_replace('-', '_', $this->args['opt_name']);
            //     }
            //     $this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo'), $v);
            // } else {
            //     $this->args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo');
            // }

            // // Add content after the form.
            // $this->args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo');
        }

    }
    
    global $reduxConfig;
    $reduxConfig = new Redux_Framework_sample_config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;
