<?php 
wp_reset_postdata();
get_header(); 

?>

<?php 
  global $elise_options;
  global $post;

  // Sidebar position
  if ($elise_options['opt-blog-page-style-custom'] != 1) {
    if ($elise_options['opt-blog-style'] != 4) {
      if ($elise_options['opt-show-sidebar'] == 1) {
        if ($elise_options['opt-blog-sidebar-position'] == 1) {
          $section_class = 'col-md-9 sidebar-content';
          $sidebar_class = 'col-md-3';
          $content_class = 'sidebar-right';
        }
        else {
          $section_class = 'col-md-9 col-md-push-3 sidebar-content';
          $sidebar_class = 'col-md-3 col-md-pull-9';
          $content_class = 'sidebar-left';
        }
      }
      else {
        $section_class = 'col-md-12 no-sidebar';
      }
    }
    else {
      $section_class = 'col-md-8 col-md-offset-2 no-sidebar';
    }
  } 
  else {

      if ($elise_options['opt-blog-page-style'] == 1) {
        $section_class = 'col-md-9 sidebar-content';
        $sidebar_class = 'col-md-3';
        $content_class = 'sidebar-right';
      }

      elseif ($elise_options['opt-blog-page-style'] == 2) {
        $section_class = 'col-md-9 col-md-push-3 sidebar-content';
        $sidebar_class = 'col-md-3 col-md-pull-9';
        $content_class = 'sidebar-left';
      }

      elseif ($elise_options['opt-blog-page-style'] == 3 ) {
        $section_class = 'col-md-12';
      }

      elseif ($elise_options['opt-blog-page-style'] == 4 ) {
        $section_class = 'col-md-8 col-md-offset-2 ';
      }
  }

?>
   

      <div class="content <?php if (isset($content_class)) echo esc_attr($content_class); ?> section">
        <div class="container">
          <div class="row">
            <div class="<?php echo esc_attr($section_class); ?>">


              <?php 
                if ($elise_options['opt-blog-page-style-custom'] == 1) {
                  if ($elise_options['opt-blog-page-style'] == 4) {
                  echo '<div class="blog-full-width">';
                  }
                } 
                elseif ($elise_options['opt-blog-style'] == 4) {
                  echo '<div class="blog-full-width">';
                }
              ?>
             

              <?php
              if(have_posts()) : while(have_posts()) : the_post();

                elise_set_post_count(get_the_ID());
                
                get_template_part( 'includes/post-templates/content', get_post_format() );  ?>
        
              <?php endwhile; wp_reset_postdata(); endif; ?>


              <?php 
                if ($elise_options['opt-blog-page-style-custom'] == 1) {
                  if ($elise_options['opt-blog-page-style'] == 4) {
                    echo '<div class="bc-wrap">';
                  }
                } 
                elseif ($elise_options['opt-blog-style'] == 4) {
                  echo '<div class="bc-wrap">';
                }
              ?>

              <?php if (!post_password_required() ) { ?> 
              
                <!-- Displaying post pagination links in case we have multiple page posts -->
                <?php $args = array(
                  'before'           => '<ul class="wp_link_pages"><li><span>',
                  'after'            => '</span></li></ul>',
                  'link_before'      => '',
                  'link_after'       => '',
                  'next_or_number'   => 'number',
                  'separator'        => '</span></li><li><span>',
                  'pagelink'         => '%',
                  'echo'             => 1
                   );

                   wp_link_pages( $args ); ?>

                <div class="article-addons clearfix">

                <?php if ($elise_options['opt-share-buttons'] == true) { ?>
                  <div class="article-share">

                    <ul class="social-icons">

                      <li class="facebook">
                        <a href="#" class="facebook-share">
                          <i class="fa fa-facebook"></i>
                          <i class="fa fa-facebook"></i>
                        </a>
                      </li>

                      <li class="twitter">
                        <a href="#" class="twitter-share">
                          <i class="fa fa-twitter"></i>
                          <i class="fa fa-twitter"></i>
                        </a>
                      </li>

                      <li class="google-plus">
                        <a href="#" class="google-plus-share">
                          <i class="fa fa-google-plus"></i>
                          <i class="fa fa-google-plus"></i>
                        </a>
                      </li>

                      <li class="pinterest">
                        <a href="#" class="pinterest-share">
                          <i class="fa fa-pinterest"></i>
                          <i class="fa fa-pinterest"></i>
                        </a>
                      </li>
                    </ul>

                    <span><?php _e("Share", "Elise") ?></span>
                  </div>
                  <?php } ?>

                  <?php if ($elise_options['opt-tags'] == true) { ?>
                  <?php if (has_tag()) : ?>

                    <div class="article-tag-list"><?php the_tags(__("Tags ", "Elise"), ""); ?></div>

                  <?php endif; ?>
                  <?php } ?>

              </div>

                <?php if ($elise_options['opt-author-bio'] == true) { ?>
                <div class="post-author">
                  <small><?php _e("About the Author", "Elise")?></small>

                  <div class="avatar">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?>
                  </div>

                  <div class="description">
                    <span class="author-name">
                      <?php the_author_posts_link(); ?>
                    </span>
                    <p><?php the_author_meta('description'); ?></p>
                  </div>
                </div>
                <?php } ?>

              <?php } ?>


              <?php if (comments_open()) { ?>
                <div class="comments-area" id="comments">
                  <?php comments_template('', true); ?>
                </div> <!-- end comments-area -->
              <?php } ?>


              <?php 
                if ($elise_options['opt-blog-page-style-custom'] == 1) {
                  if ($elise_options['opt-blog-page-style'] == 4) {
                    echo '</div></div>';
                  }
                } 
                elseif ($elise_options['opt-blog-style'] == 4) {
                  echo '</div></div>';
                }
              ?>

            </div>

            <?php 
              if ($elise_options['opt-blog-page-style-custom'] == 1) {
                if ($elise_options['opt-blog-page-style'] == 1 || $elise_options['opt-blog-page-style'] == 2) {
            ?>

              <aside class="<?php echo esc_attr($sidebar_class); ?> sidebar-wrap">
                <?php get_sidebar(); ?>
              </aside>

            <?php }
                } else { ?>

            <?php  if ($elise_options['opt-blog-style'] != 4 && $elise_options['opt-show-sidebar'] == 1) { ?>
            <aside class="<?php echo esc_attr($sidebar_class); ?> sidebar-wrap">

              <?php get_sidebar(); ?>

            </aside>
            <?php }} ?>

          </div>
        </div>
      </div>


<?php get_footer(); ?>