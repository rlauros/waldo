<form class="search" action="<?php echo home_url( '/' ); ?>" method="get">
	<div class="input-group">
		<input type="text" name="s" value="<?php the_search_query(); ?>" placeholder="<?php _e('Type and press enter to search...', 'Elise') ?>" />
		<span class="input-group-addon"><i class="fa fa-search"></i></span>
	</div>
</form>