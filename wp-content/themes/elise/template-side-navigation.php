<?php
/*
Template Name: Side Navigation
*/
get_header(); 

  global $elise_options;

  if ($elise_options['opt-side-navigation-position'] == 1) {
    $side_navigation_content = 'col-md-9 col-md-push-3';
    $side_navigation_nav = 'col-md-3 col-md-pull-9';
    $side_nav_page = 'snp-left';
  }
  elseif ($elise_options['opt-side-navigation-position'] == 2) {
    $side_navigation_content = 'col-md-9';
    $side_navigation_nav = 'col-md-3';
    $side_nav_page = 'snp-right';
  }
?>
     

<div class="content section side-navigation-page <?php echo esc_attr($side_nav_page) ?>">
  <div class="container">
    <div class="row">
      <div class="<?php echo esc_attr($side_navigation_content) ?> content-wrap">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
          <?php the_content(); ?>
        <?php endwhile; endif; ?>
      </div>
      <?php if (!post_password_required()) { ?>
      <aside class="<?php echo esc_attr($side_navigation_nav) ?> sidebar-wrap">

        <?php
          $page_ancestors = get_ancestors($post->ID, 'page');
          $page_parent = end($page_ancestors);
        ?>

        <ul class="side-navigation">
          <?php if ($elise_options['opt-side-navigation-title']) { ?>
            <li><small><?php echo esc_html($elise_options['opt-side-navigation-title']) ?></small></li>
          <?php } ?>

          <li <?php echo is_page($page_parent) ? 'class="current_page_item"' : '' ?>>
            <a href="<?php echo get_permalink($page_parent); ?>">
              <?php echo get_the_title($page_parent); ?>
            </a>
          </li>

        <?php
          if($page_parent) {
            $page_children = wp_list_pages("title_li=&child_of=".$page_parent."&echo=0");
          }
          else {
            $page_children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
          }
          if ($page_children) {
            echo $page_children; 
          }
        ?>
        </ul>
      </aside>
      <?php } ?>
    </div>
  </div>
</div>


<?php get_footer(); ?>