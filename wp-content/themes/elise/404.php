<?php get_header(); ?>
     

      <div class="content sidebar-right section four-zero-four">
        <div class="container">
          <div class="row">

            <div class="col-md-8 col-md-offset-2">
              <img class="error-img" src="<?php echo IMAGES . '/404.png'?>" alt="">
            
              <small><?php _e('Sorry!','Elise') ?></small>
              <h2 class="hero"><?php _e('The page you were looking for could not be found.','Elise') ?></h2>

              <hr class="shadow">

              <div class="row fof-search">

                <div class="col-md-8"><?php get_search_form(); ?></div>
                <div class="col-md-4"><a href="<?php echo home_url(); ?>" class="btn btn-default pull-right btn-block">Back to Home</a></div>

              </div>
            </div>

          </div>
        </div>
      </div>


<?php get_footer(); ?>