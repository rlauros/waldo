<?php 
wp_reset_postdata(); 
get_header();
global $elise_options;

?>
  
  <?php if(have_posts()) : while(have_posts()) : the_post();

    get_template_part('includes/project-templates/project', elise_single_project_layout() );

  endwhile; endif; ?>

  <?php if (comments_open()) { ?>
    <div class="project-comments">
      <div class="container">
        <div class="row">
          <section class="col-md-12">

            <div class="comments-area" id="comments">
              <?php comments_template('', true); ?>
            </div> <!-- end comments-area -->

          </section>
        </div>
      </div>
    </div>
  <?php } ?>


<?php get_footer(); ?>