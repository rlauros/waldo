<?php get_header(); ?>

<?php 
  global $elise_options;

  // Blog style
  if ($elise_options['opt-blog-style'] == 1) {
    $style_class = 'blog-large';
  }
  elseif ($elise_options['opt-blog-style'] == 2) {
    $style_class = 'blog-medium';
  }
  elseif ($elise_options['opt-blog-style'] == 3) {
    $style_class = 'blog-masonry row bm-hidden';
  }
  elseif ($elise_options['opt-blog-style'] == 4) {
    $style_class = 'blog-full-width';
  }

  // Sidebar position
  if ($elise_options['opt-blog-style'] != 4) {
    if ($elise_options['opt-show-sidebar'] == 1) {
      if ($elise_options['opt-blog-sidebar-position'] == 1) {
        $section_class = 'col-md-9 sidebar-content';
        $sidebar_class = 'col-md-3';
        $content_class = 'sidebar-right';
      }
      else {
        $section_class = 'col-md-9 col-md-push-3 sidebar-content';
        $sidebar_class = 'col-md-3 col-md-pull-9';
        $content_class = 'sidebar-left';
      }
    }
    else {
      $section_class = 'col-md-12 no-sidebar';
    }
  }
  else {
    $section_class = 'col-md-8 col-md-offset-2 no-sidebar';
  }
?>
     

      <div class="content blog-content-wrap <?php  if (isset($content_class)) echo esc_attr($content_class); ?> section">
        <div class="container">
          <div class="row">
            <div class="<?php echo esc_attr($section_class); ?>">
              <div class="<?php echo esc_attr($style_class); ?>">

                <?php
                if(have_posts()) : while(have_posts()) : the_post();
                  
                  get_template_part( 'includes/post-templates/content', get_post_format() );  ?>
          
                <?php endwhile; else : ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('no-posts'); ?>>

                  <h2><?php _e('No posts were found.', 'Elise'); ?></h2>
                
                </article>

               <?php endif; ?>

                <?php
                if ( function_exists('elise_pagination') ) {
                  elise_pagination();
                }
                ?>

              </div>
            </div>

            <?php  if ($elise_options['opt-blog-style'] != 4 && $elise_options['opt-show-sidebar'] == 1) { ?>
            <aside class="<?php echo esc_attr($sidebar_class); ?> sidebar-wrap">

              <?php get_sidebar(); ?>

            </aside>
            <?php } ?>

          </div>
        </div>
      </div>


<?php get_footer(); ?>