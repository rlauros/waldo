<?php

/* ------------------------------------------------------------ */
/* 	Define Constants */
/* ------------------------------------------------------------ */
define('THEMEROOT', get_template_directory_uri() );
define('IMAGES', THEMEROOT.'/img');

/* ------------------------------------------------------------ */
/* 	Load text domain
/* ------------------------------------------------------------ */
function elise_textdomain(){
    load_theme_textdomain('Elise', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'elise_textdomain');

/* ------------------------------------------------------------ */
/* 	Styles Enqueue */
/* ------------------------------------------------------------ */
function elise_theme_styles() {
	$theme_info = wp_get_theme();
	$protocol = is_ssl() ? 'https' : 'http';

    wp_register_style( 'bootstrap', THEMEROOT . '/css/bootstrap.min.css', '' , '3.3.0');
    wp_register_style( 'font-awesome', THEMEROOT . '/css/icons/css/font-awesome.min.css', '' , '4.1.0');
    wp_register_style( 'typicons', THEMEROOT . '/css/icons/css/typicons.min.css', '' , '2.0.4');
    wp_register_style( 'elise_fontello', THEMEROOT . '/css/icons/css/fontello.css', '' , '1.0');
    
    wp_register_style( 'main', get_stylesheet_directory_uri() . '/style.css', '' , $theme_info->get( 'Version' ));

    wp_enqueue_style( 'elise-gfont', "$protocol://fonts.googleapis.com/css?family=Raleway:400,300,600,700" );
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('font-awesome');
    wp_enqueue_style('typicons');
    wp_enqueue_style('elise_fontello');

    wp_enqueue_style('main');
}

add_action('wp_enqueue_scripts', 'elise_theme_styles');


/* ------------------------------------------------------------ */
/* 	Load JS Files */
/* ------------------------------------------------------------ */
function elise_load_scripts() {
	global $elise_options, $post;
	$theme_info = wp_get_theme();

    wp_enqueue_script('modernizr', THEMEROOT . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js', array('jquery'), '2.6.2', false);
    wp_enqueue_script('jplayer', THEMEROOT . '/js/vendor/jquery.jplayer.min.js', array('jquery'), '2.6.0', false);
	if(is_singular() && comments_open()) wp_enqueue_script( 'comment-reply' );
    if(is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'elise_maps') ) {
        	wp_enqueue_script( 'google-maps', 'http://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.7', array(), '1.0.0', false );
    }

    wp_enqueue_script('elise-scripts', THEMEROOT . '/js/elise_scripts.js', array('jquery'), $theme_info->get( 'Version' ), true);
    wp_enqueue_script('main', THEMEROOT . '/js/main.js', array('elise-scripts'), $theme_info->get( 'Version' ), true);
    ($elise_options['opt-smoothscroll']) ? wp_enqueue_script('smoothscroll', THEMEROOT . '/js/vendor/smoothscroll.js', array('jquery'), '1.2.1', true) : '';

}

add_action('wp_enqueue_scripts', 'elise_load_scripts');


/* ------------------------------------------------------------ */
/* 	Nav Register */
/* ------------------------------------------------------------ */
function elise_register_menus(){
	register_nav_menus(
		array(
			'main-nav' 			=> __('Main Navigation', 'Elise'),
			'full-mobile-nav' 	=> __('Full Screen/Mobile Navigation', 'Elise'),
			'copyrights-nav' 	=> __('Copyright Footer Navigation', 'Elise'),
		)
	);
}

add_action('init', 'elise_register_menus');

/* ------------------------------------------------------------ */
/* 	Content Width */
/* ------------------------------------------------------------ */
if ( ! isset( $content_width ) ) {
	$content_width = 1176;
}

/* ------------------------------------------------------------ */
/* Portfolio */
/* ------------------------------------------------------------ */
add_filter( 'portfolioposttype_args', 'elise_portfolio' );
function elise_portfolio( array $args ) {
	global $elise_options;
    $labels = array(
			'name'               => __( 'Portfolio', 'Elise' ),
			'singular_name'      => __( 'Portfolio Item', 'Elise' ),
			'menu_name'          => _x( 'Portfolio', 'admin menu', 'Elise' ),
			'name_admin_bar'     => _x( 'Portfolio Item', 'add new on admin bar', 'Elise' ),
			'add_new'            => __( 'Add New Item', 'Elise' ),
			'add_new_item'       => __( 'Add New Portfolio Item', 'Elise' ),
			'new_item'           => __( 'Add New Portfolio Item', 'Elise' ),
			'edit_item'          => __( 'Edit Portfolio Item', 'Elise' ),
			'view_item'          => __( 'View Item', 'Elise' ),
			'all_items'          => __( 'Portfolio Items', 'Elise' ),
			'search_items'       => __( 'Search Portfolio', 'Elise' ),
			'parent_item_colon'  => __( 'Parent Portfolio Item:', 'Elise' ),
			'not_found'          => __( 'No portfolio items found', 'Elise' ),
			'not_found_in_trash' => __( 'No portfolio items found in trash', 'Elise' ),
    );
    $args['labels'] = $labels;

    $get_portfolio_custom_slug = $elise_options['opt-portfolio-custom-slug'];

	if ($get_portfolio_custom_slug) {
		$portfolio_custom_slug = $get_portfolio_custom_slug;
	} else {
		$portfolio_custom_slug = 'portfolio';
	}

    // Update project single permalink format, and archive slug as well.
    $args['rewrite']     = array('slug'=>sanitize_text_field($portfolio_custom_slug),'with_front'=>true);
    $args['has_archive'] = 'projects';
    // Don't forget to visit Settings->Permalinks after changing these to flush the rewrite rules.
    return $args;
}


// Categories
add_filter( 'portfolioposttype_category_args', 'elise_portfolio_cat' );
function elise_portfolio_cat( array $args ) {
    $labels = array(
		'name'                       => __( 'Portfolio Categories', 'Elise' ),
		'singular_name'              => __( 'Portfolio Category', 'Elise' ),
		'menu_name'                  => __( 'Portfolio Categories', 'Elise' ),
		'edit_item'                  => __( 'Edit Portfolio Category', 'Elise' ),
		'update_item'                => __( 'Update Portfolio Category', 'Elise' ),
		'add_new_item'               => __( 'Add New Portfolio Category', 'Elise' ),
		'new_item_name'              => __( 'New Portfolio Category Name', 'Elise' ),
		'parent_item'                => __( 'Parent Portfolio Category', 'Elise' ),
		'parent_item_colon'          => __( 'Parent Portfolio Category:', 'Elise' ),
		'all_items'                  => __( 'All Portfolio Categories', 'Elise' ),
		'search_items'               => __( 'Search Portfolio Categories', 'Elise' ),
		'popular_items'              => __( 'Popular Portfolio Categories', 'Elise' ),
		'separate_items_with_commas' => __( 'Separate portfolio categories with commas', 'Elise' ),
		'add_or_remove_items'        => __( 'Add or remove portfolio categories', 'Elise' ),
		'choose_from_most_used'      => __( 'Choose from the most used portfolio categories', 'Elise' ),
		'not_found'                  => __( 'No portfolio categories found.', 'Elise' ),
    );
    $args['labels'] = $labels;

    return $args;
}

// tags
add_filter( 'portfolioposttype_tag_args', 'elise_portfolio_tags' );
function elise_portfolio_tags( array $args ) {
    $labels = array(
			'name'                       => __( 'Portfolio Tags', 'Elise' ),
			'singular_name'              => __( 'Portfolio Tag', 'Elise' ),
			'menu_name'                  => __( 'Portfolio Tags', 'Elise' ),
			'edit_item'                  => __( 'Edit Portfolio Tag', 'Elise' ),
			'update_item'                => __( 'Update Portfolio Tag', 'Elise' ),
			'add_new_item'               => __( 'Add New Portfolio Tag', 'Elise' ),
			'new_item_name'              => __( 'New Portfolio Tag Name', 'Elise' ),
			'parent_item'                => __( 'Parent Portfolio Tag', 'Elise' ),
			'parent_item_colon'          => __( 'Parent Portfolio Tag:', 'Elise' ),
			'all_items'                  => __( 'All Portfolio Tags', 'Elise' ),
			'search_items'               => __( 'Search Portfolio Tags', 'Elise' ),
			'popular_items'              => __( 'Popular Portfolio Tags', 'Elise' ),
			'separate_items_with_commas' => __( 'Separate portfolio tags with commas', 'Elise' ),
			'add_or_remove_items'        => __( 'Add or remove portfolio tags', 'Elise' ),
			'choose_from_most_used'      => __( 'Choose from the most used portfolio tags', 'Elise' ),
			'not_found'                  => __( 'No portfolio tags found.', 'Elise' ),
    );
    $args['labels'] = $labels;

    return $args;
}


function elise_flush_portfolio() {
	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}
add_action( 'init', 'elise_flush_portfolio' );



/* ------------------------------------------------------------ */
/* 	Nav Icons */
/* ------------------------------------------------------------ */
function navigation_icons( $items, $args ) {
	global $elise_options, $woocommerce;

	$cart_url = '';
	if (function_exists('is_woocommerce')) {
		$cart_url = $woocommerce->cart->get_cart_url();
	}

    if ($args->theme_location == 'main-nav') {
		$items .= '<li class="nav-icons"><ul>';
		if (function_exists('is_woocommerce') && $elise_options['opt-woo-shop-nav-icon'] == true) {
			$items .= '<li class="nav-shopping-bag"><a href="'. $cart_url .'"><i class="fo icon-bag"></i><span class="cart-contents woo-cart-items-count">'. $woocommerce->cart->cart_contents_count .'</span></a>';
			$items .= '<ul class="nav-shopping-cart"><li><div class="hide_cart_widget_if_empty"><div class="widget_shopping_cart_content"></div></div></li></ul></li>';
		}
        if ($elise_options['opt-nav-search'] == 1) { $items .= '<li class="nav-search"><a href="#"><i class="fa fa-search"></i></a></li>'; };
        if ($elise_options['opt-secondary-nav'] == 1) { $items .= '<li class="nav-secondary-nav"><a href="#" class="secondary-nav-btn"><i class="fa fa-bars"></i></a></li>'; };
		$items .= '</ul></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'navigation_icons', 10, 2 );


/* ------------------------------------------------------------ */
/* Add Theme Support for Post Formats, Post Thumbnails and Automatic Feed Links */
/* ------------------------------------------------------------ */
if (function_exists('add_theme_support')) {
	add_theme_support('post-formats', array('image', 'gallery', 'audio', 'video','quote', 'link'));
	add_theme_support('post-thumbnails', array('post', 'portfolio', 'product'));
	add_theme_support('automatic-feed-links');
	add_theme_support( 'woocommerce' );
}


/* ------------------------------------------------------------ */
/* Image Resizer */
/* ------------------------------------------------------------ */

require_once( dirname( __FILE__ ) .'/eliseCore/AquaResizer/aq_resizer.php');


/* ------------------------------------------------------------ */
/* Excerpts */
/* ------------------------------------------------------------ */
function elise_excerpt() {
	add_post_type_support( 'page', 'excerpt' );
}
add_action('init', 'elise_excerpt');


function elise_custom_excerpt_length( $length ) {
	global $elise_options;
	
	return 23;
	if ($elise_options['opt-blog-style'] != 2) {
		if ($elise_options['opt-excerpts'] == 1) {
			return $elise_options['opt-excerpt-lenght'];
		}
	}

	if (is_search()) {
		return 35;
	}
}
add_filter( 'excerpt_length', 'elise_custom_excerpt_length', 999 );

function elise_new_excerpt_more( $more ) {
	return ' [...]<br><br><a class="more-link" href="'. get_permalink( get_the_ID() ) . '">' . __('Continue Reading', 'Elise') . '</a>';
}
add_filter( 'excerpt_more', 'elise_new_excerpt_more' );


/* ------------------------------------------------------------ */
/* Add Sidebar Support */
/* ------------------------------------------------------------ */
if (function_exists('register_sidebar')) {

	register_sidebar(
		array(
			'name' 			=> __( 'Main Sidebar', 'Elise' ),
			'id'			=> 'main-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' 			=> __( 'Footer Area 1', 'Elise' ),
			'id'			=> 'footer-area-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' 			=> __( 'Footer Area 2', 'Elise' ),
			'id'			=> 'footer-area-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);

	register_sidebar(
		array(
			'name' 			=> __( 'Footer Area 3', 'Elise' ),
			'id'			=> 'footer-area-3',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' 			=> __( 'Footer Area 4', 'Elise' ),
			'id'			=> 'footer-area-4',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' 			=> __( 'Sidebar Navigation', 'Elise' ),
			'id'			=> 'sidebar-nav',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);
	register_sidebar(
		array(
			'name' 			=> __( 'Shop Sidebar', 'Elise' ),
			'id'			=> 'shop-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>', 
			'before_title'  => '<h6>', 
			'after_title'   => '</h6>'
		)
	);

}


/* ------------------------------------------------------------ */
/* Stag Sidebar */
/* ------------------------------------------------------------ */

function elise_stag_sidebars() {
	$stag_sidebar_h = array(
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6>',
			'after_title'   => '</h6>',
	);

	return $stag_sidebar_h;
}

add_filter('stag_custom_sidebars_widget_args', 'elise_stag_sidebars', 10, 2);

/* ------------------------------------------------------------ */
/* Wp Title */
/* ------------------------------------------------------------ */
 add_theme_support( 'title-tag' );

if ( ! function_exists( '_wp_render_title_tag' ) ) {
	function theme_slug_render_title() {
?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php
	}
	add_action( 'wp_head', 'theme_slug_render_title' );
}

/* ------------------------------------------------------------ */
/* Apple Touch Icons */
/* ------------------------------------------------------------ */
function apple_touch_icons( $size ) {
  global $elise_options;

  $apple_touch_icon = $elise_options['opt-apple-icons']['url'];

  if ($apple_touch_icon) {
    $icon_url = aq_resize( $apple_touch_icon, $size, $size, false, true ); //resize & crop the image
  
    return $icon_url;
  }
}

/* ------------------------------------------------------------ */
/* Custom Widgets */
/* ------------------------------------------------------------ */

//Recent/popular Posts Tabs
include('includes/custom-widgets/widget-recent-posts-tab.php');
//Recent Projects
include('includes/custom-widgets/widget-recent-projects.php');
//Social Icons
include('includes/custom-widgets/widget-social-icons.php');

add_filter('widget_text', 'do_shortcode');


/* ------------------------------------------------------------ */
/* Post Counter */
/* ------------------------------------------------------------ */

function elise_get_post_count($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
        return __('View: 1', 'Elise');
    }
    return __('Views: ', 'Elise').$count;
}

function elise_set_post_count($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 1;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '1');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


/* ------------------------------------------------------------ */
/* 	Headers */
/* ------------------------------------------------------------ */

function elise_header_body_class($classes) {
	global $elise_options; 

	if ($elise_options['opt-navbar-style'] != 3) {
		if ($elise_options['opt-navbar-transparent'] == 1) {
			$transparent_class = 'header-transparent';
		}
		else {
			$transparent_class = '';
		}
	}

	if ($elise_options['opt-navbar-style'] == 1) {
		$classes[] = 'header-standard '.$transparent_class. ''; 
	} 
	elseif ($elise_options['opt-navbar-style'] == 2) {
		$classes[] = 'header-centered '.$transparent_class. ''; 
	}
	elseif ($elise_options['opt-navbar-style'] == 3) {
		$classes[] = 'header-extended'; 
	}
	elseif ($elise_options['opt-navbar-style'] == 4  ) {
		$classes[] = 'header-bar header-transparent'; 
	}

	if ($elise_options['opt-navbar-style'] != 4 && $elise_options['opt-nav-full-width'] == true) {
		$classes[] = 'header-full-width'; 
	}

	return $classes;
}

add_filter( 'body_class', 'elise_header_body_class' );

function elise_header_style() {
	global $elise_options;

	if ($elise_options['opt-navbar-style'] == 1 || $elise_options['opt-navbar-style'] == 4) {
		$header_style = '';
	} 
	elseif ($elise_options['opt-navbar-style'] == 2) {
		$header_style = 'centered';
	}
	elseif ($elise_options['opt-navbar-style'] == 3) {
		$header_style = 'extended';
	}

	return $header_style;
}

/* ------------------------------------------------------------ */
/* 	Layout */
/* ------------------------------------------------------------ */

function elise_layout_body_class($classes) {
	global $elise_options;

	if ($elise_options['opt-layout'] == 1) {
		$classes[] = 'layout-wide'; 
	} 
	elseif ($elise_options['opt-layout'] == 2) {
		$classes[] = 'layout-boxed'; 
	}
	elseif ($elise_options['opt-layout'] == 3) {
		$classes[] = 'layout-bordered'; 
	}

	// Responsive
	if ($elise_options['opt-responsive'] != true) {
		$classes[] = 'elise-non-responsive'; 
	}

	return $classes;
}

add_filter( 'body_class', 'elise_layout_body_class' );

/* ------------------------------------------------------------ */
/* 	Single Project Layout */
/* ------------------------------------------------------------ */

function elise_single_project_layout() {
	global $elise_options;

	if ($elise_options['opt-project-layout'] == 1) {
		$single_project_layout = '';
	}
	elseif ($elise_options['opt-project-layout'] == 2) {
		$single_project_layout = 'medium';
	}
	elseif ($elise_options['opt-project-layout'] == 3) {
		$single_project_layout = 'wide';
	}
	elseif ($elise_options['opt-project-layout'] == 4) {
		$single_project_layout = 'clean';
	}

	return $single_project_layout;

}


/* ------------------------------------------------------------ */
/* Secondary Navigation */
/* ------------------------------------------------------------ */

function secondary_nav_style() {
	global $elise_options;

	if ($elise_options['opt-secondary-nav-style'] == 1) {
		$secondary_nav_style = 'sidebar';
	} 
	elseif ($elise_options['opt-secondary-nav-style'] == 2) {
		$secondary_nav_style = 'full';
	}

	return $secondary_nav_style;
}

/* ------------------------------------------------------------ */
/* Page Title */
/* ------------------------------------------------------------ */

function elise_page_title() {

	global $post;
	global $elise_options, $woocommerce;

	if ($elise_options['opt-title-bar'] != 0) { 

		if ($elise_options['opt-title-bar-centered'] == 1) {
			$pt_title_position = 'page-title-left';
		}  
		elseif ($elise_options['opt-title-bar-centered'] == 2) {
			$pt_title_position = 'page-title-centered';
		}  

		if ($elise_options['opt-breadcrumbs-bar'] == true && $elise_options['opt-breadcrumbs-style'] == 2) {
			$pt_title_position .= ' pt-w-breadcrumbs';
		}


		$elise_page_title = $elise_options['opt-custom-page-title'];
		$elise_page_subttitle = $elise_options['opt-page-subtitle'];

		// $allowed_tags = wp_kses_allowed_html( 'post' );
		// var_dump( $allowed_tags );

		$allowed_tags = array( 'a' => array( 'href' => array(), 'title' => array() ), 'i' => array( 'class' => array(), ), 'strong' => array(), 'br' => array(), 'h1' => array( 'class' => array(), 'style' => array() ), 'h2' => array( 'class' => array(), 'style' => array() ), 'h3' => array( 'class' => array(), 'style' => array() ), 'h4' => array( 'class' => array(), 'style' => array() ), 'h5' => array( 'class' => array(), 'style' => array() ), 'h6' => array( 'class' => array(), 'style' => array() ), 'small' => array( 'class' => array(), 'style' => array() ), 'span' => array( 'class' => array(), 'style' => array() ) );

	?>

      <!-- Page Title Container -->
      <section class="page-title-container <?php echo esc_attr($pt_title_position) ?>">

      	<?php if ($elise_options['opt-pt-overlay'] == 1) { ?>
      		<div class="pt-overlay"></div>
      	<?php } ?>

        <div class="container">
          <div class="row">
            <!-- Page Title -->
            <div class="col-md-12">
              <div class="page-title">
              	<div class="title-wrap">
              	<?php if (is_search()) { ?>
              	<h1 class="elise-title"><?php _e('Search results for: ', 'Elise'); echo get_search_query(); ?></h1>
              	<?php } elseif (is_category()) { ?> 
              	<h1 class="elise-title"><?php _e('Category: ', 'Elise'); echo single_cat_title(); ?></h1>
              	<?php } elseif (is_tag()) { ?> 
              	<h1 class="elise-title"><?php _e('Tag: ', 'Elise'); echo single_tag_title(); ?></h1>
              	<?php } elseif (is_author()) { ?> 
              	<h1 class="elise-title"><?php _e('Author: ', 'Elise'); echo get_the_author(); ?></h1>
              	<?php } elseif (is_404()) { ?> 
              	<h1 class="elise-title"><?php _e('Page not Found!', 'Elise'); ?></h1>
              	<?php } elseif ($woocommerce && is_shop() || $woocommerce && is_product_category() || $woocommerce && is_product_tag()) { ?> 
      				<?php if (!empty($elise_options['opt-shop-welcome']) && $elise_options['opt-shop-welcome'] != 'Shop') { ?>
      					<h1 class="elise-title"><?php echo esc_html($elise_options['opt-shop-welcome']) ?></h1>
      				<?php } else { ?>
      					<h1 class="elise-title"><?php _e('Shop', 'Elise') ?></h1>
      				<?php } ?>
              	<?php } elseif (is_archive()) { ?> 
              	<h1 class="elise-title"><?php _e('Archive ', 'Elise'); ?></h1>
              	<?php } elseif (is_front_page() && is_page()) { ?> 
	              	<?php if ($elise_options['opt-custom-page-title']) { ?> 
	              	<?php echo wp_kses($elise_page_title, $allowed_tags); ?>
	              	<?php } else { ?>
              		<h1 class="elise-title"><?php echo get_the_title($post->ID); ?></h1>
              		<?php } ?> 
              	<?php } elseif (is_home() && is_front_page()) { 
              		if (!empty($elise_options['opt-blog-welcome']) && $elise_options['opt-blog-welcome'] != 'Welcome to our Blog') { ?>
              			<h1 class="elise-title"><?php echo esc_html($elise_options['opt-blog-welcome']) ?></h1>
              		<?php } else { ?>
              			<h1 class="elise-title"><?php _e('Welcome to our Blog', 'Elise') ?></h1>
              		<?php } ?>
              	<?php } elseif ($elise_options['opt-custom-page-title']) { ?> 
              	<?php echo wp_kses($elise_page_title, $allowed_tags) ?>
              	<?php } else { ?> 
                <h1 class="elise-title"><?php single_post_title(); ?></h1>
                <?php 
                	if (!empty($elise_options['opt-page-subtitle'])) { ?>
                		<h5 class="elise-subtitle"><span><?php echo esc_html($elise_page_subttitle); ?></span></h5>
                	<?php }
                ?>
                <?php } ?>
            	</div>

	            <?php  if ($elise_options['opt-breadcrumbs-bar'] == 1 && $elise_options['opt-breadcrumbs-style'] == 2 && $elise_options['opt-title-bar-centered'] == 1) {
	    		  elise_breadcrumbs();
	    		} // breadcrumbs style - bar ?>
              </div>
            </div><!-- Page Title End -->
          </div>
        </div>
      </section><!-- Page Title Container End -->

      <?php }
}

/* ------------------------------------------------------------ */
/* 	Redux */
/* ------------------------------------------------------------ */


if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/eliseCore/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/eliseCore/ReduxCore/framework.php' );
}

if ( !isset( $elise_options ) && file_exists( dirname( __FILE__ ) . '/eliseCore/ReduxCore/elise-meta-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/eliseCore/ReduxCore/elise-meta-config.php' );
}

if ( !isset( $elise_options ) && file_exists( dirname( __FILE__ ) . '/eliseCore/ReduxCore/elise-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/eliseCore/ReduxCore/elise-config.php' );
}

function addPanelCSS() {
    wp_register_style('elise-redux', THEMEROOT .'/eliseCore/css/elise-redux.css', array( ), time(), 'all');  
    wp_enqueue_style('elise-redux');
}
// This example assumes your opt_name is set to redux_demo, replace with your opt_name value
add_action( 'redux/page/elise_options/enqueue', 'addPanelCSS', 9999 );


/* ------------------------------------------------------------ */
/* 	Breadcrumbs */
/* ------------------------------------------------------------ */
function elise_breadcrumbs() {
	global $elise_options, $post;

	if ($elise_options['opt-breadcrumbs-bar'] == 1) {
	?>

	<?php if ($elise_options['opt-breadcrumbs-style'] == 1) { ?>
	<div class="page-navigation">
	<div class="container">
	<div class="row">
	<div class="col-md-12">
	<?php } // breadcrumbs style - bar ?>

	<?php
    echo '<ol class="breadcrumb">';
    //echo '<li>You are here: </li>';
    echo '<li><i class="fa fa-home"></i> <a href="'. home_url() .'">'. __('Home', 'Elise') .'</a></li>';
    if (is_singular('post')) { echo '<li>'; the_category('<span class="cat-sep"> / </span>'); echo '</li>';
    	if (is_single()) { echo '<li>'. __('Blog Post', 'Elise') .'';  echo '</li>';
    	}
    } elseif (is_home() && !is_front_page()) { echo '<li>'; single_post_title(); echo '</li>';

    } elseif (function_exists('is_woocommerce') && is_woocommerce()) {
   	$args = array(
	    'delimiter'  => '',
	    'wrap_before'  => '',
	    'wrap_after' => '',
	    'before'   => '<li>',
	    'after'   => '</li>',
	    'home'    => ''
	);
    woocommerce_breadcrumb( $args );
    } elseif (is_category()) { echo '<li>'; single_cat_title(); echo '</li>';
    } elseif (is_singular('portfolio')) { echo '<li>'. __('Project details', 'Elise') .''; echo '</li>';
    } elseif (is_page() && (!is_front_page())) {     
    	if ($post->post_parent) {
	    	$parent_title = get_the_title($post->post_parent);
	    	$parent_link = get_permalink($post->post_parent);
			echo '<li><a href="'. $parent_link .'">' . $parent_title .'</a></li>';
    	}
    	echo '<li>'; the_title(); echo '</li>';
    } elseif (is_tag()) { echo '<li>'.__('Tag: ', 'Elise').''; single_tag_title(); echo '</li>';
    } elseif (is_day()) { echo'<li>'. __('Archive for ', 'Elise') .''; the_time('F jS, Y');     echo'</li>';
    } elseif (is_month()) { echo'<li>'. __('Archive for ', 'Elise') .''; the_time('F, Y'); echo'</li>';
    } elseif (is_year()) { echo'<li>'. __('Archive for ', 'Elise') .''; the_time('Y'); echo'</li>';
    } elseif (is_author()) { echo'<li>'. __('Author Archives', 'Elise') .''; echo'</li>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { echo '<li>'. __('Blog Archives', 'Elise') .''; echo'</li>';
    } elseif (is_search()) { echo'<li>'. __('Search Results', 'Elise') .''; echo'</li>'; }

    echo '</ol>';
    ?>

	<?php if ($elise_options['opt-breadcrumbs-style'] == 1) { ?>
    <ul class="buttons">
    	
    	<?php if (is_singular(array('post', 'portfolio'))) {
				if (comments_open( $post->ID )) { ?>
					<li class="btn-comments" title="<?php _e('Go to Comments', 'Elise') ?>"><a href="#comments"><i class="fa fa-comments"></i><?php comments_number(' 0',' 1',' %'); ?></a></li>
    	<?php }} ?>

    	<?php if (is_page_template('portfolio.php') && $elise_options['opt-portfolio-filtering'] == 1) { ?>
    		<li class="btn-filtering"><a href="#"><?php _e('Filter Projects ', 'Elise') ?><i class="fa fa-caret-down"></i></a>
    		</li>
    	<?php } ?>

	    <?php 
	      	if (is_singular(array('post', 'portfolio'))) {
	      		single_navigation(get_post_type());
	      	}
	    ?>

	    <?php if ($elise_options['opt-breadcrumbs-share'] == 1) { ?>
    	<li class="btn-share"><a href="#" title="<?php _e('Share', 'Elise') ?>"><i class="fo icon-share-1"></i></a>
			<ul class="pn-share-buttons social-icons">
                  <li class="facebook">
                    <a href="#" class="facebook-share">
                      <i class="fa fa-facebook"></i>
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="twitter">
                    <a href="#" class="twitter-share">
                      <i class="fa fa-twitter"></i>
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li class="google-plus">
                    <a href="#" class="google-plus-share">
                      <i class="fa fa-google-plus"></i>
                      <i class="fa fa-google-plus"></i>
                    </a>
                  </li>
                  <li class="pinterest">
                    <a href="#" class="pinterest-share">
                      <i class="fa fa-pinterest"></i>
                      <i class="fa fa-pinterest"></i>
                    </a>
                  </li>
			</ul>
    	</li>
    	<?php } ?>
    </ul>
	<?php } // breadcrumbs style - bar ?>

	<?php if ($elise_options['opt-breadcrumbs-style'] == 1) { ?>
	</div>
	</div>
	</div>
	</div> <!-- page navigation end -->
	<?php } // breadcrumbs style - bar ?>

    <?php
	// }
	} //breadcrumbs bar endif

}

/* ------------------------------------------------------------ */
/* 	Single Navigation */
/* ------------------------------------------------------------ */
function single_navigation( $nav_post_type ) {
	global $post;

	if (is_singular($nav_post_type)) { 


		$portfolio = array('post_type' => $nav_post_type);
		$wp_query = new WP_Query($portfolio); 

		if (have_posts()) : while (have_posts()) : the_post(); ?>

			<li class="btn-next" title="<?php _e('Prev', 'Elise') ?>"><?php next_post_link('%link','<i class="fa fa-chevron-left"></i>'); ?></li>
			<li class="btn-prev" title="<?php _e('Next', 'Elise') ?>"><?php previous_post_link('%link','<i class="fa fa-chevron-right"></i>'); ?></li>
			

		<?php endwhile; endif;

		wp_reset_query(); 

	}

}



/* ------------------------------------------------------------ */
/* Top Bar */
/* ------------------------------------------------------------ */
function elise_topbar() {
	global $elise_options;

	$top_bar_text = $elise_options['opt-top-bar-text'];
	$top_bar_kses = array(
	    'a' => array(
	        'href' => array(),
	        'title' => array()
	    ),
	    'i' => array(
	        'class' => array(),
	    ),
	    'strong' => array(),
	);

	$current_user = wp_get_current_user(); ?>



	<?php
	if ($elise_options['opt-show-top-bar'] == 1) { ?>
		<!-- Top bar -->
	<div id="top-bar" class="hidden-xs hidden-sm">
		<div class="container">
		  <div class="row">
		    <div class="col-md-12">
		      <?php if (!empty($elise_options['opt-top-bar-text'])) { ?>
		        <div class="top-bar-content">
		        <p><?php echo wp_kses( $top_bar_text, $top_bar_kses); ?></p>
		        </div>
		        <?php do_action('icl_language_selector'); ?>
		      <?php } ?>

		      <?php 
		      	if (function_exists('is_woocommerce')) {
		      		if ( is_user_logged_in() ) {
			      		?>
			      		<div class="woo-settings">
			      			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ) ?>" class="woo-settings-cog"><i class="fa fa-cog"></i> <?php echo $current_user->user_login ?></a>
			      			<ul>
			      				<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ) ?>"><?php _e('My Account', 'Elise') ?></a></li>
			      				<li><a href="<?php echo wc_customer_edit_account_url() ?>"><?php _e('Edit Settings', 'Elise') ?></a></li>
			      				<li><a href="<?php echo wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ) ?>"><?php _e('Sign out', 'Elise') ?></a></li>
			      			</ul>
			      		</div>
			      		<?php
		      		} else { ?>
			      		<div class="woo-settings">
			      			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="woo-settings-login"><?php ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) ? _e('Login/Register', 'Elise') : _e('Login', 'Elise') ?></a>
			      		</div>
		      		<?php
		      		}
		      	}
		      ?>

		      <div class="nav-social-icons"><?php header_social_icons(); ?></div>

		    </div>
		  </div>
		</div>
	</div><!-- Top bar End -->
	<?php } ?>
<?php
}


/* ------------------------------------------------------------ */
/* Pagination */
/* ------------------------------------------------------------ */
function elise_pagination( $query=null ) {
	global $wp_query;
	$query = $query ? $query : $wp_query;
	$big = 999999999;

	$paginate = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'type' => 'array',
		'total' => $query->max_num_pages,
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'prev_text' => '<i class="typcn typcn-arrow-left"></i>',
		'next_text' => '<i class="typcn typcn-arrow-right"></i>',
	)
	);


	if ($query->max_num_pages > 1) :
	?>

	<div class="row">
		<div class="col-md-12">
			<div class="pagination-wrap">
				<ul class="pagination">
				<?php
					foreach ( $paginate as $page ) {
					echo '<li>' . $page . '</li>';
					}
				?>
				</ul>
			</div>
		</div>
	</div>
	
	<?php
  	endif;
}


	
/* ------------------------------------------------------------ */
/* Custom Function for Displaying Comments */
/* ------------------------------------------------------------ */
function elise_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;

	if (get_comment_type() == 'pingback' || get_comment_type() == 'trackback') : ?>
	
		<li class="pingback" id="comment-<?php comment_ID(); ?>">
			<article <?php comment_class('the-comment'); ?>>
				<div class="comment-content">
					<div class="meta"><?php _e('Pingback:', 'Elise'); ?></div>
					<p><?php edit_comment_link(); ?></p>
				</div>
				<?php comment_author_link(); ?>
			</article>
	<?php endif; ?>
	
	<?php if (get_comment_type() == 'comment') : ?>
		<li id="comment-<?php comment_ID(); ?>" >
			<article <?php comment_class('the-comment'); ?>>
	            <div class="avatar">
					<?php 
						$avatar_size = 70;
						if ($comment->comment_parent != 0) {
							$avatar_size = 48;
						}
						
						echo get_avatar($comment, $avatar_size);
					?>
	            </div>
				<div class="comment-content">
					<div class="meta">by <?php comment_author_link(); ?> at <?php comment_date(); ?>, <?php comment_time(); ?>  <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'before' => '&middot; '))); ?></div>
				<p></p>
				<?php if ($comment->comment_approved == '0') : ?>
					<p class="awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'Elise'); ?></p>
				<?php endif; ?>
				<?php comment_text(); ?>
				</div>
			</article>
			
	<?php endif;	
}


/* ------------------------------------------------------------ */
/* Custom Comment Form */
/* ------------------------------------------------------------ */
function elise_custom_comment_form($defaults) {
	
	$defaults['comment_notes_before'] = '';
	$defaults['id_form'] = 'comment-form';
	$defaults['comment_field'] = '<textarea placeholder="'. __('Comment...', 'Elise') .'" name="comment" id="comment" cols="30" rows="7"></textarea>';

	return $defaults;
}

add_filter('comment_form_defaults', 'elise_custom_comment_form');

function elise_custom_comment_fields() {
	$commenter = wp_get_current_commenter();
	$req = get_option('require_name_email');
	$aria_req = ($req ? " aria-required='true'" : '');
	
	$fields = array(
		'author' => '<div id="comment-input"><div class="input-group">' . 
						'<input id="author" name="author" placeholder="'. __('Name', 'Elise') .''. ($req ? __(' (required)', 'Elise') : '') .'" type="text" value="' . esc_attr($commenter['comment_author']) . '" ' . $aria_req . ' />' .
						'<span class="input-group-addon"><i class="typcn typcn-user-outline"></i></span>' .
		            '</div>',
		'email' => '<div class="input-group">' . 
						'<input id="email" name="email" placeholder="'. __('Email', 'Elise') .''. ($req ? __(' (required)', 'Elise') : '') .'" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" ' . $aria_req . ' />' .
						'<span class="input-group-addon"><i class="typcn typcn-mail"></i></span>' .
		            '</div>',
		'url' => '<div class="input-group">' . 
						'<input id="url" name="url" type="text" placeholder="'. __('Website', 'Elise') .'" value="' . esc_attr($commenter['comment_author_url']) . '" />' .
						'<span class="input-group-addon"><i class="typcn typcn-home-outline"></i></span>' .
		          '</div></div>'
	);

	return $fields;
}

add_filter('comment_form_default_fields', 'elise_custom_comment_fields');


/* ------------------------------------------------------------ */
/* Comments Disable for page and portfolio */
/* ------------------------------------------------------------ */

function default_comments_off( $data ) {
    if( $data['post_type'] == 'portfolio' && $data['post_status'] == 'auto-draft' ) {
        $data['comment_status'] = 0;
        // $data['ping_status'] = 0;
    }

    return $data;
}
add_filter( 'wp_insert_post_data', 'default_comments_off' );

/* ------------------------------------------------------------ */
/* Category / Archive Count Fix */
/* ------------------------------------------------------------ */

function cat_count_span($links) {
  $links = str_replace('</a> (', ' (', $links);
  $links = str_replace(')', ')</a>', $links);
  return $links;
}
add_filter('wp_list_categories', 'cat_count_span');


function archive_count_span($links) {
  $links = str_replace('</a>&nbsp;(', ' (', $links);
  $links = str_replace(')', ')</a>', $links);
  return $links;
}
add_filter('get_archives_link', 'archive_count_span');


/* ------------------------------------------------------------ */
/* Custom styles */
/* ------------------------------------------------------------ */
include('eliseCore/css/dynamic-styles.php');


/* ------------------------------------------------------------ */
/* Header Social icons */
/* ------------------------------------------------------------ */
include('includes/extras/header-social-icons.php');


/* ------------------------------------------------------------ */
/* Video conversion */
/* ------------------------------------------------------------ */
function elise_convert_videos($string) {
  $rules = array(
    '#https?://(www\.)?youtube\.com/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)#i' => '<iframe width="560" height="315" src="http://www.youtube.com/embed/$2" style="border:none" allowfullscreen></iframe>',
 
    '#https?://(www\.)?vimeo\.com/([^ ?\n/]+)((\?|/).*?(\n|\s))?#i' => '<iframe src="http://player.vimeo.com/video/$2" width="500" height="281" style="border:none" allowfullscreen></iframe>'
  );
 
  foreach ($rules as $link => $player)
    $string = preg_replace($link, $player, $string);

  return $string;
}

/* ------------------------------------------------------------ */
/* TinyMce Elise Customisation */
/* ------------------------------------------------------------ */

function elise_mce_editor_buttons( $buttons ) {

    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'mce_buttons_2', 'elise_mce_editor_buttons' );

// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'elise_formats_tinymce' ) ) {
	function elise_formats_tinymce( $settings ) {

		// Create array of new styles
		$new_styles = array(
			array(
				'title'	=> __( 'Typography Colors', 'Elise' ),
				'items'	=> array(
					// array(
					// 	'title'		=> __('Small','Elise'),
					// 	'block'	=> 'small',
					// 	// 'wrapper'	=> true,
					// ),
					array(
						'title'		=> __('Text Color - Accent','Elise'),
						// 'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'inline' => 'span',
						'classes'	=> 'color-accent',
						'wrapper'	=> true,
					),
					array(
						'title'		=> __('Highlight - Accent Color','Elise'),
						// 'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'inline' => 'span',
						'classes'	=> 'highlight-accent',
						'wrapper'	=> true,
					),
					array(
						'title'		=> __('Highlight - Dark','Elise'),
						// 'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'inline' => 'span',
						'classes'	=> 'highlight-dark',
						'wrapper'	=> true,
					),
					array(
						'title'		=> __('Highlight - Light','Elise'),
						// 'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'inline' => 'span',
						'classes'	=> 'highlight-light',
						'wrapper'	=> true,
					),
					array(
						'title'		=> __('Highlight - Grey','Elise'),
						// 'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'inline' => 'span',
						'classes'	=> 'highlight-grey',
						'wrapper'	=> true,
					),
				),
			),
			array(
				'title'	=> __( 'Typography Sizes', 'Elise' ),
				'items'	=> array(
					// array(
					// 	'title'		=> __('Small','Elise'),
					// 	'block'	=> 'small',
					// 	// 'wrapper'	=> true,
					// ),
					array(
						'title'		=> __('Jumbo Class - 3.6em size','Elise'),
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'classes'	=> 'jumbo',
						'wrapper'	=> true,
					),
					array(
						'title'		=> __('Hero Class - H1 size','Elise'),
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'classes'	=> 'hero',
						'wrapper'	=> true,
					),
					array(
						'title'		=> __('Title Class','Elise'),
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,small',
						'classes'	=> 'elise-title',
						'wrapper'	=> true,
					),
				),
			),
			array(
				'title'	=> __( 'Margin Bottom', 'Elise' ),
				'items'	=> array(
					array(
						'title'		=> '0px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '0px'),
					),
					array(
						'title'		=> '5px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '5px'),
					),
					array(
						'title'		=> '10px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '10px'),
					),
					array(
						'title'		=> '15px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '15px'),
					),
					array(
						'title'		=> '20px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '20px'),
					),
					array(
						'title'		=> '30px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '30px'),
					),
					array(
						'title'		=> '50px',
						'selector'	=> 'p,h1,h2,h3,h4,h5,h6,div,hr',
						'styles'	=> array('margin-bottom' => '50px'),
					),
				),
			),
		);

		// Merge old & new styles
		$settings['style_formats_merge'] = true;

		// Add new styles
		$settings['style_formats'] = json_encode( $new_styles );
		$settings['preview_styles'] = '';

		// Return New Settings
		return $settings;

	}
}
add_filter( 'tiny_mce_before_init', 'elise_formats_tinymce' );

function elise_add_my_editor_style() {
	add_editor_style( THEMEROOT.'/eliseCore/css/editor-style.css');
}
add_action( 'admin_init', 'elise_add_my_editor_style' );


/* ------------------------------------------------------------ */
/* WooCommerce Custom */
/* ------------------------------------------------------------ */

// WooCommerce Items Per Page
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9;' ), 20 );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
 
/**
 * WooCommerce Loop Product Thumbs
 **/
 
 if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
 
	function woocommerce_template_loop_product_thumbnail() {
		echo woocommerce_get_product_thumbnail();
	} 
 }
 
 
/**
 * WooCommerce Product Thumbnail
 **/
 if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	
	function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
		global $post, $woocommerce, $product;
 
			$placeholder_sizes = wc_get_image_size( 'shop_catalog' );
			
			$output = '';
 
			if ( has_post_thumbnail() ) {

				$elise_product_gallery = $product->get_gallery_attachment_ids();

				//$output .= get_the_post_thumbnail( $post->ID, $size ); 

				$image_url_thumb_id = get_post_thumbnail_id();
				$image_url_thumb = wp_get_attachment_url( $image_url_thumb_id, 'full' );
				$image_thumb = aq_resize( $image_url_thumb, $placeholder_sizes['width'], $placeholder_sizes['height'], $placeholder_sizes['crop'], false );
				$output .= '<img class="elise_gallery_image_thumb" src="'. $image_thumb[0] .'" width="'. $image_thumb[1] .'" height="'. $image_thumb[2] .'"  alt="" />';

				if ( $elise_product_gallery ) {
					$image_url = wp_get_attachment_url($elise_product_gallery[0], 'full');
					$image = aq_resize( $image_url, $placeholder_sizes['width'], $placeholder_sizes['height'], $placeholder_sizes['crop'], false );
					$output .= '<img class="elise_gallery_image_thumb_overlay" src="'. $image[0] .'" width="'. $image[1] .'" height="'. $image[2] .'"  alt="" />';
				}
				
			} else {

				$elise_product_gallery = $product->get_gallery_attachment_ids();

				if ( $elise_product_gallery ) {
					$image_url = wp_get_attachment_url($elise_product_gallery[0], 'full');
					$image = aq_resize( $image_url, $placeholder_sizes['width'], $placeholder_sizes['height'], $placeholder_sizes['crop'], false );
					$output .= '<img class="elise_gallery_image_thumb" src="'. $image[0] .'" width="'. $image[1] .'" height="'. $image[2] .'"  alt="" />';
				} else {
					$output .= apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="Placeholder" />', wc_placeholder_img_src() ), $post->ID );
				}
			}
			
			// $output .= '</div>';
			
			return $output;
	}
 }

 
/**
 * WooCommerce Category Name - Catalog
 **/

function wc_category_title_archive_products(){
    $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );
    if ( $product_cats && ! is_wp_error ( $product_cats ) ){
        $single_cat = array_shift( $product_cats );
        echo '<div class="product-category-title">' .$single_cat->name .'</div>';
	}
}

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'wc_category_title_archive_products', 10 );
// add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 15 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 5 );

function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 6;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
 
	$args['posts_per_page'] = 4; // 4 related products
	$args['columns'] = 4; // arranged in 2 columns
	return $args;
}

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
 
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;
	
	ob_start();
	
	?>
	<span class="cart-contents woo-cart-items-count"><?php echo $woocommerce->cart->cart_contents_count ?></span>
	<?php
	
	$fragments['span.cart-contents'] = ob_get_clean();
	
	return $fragments;
}









/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/eliseCore/TGM/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'elise_register_js_composer_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function elise_register_js_composer_plugins() {
    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        // This is an example of how to include a plugin pre-packaged with a theme
        array(
            'name'			=> 'WPBakery Visual Composer', // The plugin name
            'slug'			=> 'js_composer', // The plugin slug (typically the folder name)
            'source'			=> get_stylesheet_directory() . '/plugins/js_composer.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '4.4.3', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'			=> 'Envato WordPress Toolkit', // The plugin name
            'slug'			=> 'envato-wordpress-toolkit', // The plugin slug (typically the folder name)
            'source'			=> get_stylesheet_directory() . '/plugins/envato-wordpress-toolkit.zip', // The plugin source
            'required'			=> false, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.7.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'			=> 'Revolution Slider', // The plugin name
            'slug'			=> 'revslider', // The plugin slug (typically the folder name)
            'source'			=> get_stylesheet_directory() . '/plugins/revslider.zip', // The plugin source
            'required'			=> false, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '4.6.5', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '', // If set, overrides default API URL and points to an external URL
        ),
        array(
            'name'      => 'Portfolio Post Type',
            'slug'      => 'portfolio-post-type',
            'required'  => false,
        ),
        array(
            'name'      => 'Stag Custom Sidebars',
            'slug'      => 'stag-custom-sidebars',
            'required'  => false,
        ),
        array(
            'name'      => 'Woocommerce',
            'slug'      => 'woocommerce',
            'required'  => false,
        ),
        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => false,
        ),
    );
 
    // Change this to your theme text domain, used for internationalising strings
    $theme_text_domain = 'Elise';
 
    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'domain'		=> $theme_text_domain, // Text domain - likely want to be the same as your theme.
        'default_path'		=> '', // Default absolute path to pre-packaged plugins
        'parent_menu_slug'	=> 'themes.php', // Default parent menu slug
        'parent_url_slug'	=> 'themes.php', // Default parent URL slug
        // 'menu'			=> 'install-required-plugins', // Menu slug
        'has_notices'		=> true, // Show admin notices or not
        'is_automatic'		=> true, // Automatically activate plugins after installation or not
        'message'		=> '', // Message to output right before the plugins table
        'strings'		=> array(
            'page_title'			=> __( 'Install Required Plugins', $theme_text_domain ),
            'menu_title'			=> __( 'Install Plugins', $theme_text_domain ),
            'installing'			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
            'oops'				=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
            'notice_can_install_required'	=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_install_recommended'	=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_install'		=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
            'notice_can_activate_required'	=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_activate_recommended'	=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_activate'		=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
            'notice_ask_to_update'		=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_update'		=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
            'install_link'			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link'			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
            'return'				=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
            'plugin_activated'			=> __( 'Plugin activated successfully.', $theme_text_domain ),
            'complete'				=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
            'nag_type'				=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
        )
    );
    tgmpa( $plugins, $config );
}
 
/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
add_action( 'vc_before_init', 'elise_vcSetAsTheme' );
function elise_vcSetAsTheme() {
	vc_set_as_theme();
	vc_disable_frontend();
}
if (class_exists('WPBakeryVisualComposerAbstract')) {
	function elise_addons_to_vc(){
		require_once locate_template('/eliseCore/vc_custom/elise_vc_custom.php');

		$dir = get_template_directory() . '/eliseCore/vc_custom/elise_vc_templates/';
		vc_set_shortcodes_templates_dir($dir);

	}
	add_action('vc_before_init','elise_addons_to_vc', 5);
}


add_filter( 'vc_shortcodes_css_class', 'elise_custom_css_classes_for_vc_row', 10, 2 );
function elise_custom_css_classes_for_vc_row( $class_string, $tag ) {
  if ( $tag == 'vc_column' || $tag == 'vc_column_inner') {
  	if (!vc_is_frontend_editor()) {
    	$class_string = preg_replace( '/vc_col-sm-(\d{1,2})/', 'vc_col-md-$1', $class_string ); // This will replace "vc_col-sm-%" with "my_col-sm-%"
  	} else {
    	$class_string = preg_replace( '/vc_col-sm-(\d{1,2})/', 'vc_col-sm-$1', $class_string ); // This will replace "vc_col-sm-%" with "my_col-sm-%"
  	}
  }
  return $class_string; // Important: you should always return modified or original $class_string
}

add_filter('wpb_widget_title', 'elise_override_widget_title', 10, 2);
function elise_override_widget_title($output = '', $params = array('')) {
  $extraclass = (isset($params['extraclass'])) ? " ".$params['extraclass'] : "";
  return '<h6 class="entry-title'.$extraclass.'">'.$params['title'].'</h6>';
}

add_action( 'vc_after_init', 'vc_remove_wp_admin_bar_button' );
function vc_remove_wp_admin_bar_button() {
    remove_action( 'admin_bar_menu', array( vc_frontend_editor(), 'adminBarEditLink' ), 1000 );
}

// Remove Grid Elements from Menu
function elise_grid_elements_disable(){
  remove_menu_page( 'edit.php?post_type=vc_grid_item' );
}
add_action( 'admin_menu', 'elise_grid_elements_disable' );



?>
