<?php 
  global $elise_options;

?> 

  <?php if ($elise_options['opt-show-footer'] == 1) { ?>

  <footer>

    <?php if ($elise_options['opt-footer-widget-area'] == 1) { 

      if ($elise_options['opt-footer-widget-columns'] == 1) {
        $foot_widget_column_1 = 'col-md-6';
        $foot_widget_column_2 = 'col-md-6';
      }
      elseif ($elise_options['opt-footer-widget-columns'] == 2) {
        $foot_widget_column_1 = 'col-md-6';
        $foot_widget_column_2 = 'col-md-3';
        $foot_widget_column_3 = 'col-md-3';
      }
      elseif ($elise_options['opt-footer-widget-columns'] == 3) {
        $foot_widget_column_1 = 'col-md-4';
        $foot_widget_column_2 = 'col-md-4';
        $foot_widget_column_3 = 'col-md-4';
      }
      elseif ($elise_options['opt-footer-widget-columns'] == 4) {
        $foot_widget_column_1 = 'col-md-3';
        $foot_widget_column_2 = 'col-md-3';
        $foot_widget_column_3 = 'col-md-3';
        $foot_widget_column_4 = 'col-md-3';
      }

    ?>

    <!-- Footer - Widgets Area -->
    <section id="footer-widget-area">
      <div class="container">
        <div class="row">

          
          <div class="<?php echo esc_attr($foot_widget_column_1) ?>">

            <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('footer-area-1') ) : else : ?>   
              <div class="widget">      
                <?php if (current_user_can( 'administrator' )) { ?>
                  <h6><?php _e('Footer Area 1', 'Elise') ?></h6>
                  <p class="no-widget-added">
                    <a href="<?php echo admin_url('widgets.php'); ?>"><?php _e('Click here to assign a widget to this area.', 'Elise') ?></a>
                  </p>
                <?php } ?>
              </div>       
            <?php endif; ?>     

          </div>

          
          <div class="<?php echo esc_attr($foot_widget_column_2) ?>">

            <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('footer-area-2') ) : else : ?>   
              <div class="widget">      
                <?php if (current_user_can( 'administrator' )) { ?>
                  <h6><?php _e('Footer Area 2', 'Elise') ?></h6>
                  <p class="no-widget-added">
                    <a href="<?php echo admin_url('widgets.php'); ?>"><?php _e('Click here to assign a widget to this area.', 'Elise') ?></a>
                  </p>
                <?php } ?>
              </div>       
            <?php endif; ?>     

          </div>

          
        <?php  if ($elise_options['opt-footer-widget-columns'] != 1) { ?>
          <div class="<?php echo esc_attr($foot_widget_column_3) ?>">

            <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('footer-area-3') ) : else : ?>   
              <div class="widget">      
                <?php if (current_user_can( 'administrator' )) { ?>
                  <h6><?php _e('Footer Area 3', 'Elise') ?></h6>
                  <p class="no-widget-added">
                    <a href="<?php echo admin_url('widgets.php'); ?>"><?php _e('Click here to assign a widget to this area.', 'Elise') ?></a>
                  </p>
                <?php } ?>
              </div>       
            <?php endif; ?>  

          </div>
        <?php } ?>

          
        <?php  if ($elise_options['opt-footer-widget-columns'] == 4) { ?>
          <div class="<?php echo esc_attr($foot_widget_column_4) ?>">

            <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('footer-area-4') ) : else : ?>   
              <div class="widget">    
                <?php if (current_user_can( 'administrator' )) { ?>  
                  <h6><?php _e('Footer Area 4', 'Elise') ?></h6>
                  <p class="no-widget-added">
                    <a href="<?php echo admin_url('widgets.php'); ?>"><?php _e('Click here to assign a widget to this area.', 'Elise') ?></a>
                  </p>
                <?php } ?>
              </div>       
            <?php endif; ?>  

          </div>
        <?php } ?>

        </div>
      </div>
    </section><!-- Footer - Widgets Area End -->
    <?php } // if footer widget area ?>

    <!-- Footer - Copyrights -->
    <div id="copyrights">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <?php 

              $copyrights_text = $elise_options['opt-copyrights'];
              $copyrights_text_kses = array(
                  'a' => array(
                      'href' => array(),
                      'title' => array()
                  ),
                  'i' => array(
                      'class' => array(),
                  ),
                  'img' => array(
                      'src' => array(),
                      'alt' => array(),
                  ),
                  'strong' => array(),
                  'br' => array(),
                  'p' => array(),
              );

              echo wp_kses( $copyrights_text, $copyrights_text_kses);

            ?>
          </div>
          <div class="col-md-6">
            <?php 
              if ( has_nav_menu( 'copyrights-nav' ) ) {
                wp_nav_menu(
                  array(
                    'theme_location' => 'copyrights-nav',
                    'depth'          => -1,
                  ));
              }
            ?>
          </div>
        </div>
      </div>
    </div><!-- Footer - Copyrights End -->

  </footer>

  <?php } // blank template endif ?>

  </div><!-- layout wrapper end -->
  
    <?php 
      // if ($elise_options['opt-layout'] != 1) {
      //   echo '</div><!-- layout wrapper end -->';
      // }
      

      get_template_part('includes/secondary-nav/nav', secondary_nav_style() );


      if ($elise_options['opt-back-to-top'] == 1) { ?>
        <div class="back-to-top btt-hidden hidden-xs hidden-sm">
          <a href="#" class="back-to-top-btn" title="<?php _e('Back to Top', 'Elise') ?>">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      <?php }

    ?>
    
    <?php wp_footer(); ?>

  </body>
</html>