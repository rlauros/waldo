<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $woocommerce, $product;

?>
<div class="images">

	<?php
		if ( has_post_thumbnail() ) {

			$image_title = esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_link  = wp_get_attachment_url( get_post_thumbnail_id() );
			// $image       = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
			// 	'title' => $image_title
			// 	) );

			$placeholder_sizes = wc_get_image_size( 'shop_single' );
			$image_url_thumb_id = get_post_thumbnail_id();
			$image_url_thumb = wp_get_attachment_url( $image_url_thumb_id, 'full' );
			$image_thumb = aq_resize( $image_url_thumb, $placeholder_sizes['width'], $placeholder_sizes['height'], $placeholder_sizes['crop'], false );
			$aq_image = '<img class="attachment-shop_single elise_product_thumb" src="'. $image_thumb[0] .'" width="'. $image_thumb[1] .'" height="'. $image_thumb[2] .'"  alt="" title='. $image_title .' />';


			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $aq_image ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>

	<?php do_action( 'woocommerce_product_thumbnails' ); ?>

</div>
