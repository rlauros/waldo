<?php 
get_header(); 
global $elise_options;

$elise_woo_products_columns = $elise_options['opt-woo-shop-items-columns'];

if (!function_exists('loop_columns')) {
  function loop_columns() {
    global $elise_options;
    $elise_woo_products_columns = $elise_options['opt-woo-shop-items-columns'];
    
    return $elise_woo_products_columns; // products per row
  }
}

add_filter('loop_shop_columns', 'loop_columns');

if (!is_product() && $elise_options['opt-woo-shop-layout'] == 2) { 

  if ($elise_options['opt-woo-shop-sidebar-position'] == 1)  { //sidebar right
    $elise_woo_column_products = 'col-md-9 sidebar-content';
    $elise_woo_column_sidebar = 'col-md-3';
    $elise_woo_sidebar_position = 'sidebar-right';
  }
  elseif ($elise_options['opt-woo-shop-sidebar-position'] == 2)  { //sidebar left
    $elise_woo_column_products = 'col-md-9 col-md-push-3 sidebar-content';
    $elise_woo_column_sidebar = 'col-md-3 col-md-pull-9';
    $elise_woo_sidebar_position = 'sidebar-left';
  }
} else {
  $elise_woo_column_products = 'col-md-12';
  $elise_woo_sidebar_position = 'woo_fullwidth';
}


?>


      <div class="content elise_woocommerce <?php echo 'elise_woo-col-'.esc_attr($elise_woo_products_columns) ?> <?php echo esc_attr($elise_woo_sidebar_position) ?> section">
        <div class="container">
          <div class="row">

            <div class="<?php echo esc_attr($elise_woo_column_products) ?>">

              <?php if ( have_posts() ) : ?>

                <?php woocommerce_content(); ?>
          
               <?php endif; ?>

            </div>

            <?php if (!is_product() && $elise_options['opt-woo-shop-layout'] == 2) { ?>
              <aside class="<?php echo esc_attr($elise_woo_column_sidebar) ?> sidebar-wrap">

            <?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('shop-sidebar') ) : else : ?>   

              <div class="sidebar-widget">
                <?php get_search_form(); ?>
              </div> <!-- end sidebar-widget -->     

            <?php endif; ?>  

              </aside>
            <?php } ?>

          </div>
        </div>
      </div>


<?php get_footer(); ?>