<?php 
  global $elise_options;
  // wp_reset_postdata();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
      <meta charset="<?php bloginfo('charset'); ?>">
      <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
      <?php if ($elise_options['opt-responsive'] == 1) { ?>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?php } ?>

      <!-- Pingbacks -->
      <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
      
      <?php if (isset($elise_options['opt-favicon']['url']) && $elise_options['opt-favicon']['url']) { ?>      
        <link rel="icon" type="image" href="<?php echo esc_url($elise_options['opt-favicon']['url']) ?>"/>
      <?php } ?>

      <?php if (isset($elise_options['opt-apple-icons']['url']) && $elise_options['opt-apple-icons']['url'] ) { ?>

        <link href="<?php echo esc_url(apple_touch_icons(72)); ?>" rel="apple-touch-icon" sizes="76x76" />
        <link href="<?php echo esc_url(apple_touch_icons(120)); ?>" rel="apple-touch-icon" sizes="120x120" />
        <link href="<?php echo esc_url(apple_touch_icons(152)); ?>" rel="apple-touch-icon" sizes="152x152" />
        <link href="<?php echo esc_url(apple_touch_icons(180)); ?>" rel="apple-touch-icon" sizes="180x180" />

      <?php } ?>

      <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>

    <?php 
    if ($elise_options['opt-show-header'] == 1) {
      if ($elise_options['opt-show-sticky-header'] == 1) {
        include('includes/headers/addons/sticky-header.php');
      }
    }

    $layout_wrapper_shadow = '';
    if ($elise_options['opt-layout'] != 1) {
      if ($elise_options['opt-layout-wrapper-shadow'] == 1) {
        $layout_wrapper_shadow = ' layout-wrapper-shadow';
      }
    }
    ?>

    <div class="layout-wrapper <?php echo esc_attr($layout_wrapper_shadow) ?>">

    <?php if ($elise_options['opt-show-header'] == 1) { ?>

    <?php 

    get_template_part('includes/headers/header', elise_header_style() );

    if (function_exists('elise_page_title')) {
      elise_page_title(); 
    }

    if ($elise_options['opt-breadcrumbs-style'] == 1) {
      elise_breadcrumbs();
    } // breadcrumbs style - bar 

    ?>

    <?php } // blank template endif ?>