<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'waldo');

/** MySQL database username */
define('DB_USER', 'waldo');

/** MySQL database password */
define('DB_PASSWORD', 'waldo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'EdfypGu~7o3a6lV)$!E2-%z]T;6MiNs.,hLF1{tKMlSI%Sy/XZ3>t3iB!>*qXqU_');
define('SECURE_AUTH_KEY',  'gNyb`rK9Qyse5fios2p({L`HYV;an+W%BngX&~`@8.uJsZ}<|<wTaaA{mRMy=QKi');
define('LOGGED_IN_KEY',    '&R mo;|_ bwb;wG6S+KFr%YcBiH1=;CzJf7[T.og%T^{P;(MuOaXabVKh8{h(f>L');
define('NONCE_KEY',        'WyK9arV]:j9FRRg)b&sp~POSj?t}l4;gsPOhgHa^K7J.t|nkYv;1-{P#h2mv;`YE');
define('AUTH_SALT',        '@+A2xNHBWD:tkZu.M*0fz3>5cK*rDs#_I&G/l}m8t,E]CGc2^cK]-e5AO&]_FjJM');
define('SECURE_AUTH_SALT', '^|B[!o?S4+bMqd,k@y%ks. 32$wbTA6*#ZJd/R*A}Z>U+{K}a.(}WDe6>8,+v<<q');
define('LOGGED_IN_SALT',   '9yZrk3fS,Uzz[v3AdYVhv<qt)pMzD#5ZU1`@-OWDQPWA8Jj589.ozrM&#Tam//^/');
define('NONCE_SALT',       'M{(im=:[9NAtH@)f]p7@V?)ncx>Y%l*e@a!IC_:@>h}g7Psi:NRaIHk$S2anYIus');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
